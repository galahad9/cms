<?php
header("Content-Type: text/javascript", true);
header("X-Content-Type-Options: nosniff");

$cacheSeconds = 60*60*24*30;
session_cache_limiter('');
header('Pragma: public');
header('Cache-Control: max-age='.$cacheSeconds.', public');
header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + ($cacheSeconds)));

define('IS_JS', 1);
define('IS_ASSET', 1);

require('firstment.inc.php');


$jsFileUrl = __DIR__ . $_GET['file'];
// dd($jsFileUrl);
if(file_exists($jsFileUrl)){
	include($jsFileUrl);
} else {
	header("HTTP/1.1 404 Not Found");
}



// echo $cssFile;
// use MatthiasMullie\Minify;
