<?php
require('firstment.inc.php');
if ( ($_SERVER['REQUEST_URI'] != '/') && ($_SERVER['REQUEST_URI'] != rtrim($_SERVER['REQUEST_URI'], '/')) ) {
	$redirectUrl = getProtocol() . $_SERVER['HTTP_HOST'] . rtrim($_SERVER['REQUEST_URI'], '/');
	header('Status: 301 Moved Permanently');
	header('Location: '.$redirectUrl, 301);
	exit;
}

$settings = loadSettings('SITE');

$language = $settings['Default Language'];
define("DEFAULT_LANGUAGE", $language);

$languageUrl = $_SERVER['REQUEST_URI'];
$additionalLanguages = explode(',', $settings['Additional Languages']);
foreach($additionalLanguages as $additionalLanguage) {
	if ( (!empty($additionalLanguage)) && ((strpos($languageUrl, '/'.$additionalLanguage.'/') === 0) || ($languageUrl == '/'.$additionalLanguage)) ) {
		$language = $additionalLanguage;
		$languageUrl = substr($languageUrl, 3);
	}
}

define("LANGUAGE", $language);
define("LANGUAGE_URL", $languageUrl);
define("BASE_URL",  getProtocol() . $_SERVER['HTTP_HOST'] . ((LANGUAGE != DEFAULT_LANGUAGE) ? ('/'.LANGUAGE) : ''));



$redirect = DB::SITE()->query("SELECT * FROM redirects WHERE `from`=:from AND active=1")->bind(':from', trim($_SERVER['REQUEST_URI'],'/'))->single();
if($redirect){
	$redirect_to = trim(BASE_URL.'/').'/'.$redirect['to'];
	redirect($redirect_to, $redirect['responsecode']);
}

$page = DB::SITE()->query("SELECT * FROM menu WHERE type='item' AND url=:url AND language=:language ORDER BY sequence ASC LIMIT 1", array(
	':url' => ( (!empty(LANGUAGE_URL)) ? LANGUAGE_URL : '/' ),
	':language' => LANGUAGE
))->single();

if (!is_array($page)) {
	$found = false;
	$rewrites = getCustomRewrites();

	$uri = $_SERVER['REQUEST_URI'];
	$output = null;

	foreach ($rewrites as $rewrite => $function) {
		preg_match('/\/?'.$rewrite.'$/m', $uri, $output);

		if($output){
			$page = call_user_func($function, $uri);
			if($page['template']){
				$found = true;
			} else {
				throw new Exception($rewrite.': template not defined');
			}
		}
	}

	if(! $found){
		$prefix = (LANGUAGE != DEFAULT_LANGUAGE) ? (LANGUAGE.'/') : '';
		header('HTTP/1.1 404 Not Found');
		die('404');
		// redirect($prefix.'404');
	}
}

ob_start();
?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE ?>">
	<head>
		<?php
			require('website/head.inc.php');
			require('website/headTracking.inc.php');
		?>
		
	</head>
	<body>
		<div class="is-wrapper">
			<?php
				
				echo processTemplate($page['template']);
				
				require('website/foot.inc.php');
			?>
		</div>
	</body>
</html>

<?php
$html = ob_get_contents();
ob_end_clean();
echo $html;

	
?>
