<?php

$url = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'];


define('ROOT_PATH', __DIR__);
define('DOMAIN', $url);
define('CMS_PUBLIC_PATH', '/cms/');
define('WEBSITE_ROOT_PATH', __DIR__.'/website/');
define('CMS_ROOT_PATH', __DIR__.'/cms/');
define('CMS_PUBLIC_URL', DOMAIN.CMS_PUBLIC_PATH);
define('MODULE_DIR', WEBSITE_ROOT_PATH.'modules/');
define('PLUGINS_DIR', MODULE_DIR.'plugins/');
define('SNIPPETS_DIR', CMS_ROOT_PATH.'libs/builder/snippets');
define('PLUGINS_URL', '/website/modules/plugins/');
define('UPLOAD_FOLDER', 'uploads/');
define('UPLOAD_DIR', WEBSITE_ROOT_PATH.UPLOAD_FOLDER);

//settings
define('DEFAULT_LANGUAGE', 'nl');


// ENVOIRNMENTS
define('PHP', 'php7.2');





// Standards
if(! defined('PHP')){
	define('PHP', 'php');
}
if(! defined('IS_INSTALL')){
	define('IS_INSTALL', false);
}
if(! defined('IS_ASSET')){
	define('IS_ASSET', false);
}
if(! defined('IS_JS')){
	define('IS_JS', false);
}
if(! defined('IS_CSS')){
	define('IS_CSS', false);
}