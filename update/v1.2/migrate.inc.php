<?php

require_once('update.inc.php');

Console::line();
Console::log('Migrating');

// Add settings
DB::SITE()->query("INSERT INTO settings (id,name,value,category,type) VALUES (NULL,'Website Dev Url','','website','text')")->execute();
DB::SITE()->query("INSERT INTO settings (id,name,value,category,type) VALUES (NULL,'Street','','website','text')")->execute();
DB::SITE()->query("INSERT INTO settings (id,name,value,category,type) VALUES (NULL,'Zipcode','','website','text')")->execute();
DB::SITE()->query("INSERT INTO settings (id,name,value,category,type) VALUES (NULL,'City','','website','text')")->execute();
DB::SITE()->query("INSERT INTO settings (id,name,value,category,type) VALUES (NULL,'Phone number','','website','text')")->execute();
DB::SITE()->query("INSERT INTO settings (id,name,value,category,type) VALUES (NULL,'Email address','','website','text')")->execute();

// Add menu column
DB::SITE()->query("ALTER TABLE `menu` ADD COLUMN `resource` TEXT");

// Add posttype 
DB::SITE()->query("CREATE TABLE 'posttype_category' ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'posttype' TEXT NOT NULL, 'name' TEXT, 'slug' TEXT)")->execute();
DB::SITE()->query("CREATE TABLE 'posttype_data' ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'posttype' TEXT NOT NULL, 'data' BLOB, 'category' INTEGER, 'created_at' DATETIME DEFAULT CURRENT_TIMESTAMP, 'deleted_at' DATETIME DEFAULT NULL, 'updated_at' DATETIME)")->execute();

// Add Redirects table
DB::SITE()->query("CREATE TABLE 'redirects' ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'from' TEXT NOT NULL, 'to' TEXT NOT NULL, 'created_at' DATETIME DEFAULT CURRENT_TIMESTAMP, 'responsecode' INTEGER DEFAULT 301 , 'active' INTEGER DEFAULT 1)");

Console::line();
Console::log('Updating Composer');