<?php

foreach ($databaseConfig as $configName => $config) {
	DB::addConnection($configName, new DBConnection($config));
}


unset($databaseConfig);


abstract class DB {

	static protected $cms;
	static protected $site;

	static function addConnection($name, DBConnection $db){
		self::$$name = $db;
	}

	static function CMS(){
		return self::$cms;
	}

	static function SITE(){
		return self::$site;
	}

	static function disconnect(){
		try{
			if (is_object(self::$cms)) {
				self::$cms->disconnect();
			}
			if (is_object(self::$site)) {
				self::$site->disconnect();
			}
		}
		catch(Exception $e) {
			//Fallback when not all databases are used
		}
	}
}

class DBConnection {
	private $dbh = null;
	private $isExecuted;
	private $stmt;
	private $query;
	private $name;
	
	protected $execution_time;

	public $lastError;

	function __construct($cfg=null){
		if(is_array($cfg)){
			$this->connect($cfg);
			$this->name = $cfg['name'];
			unset($cfg);
		}
	}

	function __destruct(){
		$this->disconnect();
	}

	public function disconnect(){
		$this->dbh = null;
	}

	public function connect($cfg){
		try{
			$this->dbh = new PDO('sqlite:' . __DIR__ . '/database/' . $cfg['name'] . '.sqlite3');
			$this->dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			unset($cfg);
		}catch(PDOException $e){
		 	print_r($e->getMessage());
		 	header('HTTP/1.1 500 Internal Server Error');
        	die('<h3>Something went wrong</h3><p>Please, try again in a few minutes</p>');

        }

        return $this;
	}
	
	public function query($query, $binds=null){

		$this->query = $query;
		$this->binds = array();
		$this->isExecuted = false;
		$this->execution_time = 0;
	    $this->stmt = $this->dbh->prepare($query);

	    if(is_array($binds)){
	    	foreach ($binds as $key => $value) {
		    	$this->bind($key, $value);
		    }
	    }
	    return $this;
	}

	public function bind($param, $value=null, $type = null){
		if(is_array($param)){
			foreach ($param as $key => $val) {
				if(is_array($key)){
					throw new Exception('Binding an array is not possible..');
				}
				$this->bind($key, $val);
			}
		}
		else {
		    if (is_null($type)) {
		        switch (true) {
		            case is_int($value):
		                $type = PDO::PARAM_INT;
		                break;
		            case is_bool($value):
		                $type = PDO::PARAM_BOOL;
		                break;
		            case is_null($value):
		                $type = PDO::PARAM_NULL;
		                break;
		            default:
		                $type = PDO::PARAM_STR;
		        }
		    }
		    $this->binds[] = array($param, $value);
		    $this->stmt->bindValue($param, $value, $type);
		}
	    return $this;
	}

	public function log(){
//		$query = $this->stmt->queryString;
//		writeLog('database.log', $query);
		return $this;
	}

	public function execute($force_error=false){
		$start = microtime(true);
		try{
			$this->stmt->execute();
			$this->isExecuted = true;
			$this->execution_time = microtime(true) - $start;

			if (stripos($this->stmt->queryString, '`revisions`') === false) {
				$logTypes = ['INSERT INTO ', 'UPDATE '];
				foreach($logTypes as $logType) {
					if (stripos($this->stmt->queryString, $logType) !== false) {
						$logType = trim($logType);
						$rawQuery = $this->interpolateQuery($this->stmt->queryString, $this->binds);

						$where = false;
						if (stripos(trim($rawQuery), 'UPDATE') === 0) {
							$table = trim(strtok(str_replace('UPDATE ', '', $rawQuery), " "), " \t\n\r`");
							$wherePos = stripos($rawQuery, 'WHERE');
							if ($wherePos !== false) {
								$where = substr($rawQuery, $wherePos);
							}
						}
						else if (stripos(($rawQuery), 'INSERT INTO') === 0) {
							$table = trim(strtok(str_replace('INSERT INTO ', '', $rawQuery), " "), " \t\n\r`");
							$where = 'WHERE id='.$this->lastInsertId();
						}

						if ($where) {
							try{
								$dataRows = $this->query('SELECT * FROM '.$table.' '.$where)->fetch();
							}
							catch(Exception $e) {
								$dataRows = [[]]; //If we use an insert id an the table has no id..
							}
						}

						if (!empty($dataRows)) {
							foreach($dataRows as $row) {
								try{
									$id = 0;
									if (isset($row['id'])) {
										$id = intval($row['id']);
									}
									$data = json_encode($row);
									DB::CMS()->query("INSERT INTO `revisions` (`query`, `type`, `date`, `duration`, `database`, `table`, `data`, `table_id`) VALUES (:query, :type, :date, :duration, :database, :table, :data, :table_id)", [
										':query' => $rawQuery,
										':type' => trim($logType),
										':date' => date('Y-m-d H:i:s', time()),
										':duration' => $this->execution_time,
										':database' => $this->name,
										':table' => $table,
										':data' => $data,
										':table_id' => $id
									])->execute();
								}
								catch(Execution $e) { }
							}
						}
						break;
					}
				}
			}

			return true;
		}catch(PDOException $e){
			if(DEBUG === true || $force_error){
				$backtrace = debug_backtrace();
				$lasterror = end($backtrace);
				echo '<h2>Database Execution Error</h2>';
				var_dump($e->getMessage());
				debug($backtrace);
				$this->lastError = $e->getMessage();
				throw new Exception($e->getMessage());
			}
			else {
				writeLog('queries.log', $e->getMessage());
				$this->lastError = $e->getMessage();
			}
			$this->execution_time = microtime(true) - $start;
			return false;
		}
	}

	public function affected(){
		if(! $this->isExecuted){
			throw new Exception('Statement is not executed');
		}
		return $this->stmt->rowCount();
	}

	public function results(){
    	if(! $this->isExecuted){
			$this->execute(false);
		}
    	return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function fetch(){
		return $this->results();
	}
	
	public function single($style=null){
	    if(! $this->isExecuted){
			$this->execute();
		}

		if(! $style){
			$style = PDO::FETCH_ASSOC;
		}
	    return $this->stmt->fetch($style);
	}

	public function count(){
		if(! $this->isExecuted){
			$this->execute(false);
		}
	    return $this->stmt->rowCount();
	}

	public function get($column){
		if(! $this->isExecuted){
			$this->execute();
		}
		$row = $this->single(PDO::FETCH_BOTH);
		return $row[$column];
	}

	public function lastInsertId(){
	    return $this->dbh->lastInsertId();
	}

	public function beginTransaction(){
	    return $this->dbh->beginTransaction();
	}

	public function endTransaction(){
	    return $this->dbh->commit();
	}
	
	public function cancelTransaction(){
	    return $this->dbh->rollBack();
	}
	
	public function debugDumpParams(){
	    return $this->stmt->debugDumpParams();
	}
	
	public function executionTime(){
		return $this->execution_time;
	}

	public function nextRowset(){
		if(! $this->isExecuted){
			$this->execute(false);
		}
    	$this->stmt->nextRowset();
    	return $this;
	}

	private function between($string, $start, $end){
	    $string = " ".$string;
	    $ini = stripos($string,$start);
	    if ($ini == 0) return "";
	    $ini += strlen($start);
	    $len = stripos($string,$end,$ini) - $ini;
	    return substr($string,$ini,$len);
	}

	public function interpolateQuery($query, $inputParams) {
		$keys = [];
		$params = [];

		foreach($inputParams as $key => $value) {
			if (is_array($value)) {
				$params[$value[0]] = $value[1];
			}
			else {
				$params[$key] = $value;
			}
		}

		$values = $params;

		foreach ($params as $key => $value) {
			if (substr($key, 0, 1) == ':') {
				$keys[] = '/'.$key.'/';
			}
			else if (is_string($key)) {
				$keys[] = '/:'.$key.'/';
			} else {
				$keys[] = '/[?]/';
			}

			if (is_string($value))
				$values[$key] = "'" . $value . "'";

			if (is_array($value))
				$values[$key] = "'" . implode("','", $value) . "'";

			if (is_null($value))
				$values[$key] = 'NULL';
		}

		$query = preg_replace($keys, $values, $query);

		return $query;
	}
}	

?>