<?php

/* Old translations code */
// function trans($string) {
//     $defaultLanguage = 'nl';
//     $targetLanguage = 'en';

//     $id = DB::SITE()->query("SELECT * FROM translations WHERE string=:string", [':string' => $string])->get('id');
//     if (!$id) {
//         DB::SITE()->query("INSERT INTO translations (string) VALUES (:string)", [':string' => $string])->execute();
//     }
//     else {
//         $translation = DB::SITE()->query("SELECT string FROM translations_values WHERE translations_id=:id", [':id' => $id])->get('string');
//         if (!empty($translation)) {
//             return $translation;
//         }
//     }
//     return $string;
// }

function getProtocol() {
		if (
				isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
				isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'
		) {
				return 'https://';
		}
		return 'http://';
}

function getLanguageUrls($skipMissingPages=false) {
		global $settings;

		$page = DB::SITE()->query("SELECT id, language_id FROM menu WHERE type='item' AND url=:url AND language=:language ORDER BY sequence ASC LIMIT 1", array(
		':url' => LANGUAGE_URL,
		':language' => LANGUAGE
	))->single();

	$parentID = ($page['language_id']) ? $page['language_id'] : $page['id'];

	$getPages = DB::SITE()->query("SELECT url, language FROM menu WHERE id=:id1 OR language_id=:id2", array(
		':id1' => $parentID,
		':id2' => $parentID,
		))->fetch();
		

	$urls = [];
	foreach($getPages as $page) {
		$urls[$page['language']] = $page['url'];
		}

		$languages = explode(',', $settings['Additional Languages']);
	array_unshift($languages, $settings['Default Language']);

		foreach ($languages as $key => $language) {
		$language = trim($language);
		
				$languageUrl = (( DEFAULT_LANGUAGE != $language) ? ('/'.$language) : '');
				$languageUrl .= (isset($urls[$language])) ? $urls[$language] : '';

				if ( (!$skipMissingPages) || (isset($urls[$language])) ) {
						$urls[$language] = $languageUrl;
				}
		}
		
		foreach($urls as $urlKey => $urlValue) {
				$urls[$urlKey] = rtrim($urlValue, '/');
		}
		
		return $urls;
}

function getAnalyticsToken() {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL,'https://analytics-api.zyloncms.nl/get-token.php?key=7f00fb0e331e011544dad67c7541a74d428e60ba5ce80fff5b858fe0017a2371');
	$result=curl_exec($ch);
	curl_close($ch);
	return $result;
}

function echoCheckbox($value) {
	return ($value) ? '<i class="la la-check-circle"></i>' : '<i class="la la-circle"></i>';
}


function getCMSLanguages() {
	$availableTranslationFiles = glob(__DIR__ . '/cms/translations/*.yaml');
	foreach ($availableTranslationFiles as $index => $value) {
		$availableTranslationFiles[$index] = str_replace(array(__DIR__ . '/cms/translations/', '.yaml'), '', $value);
	}
	return $availableTranslationFiles;
}



function loadSettings($type) {
	if ($type == 'SITE') {
		$loadSettings = DB::SITE()->query("SELECT * FROM settings")->fetch();
	} elseif ($type == 'CMS') {
		$loadSettings = DB::CMS()->query("SELECT * FROM settings")->fetch();
	} else {
		return false;
	}
	$settings = array();
	foreach ($loadSettings as $setting) {
		$settings[$setting['name']] = $setting['value'];
	}

	return $settings;
}

function loadPlugins(){
	if(is_dir(PLUGINS_DIR)){
		if ($handle = opendir(PLUGINS_DIR.'.')) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != "..") {
					include(PLUGINS_DIR.''.$entry.'/plugin.php');
				}
			}
			closedir($handle);
		}
	} else {
		setAlert('Wrong Plugins Dir: '.PLUGINS_DIR);
	}
}

function setAlert($alert, $status='danger'){
	global $_GLOBALS;
	$_GLOBALS['alerts'][$status][] = $alert;
}

function getAlerts(){
	global $_GLOBALS, $settings;
	$errors = '';
	if(! IS_INSTALL){
		@ $cron_date = file_get_contents(CMS_ROOT_PATH.'logs/cron.pid');
		if(strtotime($cron_date) < strtotime('-1 hour')){
			setAlert('Cronjob not fired yet. Setup cronjob!');
		}

		if ($settings['Online'] === 'false') {
			setAlert('This website is in offline mode!', 'warning');
		}

		if(is_array($_GLOBALS['alerts']) && count($_GLOBALS['alerts'])){
			foreach ($_GLOBALS['alerts'] as $status => $alerts) {
				foreach($alerts as $alert) {
					$errors .= '<div style="margin-top:25px" class="alert alert-'.$status.'">'.$alert.'</div>';   
				}
			}
		}
	}    
	return $errors;
}

function registerNewPlugin($pluginClass){
	global $_PLUGINS, $_POSTTYPES;
	$plugin = new $pluginClass;
	$_PLUGINS[$plugin->url()] = $plugin;

	if($plugin->isPostType()){
		$_POSTTYPES[] = $plugin;
	}
	if($_GET['plugin'] == $plugin->url()){
		$_PLUGINS['current'] = $plugin; 
	}
}

function adminPluginPages(){
	global $_PLUGINS;
	return $_PLUGINS;
}


function getPlugins($nonactive=false){
	global $_PLUGINS;

	$plugins = [];
	if(is_array($_PLUGINS) && count($_PLUGINS) > 0){
		foreach ($_PLUGINS as $key => $plugin) {
		if($nonactive || ($plugin->status() == Plugin::ACTIVE && $key != 'current')){
				$plugins[$plugin->url()] = $plugin;
			}
		}
	}
	return $plugins;
}

function getPosttypes($nonactive=false){
	$allplugins = getPlugins($nonactive);
	$posttypes = [];
	if(is_array($allplugins) && count($allplugins) > 0){
		foreach ($allplugins as $key => $plugin) {
			if($plugin->isPostType()){
				$posttypes[] = $plugin;
			}
		}
	}
	return $posttypes;
}

function plugin($plugin){
	return getPlugins(true)[$plugin];
}

function getModule($module, $params=[]){
	global $settings;
	
	if(file_exists(MODULE_DIR.'core/'.$module.'.inc.php')){ // Core modules
		$file = MODULE_DIR.'core/'.$module.'.inc.php';
	} else {
		foreach(getCustomModules() as $mod){
			if(strpos($mod, $module) !== false && file_exists($mod)){
				$file = $mod;
				break;
			} 
		}   
	}


	if($file){
    	try {
			ob_start();
			include($file);
			$html = ob_get_contents();
			ob_end_clean();
    	} catch(Exception $e){
			if(DEBUG){
				throw $e;
				// $html = 'ERROR IN MODULE: '.$module;
			} else {
				errorLog($e);
			}
    	}
			
	}
	return $html;
}


function processPlaceholdersBuilder($template){
	return processPlaceholders($template, 'builder');
}

function processPlaceholders($template, $type='preview'){
	global $settings;

	preg_match_all("/{{([a-zA-Z0-9\s:_-]+)}}/", $template, $output_array);
	foreach ($output_array[0] as $key => $module_key) {
		$module = trim(strtolower(str_replace(['{{', '}}'], '', $module_key)));
		$module = explode('::', $module);
        $module_slug = $module[0];
		$module = getModule($module_slug, $module);
        if(! $module){
            $module = trim(str_replace(['{{', '}}'], '', $module_key));
            $module = explode('::', $module);
            if(@ array_key_exists($module[0], $settings)){
                $module = $settings[$module[0]];
            }
        }

		$template = str_replace($module_key, $module, $template);	
	}
	return $template;
}

function processTemplate($template){
	global $settings;

	$baseItem = DB::SITE()->query("SELECT `id`, `template` FROM menu WHERE type='template' AND name='base' AND language_id=0 LIMIT 1")->single();
	if ($baseItem['lanuage'] != LANGUAGE) {
		$languageBaseItem = DB::SITE()->query("SELECT * FROM menu WHERE language_id=:language_id AND language=:language LIMIT 1", [':language_id' => $baseItem['id'], ':language' => LANGUAGE])->single();
		if (!empty($languageBaseItem)) {
				$baseItem = $languageBaseItem;
		}
	}

	
	$base = $baseItem['template'];
	$base_first = between($base, '<div class="fullwith-section is-content-section', '<div class="column full">');
	$base = str_replace('<div class="fullwith-section is-content-section'.$base_first.'<div class="column full">', '', $base);
	$base = str_replace('{{ CONTENT }}
							</div></div></div>
					</div>
			</div>
	</div>', '{{ CONTENT }}', $base);

	// $plugins 
	$base = str_replace('{{ CONTENT }}', $template, $base);
	$base = processPlaceholders($base);
	

	return $base;
}

function errorLog($error){
		// Maybe some handy error logging and alerting?
}

function image_resize($src, $dst, $width, $height, $crop=0){

	if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";

	$type = strtolower(substr(strrchr($src,"."),1));
	if($type == 'jpeg') $type = 'jpg';
	switch($type){
		case 'bmp': $img = imagecreatefromwbmp($src); break;
		case 'gif': $img = imagecreatefromgif($src); break;
		case 'jpg': $img = imagecreatefromjpeg($src); break;
		case 'png': $img = imagecreatefrompng($src); break;
		default : return "Unsupported picture type!";
	}
	
	if(! $width){
		$width = $w;
	}
	if(! $height){
		$height = $w;
	}
	
	// resize
	if($crop){
		$ratio = max($width/$w, $height/$h);
		$h = $height / $ratio;
		$x = ($w - $width / $ratio) / 2;
		$w = $width / $ratio;
	}
	else {
		$ratio = min($width/$w, $height/$h);
		$width = $w * $ratio;
		$height = $h * $ratio;
		$x = 0;
	}

	$new = imagecreatetruecolor($width, $height);

	// preserve transparency
	if($type == "gif" or $type == "png"){
		imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
		imagealphablending($new, false);
		imagesavealpha($new, true);
	}

	imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

	switch($type){
		case 'bmp': imagewbmp($new, $dst); break;
		case 'gif': imagegif($new, $dst); break;
		case 'jpg': imagejpeg($new, $dst); break;
		case 'png': imagepng($new, $dst); break;
	}
	return true;
}


function getCustomRewrites(){
	$rewrites = [];
	foreach (getPlugins() as $key => $plugin) {
		if($plugin->status() == Plugin::ACTIVE && $plugin->rewrites){
			 $rewrites = array_merge($rewrites, $plugin->rewrites);
		}
	}
	return $rewrites;
}

function getCustomModules(){
	$modules = [];

	foreach (getPlugins() as $key => $plugin) {
		if(is_array($plugin->modules())){
	 		$modules = array_merge($modules, $plugin->modules());
	 	}
	}

	return $modules;
}

function getSnippetCategories(){
	$snippets = [];

	if ($handle = opendir(SNIPPETS_DIR.'/.')) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != ".." && !is_dir(SNIPPETS_DIR.'/'.$entry)) {
				$cat = explode('-', $entry);
				$id = $cat[0];
				$name = str_replace(['.inc.php', '-en-', '-'], ['',' & ', ' '], $cat[1]);
				$file = $entry;
				$snippets[] = ['id' => $id, 'file' => SNIPPETS_DIR.'/'.$file, 'name' => ucfirst($name) ];
					// include(__DIR__.'/snippets/'.$entry);
			}
		}
		closedir($handle);
	}

	$snippets[] = ['id' => 999, 'name' => 'Modules'];

	return $snippets;
}


function dd($array) {
	debug($array, true);
}

function dump($array) {
	debug($array);
}
	
function debug($array, $die=false) {
	$ipadresses = array (
		'84.29.238.102', //bo thuis
		'212.178.137.42', //sienn kantoor
				'77.160.139.92' // rick thuis
	);
	if (in_array($_SERVER['REMOTE_ADDR'], $ipadresses)) {
		echo "<pre>";
		if(is_null($array)){
			var_dump($array);
		} else {
			 print_r ($array);
		}
		echo "</pre>";
		if($die){
			die;
		}
	}
}


/**
 * Lightens/darkens a given colour (hex format), returning the altered colour in hex format.7
 * @param str $hex Colour as hexadecimal (with or without hash);
 * @percent float $percent Decimal ( 0.2 = lighten by 20%(), -0.4 = darken by 40%() )
 * @return str Lightened/Darkend colour as hexadecimal (with hash);
 */
function colorToDarker($hex, $percent=-0.4){ 
		$hex = preg_replace( '/[^0-9a-f]/i', '', $hex );
	$new_hex = '#';
	
	if ( strlen( $hex ) < 6 ) {
		$hex = $hex[0] + $hex[0] + $hex[1] + $hex[1] + $hex[2] + $hex[2];
	}
	
	// convert to decimal and change luminosity
	for ($i = 0; $i < 3; $i++) {
		$dec = hexdec( substr( $hex, $i*2, 2 ) );
		$dec = min( max( 0, $dec + $dec * $percent ), 255 ); 
		$new_hex .= str_pad( dechex( $dec ) , 2, 0, STR_PAD_LEFT );
	}		
	
	return $new_hex;
}

function redirect($url, $httpResponseStatusCode = '301') {

	header('Location: ' . $url, true, $httpResponseStatusCode);
	exit;
}

function escape($text) {
	$text = htmlentities($text, ENT_QUOTES, 'utf-8');
	return $text;
}

function everything_in_tags($string, $tagname)
{
    $pattern = "#<\s*?$tagname\b[^>]*>(.*?)</$tagname\b[^>]*>#s";
    preg_match($pattern, $string, $matches);
    return $matches[1];
}

function between($str, $one, $two) {
	$str = explode($one, $str);
	$str = explode($two, $str[1]);
	
	return $str[0];
}


function strleft($str, $length) { 
	return substr($str, 0, $length); 
}


function trimleft($str, $length) {
	return substr($str,$length,strlen($str));
}


function strright($str, $length) { 
	return substr($str, strlen($str)-$length, $length); 
}


function trimright($str, $length) {
	return substr($str,0,strlen($str)-$length);
}


function cutParagraph($paragraph, $maxLength, $endingChars = '...') {
	$paragraph = strip_tags($paragraph);
	$words = explode(' ', str_ireplace('&nbsp;', ' ', strleft($paragraph, $maxLength)));
	
	$newParagraph = trim(trimright(strleft($paragraph, $maxLength), strlen(end($words))));
	
	if (strlen($paragraph) > $maxLength) {
		$newParagraph .= $endingChars;
	}
	return $newParagraph;
}


function strtourl($string, $noSlashes=true) {

		$string = html_entity_decode($string);

	$urlspecialchars = array('đ', 'Ž', 'æ','ß', 'å', 'á', 'ä', 'à', 'ã', 'Ã', 'â', 'â', 'é', 'ë', 'è', 'ê', 'í', 'ï', 'ì', 'î', 'ó', 'ö', 'ò', 'õ', 'ô', 'ú', 'ü', 'ù', 'û', 'ý', 'ÿ', 'ñ', 'ç', 'Á', 'Ä', 'À', 'Ã', 'Â', 'É', 'Ë', 'È', 'Ê', 'Í', 'Ï', 'Ì', 'Î', 'Ó', 'Ö', 'Ò', 'Õ', 'Ô', 'Ú', 'Ü', 'Ù', 'Û', 'Ý', 'Ñ', 'Ç', '\'', '\\', '"', ',', '+', ';', '`', "(", ")", ",", "'", "@", ":", ";", "!", "?", "$", '.', '%', '”', '“', '*', '>', '<', '#', '®', '©', '™', '°', '|', '¤', 'Ø', 'ø', '«', '»', '', ' ', '¢', '¨', '²','‘', '[', ']', '{', '}', ',','´', '’', '‚', 'Ω', '¶',  'µ', '•', '¬', '❤', '★', '…', 'ª', '½', '¼', '¾', '†', '¡', '£', '¸', '¯', '§', 'Œ', '¦', '', '·', 'Φ');

	$urlspecialcharsto = array('d', 'z', 'ae', 'ss', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'n', 'c', 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'n', 'c', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

		if ($noSlashes) {
				$urlspecialchars[] = '/';
		}

	return trim(str_replace(array('−', '–', "\r", "\t", "\n", "    ", "   ", "  ", " ", "____", "___", "__", "_", "----", "---", "--", ' '), "-", trim(str_replace(array("~","="), "-", str_replace($urlspecialchars, $urlspecialcharsto, str_replace("&", " en ", strtolower(trim($string))))))), '-_');

}

function glob_recursive($pattern, $flags=0, $maxdept=2) {
		$files = glob($pattern, $flags);
		$i = 0;
		foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir){
			$i++;
			if($i > $maxdept){ break; }
			@ $files = array_merge($files, glob_recursive($dir.'/'.basename($pattern), $flags));
		}
		return $files;
}

function rrmdir($dir) { 
	 if (is_dir($dir)) { 
		 $objects = scandir($dir); 
		 foreach ($objects as $object) { 
			 if ($object != "." && $object != "..") { 
				 if (is_dir($dir."/".$object))
					 rrmdir($dir."/".$object);
				 else
					 unlink($dir."/".$object); 
			 } 
		 }
		 rmdir($dir); 
	 } 
}



function generateFavIcon($name){
	global $settings;

	if($name == 'delete'){
		rrmdir(UPLOAD_DIR.'favicon');
		return true;
	}

		if(! is_dir(UPLOAD_DIR.'favicon')){
				if(! mkdir(UPLOAD_DIR.'favicon')){
					throw new Exception('Couldn\'t create Favicon folder: '.UPLOAD_DIR.'favicon');
				}
		}


		$post = [
				'favicon_generation' => [
						'api_key' => 'd48bbf3033149334c6ab3da2389c73782d0ac13b',
						'master_picture' => [
								'type' => 'url',
								'url' => $settings['Website Url'].'/'.UPLOAD_FOLDER.'/'.$name
						],
						'files_location'   => [
								'type' => 'path',
								'path' => '/uploads/favicon'
						],
						'favicon_design' => [
								'desktop_browser' => [],
								"ios" => [
										"picture_aspect" =>"background_and_margin",
										"margin" => "4",
										// "background_color" => "#fff",
										"assets" => [
												"ios6_and_prior_icons" => false,
												"ios7_and_later_icons" => true,
												"precomposed_icons" => false,
												"declare_only_default_icon" => true
										]
								],
								"windows" => [
										"picture_aspect" => "white_silhouette",
										"background_color" => "#000",
										"assets" => [
												"windows_80_ie_10_tile" => true,
												"windows_10_ie_11_edge_tiles" => [
														"small" => false,
														"medium" => true,
														"big" => true,
														"rectangle" => false
												]
										]
								],
								"firefox_app" => [
										"picture_aspect" => "circle",
										"keep_picture_in_circle" => "true",
										"circle_inner_margin" => "5",
										// "background_color" => "#fff",
										"manifest" => [
												"app_name" => $settings['Website Name'],
												"app_description" => $settings['Website Name']
										]
								],
								"android_chrome" => [
										"picture_aspect" => "shadow",
										"manifest" => [
												"name" => $settings['Website Name'],
												"display" => "standalone",
												"orientation" => "portrait",
												"start_url" => "/",
												"existing_manifest" => ["name" => $settings['Website Name'] ]
										],
										"assets" => [
												"legacy_icon" => true,
												"low_resolution_icons" => false
										],
										"theme_color" => "#000"
								],
								"safari_pinned_tab" => [
										"picture_aspect" => "black_and_white",
										"threshold" => 60,
										"theme_color" => "#fff",
								],
								"coast" => [
										"picture_aspect" => "background_and_margin",
										"background_color" => "#fff",
										"margin" => "12%"
								],
								"open_graph" => [
										"picture_aspect" => "background_and_margin",
										// "background_color" => "#fff",
										"margin" => "10%",
										"ratio" => "1.91:1",
								],
								"yandex_browser" => [
										// "background_color" => "background_color",
										"manifest" => [
												"show_title" => true,
												"version" => "1.0"
										]
								]
						],
						"settings" => [
								"compression" => "3",
								"scaling_algorithm" => "Mitchell",
								"error_on_image_too_small" => false
						],
						"versioning" => [
								"param_name" => "ver",
								"param_value" => time()
						]
				]
		];

		$post = json_encode($post);

		$ch = curl_init('https://realfavicongenerator.net/api/favicon');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		// execute!
		$response = curl_exec($ch);

		// close the connection, release resources used
		curl_close($ch);

		$response = json_decode($response, true);
		if($response['favicon_generation_result']['result']['status'] == 'success'){
				foreach($response['favicon_generation_result']['favicon']['files_urls'] as $file){
						$parts = explode('/', $file);
						@ file_put_contents(UPLOAD_DIR.'/favicon/'.end($parts), fopen($file, 'r'));
				}

				@ file_put_contents(UPLOAD_DIR.'/favicon/html.html', $response['favicon_generation_result']['favicon']['html_code']);
		}

}

function generateFavIconCMS($name){
	global $settings;

	if($name == 'delete'){
		rrmdir(CMS_ROOT_PATH.'/assets/img/favicon');
		return true;
	}

		if(! is_dir(CMS_ROOT_PATH.'/assets/img/favicon')){
				if(! mkdir(CMS_ROOT_PATH.'/assets/img/favicon')){
					throw new Exception('Couldn\'t create Favicon folder: '.CMS_ROOT_PATH.'/assets/img/favicon');
				}
		}


		$post = [
				'favicon_generation' => [
						'api_key' => 'd48bbf3033149334c6ab3da2389c73782d0ac13b',
						'master_picture' => [
								'type' => 'url',
								'url' => CMS_PUBLIC_URL.'/assets/img/'.$name
						],
						'files_location'   => [
								'type' => 'path',
								'path' => '/cms/assets/img/favicon'
						],
						'favicon_design' => [
								'desktop_browser' => [],
								"ios" => [
										"picture_aspect" =>"background_and_margin",
										"margin" => "4",
										// "background_color" => "#fff",
										"assets" => [
												"ios6_and_prior_icons" => false,
												"ios7_and_later_icons" => true,
												"precomposed_icons" => false,
												"declare_only_default_icon" => true
										]
								],
								"windows" => [
										"picture_aspect" => "white_silhouette",
										"background_color" => "#000",
										"assets" => [
												"windows_80_ie_10_tile" => true,
												"windows_10_ie_11_edge_tiles" => [
														"small" => false,
														"medium" => true,
														"big" => true,
														"rectangle" => false
												]
										]
								],
								"firefox_app" => [
										"picture_aspect" => "circle",
										"keep_picture_in_circle" => "true",
										"circle_inner_margin" => "5",
										// "background_color" => "#fff",
										"manifest" => [
												"app_name" => $cmsSettings['Name'],
												"app_description" => $cmsSettings['Name']
										]
								],
								"android_chrome" => [
										"picture_aspect" => "shadow",
										"manifest" => [
												"name" => $cmsSettings['Name'],
												"display" => "standalone",
												"orientation" => "portrait",
												"start_url" => "/",
												"existing_manifest" => ["name" => $cmsSettings['Name'] ]
										],
										"assets" => [
												"legacy_icon" => true,
												"low_resolution_icons" => false
										],
										"theme_color" => "#000"
								],
								"safari_pinned_tab" => [
										"picture_aspect" => "black_and_white",
										"threshold" => 60,
										"theme_color" => "#fff",
								],
								"coast" => [
										"picture_aspect" => "background_and_margin",
										"background_color" => "#fff",
										"margin" => "12%"
								],
								"open_graph" => [
										"picture_aspect" => "background_and_margin",
										// "background_color" => "#fff",
										"margin" => "10%",
										"ratio" => "1.91:1",
								],
								"yandex_browser" => [
										// "background_color" => "background_color",
										"manifest" => [
												"show_title" => true,
												"version" => "1.0"
										]
								]
						],
						"settings" => [
								"compression" => "3",
								"scaling_algorithm" => "Mitchell",
								"error_on_image_too_small" => false
						],
						"versioning" => [
								"param_name" => "ver",
								"param_value" => time()
						]
				]
		];

		$post = json_encode($post);

		$ch = curl_init('https://realfavicongenerator.net/api/favicon');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		// execute!
		$response = curl_exec($ch);

		// close the connection, release resources used
		curl_close($ch);

		$response = json_decode($response, true);
		if($response['favicon_generation_result']['result']['status'] == 'success'){
				foreach($response['favicon_generation_result']['favicon']['files_urls'] as $file){
						$parts = explode('/', $file);
						@ file_put_contents(CMS_ROOT_PATH.'/assets/img/favicon/'.end($parts), fopen($file, 'r'));
				}

				@ file_put_contents(CMS_ROOT_PATH.'/assets/img/favicon/html.html', $response['favicon_generation_result']['favicon']['html_code']);
				return true;
		} else {
				return $response['favicon_generation_result']['result']['error_message'];
		}

}

class UploadException extends Exception 
{ 
		public function __construct($code) { 
				$message = $this->codeToMessage($code); 
				parent::__construct($message, $code); 
		} 

		private function codeToMessage($code) 
		{ 
				switch ($code) { 
						case UPLOAD_ERR_INI_SIZE: 
								$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini"; 
								break; 
						case UPLOAD_ERR_FORM_SIZE: 
								$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
								break; 
						case UPLOAD_ERR_PARTIAL: 
								$message = "The uploaded file was only partially uploaded"; 
								break; 
						case UPLOAD_ERR_NO_FILE: 
								$message = "No file was uploaded"; 
								break; 
						case UPLOAD_ERR_NO_TMP_DIR: 
								$message = "Missing a temporary folder"; 
								break; 
						case UPLOAD_ERR_CANT_WRITE: 
								$message = "Failed to write file to disk"; 
								break; 
						case UPLOAD_ERR_EXTENSION: 
								$message = "File upload stopped by extension"; 
								break; 

						default: 
								$message = "Unknown upload error"; 
								break; 
				} 
				return $message; 
		} 
} 