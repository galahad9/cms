<?php

require_once(__DIR__.'/apiFirstment.inc.php');
header('Content-Type: application/json');

if($_REQUEST['action'] == 'version'){
	echo json_encode([
		'current' => $core->git()->branche('current'), 
		'next' => $core->git()->branche('next'),
		'last' => $core->git()->branche('last'),
		'update' => (bool) ($core->git()->branche('next'))
	]);
} elseif($_REQUEST['action'] == 'versions'){
	echo json_encode( $core->git()->tags() );
} elseif($_REQUEST['action'] == 'branches'){
	echo json_encode( $core->git()->branches() );
}

