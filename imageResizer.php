<?php

require(__DIR__ . '/defines.inc.php');
require(__DIR__ . '/vendor/autoload.php');

header('Access-Control-Allow-Origin: *');  
ini_set('memory_limit', '256M');

$directReturn = false;

$cacheSeconds = 60*60*24*30;
$cache = false;
define('IMAGE_EXPIRE_TIME', $cacheSeconds);


if (!function_exists('getallheaders'))  {
    function getallheaders()
    {
        if (!is_array($_SERVER)) {
            return array();
        }

        $headers = array();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

function setImageHeaders($graphicFileName, $fileType='jpeg'){
	$fileModTime = filemtime($graphicFileName);

    $headers = getallheaders();

    // header('Expires: ' . gmdate('D, d M Y H:i:s', $fileModTime + $cacheSeconds) . ' GMT');
    header('Cache-Control: public');

    if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) == $fileModTime)) {
        header('Last-Modified: '.gmdate('D, d M Y H:i:s', $fileModTime).' GMT', true, 304);
        return false;
    } else {
        header('Last-Modified: '.gmdate('D, d M Y H:i:s', $fileModTime).' GMT', true, 200);
        header('Content-type: image/'.$fileType);
        header('Content-transfer-encoding: binary');
        header('Content-length: '.filesize($graphicFileName));
        return true;
    }
}

$domain = ($_SERVER['HTTP_HOST']);
$explodedDomain = explode('.', $domain);
$domainWithoutTLD = $explodedDomain[0] . '.' . $explodedDomain[1] . '.';

$baseName = basename($_SERVER["REQUEST_URI"]);
$fileName = explode('?', $baseName);

$imagePath = explode('/', $_GET['image']);


$fileName = WEBSITE_ROOT_PATH.UPLOAD_FOLDER.$fileName[0];
if (count($_GET) < 2) {
	$directReturn = true;
}

if(! file_exists($fileName)) {
	$fileName = urldecode($fileName);

    if(! file_exists($fileName)) {
		header('Content-Type: image/jpg');
    	header("HTTP/1.1 404 Not Found");
    	die;
    }
}


$ext = pathinfo($baseName, PATHINFO_EXTENSION);
if($ext == 'svg'){ $directReturn = true; }
if ($directReturn) {
	$type = 'jpeg'; // Default
	if($ext == 'png'){
		$type = 'png';
	} elseif($ext == 'ico'){
		$type = 'ico';
	} elseif($ext == 'svg'){
		$type = 'svg+xml';
	} elseif($ext == 'gif'){
		$type = 'gif';
	}

	setImageHeaders($fileName, $type);
	
	echo file_get_contents($fileName);
	exit;
}

if ($cache) {
    $cacheFolder = UPLOAD_FOLDER.'cache/';
    if(! is_dir($cacheFolder)){
    	mkdir($cacheFolder, 0777);
    }
	if (file_exists($cacheFolder . $baseName)) {
		
		if (stripos($baseName, '.png') !== false) {
			header('Content-Type: image/png');
			header('Content-Source: cache');
			echo file_get_contents($cacheFolder . $baseName);
			exit;
		}
		else if (stripos($baseName, '.gif') !== false) {
			header('Content-Type: image/gif');
			header('Content-Source: cache');
			echo file_get_contents($cacheFolder . $baseName);
			exit;
		} else {
			header('Content-Type: image/jpg');
			header('Content-Source: cache');
			echo file_get_contents($cacheFolder . $baseName);
			exit;
		}
		
	}
}

$long = isset($_GET['long']) ? intval($_GET['long']) : 0;
$width = isset($_GET['width']) ? intval($_GET['width']) : 0;
$height = isset($_GET['height']) ? intval($_GET['height']) : 0;
$maxwidth = isset($_GET['maxwidth']) ? intval($_GET['maxwidth']) : 0;
$maxheight = isset($_GET['maxheight']) ? intval($_GET['maxheight']) : 0;

if (!empty($maxwidth)) {
	$long = $maxwidth;
	unset($maxwidth);
}
if (!empty($maxheight)) {
	$long = $maxheight;
	unset($maxheight);
}

if(isset($_GET['cut'])) {
	$mode = 'cut';
} elseif(isset($_GET['fill'])) {
	$mode = 'fill';
} elseif(isset($_GET['rotate'])) {
	$mode = 'rotate';
	$rotate = (int)$_GET['rotate'];
} else {
	$mode = 'resize';
}


if($width < 0 || $height < 0) {
	die;
}

if(($width * $height) > 9000000) { // 3000x3000 limit.
	header('HTTP/1.1 403 Forbidden');
	die;
}


$fileTypes = @ getimagesize($fileName);
if(! $fileTypes && pathinfo($fileName, PATHINFO_EXTENSION) == 'svg'){
	$fileTypes['mime'] = 'image/svg+xml';
}

$png = false;
$image = new \Gumlet\ImageResize($fileName);
if($_GET['long']){
	$image->resizeToLongSide($_GET['long']);
} elseif($_GET['width'] && $_GET['height']){
	if($_GET['cut']){
		$image->crop($_GET['width'], $_GET['height'], \Gumlet\ImageResize::CROPCENTER);
	} elseif($_GET['fill']) {
		$image->resizeToBestFit($_GET['width'], $_GET['height']);
	} else {
		$image->resizeToBestFit($_GET['width'], $_GET['height']);
	}
} elseif($_GET['width']){
	$image->resizeToWidth($_GET['width']);
} elseif($_GET['height']){
	$image->resizeToHeight($_GET['height']);
}

$image->quality_jpg = 75;
$image->quality_png = 9;

header("X-Image-Debug: $xsrc $ysrc / $newwidth $newheight / {$fileTypes[0]} {$fileTypes[1]}");
header('Pragma: public');
header('Content-Source: generated');
header('Cache-Control: max-age='.IMAGE_EXPIRE_TIME.', public');
header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (IMAGE_EXPIRE_TIME)));

if($cache){
	$image->save($cacheFolder.$baseName);
}

$image->output();

