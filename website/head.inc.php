<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


<?php
	if ($settings['Online'] === 'false') {
		echo '<meta name="robots" content="noindex,nofollow" />';
	}
	else {
		echo '<meta name="robots" content="index,follow" />';
	}
?>

<title><?= $page['title'] ?> - <?= $settings['Website Name']?></title>
<meta name="description" context="<?= $page['description'] ?>">

<!-- Editor CSS -->
<link href="<?= CMS_PUBLIC_URL ?>/assets/css/editor.css" rel="stylesheet">

<!-- Wrapkit CSS -->
<link href="<?= CMS_PUBLIC_URL ?>/assets/wrapkit/wrapkit.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?= CMS_PUBLIC_URL ?>/assets/demo/zylon/site.css" rel="stylesheet">
<link href="<?= CMS_PUBLIC_URL ?>/assets/css/site.css" rel="stylesheet">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<?php
	if(defined('LANGUAGE_URL')){
		$alternates = getLanguageUrls(true);

		foreach($alternates as $language => $url) {
			echo '<link rel="alternate" href="'.getProtocol().$_SERVER['HTTP_HOST'].$url.'" hreflang="'.$language.'" />'."\n";
		}
	}

	if(file_exists(UPLOAD_DIR.'favicon/html.html')){
		include(UPLOAD_DIR.'favicon/html.html');
	}


if(file_exists(CMS_ROOT_PATH.'assets/js/js.js')){ ?>
	<script src="/cms/assets/js/js.js" type="text/javascript"></script>
<?php } ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="/cms/assets/js/popper.min.js"></script>
<script src="/cms/assets/js/bootstrap.min.js"></script>


<script src="/cms/assets/wrapkit/node_modules/aos/dist/aos.js" type="text/javascript"></script>
<link href="/cms/assets/wrapkit/node_modules/aos/dist/aos.css" rel="stylesheet">

<script src="/cms/assets/wrapkit/node_modules/owl-carousel-2/owl.carousel.min.js" type="text/javascript"></script>
<link href="/cms/assets/wrapkit/node_modules/owl-carousel-2/assets/owl.carousel.min.css" rel="stylesheet">

<script src="/cms/assets/wrapkit/node_modules/bootstrap-touch-slider/bootstrap-touch-slider-min.js" type="text/javascript"></script>
<link href="/cms/assets/wrapkit/node_modules/bootstrap-touch-slider/bootstrap-touch-slider.css" rel="stylesheet">

<script src="/cms/assets/wrapkit/wrapkit.js" type="text/javascript"></script>