<?php
/**
 *  @author Sienn
 *  @version 1.0
 *  @name DataFeeds
 *  @description Lorum Ipsum donor sit amet
 *  
 */
class DataFeeds extends Plugin implements PluginInterface {

	public $rewrites = [];
	public $modules = __DIR__.'/modules/*.php';
	public $snippets = __DIR__.'/cms/snippets/*.php';
		
	function __construct(){
		parent::__construct();

		if($this->status() == self::ACTIVE){
			$this->rewrites = $this->rewrites();
		}
	}


	public function activate(){
		parent::activate();

		DB::SITE()->query("CREATE TABLE IF NOT EXISTS `datafeeds` (
			`id` integer PRIMARY KEY,
			`feed` text DEFAULT NULL,
			`list_template` text, 
			`details_slug_prefix` text, 
			`details_slug` text, 
			`details_template` text 
		)")->execute();
	}

	public function deactivate(){
		parent::deactivate();
		DB::SITE()->query("DROP TABLE `datafeeds`;")->execute();
	}

	private function rewrites(){
		$dataRewrites = [];
		$dataSources = DB::SITE()->query("SELECT * FROM datafeeds")->fetch();
		if (!empty($dataSources)) {
			foreach($dataSources as $dataSource) {
				$prefixSlug = strtourl(trim($dataSource['details_slug_prefix']));
				if (!empty($prefixSlug)) {
					$dataRewrites[strtourl($prefixSlug).'\/[(0-9)+]\/([A-Za-z0-9-_\.]+)'] = [$this, 'detail'];
				}
			}
		}
		return $dataRewrites;
	}

	public function parseFeed($feedID) {
		$dataFeed = DB::SITE()->query("SELECT * FROM datafeeds WHERE id=:id", [':id' => $feedID])->single();
		$dataFeed['items'] = [];
		
		$timeout = 10;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $dataFeed['feed']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_ENCODING,  '');
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout*1000);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		$response = curl_exec($ch);

		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$body = substr($response, $header_size);
		curl_close($ch);

		if (strpos($header, 'Content-Type: gzip') !== false) {
			$body = gzdecode($body);
		}

		if (strpos($header, 'application/json') !== false) {
			$dataFeed['items'] = json_decode($body, true);
		}

		return $dataFeed;
	}


	public function detail($url){
		$url = explode('/', $url);
		
		// $news = DB::SITE()->query("SELECT * FROM news WHERE id=:id LIMIT 1")->bind(':id', $url[2])->single();
		// $news['image'] = '/'.UPLOAD_FOLDER.$news['image'];

		// $template = parent::setting('News Template');
		// return parent::template($template, $news);
	}

	public function fetchList($feedID) {
		$feed = $this->parseFeed($feedID);
		
		$items = [];

		foreach($feed['items'] as $itemData) {
			$item = $feed['list_template'];
			foreach($itemData as $field => $value) {
				$item = str_ireplace('{{ '.$field.' }}', $value, $item);
			}
			$items[] = $item;
		}
		return $items;
	}


}

registerNewPlugin(DataFeeds::class);