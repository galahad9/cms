<?php

if($_POST['action'] == 'update'){

	if($_GET['id'] != 'new'){
		DB::SITE()->query("UPDATE datafeeds SET feed=:feed, list_template=:list_template, details_slug_prefix=:details_slug_prefix, details_slug=:details_slug, details_template=:details_template WHERE id=:id", [
			':feed' => $_POST['feed'],
			':list_template' => $_POST['list_template'],
			':details_slug_prefix' => $_POST['details_slug_prefix'],
			':details_slug' => $_POST['details_slug'],
			':details_template' => $_POST['details_template'],
			':id' => $_GET['id']
		])->execute();
		$msg = '<div class="alert alert-success alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">Feed updated</div></div>';
	} else {
		DB::SITE()->query("INSERT INTO datafeeds (feed, list_template, details_slug_prefix, details_slug, details_template) VALUES (:feed, :list_template, :details_slug_prefix, :details_slug, :details_template)", [
			':feed' => $_POST['feed'],
			':list_template' => $_POST['list_template'],
			':details_slug_prefix' => $_POST['details_slug_prefix'],
			':details_slug' => $_POST['details_slug'],
			':details_template' => $_POST['details_template']
		])->execute();
		$id = DB::SITE()->lastInsertId();
		header("Location: index.php?module=datafeeds&page=feed-edit&id=".$id);
	}
}

if($_GET['id'] != 'new'){
	$feed = DB::SITE()->query("SELECT * FROM datafeeds WHERE id=:id")->bind(':id', $_GET['id'])->single();
}


?>
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">DATA FEEDS</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="index.php?page=modules">Modules</a></li>
					<li class="breadcrumb-item"><a href="index.php?page=datafeeds">Data feeds</a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= $_GET['id'] ?></li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<li class="dropdown">
				<a href="index.php?module=datafeeds" class="btn btn-secondary btn-rounded btn-outline" >
					&lt; Cancel
				</a>
			</li>
			<?php
			if($_GET['id'] != 'new'){ ?>
				<li class="dropdown">
					<a href="index.php?module=datafeeds" class="btn btn-danger btn-rounded">
						DELETE
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
</header>

<?php

if($msg){
	echo $msg;
}
?>

<section class="page-content">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<form  method="post" action="">
					<input type="hidden" name="action" value="updateSettings">
					<div class="card-body">

						<div class="form-group row">
							<label for="title" class="col-sm-3 col-form-label">Feed url</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="feed" name="feed" value="<?= $feed['feed'] ?>">
							</div>
						</div>

						<div class="form-group row">
							<label for="title" class="col-sm-3 col-form-label">List template</label>
							<div class="col-sm-9">
								<textarea class="form-control" id="list_template" name="list_template"><?= $feed['list_template'] ?></textarea>
							</div>
						</div>

						<div class="form-group row">
							<label for="title" class="col-sm-3 col-form-label">
								Details slug prefix<br>
								<i>Leave blank to disable a details page</i>
								</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="details_slug_prefix" name="details_slug_prefix" value="<?= $feed['details_slug_prefix'] ?>">
							</div>
						</div>

						<div class="form-group row">
							<label for="title" class="col-sm-3 col-form-label">
								Details slug<br>
							</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="details_slug" name="details_slug" value="<?= $feed['details_slug'] ?>">
							</div>
						</div>

						<div class="form-group row">
							<label for="title" class="col-sm-3 col-form-label">Details template</label>
							<div class="col-sm-9">
								<textarea class="form-control" id="details_template" name="details_template"><?= $feed['details_template'] ?></textarea>
							</div>
						</div>

					</div>
					<div class="card-footer bg-light">
						<div class="form-group m-b-0 row">
							<div class="col-sm-10">
								<button type="submit" name="action" value="update" class="btn btn-primary">Save</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>