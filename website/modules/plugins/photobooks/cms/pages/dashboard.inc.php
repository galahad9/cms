
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">PHOTOBOOKS</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="index.php?page=modules">Modules</a></li>
					<li class="breadcrumb-item active" aria-current="page">Photobooks</li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<li class="dropdown">
				<a href="index.php?module=photobooks&page=edit&id=new" class="btn btn-fab" >
					<i class="la la-plus"></i>
				</a>
			</li>
			<?php
			if($_PLUGINS[$_GET['module']]->settingGroups){ ?>
				<li class="dropdown">
					<a href="index.php?page=settings&type=plugin&plugin=photobooks" class="btn btn-fab" >
						<i class="la la-cog"></i>
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
</header>

<?php
if($_POST['action'] == 'delete'){
	DB::SITE()->query("DELETE FROM photobooks WHERE id=:id")->bind(':id', $_POST['id'])->execute();
	DB::SITE()->query("DELETE FROM photobooks_images WHERE photobooks_id=:id")->bind(':id', $_POST['id'])->execute();
	$msg = '<div class="alert alert-success alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">Photobook removed</div></div>';
}

if($msg){
	echo $msg;
}
?>

<section class="page-content">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Title</th>
								<th>Photos</th>
								<th style="width:200px">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$photobooks = DB::SITE()->query("SELECT * FROM photobooks")->fetch();
							foreach ($photobooks as $key => $photobook) {
								$amount = DB::SITE()->query("SELECT COUNT(id) AS amount FROM photobooks_images WHERE photobooks_id=:id")->bind(':id', $photobook['id'])->get('amount');
								echo '
								<tr>
									<td>'.$photobook['id'].'</td>
									<td>'.$photobook['title'].'</td>
									<td>'.$amount.'</td>
									<td><form method="post">
										<input type="hidden" name="id" value="'.$photobook['id'].'">
										<input type="hidden" name="action" value="delete">
										<a href="index.php?module=photobooks&page=edit&id='.$photobook['id'].'" class="btn btn-sm btn-primary">Edit</a> 
										<button type="submit" name="action" value="delete" class="btn btn-sm btn-danger btn-confirm">Remove</button>

									</form></td>
								</tr>';
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$('#bs4-table').DataTable();

	$(".btn-confirm").click(function(e){
		e.preventDefault();
		$btn = $(this);

		swal({
	        title: "Are you sure?",
	        text: "",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: '#DD6B55',
	        confirmButtonText: 'Yes, I am sure!',
	        cancelButtonText: "No, cancel it!",
	    }).then(function() {
	      $btn.closest('form').submit();

	  });
	})
	
</script>