<?php

ini_set('memory_limit', '1G');
ini_set('upload_max_filesize', '1G');
ini_set('post_max_size', '1G');
ini_set('max_input_vars', '5000');

if($_POST['action'] == 'update'){
	if($_GET['id'] != 'new') {
		DB::SITE()->query("UPDATE photobooks SET title=:title, 	items_per_row_desktop=:items_per_row_desktop, items_per_row_mobile=:items_per_row_mobile, carousel=:carousel WHERE id=:id", [
			':title' => $_POST['title'],
			':items_per_row_desktop' => $_POST['items_per_row_desktop'],
			':items_per_row_mobile' => $_POST['items_per_row_mobile'],
			':carousel' => isset($_POST['carousel']),
			':id' => $_GET['id']
		])->execute();
		$msg = '<div class="alert alert-success alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">Potobook updated</div></div>';
		$bookID = intval($_GET['id']);
	} else {
		DB::SITE()->query("INSERT INTO photobooks (title, items_per_row_desktop, items_per_row_mobile, carousel) VALUES (:title, :items_per_row_desktop, :items_per_row_mobile, :carousel)", [
			':title' => $_POST['title'],
			':items_per_row_desktop' => $_POST['items_per_row_desktop'],
			':items_per_row_mobile' => $_POST['items_per_row_mobile'],
			':carousel' => isset($_POST['carousel']),
		])->execute();
		$bookID = DB::SITE()->lastInsertId();
	}

	if (!empty($_POST['image-name'])){
		$sort = 0;
		foreach($_POST['image-name'] as $imageKey => $imageName) {
			$imageName = preg_replace('/[^a-z0-9.]+/', '-', strtolower($imageName));
			$sort++;
			$imageID = intval($_POST['image-id'][$imageKey]);
			$delete = intval($_POST['image-delete'][$imageKey]);
			if (!$imageID) {
				if ($delete) {
					continue;
				}
				DB::SITE()->query("INSERT INTO photobooks_images (photobooks_id, image, sort) VALUES (:book, :image, :sort)", [
					':book' => $bookID,
					':image' => $imageName,
					':sort' => $sort
				])->execute();
				$imageID = DB::SITE()->lastInsertId();
				$imageName = $bookID.'-'.$imageID.'-'.$imageName;
				DB::SITE()->query("UPDATE photobooks_images SET image=:image WHERE id=:id", [
					':id' => $imageID,
					':image' => $imageName
				])->execute();

				$imageSrc = $_POST['image-src'][$imageKey];
				base64toImage($imageSrc, UPLOAD_DIR . '/' . $imageName);
			}
			else {
				if ($delete) {
					$deletePhoto = DB::SITE()->query("SELECT * FROM photobooks_images WHERE photobooks_id=:book and id=:id", [
						':book' => $bookID, 
						':id' => $imageID
					])->single();
					if ($deletePhoto['id']) {
						unlink(UPLOAD_DIR . '/' . $deletePhoto['image']);
						DB::SITE()->query("DELETE FROM photobooks_images WHERE photobooks_id=:book and id=:id", [
							':book' => $bookID,
							':id' => $imageID
						])->execute(true);
					}
				}
				else {
					DB::SITE()->query("UPDATE photobooks_images SET sort=:sort WHERE id=:id AND photobooks_id=:book", [
						':id' => $imageID, 
						':book' => $bookID, 
						':sort' => $sort
					])->execute();
				}
			}
		}
	}

	if ($_GET['id'] == 'new') {
		header("Location: index.php?module=photobooks&page=edit&id=" . $bookID);
	}
}

$photos = [];
if($_GET['id'] != 'new'){
	$photobook = DB::SITE()->query("SELECT * FROM photobooks WHERE id=:id")->bind(':id', $_GET['id'])->single();
	$photos = DB::SITE()->query("SELECT * FROM photobooks_images WHERE photobooks_id=:book ORDER BY sort ASC")->bind(':book', $photobook['id'])->fetch();
}



if (!$photobook['items_per_row_desktop']) {
	$photobook['items_per_row_desktop'] = 4;
}
switch($photobook['items_per_row_desktop']) {
	case '2':
		$photoColDesktop = 'col-md-6';
		break;
	case '3':
		$photoColDesktop = 'col-md-4';
		break;
	case '4':
		$photoColDesktop = 'col-md-3';
		break;
	case '6':
		$photoColDesktop = 'col-md-2';
		break;
}


if (!$photobook['items_per_row_mobile']) {
	$photobook['items_per_row_mobile'] = 1;
}
switch ($photobook['items_per_row_mobile']) {
	case '1':
	default:
		$photoColMobile = 'col-xs-12';
		break;
	case '2':
		$photoColMobile = 'col-xs-6';
		break;
	case '3':
		$photoColMobile = 'col-xs-4';
		break;
}


?>
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">PHOTOBOOKS</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="index.php?page=modules">Modules</a></li>
					<li class="breadcrumb-item"><a href="index.php?module=photobooks">Photobooks</a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= $_GET['id'] ?></li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<li class="dropdown">
				<a href="index.php?module=photobooks" class="btn btn-secondary btn-rounded btn-outline" >
					&lt; Cancel
				</a>
			</li>
			<?php
			if($_GET['id'] != 'new'){ ?>
				<li class="dropdown">
					<a href="index.php?module=photobooks" class="btn btn-danger btn-rounded">
						DELETE
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
</header>

<?php

if($msg){
	echo $msg;
}
?>

<section class="page-content">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<form  method="post" action="" enctype="multipart/form-data">
					<input type="hidden" name="action" value="updateSettings">
					<div class="card-body">

						<div class="form-group row">
							<label for="title" class="col-sm-3 col-form-label">Title</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="title" name="title" value="<?= $photobook['title'] ?>" required>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="title" class="col-sm-3 col-form-label">Photos per row (desktop)</label>
							<div class="col-sm-9">
								<select class="form-control" id="items_per_row_desktop" name="items_per_row_desktop">
									<option value="2"<?= (($photobook['items_per_row_desktop'] == 2) ? ' selected' : '') ?>>2</option>
									<option value="3"<?= (($photobook['items_per_row_desktop'] == 3) ? ' selected' : '') ?>>3</option>
									<option value="4"<?= (($photobook['items_per_row_desktop'] == 4) ? ' selected' : '') ?>>4</option>
									<option value="6"<?= (($photobook['items_per_row_desktop'] == 6) ? ' selected' : '') ?>>6</option>
								</select>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="title" class="col-sm-3 col-form-label">Photos per row (mobile)</label>
							<div class="col-sm-9">
								<select class="form-control" id="items_per_row_mobile" name="items_per_row_mobile">
									<option value="1"<?= (($photobook['items_per_row_mobile'] == 1) ? ' selected' : '') ?>>1</option>
									<option value="2"<?= (($photobook['items_per_row_mobile'] == 2) ? ' selected' : '') ?>>2</option>
									<option value="3"<?= (($photobook['items_per_row_mobile'] == 3) ? ' selected' : '') ?>>3</option>
								</select>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="title" class="col-sm-3 col-form-label">Carousel</label>
							<div class="col-sm-9">
								<div class="custom-control custom-checkbox checkbox-primary form-check">
									<input type="checkbox" class="custom-control-input" name="carousel" id="carousel" <?= (($photobook['carousel'] == 1) ? ' checked' : '') ?>>
									<label class="custom-control-label" for="carousel">Display as carousel</label>
								</div>
							</div>
						</div>
						
						<h2>Photos</h2>
						<div class="dz-message dropzone-fake needsclick singleFileUpload" style="cursor:pointer;" data-for="#photobook-upload">
							<h6 class="card-title text-center">Click to upload.</h6>
							<div class="text-center"><i class="icon dripicons-cloud-upload gradient-1" style="font-size: 4rem;"></i></div>
							<div class="d-block text-center d-none"><img src="" class="img-responsive" style="max-height:80px"></div>
							<div class="d-block text-center">
								<button type="button" class="btn btn-primary btn-rounded btn-floating btn-md">Select images</button>
							</div>
						</div>
						<input style="visibility:hidden;" type="file" id="photobook-upload" name="photobook-uploads[]" multiple value="">

						<hr>

						<div id="photobook-container" class="row">
							<?php
							foreach($photos as $photo) {
							?>
							<div class="<?= $photoColDesktop ?> <?= $photoColMobile ?> sortable-img">
								<div class="img-sort-item">
									<img class="img-sort" src="<?= $settings['Website Url'] . '/' . UPLOAD_FOLDER . '/' . $photo['image']  ?>?width=320" data-filename="<?= $photo['image'] ?>">
									<input type="hidden" name="image-src[]" value="<?= $photo['image'] ?>">
									<input type="hidden" name="image-name[]" value="<?= $photo['image'] ?>">
									<input type="hidden" name="image-id[]" class="img-sort-id" value="<?= $photo['id'] ?>">
									<input type="hidden" name="image-delete[]" class="img-sort-delete" value="0">
									<button type="button" class="btn btn-danger btn-sm btn-delete-image"><span class="la la-times"></span></button>
								</div>
							</div>
							<?php
							}
							?>
						</div>					

					</div>
					<div class="card-footer bg-light">
						<div class="form-group m-b-0 row">
							<div class="col-sm-10">
								<button type="submit" name="action" value="update" class="btn btn-primary">Save</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<style>
.img-sort-item{
	position: relative;
	display: block;
	box-sizing: border-box;
	height: 10rem;
	line-height: 8rem;
	text-align: center;
	background-color: #f0f6ff;
	border: 1px solid #ccc;
	padding: 1rem;
	margin-bottom: 1rem;
	cursor: move;
}

.img-sort{
	display: inline-block;
	max-width: 100%;
	max-height: 8rem;
}

.btn-delete-image{
	position: absolute;
	top: .3rem;
	right: .3rem;
}
</style>

<script type="text/javascript">
	$(".dropzone-fake").click(function(e){
		e.preventDefault(); e.stopPropagation();
		var $id = $(this).data('for');
		$($id).click();
	});

	function readURL(input) {
		if (input.files) {
			$.each(input.files, function(index, file) {
				var reader = new FileReader();

				reader.onload = function(e) {
					/* image-id 0 for new images*/
					$('#photobook-container').append(
						'<div class="<?= $photoColDesktop ?> <?= $photoColMobile ?> sortable-img">' +
							'<div class="img-sort-item">' +
								'<img class="img-sort" src="' + e.target.result + '" data-filename="' + file.name + '">' +
								'<input type="hidden" name="image-src[]" value="' + e.target.result + '">' +
								'<input type="hidden" name="image-name[]" value="' + file.name + '">' +
								'<input type="hidden" name="image-id[]" class="img-sort-id" value="0">' +
								'<input type="hidden" name="image-delete[]" class="img-sort-delete" value="0">' +
								'<button type="button" class="btn btn-danger btn-sm btn-delete-image"><span class="la la-times"></span></button>' +
							'</div>'+
						'</div>'
					);
				}

				reader.readAsDataURL(file);
			});
			$(input).val('');
		}
	}

	$("#photobook-upload").change(function() {
		readURL(this);
	});

	$("#photobook-container").sortable({
		vertical: false,
		nested: false,
		containerSelector: "row",
		itemSelector: "sortable-img"
	});

	$(document).on('click', '.btn-delete-image', function() {
		let $item = $(this).closest('.sortable-img');
		let id = parseInt($item.find('.img-sort-id').val());
		console.log(id);
		if (id) {
			$item.hide().find('.img-sort-delete').val('1');
		}
		else {
			$item.remove();
		}
	});
</script>

<?php
function base64toImage($base64_string, $output_file) {
	$ifp = fopen($output_file, 'wb'); 
	$data = explode(',', $base64_string);
	fwrite($ifp, base64_decode($data[1]));
	fclose($ifp);
	return $output_file;
}
?>