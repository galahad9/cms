<?php

$photobookGroup = plugin('photobooks')->getPhotos($params[1]);
$photobook = $photobookGroup['book'];
$photos = $photobookGroup['photos'];

if (!$photobook['items_per_row_desktop']) {
    $photobook['items_per_row_desktop'] = 4;
}
switch ($photobook['items_per_row_desktop']) {
    case '2':
        $photoColDesktop = 'col-md-6';
        break;
    case '3':
        $photoColDesktop = 'col-md-4';
        break;
    case '4':
        $photoColDesktop = 'col-md-3';
        break;
    case '6':
        $photoColDesktop = 'col-md-2';
        break;
}

if (!$photobook['items_per_row_mobile']) {
    $photobook['items_per_row_mobile'] = 1;
}
switch ($photobook['items_per_row_mobile']) {
    case '1':
    default:
        $photoColMobile = 'col-xs-12';
        break;
    case '2':
        $photoColMobile = 'col-xs-6';
        break;
    case '3':
        $photoColMobile = 'col-xs-4';
        break;
}

$useCarousel = boolval($photobook['carousel']);

if (!defined('FANCYBOX_LOADED')) {
    define('FANCYBOX_LOADED', true);
    echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css" integrity="sha256-5yrE3ZX38R20LqA/1Mvh3KHJWG1HJF42qtZlRtGGRgE=" crossorigin="anonymous" />';
    echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js" integrity="sha256-ULR2qlEu6WigJY4xQsDsJeW76e9tEE2EWjnKEQ+0L8Q=" crossorigin="anonymous"></script>';
}

if ($useCarousel) {

    echo '<div class="slider-parent">';
    echo '<div id="photobook-' . $photobook['id'] . '" class="slider-on-content" style="height:300px; width: 100%; ">';
    foreach ($photos as $photo) {
		$caption = basename($photo);
		$dotIndex = strrpos($caption, ".");
		$caption = substr($caption, 0, $dotIndex);
		/*This part should be improved, not the best code*/
		$caption = explode('-', $caption);
		unset($caption[0]);
		unset($caption[1]);
		$caption = implode('-', $caption);
		/*-----------------------------------------------*/
        echo '<a class="is-boxes slider-image" style="background-image:url(\'/' . UPLOAD_FOLDER . '/' . $photo . '?width=550\');" data-fancybox="gallery' . $photobook['id'] . '" data-caption="'.$caption.'" href="/' . UPLOAD_FOLDER . '/' . $photo . '"></a>';
    }
    echo '</div>';
    echo '</div>';

    ?>
	<style>
	*{
		min-height: 0;
		min-width: 0;
	}

	.slider-parent .slider-image {
		background-size: contain;
	}
	</style>
	<script>
		var docReadyPhotobook<?=$photobook['id']?> = function (fn) {
			var stateCheck = setInterval(function () {
				if (document.readyState !== "complete") return;
				clearInterval(stateCheck);
				try {
					fn()
				} catch (e) {}
			}, 1);
		};
		docReadyPhotobook<?=$photobook['id']?>(function () {
			jQuery("#photobook-<?=$photobook['id']?>").slick({
				test: 1,
				dots: true,
				arrows: true,
				infinite: true,
				speed: 500,
				cssEase: "linear",
				slidesToShow: 5,
				slidesToScroll: 5,
				autoplay: false,
				autoplaySpeed: 3000,
				fade: false,
				adaptiveHeight: true,
				responsive: [{
					breakpoint: 920,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				}, {
					breakpoint: 480,
					settings: {
						arrows: false,
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}]
			});
		});
	</script>
	<?php

} else { //Default photobook (no carousel)

    echo '<div class="photobook-row row row-eq-height m-t-40">';
    if (!empty($photos)) {
        foreach ($photos as $photo) {
			$caption = basename($photo);
            echo '<div class="' . $photoColDesktop . ' ' . $photoColMobile . ' photobook-col">';
            echo '<a class="photobook-image d-flex align-items-center" data-fancybox="gallery' . $photobook['id'] . '" data-caption="'.$caption.'" href="/' . UPLOAD_FOLDER . '/' . $photo . '">';
            echo '<img src="/' . UPLOAD_FOLDER . '/' . $photo . '?width=550">';
            echo '</a>';
            echo '</div>';
        }
    }
    echo '</div>';

    if (!defined('PHOTOBOOK_STYLED')) {
        define('PHOTOBOOK_STYLED', true);
        echo '<style>
			.photobook-row .photobook-col{
				margin-bottom: 1rem;
				text-align: center;
			}
			.photobook-row .photobook-image{
				position: relative;
				display: block;
				width: 100%;
				height: 100%;
				background-color: rgba(0, 0, 0, 0.05);
				padding: 1rem;
				margin-bottom: 1rem;
				box-sizing: bordre-box;
			}

			.photobook-row img{
				position: relative;
				display: inline-block;
				max-width: 100%;
				max-height: 250px;
				margin: 0;
		</style>';
    }

}