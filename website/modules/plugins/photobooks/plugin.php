<?php
/**
 *  @author Sienn
 *  @version 1.0
 *  @name FotoBooks
 *  @description Lorum Ipsum donor sit amet
 *  
 */
class PhotoBooks extends Plugin implements PluginInterface {

	public $rewrites = [];
	public $modules = __DIR__.'/modules/*.php';
	public $snippets = __DIR__.'/cms/snippets/*.php';
		
	function __construct(){
		parent::__construct();

		if($this->status() == self::ACTIVE){
			// $this->rewrites = $this->rewrites();
		}
	}

	public function activate(){
		parent::activate();

		DB::SITE()->query("CREATE TABLE IF NOT EXISTS `photobooks` (
			`id` integer PRIMARY KEY,
			`title` text,
			`items_per_row_desktop` integer,
			`items_per_row_mobile` integer,
			`carousel` boolean
		)")->execute();

		DB::SITE()->query("CREATE TABLE IF NOT EXISTS `photobooks_images` (
			`id` integer PRIMARY KEY,
			`photobooks_id` integer,
			`image` text,
			`sort` integer
		)")->execute();
	}

	public function deactivate(){
		parent::deactivate();
		DB::SITE()->query("DROP TABLE `photobooks`;")->execute();
		DB::SITE()->query("DROP TABLE `photobooks_images`;")->execute();
	}

	public function getPhotos($bookID) {
		$photos = [];
		$book = DB::SITE()->query("SELECT * FROM photobooks WHERE id=:id", [':id' => $bookID])->single();
		if ($book['id']) {
			$photos = DB::SITE()->query("SELECT image FROM photobooks_images WHERE photobooks_id=:id ORDER BY sort ASC", [':id' => $bookID])->fetch();
			$photos = array_column($photos, 'image');
			return ['book' => $book, 'photos' => $photos];
		}
		return [];
	}
}

registerNewPlugin(PhotoBooks::class);