<?php

$id = $_GET['id'];
if($_POST['action'] == 'update'){

	$title = htmlentities($_POST['title']);

	$fields = [];
	$rows = [];
	foreach ($_POST['field'] as $key => $value) {
		$rows[] = $key;
	}
	foreach ($_POST['field']['label'] as $key => $value) {
		if($value){
			$field = [];
			foreach ($rows as $row) {
				$field[$row] = $_POST['field'][$row][$key];
			}
			$field['name'] = strtourl($field['label']);
			$fields[] = $field;
		}
	}


	$rules = [];
	$rule_rows = [];
	foreach ($_POST['rules'] as $key => $value) {
		$rule_rows[] = $key;
	}
	foreach ($_POST['rules']['type'] as $key => $value) {
		if($value){
			$rule = [];
			foreach ($rule_rows as $row) {
				$rule[$row] = $_POST['rules'][$row][$key];
			}
			$rules[] = $rule;
		}
	}
	

	if($_GET['id'] == 'new'){
		DB::SITE()->query("INSERT INTO customfields (`sequence`, `language`, `group`, `rules`, `data`) 
			VALUES ( :sequence, :language, :group, :rules, :data)
		", [
			':sequence' => 1,
			':language' => NULL,
			':group' => $title,
			':rules' => json_encode($rules),
			':data' => json_encode($fields)
		])->execute();

		$id = DB::SITE()->lastInsertId();
		flash_message('Custom Fields created');
		header('Location: /cms/index.php?module=customfields&page=group-edit&id='.$id);
	} else {
		DB::SITE()->query("UPDATE customfields SET `group`=:group, `rules`=:rules, `data`=:data WHERE id=:id
		", [
			':id' => $id,
			':group' => $title,
			':rules' => json_encode($rules),
			':data' => json_encode($fields)
		])->execute();

		flash_message('Custom Fields updated');
		header('Location: /cms/index.php?module=customfields&page=group-edit&id='.$id);
	}
} elseif($_POST['action'] == 'remove'){
	DB::SITE()->query("DELETE FROM customfields WHERE id=:id")->bind(':id', $id)->execute();
	flash_message('Custom Field group removed');
	header('Location: /cms/index.php?module=customfields');
}

$customfieldsClass = plugin('customfields');
$customfields = $customfieldsClass->get($id);


$fields = $customfields['data'];
$fields[] = [
	'new' => 1,
	'group' => '',
	'label' => '',
	'type' => '',
	'classes' => 'empty new d-none'
];

$rules = $customfields['rules'];

// $types = getPosttypes();
// foreach ($types as $key => $value) {
// 	dump($value->name().' : '.$value->isPosttype());
// 	dump($value->fields());
// }

// exit;
// $news = $type[2];
// dd($news->fields());

?>
<form  method="post" action="" enctype="multipart/form-data">
	<header class="page-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h1 class="separator">CUSTOM FIELDS</h1>
				<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
						<li class="breadcrumb-item"><a href="index.php?page=modules">Modules</a></li>
						<li class="breadcrumb-item"><a href="index.php?page=customfields">Custom Fields</a></li>
						<li class="breadcrumb-item active" aria-current="page"><?= $_GET['id'] ?></li>
					</ol>
				</nav>
			</div>
			<ul class="actions top-right">
				<li class="dropdown">
					<a href="index.php?module=customfields" class="btn btn-secondary btn-rounded btn-outline" >
						< Cancel
					</a>
				</li>
				<?php
				if($_GET['id'] != 'new'){ ?>
					<li class="dropdown">
						<button type="submit" name="action" value="remove" class="btn btn-danger btn-rounded">
							DELETE
						</button>
					</li>
				<?php } ?>

				<li class="dropdown">
					<button type="submit" name="action" value="update" class="btn btn-primary btn-rounded">
						<i class="icon dripicons-save"></i> SAVE
					</button>
				</li>

			</ul>
		</div>
	</header>

	<?php

	if($msg){
		echo $msg;
	}
	?>


	<section class="page-content">
		<div class="row">
			<div class="col-12">
				<div class="card" data-widget="dropzone">
					<h5 class="card-header">New group title</h5>
					<div class="card-body">
						<div class="form-group row">
							<div class="col-sm-12">
								<input type="text" class="form-control" id="title" name="title" value="<?= $customfields['group'] ?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="page-content">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<h5 class="card-header">Fields</h5>
					<div class="card-body" style="padding-top:0px; padding-left:0px; padding-right:0px; padding-bottom:0px !important;">
						<table class="table table-bordered table-customfields">
							<thead>
								<tr>
									<th style="width:25%">Sequence</th>
									<th style="width:37.5%">Label</th>
									<th style="width:37.5%">Type</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th colspan="4">
										<i class="zmdi zmdi-arrows zmdi-hc-fw"></i> Drag to change sequence
										<button type="button" class="btn btn-primary btn-sm pull-right btn-new-field">+ New field</button>
									</th>
								</tr>
							</tfoot>
							<tbody>
								<tr>
									<td colspan="4" style="padding:0" class="td-customfields" id="sortable">
										<?php
										foreach ($fields as $key => $field) {
											echo '<div class="customfields-fields '.$field['classes'].'">';
												echo '<div class="head">';
													echo '<div class="head-row row">';
														echo '<div class="col-sm-3 col-sequence"><span>'.($key+1).'</span></div>';
														echo '<div class="col col-name"><span>'.$field['name'].'</span></div>';
														echo '<div class="col col-type"><span>'.$field['type'].'</span></div>';
													echo '</div>';
												echo '</div>';
												echo '<div class="fields" style="display:none">';
													
													echo '<div class="fields-row row">';
														echo '<div class="label col-sm-4 col-md-3 ">';
															echo '<label>Label <span class="required">*</span></label>';
														echo '</div>';
														echo '<div class="content col-sm-8 col-md-9 align-self-center">';
															echo '<input type="text" '.(($field['new']) ? '' : 'required').' id="label" name="field[label][]" value="'.$field['label'].'" class="form-control">';
														echo '</div>';
													echo '</div>';

													echo '<div class="fields-row row">';
														echo '<div class="label col-sm-4 col-md-3 ">';
															echo '<label>Type <span class="required">*</span></label>';
														echo '</div>';
														echo '<div class="content col-sm-8 col-md-9 align-self-center">';
															echo '<select '.(($field['new']) ? '' : 'required').' class="form-control" name="field[type][]">';
																echo '<optgroup label="Basis">';
																	foreach ($customfieldsClass->types() as $value => $name) {
																		echo '<option '.(($field['type'] == $value) ? 'selected' : '').' value="'.$value.'">'.$name.'</option>';
																	}
																echo '</optgroup>';
															echo '</select>';
														echo '</div>';
													echo '</div>';

													echo '<div class="fields-row row">';
														echo '<div class="label col-sm-4 col-md-3 ">';
															echo '<label>Instructions</label>';
															echo '<p class="description">Explanation for users. Displayed when filling in the field.</p>';
														echo '</div>';
														echo '<div class="content col-sm-8 col-md-9 align-self-center">';
															echo '<textarea name="field[instructions][]" class="form-control">'.$field['instructions'].'</textarea>';
														echo '</div>';
													echo '</div>';

													echo '<div class="fields-row row">';
														echo '<div class="label col-sm-4 col-md-3 ">';
															echo '<label>Has relation</label>';
														echo '</div>';
														echo '<div class="content col-sm-8 col-md-9 align-self-center">';
															echo '<input class="tgl tgl-light tgl-primary" id="field-hasrelation-'.$key.'" type="checkbox" value="1" '.(($field['hasrelation']) ? 'checked' : '').'>
													
																<label class="tgl-btn" for="field-hasrelation-'.$key.'"></label>';
														echo '</div>';
													echo '</div>';

													echo '<div class="fields-row row" data-condition="field-relation-'.$key.':1">';
														echo '<div class="label col-sm-4 col-md-3 ">';
															echo '<label>Relation <span class="required">*</span></label>';
														echo '</div>';
														echo '<div class="content col-sm-8 col-md-9 align-self-center">';
															echo '<select class="form-control" name="field[relation][]" id="field-relation-'.$key.'"><option value=""></option>';
																
																	foreach (getPosttypes() as $value => $posttype) {
																		
																		if(count($posttype->fields()) > 0){
																			
																			echo '<optgroup label="'.$posttype->name().'">';
																			foreach ($posttype->fields() as $label => $postfield) {

																				echo '<option '.(($field['relation'] == $value) ? 'selected' : '').' value="'.$label.'">'.$postfield['name'].'</option>';
																			}
																			echo '</optgroup>';
																		}
																		// $posttype
																		
																		
																	}
																
															echo '</select>';
														echo '</div>';
													echo '</div>';


													echo '<div class="fields-row row">';
														echo '<div class="label col-sm-4 col-md-3 ">';
															echo '<label>Required?</label>';
														echo '</div>';
														echo '<div class="content col-sm-8 col-md-9 align-self-center">';
															echo '<input class="tgl tgl-light tgl-primary" id="field-required-'.$key.'" name="field[required][]" type="checkbox" value="1" '.(($field['required']) ? 'checked' : '').'>
													
																<label class="tgl-btn" for="field-required-'.$key.'"></label>';
														echo '</div>';
													echo '</div>';

													echo '<div class="fields-row row">';
														echo '<div class="label col-sm-4 col-md-3 ">';
															echo '<label>Default value</label>';
															echo '<p class="description">Appears when creating a new post</p>';
														echo '</div>';
														echo '<div class="content col-sm-8 col-md-9 align-self-center">';
															echo '<input type="text" name="field[default][]" class="form-control" value="'.$field['default'].'">';
														echo '</div>';
													echo '</div>';

													echo '<div class="fields-row row">';
														echo '<div class="label col-sm-4 col-md-3 ">';
															echo '<label></label>';
														echo '</div>';
														echo '<div class="content col-sm-8 col-md-9 align-self-center">';
															echo '<button type="button" class="btn btn-primary btn-close-field btn-sm">Close field</button>';
														echo '</div>';
													echo '</div>';

												echo '</div>';
											echo '</div>';
										} 

										if(count($fields) === 1){
											echo '<div class="no-fields">
												<p>No fields. Press the + New field button to create your first field.</p>
											</div>';
										}
										?>
										
									</td>
								</tr>
							</tbody>	
								
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="page-content">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<h5 class="card-header">Location</h5>
					<div class="row">
						<div class="col-sm-4 col-md-3">
							<div class="card-body" style="background-color:#EEF2F5">
								<h4>Rules</h4>
								<p>Create rules to determine on which edit screen your extra fields appear</p>
							</div>
						</div>
						<div class="col-sm-8 col-md-9">
							<div class="card-body">
								<label>Show this field group if</label>
								<div class="form-group row">
									<div class="col-sm-4">
										<select name="rules[type][]" class="form-control rules-type">
											<optgroup label="Basis">
												<option <?= (($rules[0]['type'] == 'posttype') ? 'selected' : '') ?> value="posttype">Post Type</option>
												<option <?= (($rules[0]['type'] == 'role') ? 'selected' : '') ?> value="role">User role</option>
											</optgroup>
										</select>
									</div>
									<div class="col-sm-3">
										<select name="rules[operator][]" class="form-control">
											<option <?= (($rules[0]['operator'] == '==') ? 'selected' : '') ?> value="==">is equal to</option>
											<option <?= (($rules[0]['operator'] == '!=') ? 'selected' : '') ?> value="!=">is not equal to</option>
										</select>
									</div>
									<div class="col-sm-4">
										<select name="rules[value][]" class="form-control rules-values">
											<optgroup id="posttype" label="Post Types">
												<?php
												foreach (getCustomPostTypes() as $key => $posttype) {
													echo '<option '.(($rules[0]['value'] == $posttype) ? 'selected' : '').' value="'.$posttype.'">'.ucfirst($posttype).'</option>';
												}
												?>
											</optgroup>
											<optgroup id="role" label="Roles" style="display:none">
												<?php
												foreach(getUserGroups() as $role){
													echo '<option '.(($rules[0]['value'] == $role['role']) ? 'selected' : '').' value="'.$role['role'].'">'.$role['role'].'</option>';
												}
												?>
											</optgroup>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</form>


<script type="text/javascript">
	$(".rules-type").change(function(e){
		var type = $(this).val();

		$(this).closest('.form-group').find('.rules-values optgroup').each(function(index, el) {
			var $group = $(el);
			$group.hide();
			if($group.attr('id') == type){
				console.log($group);
				$group.show();
			}
		});
	});
	$(".table-customfields").on('click', ".head", function(e){
		$(this).toggleClass('active');
		$(this).parent().toggleClass('active');
		$(this).next('.fields').slideToggle();
	});

	$(".table-customfields").on('click', ".btn-close-field", function(e){
		$(this).closest('.customfields-fields').find('.head').click();
	});

	$(".table-customfields").on('input', "input[name='field[label][]'], select[name='field[type][]']", function(e){
		var $group = $(this).closest('.fields');
		var label = $group.find("input[name='field[label][]']").val();
		var type = $group.find("select[name='field[type][]']").val();
		
		$(this).closest('.customfields-fields').find('.col-name span').text(label);
		$(this).closest('.customfields-fields').find('.col-type span').text(type);
	});

	$(".btn-new-field").click(function(e){
		$(".no-fields").slideUp();
		var $field = $(".table-customfields .customfields-fields.new").clone();
		var number = parseInt($(".table-customfields .customfields-fields").last().find('.col-sequence span').text()) + 1;
		$field.removeClass('d-none new empty');
		$field.find('.col-sequence span').text( (number - 1) );

		$field.find('input[type=checkbox]').each(function(index, el) {
			var id = 'field-required-'+makeid();
			$(el).attr('id', id);
			$(el).next('label').attr('for', id);			
		});

		$(".table-customfields .td-customfields").append($field);

		$field.find('.head').click();
	});

	$(function(){
    	$("#sortable").sortable({
    		stop: function( event, ui ) {
    			var i = 1;
    			$(".table-customfields .customfields-fields .col-sequence").each(function(index, el) {
    				$(el).find('span').text(i);
    				i++;
    			});
    		}
    	});

    	$("input, select").on('input', function(e) {
    		var $input = $(this);
    		var $fields = $(this).closest('.fields');
    		$fields.find(".fields-row:not([data-condition=''])").each(function(index, el) {
    			var $el = $(el);
    			var condition = $el.attr('data-condition');
    			if(condition){
	    			console.log(condition);
	    			condition = condition.split(':');
	    			console.log(condition);
	    			var $field = $fields.find('#'+condition[0]);

	    			var value = $input.val();
	    			if($input.attr('type') == 'checkbox'){
	    				value = ($input.is(':checked')) ? 1 : 0;
	    			}
	    			console.log($input);
	    			console.log($input.val());
	    			if(value == condition[1]){
	    				$field.closest('.fields-row').css('display', 'flex');
	    			} else {
	    				$field.closest('.fields-row').css('display', 'none');
	    			}
	    		}
    		});
    	});
    });

	function makeid() {
	  var text = "";
	  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	  for (var i = 0; i < 5; i++)
	    text += possible.charAt(Math.floor(Math.random() * possible.length));

	  return text;
	}

</script>
<style>
	.mce-tinymce iframe { height:600px !important }
	.head { cursor:pointer; }
	.table.table-noborders td, .table .table.table-noborders td { border:0px; }
	.table.table-customfields  { margin-bottom:0px }
	.table.table-customfields tfoot { background: #dee7f3; }
	.customfields-fields .head, .no-fields { padding:25px 15px; border-bottom:1px solid #dee2e6; }
	.customfields-fields .head .col-sequence span { cursor:move;  border:1px solid #dee2e6; border-radius:50px; padding: 5px 10px; }

	.customfields-fields .fields {}
	.customfields-fields .fields .fields-row  { margin:0px !important; }
	.customfields-fields .fields .fields-row:last-child  { margin-bottom:20px !important; }
	.customfields-fields .fields .label { background:#EEF2F5; padding: 15px; }
	.customfields-fields .fields .label p:empty { display:none }
	.customfields-fields .fields .label .required { color:#971723; }
	
	.customfields-fields .fields .fields-row[data-condition]:not([data-condition=""]) { display:none; }
	
</style>










