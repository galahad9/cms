
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">CUSTOM FIELD GROUPS</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="index.php?page=modules">Modules</a></li>
					<li class="breadcrumb-item active" aria-current="page">Custom fields</li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<li class="dropdown">
				<a href="index.php?module=customfields&page=group-edit&id=new" class="btn btn-fab" >
					<i class="la la-plus"></i>
				</a>
			</li>
			<?php
			if($_PLUGINS[$_GET['module']]->settingGroups){ ?>
				<li class="dropdown">
					<a href="index.php?page=settings&type=plugin&plugin=customfields" class="btn btn-fab" >
						<i class="la la-cog"></i>
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
</header>

<?php
if($_POST['action'] == 'delete'){
	// DB::SITE()->query("DELETE FROM news WHERE id=:id")->bind(':id', $_POST['id'])->execute();
	// $msg = '<div class="alert alert-success alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">News item removed</div></div>';
}

if($msg){
	echo $msg;
}
?>

<section class="page-content">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<table class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>Group</th>
								<th>Fields</th>
								<!-- <th style="width:200px">Actions</th> -->
							</tr>
						</thead>
						<tbody>
							<?php
							$groups = DB::SITE()->query("SELECT * FROM `customfields` GROUP BY `group` ORDER BY `group` ASC")->fetch();
							foreach ($groups as $key => $group) {
								$data = json_decode($group['data'], true);
								echo '
								<tr style="cursor:pointer" onclick="window.location.href=\'/cms/index.php?module=customfields&page=group-edit&id='.$group['id'].'\'">
									<td>'.$group['group'].'</td>
									<td>'.count($data).'</td>';
									// <td><form method="post">
									// 	<input type="hidden" name="id" value="'.$item['id'].'">
									// 	<input type="hidden" name="action" value="delete">
									// 	<button type="submit" name="action" value="delete" class="btn btn-sm btn-danger btn-confirm">Remove</button>

									// </form></td>
									
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$('#bs4-table').DataTable();

	$(".btn-confirm").click(function(e){
		e.preventDefault();
		$btn = $(this);

		swal({
	        title: "Are you sure?",
	        text: "",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: '#DD6B55',
	        confirmButtonText: 'Yes, I am sure!',
	        cancelButtonText: "No, cancel it!",
	    }).then(function() {
	      $btn.closest('form').submit();

	  });
	})
	
</script>