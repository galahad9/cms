<?php
/**
 *  @author Sienn
 *  @version 1.0
 *  @name CustomFields
 *  @description Lorum Ipsum donor sit amet
 *  
 */

class CustomFieldsHelper extends Plugin implements PluginInterface {

	private $customfields;

	public function types(){
		return [
			'text' => 'Text',
			'textarea' => 'Textarea',
			'number' => 'Number',
			'email' => 'Email',
			'password' => 'Password',
			'picture' => 'Picture',
			'select' => 'Select'
		];
	}

	public function get($id){
		$this->customfields = DB::SITE()->query("SELECT * FROM customfields WHERE id=:id")->bind(':id', $id)->single();
		$this->prettify();
		return $this->customfields;
	}

	public function where($value){
		$type = 'posttype';
		$operator = '==';

		$groups = DB::SITE()->query("SELECT * FROM customfields")->fetch();
		foreach ($groups as $key => &$group) {
			$group['rules'] = json_decode($group['rules'], true);
			$group['data'] = json_decode($group['data'], true);

			foreach ($group['rules'] as  $rule) {
				if($rule['type'] == $type && checkOperator(strtolower($rule['value']), $rule['operator'], strtolower($value))){
					
				} elseif($rule['type'] == 'role' && checkOperator($rule['value'], $rule['operator'], $_SESSION['user']['role'])){

				} else {
					unset($groups[$key]);
				}
			}
		}


		return $groups;
	}

	public function prettify(){
		$this->customfields['rules'] = json_decode($this->customfields['rules'], true);
		$this->customfields['data'] = json_decode($this->customfields['data'], true);
	}
}



class CustomFields extends CustomFieldsHelper  {

	public function activate(){
		parent::activate();

		DB::SITE()->query("CREATE TABLE IF NOT EXISTS `customfields` (
		  `id` integer PRIMARY KEY,
		  `sequence` integer DEFAULT NULL,
		  `language` text DEFAULT '',
		  `group` text DEFAULT NULL,
		  `rules` BLOB DEFAULT NULL,
		  `data` BLOB DEFAULT NULL,
		  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		)")->execute();
	}

	public function deactivate(){
		parent::deactivate();

		DB::SITE()->query("DROP TABLE `customfields`;")->execute();
	}

}


registerNewPlugin(CustomFields::class);