<?php

if($params[1]){
	$form = plugin('forms')->form($params[1]);
	if($form){

		if($_POST){
			if($form->process($_POST)){
				echo '<div class="alert alert-success">'.(($form->form('success')) ? $form->form('success') : 'Bericht verzonden').'</div>';
			} else {
				echo '<div class="alert alert-danger">'.(($form->form('alert')) ? $form->form('alert') : 'Bericht is niet verzonden. Probeer het later nogmaals.').'</div>';
			}
		}

		echo $form->html();
	}
}
?>