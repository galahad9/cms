<?php

class SiennForm {

	private $form_id;
	private $form;
	private $data;

	function __construct($form_id){
		$this->form_id = $form_id;
		$this->form = DB::SITE()->query("SELECT * FROM forms WHERE id=:id")->bind(':id', $form_id)->single();
		
		return $this;
	}

	public function count(){
		return count($this->data());
	}

	public function data(){
		if(! $this->data){
			$this->data = DB::SITE()->query("SELECT * FROM forms_data WHERE form_id=:id ORDER BY created_at DESC")->bind(':id', $this->form_id)->fetch();
			foreach ($this->data as $key => $value) {
				$this->data[$key]['post'] = json_decode($this->data[$key]['post'], true);
			}
		}

		return $this->data;
	}

	public function form($key){
		return $this->form[$key];
	}

	public function html(){
		return $this->form['content'];
	}

	public function process($data){

		foreach ($this->form as $key => $value) {
			if(strpos($value, '{{') !== false){
				preg_match_all("/{{(.+)}}/Um", $value, $output_array);

				$output_array = $output_array[1];
				

				foreach ($output_array as $placeholder) {
					$placeholder = trim($placeholder);
					if(strpos($placeholder, '|date') !== false){
						$filter = explode('|date', $placeholder);
						if(! $filter[1]){
							$filter[1] = '(d-m-Y)';
						}
						$filter[1] = trim(str_replace(['(', ')'], '', $filter[1]));

						$data[$placeholder] = date($filter[1], strtotime($data[$filter[0]]));
					}

					$this->form[$key] = str_replace(['{{ '.$placeholder. ' }}', '{{'.$placeholder.'}}'], $data[$placeholder], $this->form[$key]);
			    }
			}	
		}

		DB::SITE()->query("INSERT INTO forms_data (`form_id`, `post`, `to`, `cc`, `bcc`, `from`, `reply`, `subject`, `headers`) VALUES (:form_id, :post, :to, :cc, :bcc, :from, :reply,  :subject, :headers)", [
			':form_id' => $this->form_id,
			':post' => json_encode($data),
			':to' => $this->form['to'],
			':cc' => $this->form['cc'],
			':bcc' => $this->form['bcc'],
			':from' => $this->form['from'],
			':reply' => $this->form['reply'],
			':subject' => $this->form['subject'],
			':headers' => $this->form['headers']
		])->execute();
		$form_data_id = DB::SITE()->lastInsertId();


		if(! $this->form['from']){
			$this->form['from'] = [plugin('forms')->setting('Emailaddress')];
		}

		if($this->form['to']){

			$content = '<p>The "'.$this->form['title'].'" form is sended. See the data below.</p><p>&nbsp;</p>'.$this->dataAsTable($data);
			$mail = new Mail;
			$message = $mail->message()
				->setSubject($this->form['subject'])
				->setFrom($this->form['from'])
  				->setTo([$this->form['to']])
  				->setBody($content, 'text/html')
  			;

  			if($this->form['reply']){
  				$message->setReplyTo($this->form['reply']);
  			}

  			DB::SITE()->query("UPDATE forms_data SET raw=:raw WHERE id=:id", [
  				':raw' => $content,
  				':id' => $form_data_id
  			])->execute();

  			

  			if($this->form['confirmation_active'] && $this->form['confirmation_to']){
				$confirmation_message = $mail->message()
					->setSubject($this->form['confirmation_subject'])
					->setFrom($this->form['from'])
	  				->setTo([$this->form['confirmation_to']])
	  				->setBody(nl2br($this->form['confirmation_mail']), 'text/html')
	  			;
	  			$mail->send($confirmation_message);
  			}

  			return $mail->send($message);
		}

		return true;
	}

	public function dataAsTable($row){
		if(! is_array($row)){
			$row = json_decode($row, true);
		}

		$keys = array_keys($row);
		$table = '<table>';
			$table .= '<tbody>';
				foreach ($row as $key => $value) {
					
					if(strtotime($value) && substr_count($value, '-') == 2){
						$value = date('d-m-Y', strtotime($value));
					}

					$table .= '<tr>';
						$table .= '<td><strong>'.$key.'</strong> &nbsp;&nbsp;</td>';
						$table .= '<td>'.$value.'</td>';
					$table .= '</tr>';
				}
			$table .= '</tbody>';
		$table .= '</table>';

		return $table;
	}

}
