<?php
$form_id = intval($_GET['form']);
$form = plugin('forms')->form($form_id);
$data = $form->data();


?>
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">FORM</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="index.php?page=modules">Modules</a></li>
					<li class="breadcrumb-item"><a href="index.php?module=forms&page=dashboard">Forms</a></li>
					<li class="breadcrumb-item active" aria-current="page">Data</li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<li class="dropdown">
				<a href="index.php?module=forms" class="btn btn-secondary btn-rounded btn-outline" >
					< Back to Forms
				</a>
			</li>
		</ul>
	</div>
</header>

<section class="page-content p-t-30" id="mail-wrapper">
	<div class="card">
		<div class="d-flex flex fixed-height" id="3-col-wrapper">						
			<section class="center-content flex-fill col-sm-4">
				<div class="toolbar border-bottom">
					<ul class="actions top-left">
						<li id="mobile-aside-menu-trigger">
							<a href="javascript:void(0)" data-q-action="page-aside-left-open"><i class="icon svg-icon">
								<svg id="i-menu" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
									<path d="M4 8 L28 8 M4 16 L28 16 M4 24 L28 24" />
								</svg>
							</i></a>
						</li>
					</ul>
				</div>
				<div class="search-wrapper">
					<button class="search-button-submit" type="button"><i class="icon dripicons-search"></i></button>
					<input type="text" class="search-input" placeholder="Search...">
					<i class="icon dripicons-cross close-search"></i>
				</div>
				<section class="scroll-y">
					<ul class="list-group list-group-messages" data-scroll="minimal-dark">
						<?php
						foreach ($data as $key => $item) {
							echo '<li class="list-group-item '.(($key == 0) ? 'active' : '').'" data-message="'.$item['id'].'">
								<a href="javascript:void(0)" class="item d-flex" data-q-action="page-aside-right-open">
					
									<div class="row-content order-2">
										<!-- <small class="text-muted float-right">1:30pm</small> -->
										<h6 class="list-group-item-heading">'.$item['subject'].'</h6>
										<p class="list-group-item-title">'.date('d-m-Y H:i:s', strtotime($item['created_at'])).'</p>
										<p class="list-group-item-text">'.cutParagraph($item['raw'], 100).'</p>
									</div>
								</a>
							</li>';
						}
						?>
						
					</ul>
				</section>
			</section>
												
			<aside class="aside aside-right flex-fill col-sm-8">
				<div class="toolbar border-bottom">
					<ul class="actions top-left">
						<!-- <li>
							<a href="javascript:void(0)"><i class="icon dripicons-chevron-left"></i></a>
						</li>
						<li>
							<a href="javascript:void(0)"><i class="icon dripicons-chevron-right"></i></a>
						</li> -->
					</ul>
					<ul class="actions top-right">
						<!-- <li>
							<a href="javascript:void(0)"><i class="icon dripicons-trash"></i></a>
						</li> -->
						<!-- <li> -->
							<!-- <a href="javascript:void(0)"><i data-q-action="page-aside-right-open" class="icon dripicons-cross"></i></a> -->
						<!-- </li> -->

					</ul>
				</div>
				<section class="scroll-y">
					<div class="card no-shadow p-30" data-scroll="minimal-dark">
						<div class="card-header border-none">
							<!-- <span>Susie Obrien</span> -->
							<ul class="actions top-right">
								<!-- <li>
									<button type="button" class="btn btn-sm btn-light btn-outline">
										<i class="icon dripicons-reply"></i> Reply
									</button>
								</li>
								<li>
					      			<button type="button" class="btn btn-sm btn-light btn-outline">
										<i class="icon dripicons-forward"></i> Forward
									</button>
								</li>
								<li class="dropdown">
									<a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										<a href="#" class="dropdown-item">
											<i class="icon dripicons-forward p-r-10"></i> <span>Reply to message</span>
										</a>
										<a href="#" class="dropdown-item">
											<i class="icon dripicons-forward p-r-10"></i> <span>Forward message</span>
										</a>
										<a href="#" class="dropdown-item">
											<i class="icon dripicons-print p-r-10"></i> <span>Print message</span>
										</a>
										<a href="#" class="dropdown-item">
											<i class="icon dripicons-trash p-r-10"></i> <span>Delete this message</span>
										</a>
									</div>
								</li> -->
							</ul>
						</div>

						<?php
						foreach ($data as $key => $item) {
							echo '<div class="card-body card-body-message card-body-message-'.$item['id'].' p-t-0 '.(($key == 0) ? 'active' : '').'" data-message="'.$item['id'].'">
									<h5 class="card-title">20 Augustus 2019</h5>';
								foreach ($item['post'] as $key => $value) {
									echo '<dl class="row">
										<dt class="col-sm-4">'.ucfirst($key).'</dt>
										<dd class="col-sm-8">'.htmlentities($value).'</dd>
									</dl>';
								}
								echo '<dl class="row">
										<dt class="col-sm-4">Subject</dt>
										<dd class="col-sm-8">'.$item['subject'].'</dd>
									</dl>';

								echo '<p>&nbsp;</p>';
								echo '<hr>';
								echo '<p>&nbsp;</p>';
								echo '<h4>Raw E-mail message:</h4>';
								echo '<p>&nbsp;</p>';

								echo $item['raw'];

							echo '</div>';
						}
						?>
						<!-- <div class="card-footer">
							<section class="reply-wrapper">
								<p class="p-0 m-0">
									<a href="javascript:void(0)" class="reply">Click here to Reply, Reply to all, or Forward.</a>
								</p>
								<span class="attachment">
									<i class="icon dripicons-paperclip"></i>
								</span>
							</section>
						</div> -->
					</div>
				</section>
			</aside>
			<!-- 	END RIGHT PAGE ASIDE -->
		</div>
		<!-- 	END FLEX PAGE WRAPPER -->
	</div>
</section>

<script type="text/javascript">
	$(".list-group-messages .list-group-item a").on('click', function(e){
		e.preventDefault();
		$navitem = $(this).closest('li');
		console.log($navitem.data('message'));
		$message = $(".card-body-message-"+$navitem.data('message'));
		$(".list-group-messages .list-group-item.active").removeClass('active');
		$navitem.addClass('active');
		$(".card-body-message.active").removeClass('active');
		$message.addClass('active');
	})

	$(".search-input").on('input', function(e){
		var search = $(this).val().toLowerCase();
		$(".card-body-message").each(function(index, el) {
			var text = $(el).text().toLowerCase();
			var message_id = $(el).data('message');
			if(text.indexOf(search) > -1){
				$(".list-group-messages .list-group-item[data-message="+message_id+"]").show();
			} else {
				$(".list-group-messages .list-group-item[data-message="+message_id+"]").hide();
			}
			
		});
	});
</script>
<style>
	.card-body-message { display:none; }
	.card-body-message.active { display:block; }
</style>