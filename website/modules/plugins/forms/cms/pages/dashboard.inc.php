
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">Forms</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="index.php?page=modules">Modules</a></li>
					<li class="breadcrumb-item active" aria-current="page">Forms</li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<li class="dropdown">
				<a href="index.php?module=forms&page=form-edit&id=new" class="btn btn-fab" >
					<i class="la la-plus"></i>
				</a>
			</li>
			<?php
			if($_PLUGINS[$_GET['module']]->settingGroups){ ?>
				<li class="dropdown">
					<a href="index.php?page=settings&type=plugin&plugin=forms" class="btn btn-fab" >
						<i class="la la-cog"></i>
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
</header>

<?php
if($_POST['action'] == 'delete'){
	DB::SITE()->query("DELETE FROM forms WHERE id=:id")->bind(':id', $_POST['id'])->execute();
	$msg = '<div class="alert alert-success alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">Form removed</div></div>';
}

if($msg){
	echo $msg;
}
?>

<section class="page-content">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Title</th>
								<th>Submissions</th>
								<th style="width:200px">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$forms = DB::SITE()->query("SELECT * FROM forms")->fetch();
							foreach ($forms as $key => $item) {
								$form = plugin('forms')->form($item['id']);

								echo '
								<tr>
									<td>'.$item['id'].'</td>
									<td>'.$item['title'].'</td>
									<td>'.$form->count().'</td>
									<td><form method="post">
										<input type="hidden" name="id" value="'.$item['id'].'">
										<input type="hidden" name="action" value="delete">
										<a href="index.php?module=forms&page=form-edit&id='.$item['id'].'" class="btn btn-sm btn-primary">Edit</a> 
										<a href="index.php?module=forms&page=data&form='.$item['id'].'" class="btn btn-sm btn-primary">Data</a> 
										<button type="submit" name="action" value="delete" class="btn btn-sm btn-danger btn-confirm">Remove</button>

									</form></td>
								</tr>';
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$('#bs4-table').DataTable();

	
	
</script>