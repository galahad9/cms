<?php

if($_POST['action'] == 'update'){
	try {
		if($_GET['id'] != 'new'){

			DB::SITE()->query("UPDATE forms SET title=:title, `to`=:to, `cc`=:cc, `bcc`=:bcc, `from`=:from, `reply`=:reply, content=:content, subject=:subject, headers=:headers, `success`=:success, `alert`=:alert,`confirmation_active`=:confirmation_active, `confirmation_subject`=:confirmation_subject, `confirmation_to`=:confirmation_to, `confirmation_headers`=:confirmation_headers, `confirmation_mail`=:confirmation_mail WHERE id=:id", [
				':title' => $_POST['title'],
				':subject' => $_POST['subject'],
				':headers' => $_POST['headers'],
				':from' => $_POST['from'],
				':reply' => $_POST['reply'],
				':to' => $_POST['to'],
				':cc' => $_POST['cc'],
				':bcc' => $_POST['bcc'],
				':content' => $_POST['content'],
				':success' => $_POST['success'],
				':alert' => $_POST['alert'],
				':id' => $_GET['id'],
				':confirmation_active' => intval($_POST['confirmation_active']),
				':confirmation_subject' => $_POST['confirmation_subject'],
				':confirmation_to' => $_POST['confirmation_to'],
				':confirmation_headers' => $_POST['confirmation_headers'],
				':confirmation_mail' => $_POST['confirmation_mail']
			])->execute();
			$msg = '<div class="alert alert-success alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">Form updated</div></div>';
		} else {
			DB::SITE()->query("INSERT INTO forms (title, subject, headers, `to`, `cc`, `bcc`, `from`, reply, content, success, alert, confirmation_active, confirmation_subject, confirmation_to, confirmation_headers, confirmation_mail) VALUES (:title, :subject, :headers, :to, :cc, :bcc, :from, :reply, :content, :success, :alert, :confirmation_active, :confirmation_subject, :confirmation_to, :confirmation_headers, :confirmation_mail)", [
				':title' => $_POST['title'],
				':subject' => $_POST['subject'],
				':headers' => $_POST['headers'],
				':from' => $_POST['from'],
				':reply' => $_POST['reply'],
				':to' => $_POST['to'],
				':cc' => $_POST['cc'],
				':bcc' => $_POST['bcc'],
				':content' => $_POST['content'],
				':success' => $_POST['success'],
				':alert' => $_POST['alert'],
				':confirmation_active' => intval($_POST['confirmation_active']),
				':confirmation_subject' => $_POST['confirmation_subject'],
				':confirmation_to' => $_POST['confirmation_to'],
				':confirmation_headers' => $_POST['confirmation_headers'],
				':confirmation_mail' => $_POST['confirmation_mail']
			])->execute();
			$id = DB::SITE()->lastInsertId();
			header("Location: index.php?module=forms");
		}
	} catch(PDOException $e){

		DB::SITE()->query("ALTER TABLE `forms` ADD COLUMN `confirmation_active` integer")->execute();
		DB::SITE()->query("ALTER TABLE `forms` ADD COLUMN `confirmation_subject` TEXT")->execute();
		DB::SITE()->query("ALTER TABLE `forms` ADD COLUMN `confirmation_to` TEXT")->execute();
		DB::SITE()->query("ALTER TABLE `forms` ADD COLUMN `confirmation_headers` TEXT")->execute();
		DB::SITE()->query("ALTER TABLE `forms` ADD COLUMN `confirmation_mail` TEXT")->execute();

		DB::SITE()->query("ALTER TABLE `forms` ADD COLUMN `cc` TEXT")->execute();
		DB::SITE()->query("ALTER TABLE `forms_data` ADD COLUMN `cc` TEXT")->execute();
		DB::SITE()->query("ALTER TABLE `forms` ADD COLUMN `bcc` TEXT")->execute();
		DB::SITE()->query("ALTER TABLE `forms_data` ADD COLUMN `bcc` TEXT")->execute();
		DB::SITE()->query("ALTER TABLE `forms` ADD COLUMN `success` TEXT")->execute();
		DB::SITE()->query("ALTER TABLE `forms` ADD COLUMN `alert` TEXT")->execute();

		DB::SITE()->query("ALTER TABLE `forms` ADD COLUMN `from` TEXT")->execute();
		DB::SITE()->query("ALTER TABLE `forms_data` ADD COLUMN `from` TEXT")->execute();
		DB::SITE()->query("ALTER TABLE `forms` ADD COLUMN `reply` TEXT")->execute();
		DB::SITE()->query("ALTER TABLE `forms_data` ADD COLUMN `reply` TEXT")->execute();


		

		$msg = '<div class="alert alert-danger alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">'.$e->getMessage().'</div></div>';
	}
}

if($_GET['id'] != 'new'){
	$form = DB::SITE()->query("SELECT * FROM forms WHERE id=:id")->bind(':id', $_GET['id'])->single();
}


?>
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">Form</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="index.php?page=modules">Modules</a></li>
					<li class="breadcrumb-item"><a href="index.php?module=forms&page=dashboard">Forms</a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= $_GET['id'] ?></li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<li class="dropdown">
				<a href="index.php?module=forms" class="btn btn-secondary btn-rounded btn-outline" >
					< Cancel
				</a>
			</li>
			<?php
			if($_GET['id'] != 'new'){ ?>
				<li class="dropdown">
					<a href="index.php?module=forms" class="btn btn-danger btn-rounded">
						DELETE
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
</header>

<?php

if($msg){
	echo $msg;
}
?>

<section class="page-content">
	<div class="row">
		<div class="col-12">
			<ul class="nav nav-tabs customtab2 m-b-0" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#form" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Form</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#alerts" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Alerts</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#confirmation" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Confirmation</span></a> </li>
            </ul>

			<div class="card" data-widget="dropzone">
				<!-- <h5 class="card-header">Tracking settings</h5> -->
				<form  method="post" action="" enctype="multipart/form-data">
					<input type="hidden" name="action" value="updateSettings">
					
					<div class="card-body">

						<div class="tab-content">
							<div class="tab-pane active" id="form" role="tabpanel">

					
								<div class="form-group row">
									<label for="title" class="col-sm-3 col-form-label">Title</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="title" name="title" value="<?= $form['title'] ?>">
									</div>
								</div>

								<div class="form-group row">
									<label for="to" class="col-sm-3 col-form-label">Send to
										<small id="textHelp1" class="form-text text-muted">Leave empty for no email</small>
									</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="to" name="to" value="<?= $form['to'] ?>">
									</div>
								</div>

								<div class="form-group row">
									<label for="cc" class="col-sm-3 col-form-label">CC</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="cc" name="cc" value="<?= $form['cc'] ?>">
									</div>
								</div>
								<div class="form-group row">
									<label for="bcc" class="col-sm-3 col-form-label">BCC</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="bcc" name="bcc" value="<?= $form['bcc'] ?>">
									</div>
								</div>

								<div class="form-group row">
									<label for="from" class="col-sm-3 col-form-label">Send from
										<small id="textHelp1" class="form-text text-muted">Default: <?= $settings['Emailaddress'] ?></small>
									</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="from" name="from" value="<?= $form['from'] ?>">
									</div>
								</div>

								<div class="form-group row">
									<label for="reply" class="col-sm-3 col-form-label">Reply to
										<small id="textHelp1" class="form-text text-muted">Leave empty for reply to sender</small>
									</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="reply" name="reply" value="<?= $form['reply'] ?>">
									</div>
								</div>
								
								<div class="form-group row">
									<label for="subject" class="col-sm-3 col-form-label">
										Mail Subject
										<small id="textHelp1" class="form-text text-muted">Placeholders allowed; example: {{ name }}</small>
									</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="subject" name="subject" value="<?= $form['subject'] ?>">
									</div>
								</div>

								<div class="form-group row">
									<label for="headers" class="col-sm-3 col-form-label">
										Additional Headers
										<small id="textHelp1" class="form-text text-muted">Placeholders allowed; example: {{ name }}</small>
									</label>
									<div class="col-sm-9">
										<textarea class="form-control" id="headers" name="headers" rows="5"><?= $form['headers'] ?></textarea>
									</div>
								</div>

								
								<div class="form-group row">
									<label for="content" class="col-sm-3 col-form-label">HTML</label>
									<div class="col-sm-9">
										<textarea name="content" id="content" class="form-control" rows="30"><?= htmlentities($form['content']) ?></textarea>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="alerts" role="tabpanel">
								<div class="form-group row">
									<label for="success" class="col-sm-3 col-form-label">
										Success Alert
										<small id="textHelp1" class="form-text text-muted">Default: Bericht verzonden</small>
									</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="success" name="success" value="<?= $form['success'] ?>">
									</div>
								</div>

								<div class="form-group row">
									<label for="alert" class="col-sm-3 col-form-label">
										Error Alert
										<small id="textHelp1" class="form-text text-muted">Default: Bericht is niet verzonden. Probeer het later nogmaals.</small>
									</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="alert" name="alert" value="<?= $form['alert'] ?>">
									</div>
								</div>
							</div>
							<div class="tab-pane" id="confirmation" role="tabpanel">
								<div class="form-group row">
									<label for="confirmation_active" class="col-sm-3 col-form-label">Active</label>
									<div class="col-sm-9">
										<input type="checkbox" <?= (($form['confirmation_active']) ? 'checked ' : '') ?>name="confirmation_active" id="confirmation_active" value="1" class="js-switch" data-color="#009efb" /> 
									</div>
								</div>
								<div class="form-group row">
									<label for="confirmation_subject" class="col-sm-3 col-form-label">Subject</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="confirmation_subject" name="confirmation_subject" value="<?= $form['confirmation_subject'] ?>">
									</div>
								</div>
								<div class="form-group row">
									<label for="confirmation_to" class="col-sm-3 col-form-label">Send to
										<small id="textHelp1" class="form-text text-muted">Leave empty for no email</small>
									</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="confirmation_to" name="confirmation_to" value="<?= $form['confirmation_to'] ?>">
									</div>
								</div>
								<div class="form-group row">
									<label for="confirmation_headers" class="col-sm-3 col-form-label">
										Additional Headers
										<small id="textHelp1" class="form-text text-muted">Placeholders allowed; example: {{ name }}</small>
									</label>
									<div class="col-sm-9">
										<textarea class="form-control" id="confirmation_headers" name="confirmation_headers" rows="5"><?= $form['confirmation_headers'] ?></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label for="confirmation_mail" class="col-sm-3 col-form-label">Mail</label>
									<div class="col-sm-9">
										<textarea name="confirmation_mail" id="confirmation_mail" class="form-control" rows="30"><?= htmlentities($form['confirmation_mail']) ?></textarea>
									</div>
								</div>
							</div>
						</div>

						

					</div>
					<div class="card-footer bg-light">
						<div class="form-group m-b-0 row">
							<div class="col-sm-10">
								<button type="submit" name="action" value="update" class="btn btn-primary">Save</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript">
	$(".dropzone-fake").click(function(e){
		e.preventDefault(); e.stopPropagation();
		var $id = $(this).data('for');
		console.log($($id));
		$($id).click();
	});

	
</script>
<style>
	.mce-tinymce iframe { height:600px !important }
</style>