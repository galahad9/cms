<?php

/**
 *  @author Sienn
 *  @version 1.0
 *  @name Forms
 *  @description Lorum Ipsum donor sit amet
 *  
 */
class Forms extends Plugin implements PluginInterface {
	
	// public $menu = [
	// 	[
	// 		'url' => 'index.php?module=forms&page=dashboard',
	// 		'name' => 'Forms'
	// 	]
	// ];
	
	public $modules = __DIR__.'/modules/*.php';

	public function activate(){
		parent::activate();

		DB::SITE()->query("CREATE TABLE IF NOT EXISTS `forms` (
		  `id` integer PRIMARY KEY,
		  `language` text DEFAULT '',
		  `title` text DEFAULT NULL,
		  `subject` text DEFAULT NULL,
		  `headers` text DEFAULT NULL,
		  `to` text DEFAULT NULL,
		  `content` text,
		  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		)")->execute();

		DB::SITE()->query("CREATE TABLE IF NOT EXISTS `forms_data` (
		  `id` integer PRIMARY KEY,
		  `form_id` integer,
		  `post` text,
		  `to` text,
		  `subject` text,
		  `headers` text,
		  `raw` text,
		  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		)")->execute();
	}

	public function deactivate(){
		parent::deactivate();
		DB::SITE()->query("DROP TABLE `forms`;")->execute();
		DB::SITE()->query("DROP TABLE `forms_data`;")->execute();
	}

	public function form($form_id){
		return new SiennForm($form_id);
	}
	
}

require('form.php');
registerNewPlugin(Forms::class);