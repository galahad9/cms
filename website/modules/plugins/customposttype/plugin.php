<?php
/**
 *  @author Sienn
 *  @version 1.0
 *  @name CustomPosttypes
 *  @description Lorum Ipsum donor sit amet
 *  
 */


class CustomPosttype extends Plugin implements PluginInterface {
	
	use Post\Posttype; use Post\Category;


	protected $posttype = 'posttype';
	protected $disableCategory = true;
	public $rewrites;

	protected $fields = [
		'title' => 'text',
	];

	public $settingGroups = [];

	public function __construct($title='posttype'){
		$this->posttype = $title;

		parent::__construct();
		if($this->status() == self::ACTIVE){
			$this->rewrites = $this->rewrites();
		}
	}

	public function setPosttype($name){
		$this->posttype = $name;
	}

	public function setFields(array $fields){
		$this->fields = $fields;
	}


	public function activate(){
		parent::activate();

	}

	public function deactivate(){
		parent::deactivate();
	}

	/**
	 * Creates url rewrites
	 * @return array 
	 */
	private function rewrites(){
		$slug = parent::setting($this->posttype.' Prefix Slug');
		if($slug){
			return [
				$slug => [$this, 'archive'],
				$slug.'\/(\d+)\/([A-Za-z0-9-_\.]+)' => [$this, 'detail']
			];
		}
	}

	/**
	 * Used as backup if posttype not in menu
	 * @return [type] [description]
	 */
	public function archive(){
		$data = [];
		ob_start();
		include(__DIR__.'/modules/builder.inc.php');
		$data['template'] = ob_get_contents();
		ob_end_clean();

		return $data;
	}

	/**
	 * Default page Detail
	 * @param  [type] $url [description]
	 * @return [type]      [description]
	 */
	public function detail($url){
		$url = explode('/', $url);
		$news = $this->find($url[2]);

		$template = parent::setting($this->posttype.' Template');
		return parent::template($template, $news);
	}


	public function data($data){
		$data['url'] = parent::setting($value['title'].' Prefix Slug').'/'.$data['id'].'/'.strtourl($data['title']);

		return $data;
	}

	public function register(){
		global $_PLUGINS;
		$items = $this->fetchAll();
		foreach ($items as $key => $value) {
			$plugin = new CustomPosttype($value['title']);
			$plugin->disableCategory = false;

			$plugin->settingGroups = [
				[
					'category' => 'General',
					'settings' => [
						'Categories' => [
							'name' => $value['title'].' Categories Enabled',
							'type' => 'boolean'
						],
						'Hide '.$value['title'].' detail' => [
							'name' => 'Hide '.$value['title'].' detail',
							'type' => 'boolean'
						],
						''.$value['title'].' Prefix Slug' => [
							'name' => ''.$value['title'].' Prefix Slug',
							'type' => 'text'
						]
					]
				],
				[
					'category' => 'Detail',
					'settings' => [
						"{$value['title']} Overview Template" => [
							'name' => ''.$value['title'].' Overview Template',
							'type' => 'builder',
							// 'snippets' => [__DIR__.'/cms/snippets/detail/*']
							
						],
						"{$value['title']} Template" => [
							'name' => ''.$value['title'].' Template',
							'type' => 'builder',
							// 'snippets' => [__DIR__.'/cms/snippets/detail/*']
							
						]
					]
				]
			];
			$_PLUGINS[strtourl($value['title'])] =  $plugin;
			if($_GET['plugin'] == $plugin->url()){
				$_PLUGINS['current'] = $plugin; 
			}

		}
	}
}


registerNewPlugin(CustomPosttype::class);