<?php
use Symfony\Component\DomCrawler\Crawler;

$prefix = (DEFAULT_LANGUAGE != LANGUAGE) ? ('/'.LANGUAGE) : '';

function setInnerHTML($element, $html)
{
    $fragment = $element->ownerDocument->createDocumentFragment();
    $fragment->appendXML($html);
    $clone = $element->cloneNode(); // Get element copy without children
    $clone->appendChild($fragment);
    $element->parentNode->replaceChild($clone, $element);
}


$items = plugin('news')->fetchAll();
$template = plugin('news')->setting('News Overview Template');


if(strpos($template, 'data-component') !== false){
	$crawler = new Crawler($template);

	// $crawler->filter('.position')->getNode(0)->
	$column = $crawler->filter('[data-component]')->html();
	
	$crawler->filter('[data-component]')->children()->each(function (Crawler $crawler) {
    	foreach ($crawler as $node) {
        	$node->parentNode->removeChild($node);
    	}
	});

	$uniqid = 'asd';
	$crawler->filter('[data-component]')->each(function ($node, $i) use ($uniqid) {
	    $node->getNode(0)->setAttribute('id', $uniqid);
	});

	// $crawler->filter('[data-component]')-
	$row_html = $crawler->html();
	
	// // $row = $crawler->filter('[data-component]');

	// $columns = "";
	// foreach ($items as $key => $item) {
	// 	if(method_exists(plugin('news'), 'data')){
	// 		$item = plugin('news')->data($item);
	// 	}
	// 	$columns .= Plugin::template($column, $item)['template'];
		
	// }


	// $domDocument = new \DOMDocument();
	// $domDocument->loadHTML($columns);


	// $columns = new Crawler($columns);
	// $columns->add('test');
	// $columns->filter('div')->each(function( Crawler $columnCrawler)){

	// });

	$crawler = new Crawler('<html><body /></html>');

	$domDocument = new \DOMDocument();
	$domDocument->loadHTML($row_html);

		
	$row = $domDocument->getElementById($uniqid);
	foreach ($items as $key => $item) {
		if(method_exists(plugin('news'), 'data')){
			$item = plugin('news')->data($item);
		}

		$frag = $domDocument->createDocumentFragment();
		

		$columns = Plugin::template($column, $item)['template'];

		$voidTags = array('area','base','br','col','command','embed','hr','img','input','keygen','link','meta','param','source','track','wbr');
		$regEx = '#<('.implode('|', $voidTags).')(\b[^>]*)>#';
		$columns = preg_replace($regEx, '<\\1\\2 />', $columns);

		// dd($columns);
		$frag->appendXML($columns);

		$row->appendChild($frag);
		
	}
	


	
	

	echo $domDocument->saveHTML();

	// $crawler->addDocument($domDocument);
	// dd($crawler);
	// $crawler->filter('[data-component]')->addDocument($domDocument);
	// $crawler->filter('[data-component]')->addNode($columns);
	// echo $crawler->html();
} else {

	foreach ($items as $key => $item) {
		if(method_exists(plugin('news'), 'data')){
			$item = plugin('news')->data($item);
		}
		$temp = Plugin::template($template, $item);
		echo $temp['template'];
	}
}
