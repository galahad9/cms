<?php

if(!empty($settings['Additional Languages'])) {

	$urls = getLanguageUrls();
	
	foreach($urls as $language => $url) {
		
		echo ('
			<li class="nav-item nav-item-language '.((LANGUAGE == $language) ? 'current' : '') .'">
				<a class="nav-link '.((LANGUAGE == $language) ? 'current' : '') .'" href="'.getProtocol().$_SERVER['HTTP_HOST'].$url.'">
					<img src="'.CMS_PUBLIC_URL.'translations/flags/'.strtoupper($language).'.png">
				</a>
			</li>
		');
		
	}

}
