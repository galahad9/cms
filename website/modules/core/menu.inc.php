<?php

$prefix = (DEFAULT_LANGUAGE != LANGUAGE) ? ('/'.LANGUAGE) : '';

$items = DB::SITE()->query("SELECT * FROM menu WHERE under=1 AND type='item' AND language_id=0 AND show=1 ORDER BY sequence ASC")->fetch();
foreach ($items as $key => $item) {

	$mainID = $item['id'];
	if ($item['language'] != LANGUAGE) {
		$item = DB::SITE()->query("SELECT * FROM menu WHERE language_id=:language_id AND language=:language AND show=1 ORDER BY sequence ASC LIMIT 1", [':language_id' => $mainID, ':language' => LANGUAGE])->single();
	}

	if (!$item['show']) {
		continue;
	}
	

	$subitems = DB::SITE()->query("SELECT * FROM menu WHERE under=:under AND type='item' AND language_id=0 AND show=1 ORDER BY sequence ASC", [':under' => $mainID])->fetch();

	if(count($subitems)){
		echo ' <li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="'.$prefix.$item['url'].'" id="dropdown'.$item['id'].'">'.$item['name'].'</a>
				<ul class="b-none dropdown-menu font-14 animated fadeInUp">';

					foreach ($subitems as $key => $item) {
						$mainID = $item['id'];
						if ($item['language'] != LANGUAGE) {
							$item = DB::SITE()->query("SELECT * FROM menu WHERE language_id=:language_id AND language=:language AND show=1 ORDER BY sequence ASC LIMIT 1", [':language_id' => $mainID, ':language' => LANGUAGE])->single();
						}

						if (!$item['show']) {
							continue;
						}
   					 	echo '<li><a class="dropdown-item" href="'.$prefix.$item['url'].'">'.$item['name'].'</a></li>';
					}
				echo '</ul>
    	</li>';
	} else {
		echo '<li class="nav-item">
			<a class="nav-link" href="'.$prefix.$item['url'].'">'.$item['name'].' </a>
    	</li>';
	}
	
}

