<?php
ob_start();

require(__DIR__ . '/../firstment.inc.php');
require(__DIR__ . '/cmsFunctions.inc.php');

if(class_exists('Whoops\Run')){
	if(defined('NO_WHOOPS') && NO_WHOOPS !== true || ! defined('NO_WHOOPS')){
		$whoops = new \Whoops\Run;
		$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
		$whoops->register();
	}
}

//session check for security (if not on loginpage)
if (parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH) != '/cms/login.php' && ! defined('NO_AUTH_REQUIRED')) {
	if (empty($_SESSION['user']['id'])) {
		if ($_COOKIE['cms-session']) {
			$session = json_decode($_COOKIE['cms-session'], true);
			$_SESSION['user'] = DB::CMS()->query("
				SELECT users.* FROM users_sessions AS sessions 
				LEFT JOIN users ON users.id=sessions.user_id 
				WHERE sessions.hash=:hash AND sessions.user_id=:user_id AND sessions.expiredate>:expiredate
			", [
				':user_id' => $session['id'],
				':hash' => $session['hash'],
				':expiredate' => date('Y-m-d H:i:s', time()),
			])->single();
		}
	}

	if (empty($_SESSION['user']['id'])) {
		redirect(CMS_PUBLIC_PATH . 'login.php');
	}
}


use Symfony\Component\Yaml\Yaml;

if (!empty($_GET['lang'])) {
	$availableLanguages = getCMSLanguages();
	if (in_array($_GET['lang'], $availableLanguages)) {
		$language = $_GET['lang'];
		setcookie('language', $_GET['lang'], time()+(3600*24*365));
	}
} elseif (!empty($_SESSION['user']['language'])) {
	$language = $_SESSION['user']['language'];
} elseif (!empty($_COOKIE['language'])) {
	$availableLanguages = getCMSLanguages();
	if (in_array($_COOKIE['language'], $availableLanguages)) {
		$language = $_COOKIE['language'];
	}
} 

if (empty($language)) {
	$language = DEFAULT_LANGUAGE;
}



if(class_exists('Symfony\Component\Yaml\Yaml')){
	$trans = Yaml::parseFile(__DIR__ . '/translations/' . $language . '.yaml');
}

?>