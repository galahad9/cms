<?php
require('../firstment.inc.php');


// if (isset($_POST['hidContent'])) {
// 	session_start(); 
// 	$_SESSION['content'] = $_POST['hidContent'];
// 	header("Location: example.php");
// }
if(! $_REQUEST['id'] && !$_REQUEST['name']){
	header('HTTP/1.1 404 Not Found');
	header('Location: '.CMS_PUBLIC_PATH);
}

if($_REQUEST['id']){
    $menu_id = $_GET['id'];
    $menu = DB::SITE()->query("SELECT * FROM menu WHERE id=:id")->bind(':id', $menu_id)->single();
    $snippets = '';
    

    if($menu['under']){
    	$pages = DB::SITE()->query("SELECT * FROM menu WHERE under=:id")->bind(':id', $menu['under'])->fetch();
    } else {
    	$pages = DB::SITE()->query("SELECT * FROM menu WHERE under=:id")->bind(':id', $menu_id)->fetch();
    }

} elseif($_REQUEST['name']){
    $menu = DB::SITE()->query("SELECT *, value as template FROM settings WHERE name=:name AND type='builder' LIMIT 1", [
       ':name' => $_REQUEST['name']
    ])->single();

    $pluginSetting = [];
    foreach ($_PLUGINS['current']->settingGroups as $key => $group) {
    	foreach ($group['settings'] as $key => $setting) {
            if($setting['name'] == $_REQUEST['name']){
                $pluginSetting = $setting;
            }
    	}
    }


    $snippets = '?snippets='.json_encode($pluginSetting['snippets']);
}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">
    <meta name="author" content="">
    <base href="">
    
    <title><?= $cmsSettings['Name'] ?></title>

    <link rel="icon" type="image/png" href="assets/img/<?= $cmsSettings['Image Favicon'] ?>" />
    
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

	<!-- elFinder CSS (REQUIRED) -->
	<link rel="stylesheet" type="text/css" href="<?= $builderSettings['assets_base'] ?>libs/elfinder/css/elfinder.min.css">
	<link rel="stylesheet" type="text/css" href="<?= $builderSettings['assets_base'] ?>libs/elfinder/css/theme.css">

    <link href="<?= $builderSettings['assets_base'] ?>css/editor.css" rel="stylesheet">
    <link href="<?= $builderSettings['assets_base'] ?>css/editor.custom.css" rel="stylesheet">
    <link href="<?= $builderSettings['assets_base'] ?>libs/fontawesome-pro/css/all.min.css" rel="stylesheet">
    <link href="<?= $builderSettings['assets_base'] ?>css/line-awesome.css" rel="stylesheet">
  </head>
<body>

	<div id="vvveb-builder">
				
		<div id="top-panel">
			
			<div class="btn-group mr-3" role="group">
			  <button class="btn btn-light" title="Undo (Ctrl/Cmd + Z)" id="undo-btn" data-vvveb-action="undo" data-vvveb-shortcut="ctrl+z">
				  <i class="la la-undo"></i>
			  </button>

			  <button class="btn btn-light"  title="Redo (Ctrl/Cmd + Shift + Z)" id="redo-btn" data-vvveb-action="redo" data-vvveb-shortcut="ctrl+shift+z">
				  <i class="la la-undo la-flip-horizontal"></i>
			  </button>
			</div>
								
			
			<div class="btn-group mr-3" role="group">
			  <button class="btn btn-light" title="Fullscreen (F11)" id="fullscreen-btn" data-toggle="button" aria-pressed="false" data-vvveb-action="fullscreen">
				  <i class="la la-arrows"></i>
			  </button>

			  <button class="btn btn-light" title="Preview" id="preview-btn" type="button" data-toggle="button" aria-pressed="false" data-vvveb-action="preview">
				  <i class="la la-eye"></i>
			  </button>

			  <button class="btn btn-light" title="Save (Ctrl + S)" id="save-btn" data-vvveb-action="save" data-vvveb-shortcut="ctrl+s">
				  <i class="la la-save"></i>
			  </button>
			  
			  <!-- <button class="btn btn-light" title="Download" id="save-btn" data-vvveb-action="download" download="index.html"> -->
				  <!-- <i class="la la-download"></i> -->
			  <!-- </button> -->

			</div>	


			<div class="btn-group float-right" role="group">
 			 <button id="mobile-view" data-view="mobile" class="btn btn-light"  title="Mobile view" data-vvveb-action="viewport">
				  <i class="ion-iphone"></i>
			  </button>

			  <button id="tablet-view"  data-view="tablet" class="btn btn-light"  title="Tablet view" data-vvveb-action="viewport">
				  <i class="ion-ipad"></i>
			  </button>
			  
			  <button id="desktop-view"  data-view="" class="btn btn-light"  title="Desktop view" data-vvveb-action="viewport">
				  <i class="ion-monitor"></i>
			  </button>

			</div>
								
		</div>	
				
		<div id="left-panel">

			<div id="filemanager"> 
				<div class="header"><a href="#">Pages</a></div>
				<div class="tree">
					<ol>
						
					</ol>
				</div>
			</div>
			  	
			<div id="components">
				<div class="header">
				  	<input class="form-control form-control-sm" placeholder="" type="text" id="component-search"  data-vvveb-action="componentSearch" data-vvveb-on="keyup">
				  	<button id="clear-backspace"  data-vvveb-action="clearComponentSearch">
						<i class="ion-backspace"></i>
					</button>
				  
				</div>

				<div id="components-sidepane" class="sidepane">
			  		<div>
			  			<ul id="components-list" class="clearfix"></ul>
			  		</div>
				</div>
			</div>

		</div>

		<div id="canvas">
			<div id="iframe-wrapper">
				<div id="iframe-layer">
					
					<div id="highlight-box">
						<div id="highlight-name"></div>
						
					</div>

					<div id="select-box">

						<div id="wysiwyg-editor">
							<a id="bold-btn" href="" title="Bold"><i><strong>B</strong></i></a>
							<a id="italic-btn" href="" title="Italic"><i>I</i></a>
							<a id="underline-btn" href="" title="Underline"><u>u</u></a>
							<a id="strike-btn" href="" title="Strikeout"><strike>S</strike></a>
							<a id="link-btn" href="" title="Create link"><strong>a</strong></a>
							<a id="placeholder-btn" href="" title="Import placeholder"><strong>{}</strong></a>
						</div>

						<div id="select-actions">
							<a id="drag-box" href="" title="Drag element"><i class="ion-arrow-move"></i></a>
							<a id="parent-box" href="" title="Select parent"><i class="ion-reply"></i></a>
							
							<a id="up-box" href="" title="Move element up"><i class="ion-arrow-up-a"></i></a>
							<a id="down-box" href="" title="Move element down"><i class="ion-arrow-down-a"></i></a>
							<a id="clone-box" href="" title="Clone element"><i class="ion-ios-copy"></i></a>
							<a id="delete-box" href="" title="Remove element"><i class="ion-trash-a"></i></a>
							<a id="seprator" title="Seperator">|</a>
							<a style="display:none" id="delete-slide" href="" title="Remove slide"><i class="ion-trash-a"></i></a>
							<a style="display:none" id="clone-slide" href="" title="Clone slide"><i class="ion-ios-copy"></i></a>
						</div>
					</div>
					
				
				</div>
				<iframe src="about:none" id="iframe1"></iframe>
			</div>
			
			
		</div>

		<div id="right-panel">
			<div id="component-properties">
			</div>
		</div>
		
		<div id="bottom-panel">

		<div class="btn-group" role="group">

 			 <button id="code-editor-btn btn-sm" data-view="mobile" class="btn btn-light btn-sm"  title="Code editor" data-vvveb-action="toggleEditor">
				  <i class="ion-code"></i> Code editor
			  </button>
			 
				<div id="toggleEditorJsExecute" class="custom-control custom-checkbox mt-1" style="display:none">
					<input type="checkbox" class="custom-control-input" id="customCheck" name="example1" data-vvveb-action="toggleEditorJsExecute">
					<label class="custom-control-label" for="customCheck"><small>Run code on edit</small></label>
				</div>
			</div>
			
			<div id="vvveb-code-editor">
				<textarea class="form-control"></textarea>
			<div>

		</div>	
	</div>
</div>


<div class="modal fade" id="elfinderModal" tabindex="-1" role="dialog" aria-labelledby="elfindelModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title" id="elfindelModalLabel">File browser</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
      		<div class="modal-body">
        		<div id="elfinder"></div>
      		</div>
    	</div>
  	</div>
</div>

<!-- templates -->

<script id="vvveb-input-textinput" type="text/html">
	
	<div>
		<input name="{%=key%}" type="text" class="form-control"/>
	</div>
	
</script>

<script id="vvveb-input-checkboxinput" type="text/html">
	
	<div class="custom-control custom-checkbox">
		  <input name="{%=key%}" class="custom-control-input" type="checkbox" id="{%=key%}_check">
		  <label class="custom-control-label" for="{%=key%}_check">{% if (typeof text !== 'undefined') { %} {%=text%} {% } %}</label>
	</div>
	
</script>

<script id="vvveb-input-radioinput" type="text/html">
	
	<div>
	
		{% for ( var i = 0; i < options.length; i++ ) { %}

		<label class="custom-control custom-radio  {% if (typeof inline !== 'undefined' && inline == true) { %}custom-control-inline{% } %}"  title="{%=options[i].title%}">
		  <input name="{%=key%}" class="custom-control-input" type="radio" value="{%=options[i].value%}" id="{%=key%}{%=i%}" {%if (options[i].checked) { %}checked="{%=options[i].checked%}"{% } %}>
		  <label class="custom-control-label" for="{%=key%}{%=i%}">{%=options[i].text%}</label>
		</label>

		{% } %}

	</div>
	
</script>

<script id="vvveb-input-radiobuttoninput" type="text/html">
	
	<div class="btn-group btn-group-toggle  {%if (extraclass) { %}{%=extraclass%}{% } %} clearfix" data-toggle="buttons">
	
		{% for ( var i = 0; i < options.length; i++ ) { %}

		<label class="btn btn-light  {%if (options[i].checked) { %}active{% } %}" for="{%=key%}{%=i%} " title="{%=options[i].title%}">
		  <input name="{%=key%}" class="custom-control-input" type="radio" value="{%=options[i].value%}" id="{%=key%}{%=i%}" {%if (options[i].checked) { %}checked="{%=options[i].checked%}"{% } %}>
		  {%if (options[i].icon) { %}<i class="{%=options[i].icon%}"></i>{% } %}
		  {%=options[i].text%}
		</label>

		{% } %}
				
	</div>
	
</script>


<script id="vvveb-input-toggle" type="text/html">
	
    <div class="toggle">
        <input type="checkbox" name="{%=key%}" value="{%=on%}" data-value-off="{%=off%}" data-value-on="{%=on%}" class="toggle-checkbox" id="{%=key%}">
        <label class="toggle-label" for="{%=key%}">
            <span class="toggle-inner"></span>
            <span class="toggle-switch"></span>
        </label>
    </div>
	
</script>

<script id="vvveb-input-header" type="text/html">

	<h6 class="header">{%=header%}</h6>
	
</script>

	
<script id="vvveb-input-select" type="text/html">

	<div>

		<select class="form-control custom-select">
			{% for ( var i = 0; i < options.length; i++ ) { %}
			<option value="{%=options[i].value%}">{%=options[i].text%}</option>
			{% } %}
		</select>
	
	</div>
	
</script>


<script id="vvveb-input-listinput" type="text/html">

	<div class="row">

		{% for ( var i = 0; i < options.length; i++ ) { %}
		<div class="col-6">
			<div class="input-group">
				<input name="{%=key%}_{%=i%}" type="text" class="form-control" value="{%=options[i].text%}"/>
				<div class="input-group-append">
					<button class="input-group-text btn btn-sm btn-danger">
						<i class="ion-trash-a"></i>
					</button>
				</div>
			  </div>
			  <br/>
		</div>
		{% } %}


		{% if (typeof hide_remove === 'undefined') { %}
		<div class="col-12">
		
			<button class="btn btn-sm btn-outline-primary">
				<i class="ion-trash-a"></i> Add new
			</button>
			
		</div>
		{% } %}
			
	</div>
	
</script>

<script id="vvveb-input-grid" type="text/html">

	<div class="row">
		<div class="mb-1 col-12">
		
			<label>Flexbox</label>
			<select class="form-control custom-select" name="col">
				
				<option value="">None</option>
				{% for ( var i = 1; i <= 12; i++ ) { %}
				<option value="{%=i%}" {% if ((typeof col !== 'undefined') && col == i) { %} selected {% } %}>{%=i%}</option>
				{% } %}
				
			</select>
			<br/>
		</div>

		<div class="col-6">
			<label>Extra small</label>
			<select class="form-control custom-select" name="col-xs">
				
				<option value="">None</option>
				{% for ( var i = 1; i <= 12; i++ ) { %}
				<option value="{%=i%}" {% if ((typeof col_xs !== 'undefined') && col_xs == i) { %} selected {% } %}>{%=i%}</option>
				{% } %}
				
			</select>
			<br/>
		</div>
		
		<div class="col-6">
			<label>Small</label>
			<select class="form-control custom-select" name="col-sm">
				
				<option value="">None</option>
				{% for ( var i = 1; i <= 12; i++ ) { %}
				<option value="{%=i%}" {% if ((typeof col_sm !== 'undefined') && col_sm == i) { %} selected {% } %}>{%=i%}</option>
				{% } %}
				
			</select>
			<br/>
		</div>
		
		<div class="col-6">
			<label>Medium</label>
			<select class="form-control custom-select" name="col-md">
				
				<option value="">None</option>
				{% for ( var i = 1; i <= 12; i++ ) { %}
				<option value="{%=i%}" {% if ((typeof col_md !== 'undefined') && col_md == i) { %} selected {% } %}>{%=i%}</option>
				{% } %}
				
			</select>
			<br/>
		</div>
		
		<div class="col-6 mb-1">
			<label>Large</label>
			<select class="form-control custom-select" name="col-lg">
				
				<option value="">None</option>
				{% for ( var i = 1; i <= 12; i++ ) { %}
				<option value="{%=i%}" {% if ((typeof col_lg !== 'undefined') && col_lg == i) { %} selected {% } %}>{%=i%}</option>
				{% } %}
				
			</select>
			<br/>
		</div>
		
		{% if (typeof hide_remove === 'undefined') { %}
		<div class="col-12">
		
			<button class="btn btn-sm btn-outline-light text-danger">
				<i class="ion-trash-a"></i> Remove
			</button>
			
		</div>
		{% } %}
		
	</div>
	
</script>

<script id="vvveb-input-textvalue" type="text/html">
	
	<div class="row">
		<div class="col-6 mb-1">
			<label>Value</label>
			<input name="value" type="text" value="{%=value%}" class="form-control"/>
		</div>

		<div class="col-6 mb-1">
			<label>Text</label>
			<input name="text" type="text" value="{%=text%}" class="form-control"/>
		</div>

		{% if (typeof hide_remove === 'undefined') { %}
		<div class="col-12">
		
			<button class="btn btn-sm btn-outline-light text-danger">
				<i class="ion-trash-a"></i> Remove
			</button>
			
		</div>
		{% } %}

	</div>
	
</script>

<script id="vvveb-input-rangeinput" type="text/html">
	
	<div>
		<input name="{%=key%}" type="range" min="{%=min%}" max="{%=max%}" step="{%=step%}" class="form-control"/>
	</div>
	
</script>

<script id="vvveb-input-imageinput" type="text/html">
	
	<div>
		<input name="{%=key%}" type="text" class="form-control"/>
		<input name="file" type="file" class="form-control"/>
	</div>
	
</script>

<script id="vvveb-input-iconpicker" type="text/html">

	<div>
		<div class="btn-group">
	        <button type="button" class="btn btn-primary iconpicker-component"><i
	                class="fa fa-fw fa-heart"></i></button>

	        <button type="button" name="{%=key%}" class="icp icp-dd btn btn-primary dropdown-toggle"
	                data-selected="fa-car" data-toggle="dropdown">
	            <span class="caret"></span>
	            <span class="sr-only">Toggle Dropdown</span>
	        </button>
	        <div class="dropdown-menu"></div>
	    </div>
	</div>
	
</script>

<script id="vvveb-input-colorinput" type="text/html">
	
	<div>
		<input name="{%=key%}" type="color" value="{%=value%}" pattern="#[a-f0-9]{6}" class="form-control"/>
	</div>
	
</script>

<script id="vvveb-input-elfinder" type="text/html">
	
	<div>
		<input name="{%=key%}" type="text" class="form-control"/>
		<button type="button" class="btn btn-primary">Choose file</button>
	</div>
	
</script>

<script id="vvveb-input-numberinput" type="text/html">
	<div>
		<input name="{%=key%}" type="number" value="{%=value%}" 
			  {% if (typeof min !== 'undefined' && min != false) { %}min="{%=min%}"{% } %} 
			  {% if (typeof max !== 'undefined' && max != false) { %}max="{%=max%}"{% } %} 
			  {% if (typeof step !== 'undefined' && step != false) { %}step="{%=step%}"{% } %} 
		class="form-control"/>
	</div>
</script>

<script id="vvveb-input-button" type="text/html">
	<div>
		<button class="btn btn-sm btn-primary">
			<i class="ion-trash-a"></i> {%=text%}
		</button>
	</div>		
</script>

<script id="vvveb-input-cssunitinput" type="text/html">
	<div class="input-group" id="cssunit-{%=key%}">
		<input name="number" type="number" value="{%=value%}" 
			  {% if (typeof min !== 'undefined' && min != false) { %}min="{%=min%}"{% } %} 
			  {% if (typeof max !== 'undefined' && max != false) { %}max="{%=max%}"{% } %} 
			  {% if (typeof step !== 'undefined' && step != false) { %}step="{%=step%}"{% } %} 
		class="form-control"/>
		 <div class="input-group-append">
		<select class="form-control custom-select small-arrow" name="unit">
			<option>em&ensp;</option>
			<option>px</option>
			<option>%</option>
			<option>rem</option>
			<option>auto</option>
		</select>
		</div>
	</div>
	
</script>


<script id="vvveb-filemanager-page" type="text/html">
	<li data-url="{%=url%}" data-page="{%=name%}">
		<label for="{%=name%}"><span>{%=title%}</span></label> <input type="checkbox" checked id="{%=name%}" />
		<ol></ol>
	</li>
</script>

<script id="vvveb-filemanager-component" type="text/html">
	<li data-url="{%=url%}" data-component="{%=name%}" class="file">
		<a href="{%=url%}"><span>{%=title%}</span></a>
	</li>
</script>

<script id="vvveb-input-sectioninput" type="text/html">

		<label class="header" data-header="{%=key%}" for="header_{%=key%}"><span>&ensp;{%=header%}</span> <div class="header-arrow"></div></label> 
		<input class="header_check" type="checkbox" {% if (typeof expanded !== 'undefined' && expanded == false) { %} {% } else { %}checked="true"{% } %} id="header_{%=key%}"> 
		<div class="section" data-section="{%=key%}"></div>		
	
</script>


<script id="vvveb-property" type="text/html">

	<div class="form-group {% if (typeof col !== 'undefined' && col != false) { %} col-sm-{%=col%} d-inline-block {% } else { %}row{% } %}" data-key="{%=key%}" {% if (typeof group !== 'undefined' && group != null) { %}data-group="{%=group%}" {% } %}>
		
		{% if (typeof name !== 'undefined' && name != false) { %}<label class="{% if (typeof inline === 'undefined' ) { %}col-sm-4{% } %} control-label" for="input-model">{%=name%}</label>{% } %}
		
		<div class="{% if (typeof inline === 'undefined') { %}col-sm-{% if (typeof name !== 'undefined' && name != false) { %}8{% } else { %}12{% } } %} input"></div>
		
	</div>		 
	
</script>

<!--// end templates -->


<!-- export html modal-->
<div class="modal fade" id="textarea-modal" tabindex="-1" role="dialog" aria-labelledby="textarea-modal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Export html</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <textarea rows="25" cols="150" class="form-control"></textarea>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
	
<!-- jquery-->
<script src="<?= $builderSettings['assets_base'] ?>js/jquery.min.js"></script>
<script src="<?= $builderSettings['assets_base'] ?>js/jquery.hotkeys.js"></script>
<script src="<?= $builderSettings['assets_base'] ?>js/jquery-ui.min.js"></script>


<!-- bootstrap-->
<script src="<?= $builderSettings['assets_base'] ?>js/popper.min.js"></script>
<script src="<?= $builderSettings['assets_base'] ?>js/bootstrap.min.js"></script>

<script src="<?= $builderSettings['assets_base'] ?>wrapkit/wrapkit.js"></script>	
<script src="<?= $builderSettings['assets_base'] ?>wrapkit/node_modules/aos/dist/aos.js" type="text/javascript"></script>
<link href="<?= $builderSettings['assets_base'] ?>wrapkit/node_modules/aos/dist/aos.css" rel="stylesheet">

<script src="<?= $builderSettings['assets_base'] ?>wrapkit/node_modules/owl-carousel-2/owl.carousel.min.js" type="text/javascript"></script>
<link href="<?= $builderSettings['assets_base'] ?>wrapkit/node_modules/owl-carousel-2/assets/owl-carousel.min.css.css" rel="stylesheet">

<script src="<?= $builderSettings['assets_base'] ?>wrapkit/node_modules/bootstrap-touch-slider/bootstrap-touch-slider-min.js" type="text/javascript"></script>
<link href="<?= $builderSettings['assets_base'] ?>wrapkit/node_modules/bootstrap-touch-slider/bootstrap-touch-slider.css" rel="stylesheet">

<!--  Icon Picker -->
<link href="<?= $builderSettings['assets_base'] ?>libs/iconpicker/css/fontawesome-iconpicker.css" rel="stylesheet"/>
<script src="<?= $builderSettings['assets_base'] ?>libs/iconpicker/js/fontawesome-iconpicker.js"></script>

<!-- builder code-->
<script src="<?= $builderSettings['assets_base'] ?>libs/builder/builder.js"></script>	
<!-- undo manager-->
<script src="<?= $builderSettings['assets_base'] ?>libs/builder/undo.js"></script>	
<!-- inputs-->
<script src="<?= $builderSettings['assets_base'] ?>libs/builder/inputs.js"></script>	
<!-- components-->
<script src="<?= $builderSettings['assets_base'] ?>libs/builder/components-bootstrap4.js"></script>	
<script src="<?= $builderSettings['assets_base'] ?>libs/builder/components-wrapkit.php?id=<?= $menu['id'] ?>"></script>	
<script src="<?= $builderSettings['assets_base'] ?>libs/builder/components-widgets.js"></script>	


<!-- plugins -->

<!-- code mirror - code editor syntax highlight -->
<link href="<?= $builderSettings['assets_base'] ?>libs/codemirror/lib/codemirror.css" rel="stylesheet"/>
<link href="<?= $builderSettings['assets_base'] ?>libs/codemirror/theme/material.css" rel="stylesheet"/>
<script src="<?= $builderSettings['assets_base'] ?>libs/codemirror/lib/codemirror.js"></script>
<script src="<?= $builderSettings['assets_base'] ?>libs/codemirror/lib/xml.js"></script>
<script src="<?= $builderSettings['assets_base'] ?>libs/codemirror/lib/formatting.js"></script>
<script src="<?= $builderSettings['assets_base'] ?>libs/builder/plugin-codemirror.js"></script>	

<!-- elfinder -->
<script src="<?= $builderSettings['assets_base'] ?>libs/elfinder/js/elfinder.min.js"></script>	

<script>
$(document).ready(function() {

	Vvveb.Builder.init('ajax/builder/getTemplate.ajax.php?<?= $_SERVER['QUERY_STRING'] ?>', function() {
		//run code after page/iframe is loaded
	});

	Vvveb.Gui.save = function(){
		$.post(
		'<?= CMS_PUBLIC_PATH ?>ajax/builder/setTemplate.ajax.php?<?= $_SERVER['QUERY_STRING'] ?>', 
		{
			id: '<?= $menu['id'] ?>', 
			html: Vvveb.Builder.getHtml()
		}, function(data, textStatus, xhr) {
			alert('saved');
		});
	}


	Vvveb.Gui.init();
	Vvveb.FileManager.init();
	Vvveb.FileManager.addPages(
	[
		<?php if(is_array($pages) && count($pages) > 0){ foreach ($pages as $key => $page) { 
			?>{name:"<?= $page['name'] ?>", title:"<?= $page['name'] ?>",  url: "ajax/builder/getTemplate.ajax.php?id=<?= $page['id'] ?>"},<?php
		} }?>
		
		
	]);


	$('#elfinder').elfinder(
		// 1st Arg - options
		{
			cssAutoLoad : false,               // Disable CSS auto loading
			baseUrl : './',                    // Base URL to css/*, js/*
			url : '/cms/assets/libs/elfinder/php/connector.minimal.php',  // connector URL (REQUIRED)
			// , lang: 'ru'                    // language (OPTIONAL)
			uiOptions: {
                toolbar : [
                    // toolbar configuration
                    ['open'],
                    ['back', 'forward'],
                    ['reload'],
                    ['home', 'up'],
                    ['mkdir', 'upload'],
                    ['info'],
                    ['quicklook'],
                    ['rm']
                ]
            },
            contextmenu : {
                files  : [
                    'getfile', '|','open', '|',
                    'rm', '|', 'edit', 'rename', '|', 'info'
                ]
            },
			getFileCallback : function(files, fm) {
				$("#elfinderModal").data('elfinder', files.url);
				$("#elfinderModal").modal('hide');
			    return false;
			},
		}
	);
});
</script>
</body>
</html>




