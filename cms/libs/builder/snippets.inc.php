<?php
$assets_url = $builderSettings['assets_base'];

foreach (getSnippetCategories() as $key => $category) {
	if($category['file']){
		$cat = $category['id'];
		include($category['file']);
	}
}