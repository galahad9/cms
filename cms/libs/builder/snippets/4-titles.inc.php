<?php
$slug = 'titles'; 
?>


<div data-num="1" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/a01.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-96 is-title1-96 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="2" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/a02.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-64 is-title1-64 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="3" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/a03.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-48 is-title1-48 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="4" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/a04.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-48 is-title1-48 is-title-lite is-upper">Lorem Ipsum is simply dummy text</h1>
        </div>
	</div>
</div>

<div data-num="5" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/a05.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-32 is-title1-32 is-title-lite is-upper">Lorem Ipsum is simply dummy text of the printing and typesetting industry</h1>
        </div>
	</div>
</div>

<div data-num="6" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/a06.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-96 is-title1-96 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="7" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/a07.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-64 is-title1-64 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="8" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/a08.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			 <h1 class="size-48 is-title1-48 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="9" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/a09.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			 <h1 class="size-48 is-title1-48 is-title-bold is-upper">Lorem Ipsum is simply dummy text of the printing industry</h1>
        </div>
	</div>
</div>

<div data-num="10" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/a10.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-32 is-title1-32 is-title-bold is-upper">Lorem Ipsum is simply dummy text of the printing and typesetting industry</h1>
        </div>
	</div>
</div>

<div data-num="<?= $cat ?>0001" data-thumb="<?= $assets_url ?>thumbnails/<?= $slug ?>-0001.png" data-cat="<?= $cat ?>">
	<div class="row justify-content-center">
        <div class="col-md-7 text-center">
            <h2 class="title">Beautifull title</h2>
            <h6 class="subtitle">Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</h6>
        </div>
    </div>
</div>


<div data-num="13" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/e01.png" data-cat="<?= $cat ?>">

	<div class="row">

        <div class="col-md-12">

            <h1>Heading 1 Text Goes Here</h1>

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        </div>

    </div>

</div>



<div data-num="14" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/e02.png" data-cat="<?= $cat ?>">

	<div class="row">

        <div class="col-md-12">

            <h2>Heading 2 Text Goes Here</h2>

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        </div>

    </div>

</div>


<div data-num="15" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/e09.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-4">

			<h1>Lorem Ipsum</h1>

			<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

		</div>

		<div class="col-md-8">

			<img src="<?= $assets_url ?>assets/minimalist-basic/e09-1.jpg">

		</div>

	</div>

</div>



<div data-num="213" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c02.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<p class="size-24 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-96 is-title1-96 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="214" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c03.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-64 is-title1-64 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="215" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c04.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			 <p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
             <h1 class="size-48 is-title1-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="216" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c05.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			 <p class="is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
             <h1 class="size-32 is-title1-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="217" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c06.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-96" style="line-height:1"></i>
            <p class="size-24 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title2-96 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="218" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c07.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-64" style="line-height:1"></i>
            <p class="size-21 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title2-64 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="219" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c08.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-64" style="line-height:1"></i>
            <p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title2-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="220" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c09.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-48" style="line-height:1"></i>
            <p class="is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title2-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="221" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c10.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title3-96 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="222" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c11.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title3-64 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="223" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c12.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title3-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="224" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c13.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title3-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="225" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c14.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div>
            	<h1 class="size-96 is-title4-96 is-title-lite" style="display:inline-block">Lorem Ipsum</h1>
			</div>
        </div>
	</div>
</div>

<div data-num="226" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c15.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div>
            	<h1 class="size-64 is-title4-64 is-title-lite" style="display:inline-block">Lorem Ipsum</h1>
        	</div>
        </div>
	</div>
</div>

<div data-num="227" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c16.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <div>
            	<h1 class="size-48 is-title4-48 is-title-lite" style="display:inline-block">LOREM IPSUM IS DUMMY TEXT</h1>
			</div>
        </div>
	</div>
</div>

<div data-num="228" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c17.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <div>
           		<h1 class="size-32 is-title4-32 is-title-lite" style="display:inline-block">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        	</div>
        </div>
	</div>
</div>

<!-- -->

<div data-num="229" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c18.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title5-96 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="230" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c19.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title5-64 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="231" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c20.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title5-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="232" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c21.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title5-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="233" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c22.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<p class="size-24 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-96 is-title1-96 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="234" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c23.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-64 is-title1-64 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="235" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c24.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			 <p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
             <h1 class="size-48 is-title1-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="236" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c25.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			 <p class="is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
             <h1 class="size-32 is-title1-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="237" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c26.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-96" style="line-height:1"></i>
            <p class="size-24 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title2-96 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="238" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c27.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-64" style="line-height:1"></i>
            <p class="size-21 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title2-64 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="239" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c28.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-64" style="line-height:1"></i>
            <p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title2-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="240" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c29.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-48" style="line-height:1"></i>
            <p class="is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title2-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="241" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c30.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title3-96 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="242" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c31.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title3-64 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="243" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c32.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title3-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="244" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c33.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title3-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="245" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c34.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div>
            	<h1 class="size-96 is-title4-96 is-title-bold" style="display:inline-block">Lorem Ipsum</h1>
        	</div>
        </div>
	</div>
</div>

<div data-num="246" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c35.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div>
            	<h1 class="size-64 is-title4-64 is-title-bold" style="display:inline-block">Lorem Ipsum</h1>
        	</div>
        </div>
	</div>
</div>

<div data-num="247" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c36.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <div>
            	<h1 class="size-48 is-title4-48 is-title-bold" style="display:inline-block">LOREM IPSUM IS DUMMY TEXT</h1>
			</div>
        </div>
	</div>
</div>

<div data-num="248" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c37.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <div>
            	<h1 class="size-32 is-title4-32 is-title-bold" style="display:inline-block">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        	</div>
        </div>
	</div>
</div>

<!-- -->

<div data-num="249" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c38.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title5-96 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="250" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c39.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title5-64 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="251" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c40.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title5-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="252" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c41.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title5-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>


<div data-num="12" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/g01.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12">

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>

        </div>

	</div>

</div>

<div data-num="21" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/g02.png" data-cat="<?= $cat ?>">

	<div class="row">

        	<div class="col-md-6">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/g02-1.jpg">

        	</div>
			<div class="col-md-6">

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        	</div>

	</div>

</div>



<div data-num="22" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/g03.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-6">

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

       		 </div>

        	<div class="col-md-6">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/g03-1.jpg">

       		</div>

	</div>

</div>



<div data-num="23" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/h01.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-6">

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        	</div>

		<div class="col-md-6">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h01-1.jpg" style="border-radius:500px;margin-right: 1.5em">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h01-2.jpg" style="border-radius:500px;margin-right: 1.5em">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h01-3.jpg" style="border-radius:500px">

		</div>

	</div>

</div>



<div data-num="24" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/h02.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-6">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h02-1.jpg" style="border-radius:500px;margin-right: 1.5em">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h02-2.jpg" style="border-radius:500px;margin-right: 1.5em">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h02-3.jpg" style="border-radius:500px">

		</div>	

		<div class="col-md-6">

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        	</div>

	</div>

</div>


<div data-num="173" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b15.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-96 is-title1-96 is-title-lite">Lorem Ipsum</h1>
            <p class="size-24">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="174" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b16.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-64 is-title1-64 is-title-lite">Lorem Ipsum</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="175" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b17.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-48 is-title1-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="176" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b18.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-32 is-title1-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<!-- -->

<div data-num="177" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b19.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-96" style="line-height:1"></i>
            <h1 class="size-96 is-title2-96 is-title-lite">Lorem Ipsum</h1>
            <p class="size-24">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="178" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b20.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-64" style="line-height:1"></i>
            <h1 class="size-64 is-title2-64 is-title-lite">Lorem Ipsum</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="179" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b21.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-64" style="line-height:1"></i>
            <h1 class="size-48 is-title2-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="180" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b22.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-48" style="line-height:1"></i>
            <h1 class="size-32 is-title2-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<!-- -->

<div data-num="181" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b23.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-96 is-title3-96 is-title-lite">Lorem Ipsum</h1>
            <p class="size-24">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="182" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b24.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-64 is-title3-64 is-title-lite">Lorem Ipsum</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="183" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b25.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-48 is-title3-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="184" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b26.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-32 is-title3-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<!-- -->

<div data-num="185" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b27.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <div>
            	<h1 class="size-96 is-title4-96 is-title-lite" style="display:inline-block">Lorem Ipsum</h1>
			</div>
            <p class="size-24">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="186" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b28.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <div>
            	<h1 class="size-64 is-title4-64 is-title-lite" style="display:inline-block">Lorem Ipsum</h1>
			</div>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="187" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b29.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <div>
            	<h1 class="size-48 is-title4-48 is-title-lite" style="display:inline-block">LOREM IPSUM IS DUMMY TEXT</h1>
			</div>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="188" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b30.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <div>
            	<h1 class="size-32 is-title4-32 is-title-lite" style="display:inline-block">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
			</div>
            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<!-- -->

<div data-num="189" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b31.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-96 is-title5-96 is-title-lite">Lorem Ipsum</h1>
            <p class="size-24">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="190" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b32.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-64 is-title5-64 is-title-lite">Lorem Ipsum</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="191" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b33.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-48 is-title5-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="192" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b34.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-32 is-title5-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<!-- -->

<div data-num="193" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b35.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-96 is-title1-96 is-title-bold">Lorem Ipsum</h1>
            <p class="size-24">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="194" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b36.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-64 is-title1-64 is-title-bold">Lorem Ipsum</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="195" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b37.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			 <h1 class="size-48 is-title1-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
             <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="196" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b38.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<h1 class="size-32 is-title1-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<!-- -->

<div data-num="197" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b39.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-96" style="line-height:1"></i>
            <h1 class="size-96 is-title2-96 is-title-bold">Lorem Ipsum</h1>
            <p class="size-24">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="198" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b40.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-96" style="line-height:1"></i>
            <h1 class="size-64 is-title2-64 is-title-bold">Lorem Ipsum</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="199" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b41.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-64" style="line-height:1"></i>
            <h1 class="size-48 is-title2-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="200" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b42.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-48" style="line-height:1"></i>
            <h1 class="size-32 is-title2-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<!-- -->

<div data-num="201" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b43.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-96 is-title3-96 is-title-bold">Lorem Ipsum</h1>
            <p class="size-24">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="202" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b44.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-64 is-title3-64 is-title-bold">Lorem Ipsum</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="203" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b45.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-48 is-title3-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="204" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b46.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-32 is-title3-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<!-- -->

<div data-num="205" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b47.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <div>
            	<h1 class="size-96 is-title4-96 is-title-bold" style="display:inline-block">Lorem Ipsum</h1>
            </div>
            <p class="size-24">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="206" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b48.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <div>
            	<h1 class="size-64 is-title4-64 is-title-bold" style="display:inline-block">Lorem Ipsum</h1>
			</div>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="207" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b49.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <div>
            	<h1 class="size-48 is-title4-48 is-title-bold" style="display:inline-block">LOREM IPSUM IS DUMMY TEXT</h1>
			</div>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="208" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b50.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <div>
            	<h1 class="size-32 is-title4-32 is-title-bold" style="display:inline-block">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
			</div>
            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<!-- -->

<div data-num="209" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b51.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-96 is-title5-96 is-title-bold">Lorem Ipsum</h1>
            <p class="size-24">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="210" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b52.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-64 is-title5-64 is-title-bold">Lorem Ipsum</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="211" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b53.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-48 is-title5-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
            <p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>

<div data-num="212" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b54.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1 class="size-32 is-title5-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
</div>
