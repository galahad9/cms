
<div data-num="140" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ij01.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size: 3em">Achievements</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="row">
            	<div class="col-md-4 center">
            		<p style="line-height:1;margin-top:0">
						<i class="icon ion-ios-people size-64"></i>
					</p>
				</div>
				<div class="col-md-8">
					<p style="line-height:1;margin-top:0"><span style="font-size: 2.5em"><b>359</b></span><br><br>Lorem Ipsum is simply.</p>
				</div>
        	</div>
		</div>
		<div class="col-md-4">
			<div class="row">
            	<div class="col-md-4 center">
            		<p style="line-height:1;margin-top:0">
						<i class="icon ion-ios-heart-outline size-64"></i>
					</p>
				</div>
				<div class="col-md-8">
					<p style="line-height:1;margin-top:0"><span style="font-size: 2.5em"><b>620</b></span><br><br>Lorem Ipsum is simply.</p>
				</div>
        	</div>
		</div>
		<div class="col-md-4">
			<div class="row">
            	<div class="col-md-4 center">
            		<p style="line-height:1;margin-top:0">
						<i class="icon ion-trophy size-64"></i>
					</p>
				</div>
				<div class="col-md-8">
					<p style="line-height:1;margin-top:0"><span style="font-size: 2.5em"><b>117</b></span><br><br>Lorem Ipsum is simply.</p>
				</div>
        	</div>
		</div>
	</div>
</div>


<div data-num="141" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ij02.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-4">	
			<div class="is-card is-dark-text shadow-1" style="display:table;border-radius: 20px;width: 250px;">				
   				<div style="display:table-cell;vertical-align: middle;text-align:center">		
					<i class="icon ion-ios-people-outline size-80" style="vertical-align: middle;margin-right: 20px;"></i>
					<b style="font-weight: 800;font-size: 2.2em;">150+</b>
  				</div>
			</div>			
		</div>
		<div class="col-md-4">
            		<div class="row">
                		<div class="col-md-12">               		
                			<div style="display:table;width:100%;height:140px">
                				<div style="display:table-cell;vertical-align:middle">
                    				<h3>More than 150 people believed us.</h3>
                    			</div>
                    		</div>                 			
                		</div>
           		</div>
		</div>
		<div class="col-md-4">
            		<div class="row">
                		<div class="col-md-12">
           					<div style="display:table;width:100%;height:140px">
                				<div style="display:table-cell;vertical-align:middle">
                    				<h3>Find Us:</h3>
                    				<div class="clearfix is-boxed-button-small">
                        				<a href="https://twitter.com/" style="background-color: #00bfff;"><i class="icon ion-social-twitter"></i></a>
                        				<a href="https://facebook.com/" style="background-color: #128BDB;"><i class="icon ion-social-facebook"></i></a>		
                        				<a href="https://www.youtube.com/" style="background-color: #E20000;"><i class="icon ion-social-youtube"></i></a>
                    				</div>
                    			</div>                 			
                			</div>		     			
                		</div>
            		</div>
       		</div>
	</div>
</div>


<div data-num="142" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ij03.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

            <h1 style="font-size: 3em">Achievements</h1>

        </div>

	</div>

	<div class="row">

		<div class="col-md-3 center">

            <i class="icon ion-laptop size-48" style="color: #333"></i>

			<p>Lorem Ipsum is simply dummy text.</p>

		</div>

		<div class="col-md-3 center">

            <i class="icon ion-ios-people size-48" style="color: #333"></i>

			<p>Lorem Ipsum is simply dummy text.</p>

		</div>

		<div class="col-md-3 center">

            <i class="icon ion-ios-heart-outline size-48" style="color: #333"></i>

			<p>Lorem Ipsum is simply dummy text.</p>

		</div>

		<div class="col-md-3 center">

            <i class="icon ion-trophy size-48" style="color: #333"></i>

			<p>Lorem Ipsum is simply dummy text.</p>

		</div>

	</div>

</div>



<div data-num="143" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ij04.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
             <h1 class="size-48">Achievements</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
            	<br>
        </div>
    </div>

	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3 center">
					<p style="line-height:1;margin-top:0">
						<i class="icon ion-ios-people-outline size-80"></i>
					</p>
				</div>
				<div class="col-md-9">
					<p style="line-height:1;margin-top:10px"><span style="font-size: 2.5em"><b>130</b></span><br><br>Lorem Ipsum is simply dummy text.</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3 center">
					<p style="line-height:1;margin-top:0">
						<i class="icon ion-ios-heart-outline size-80"></i>
					</p>
				</div>
				<div class="col-md-9">
					<p style="line-height:1;margin-top:10px"><span style="font-size: 2.5em"><b>315</b></span><br><br>Lorem Ipsum is simply dummy text.</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
            	<br>
        </div>
    </div>
    
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3 center">
					<p style="line-height:1;margin-top:0">
						<i class="icon ion-ios-paperplane-outline size-80"></i>
					</p>
				</div>
				<div class="col-md-9">
					<p style="line-height:1;margin-top:10px"><span style="font-size: 2.5em"><b>270</b></span><br><br>Lorem Ipsum is simply dummy text.</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3 center">
				<p style="line-height:1;margin-top:0">
					<i class="icon ion-ios-chatboxes-outline size-80"></i>
				</p>
				</div>
				<div class="col-md-9">
					<p style="line-height:1;margin-top:10px"><span style="font-size: 2.5em"><b>420</b></span><br><br>Lorem Ipsum is simply dummy text.</p>
				</div>
			</div>
		</div>
	</div>
</div>



<div data-num="144" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ij05.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

		   <i class="icon ion-trophy size-64"></i>

           <h1 class="size-48">Achievements</h1>

        </div>

	</div>

	<div class="row">

		<div class="col-md-3 center">

			<div class="display">

				<h1 style="font-size: 4em">120</h1>

			</div>

           <h4 style="font-size: 1.2em">HAPPY CLIENTS</h4>

        </div>

		<div class="col-md-3 center">

			<div class="display">

				<h1 style="font-size: 4em">80+</h1>

			</div>

           <h4 style="font-size: 1.2em">LAYOUTS</h4>

        </div>

		<div class="col-md-3 center">

			<div class="display">

				<h1 style="font-size: 4em">97</h1>

			</div>

           <h4 style="font-size: 1.2em">PROJECTS</h4>

        </div>

		<div class="col-md-3 center">

			<div class="display">

				<h1 style="font-size: 4em">215</h1>

			</div>

           <h4 style="font-size: 1.2em">AWARDS</h4>

        </div>

	</div>

</div>


<div data-num="145" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ij06.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
             <h1 class="size-48" style="margin:1.2em 0">Achievements</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4 center">
            <i class="icon ion-ios-people size-64"></i>
			<h3 style="margin-top:0">LOREM IPSUM</h3>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>

		<div class="col-md-4 center">
            <i class="icon ion-code-working size-64"></i>
            <h3 style="margin-top:0">LOREM IPSUM</h3>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>

		<div class="col-md-4 center">
            <i class="icon ion-trophy size-64"></i>
            <h3 style="margin-top:0">LOREM IPSUM</h3>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>
	</div>
</div>

<div data-num="146" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ij07.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-6">
			<i class="icon ion-android-star-outline size-48"></i>
        	<h2>Achievements</h2>
        	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
 			<div style="margin:2em 0">
            	<a href="#" class="btn btn-default btn-lg edit">Read More</a>
            </div>
        </div>

		<div class="col-md-6">
			<i class="icon ion-trophy size-48"></i>
            <h2>Awards</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
 			<div style="margin:2em 0">
            	<a href="#" class="btn btn-default btn-lg edit">Read More</a>
            </div>
        </div>
	</div>
</div>

<div data-num="147" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ij08.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
				<h1 style="margin:1.5em 0">Achievements</h1>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-4 center">
            <i class="icon ion-ios-people-outline size-48"></i>
			<h2 style="font-weight:800;margin-top:0">1.200</h2>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>

		<div class="col-md-4 center">
            <i class="icon ion-ios-compose-outline size-48"></i>
			<h2 style="font-weight:800;margin-top:0">879</h2>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>

		<div class="col-md-4 center">
            <i class="icon ion-ios-heart-outline size-48"></i>
			<h2 style="font-weight:800;margin-top:0">3.124</h2>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>
	</div>
</div>

<div data-num="148" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ij09.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
				<h1 style="margin:1em 0">Achievements</h1>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6 center">
            <i class="icon ion-ios-people-outline size-64"></i>
            <p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>

		<div class="col-md-6 center">
            <i class="icon ion-ios-chatboxes-outline size-64"></i>
            <p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6 center">
            <i class="icon ion-ios-star-outline size-64"></i>
            <p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
        
		<div class="col-md-6 center">
            <i class="icon ion-ios-paperplane-outline size-64"></i>
            <p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
	</div>
</div>

<div data-num="149" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ij10.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-4 center">
            <div style="border:1px solid black;padding:0 15px 10px;margin-right:1em">
            	<i class="icon ion-ios-people-outline size-64"></i>
            	<p style="margin-top:0"><span style="margin-top:0;font-size:2em;font-weight:800">1.234</span><br>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        	</div>
		</div>

		<div class="col-md-8">
            <h1>Achievements</h1>
            <p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
			<div style="margin:1.7em 0">
            	<a href="#" class="btn btn-primary btn-lg edit">Read More</a>
            </div>
        </div>
	</div>
</div>