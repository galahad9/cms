<div data-num="20001" data-thumb="<?= $assets_url ?>thumbnails/footer-0001.png" data-cat="<?= $cat ?>">
	<div class="footer4 spacer b-t">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 m-b-30">
                    <h5 class="m-b-20">Address</h5>
                    <p>71 Amsteroum Avenue Cronish Night, NY 35098</p>
                </div>
                <div class="col-lg-3 col-md-6 m-b-30">
                    <h5 class="m-b-20">Phone</h5>
                    <p>Reception : +205 123 4567
                    <br>Office : +207 235 7890</p>
                </div>
                <div class="col-lg-3 col-md-6 m-b-30">
                    <h5 class="m-b-20">Email</h5>
                    <p>Office : <a href="#" class="link">office@</a>
                    <br>Site : <a href="#" class="link">info@</a></p>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="m-b-20">Social</h5>
                    <div class="round-social light">
                        <a href="#" class="link"><i class="ti-facebook"></i></a>
                        <a href="#" class="link"><i class="ti-twitter"></i></a>
                        <a href="#" class="link"><i class="ti-google"></i></a>
                        <a href="#" class="link"><i class="ti-youtube"></i></a>
                        <a href="#" class="link"><i class="ti-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="f4-bottom-bar">
                <div class="row">
                    <div class="col-md-12">
                        <div class="d-flex font-14">
                            <div class="m-t-10 m-b-10 copyright">All Rights Reserved by companyname.</div>
                            <div class="links ml-auto m-t-10 m-b-10">
                                <a href="#" class="p-10 p-l-0">Terms of Use</a>
                                <a href="#" class="p-10">Legal Disclaimer</a>
                                <a href="#" class="p-10">Privacy Policy</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>