<div data-num="40" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/r01.png" data-cat="<?= $cat ?>">

    <div class="row">

        <div class="col-md-12">

            <div class="quote">

                <i class="icon ion-quote"></i>

	            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                <small>by Your Name</small>

            </div>

        </div>

    </div>

</div>



<div data-num="41" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/r02.png" data-cat="<?= $cat ?>">

    <div class="row">

        <div class="col-md-6">

            <div class="quote">

                <i class="icon ion-quote"></i>

	            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                <small>by Your Name</small>

            </div>

        </div>

        <div class="col-md-6">

            <div class="quote">

                <i class="icon ion-quote"></i>

	            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                <small>by Your Name</small>

            </div>

        </div>

    </div>

</div>



<div data-num="42" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/r03.png" data-cat="<?= $cat ?>">

	<div class="row">

        	<div class="col-md-6 center">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/r03-1.jpg" style="border-radius:500px;margin-top:1.5em">

        	</div>

			<div class="col-md-6">

            		<div class="quote">

                		<i class="icon ion-quote"></i>

	            		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                		<small>by Your Name</small>

            		</div>

        	</div>

	</div>

</div>



<div data-num="43" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/r04.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-6">

            		<div class="quote">

                		<i class="icon ion-quote"></i>

	            		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                		<small>by Your Name</small>

            		</div>

        	</div>

       		<div class="col-md-6 center">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/r04-1.jpg" style="border-radius:500px;margin-top:1.5em">

        	</div>

	</div>

</div>



<div data-num="74" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/r23.png" data-cat="<?= $cat ?>">

    	<div class="row">

		<div class="col-md-12 center">

            		<h1 style="font-size: 3em">Testimonials</h1>

        	</div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

            		 <i class="icon ion-quote size-80" style="color: #C0C0C0"></i>

        	</div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/r23-1.jpg" style="border-radius: 500px;">

            	<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

				<h3 style="font-size: 1.2em"><b>Your Name</b></h3>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/r23-2.jpg" style="border-radius: 500px;">

            	<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

				<h3 style="font-size: 1.2em"><b>Your Name</b></h3>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/r23-3.jpg" style="border-radius: 500px;">

            	<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

				<h3 style="font-size: 1.2em"><b>Your Name</b></h3>
			</div>
        </div>

	</div>

</div>



<div data-num="75" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/r25.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em">Happy Customers</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/r25-1.jpg" style="border-radius: 500px;" alt="">
			<br>
	        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            <p><b>Your Name, </b>Lorem Ipsum</p>
        </div>

		<div class="col-md-6 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/r25-2.jpg" style="border-radius: 500px;" alt="">
			<br>
	        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        	<p><b>Your Name, </b>Lorem Ipsum</p>
        </div>
	</div>
</div>



<div data-num="76" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/r27.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em">Testimonials</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            <div class="quote">
                <i class="icon ion-quote" style="color: #808080"></i>
	            <p style="font-size:1.2em">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                <small>by Your Name</small>
            </div>
        </div>

		<div class="col-md-6">
            <div class="quote">
                <i class="icon ion-quote" style="color: #808080"></i>
	            <p style="font-size:1.2em">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                <small>by Your Name</small>
            </div>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            <div class="quote">
                <i class="icon ion-quote" style="color: #808080"></i>
	            <p style="font-size:1.2em">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                <small>by Your Name</small>
            </div>
        </div>

		<div class="col-md-6">
            <div class="quote">
                <i class="icon ion-quote" style="color: #808080"></i>
	            <p style="font-size:1.2em">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                <small>by Your Name</small>
            </div>
        </div>
	</div>
</div>



<div data-num="77" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/r28.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em">What People Say About Us</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            <div class="row">
				<div class="col-md-4">
					<img src="<?= $assets_url ?>assets/minimalist-basic/r28-1.jpg" style="border-radius: 500px;" alt="">
				</div>
				<div class="col-md-8">
					<p style="font-size:1.2em"><i>"Lorem ipsum dolor sit amet, adipiscing elit. Vivamus leo ante."</i></p>
					<p><b><i>By Your Name</i></b></p>
				</div>
			</div>
        </div>

		<div class="col-md-6">
            <div class="row">
				<div class="col-md-4">
					<img src="<?= $assets_url ?>assets/minimalist-basic/r28-2.jpg" style="border-radius: 500px;" alt="">
				</div>
				<div class="col-md-8">
					<p style="font-size:1.2em"><i>"Lorem ipsum dolor sit amet, adipiscing elit. Vivamus leo ante."</i></p>
					<p><b><i>By Your Name</i></b></p>
				</div>
			</div>
        </div>
	</div>
</div>




<div data-num="73" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/r22.png" data-cat="<?= $cat ?>">

    	<div class="row">

		<div class="col-md-12 center">

            		<h1 style="font-size: 3em">Customer Testimonials</h1>

        	</div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
 				<i class="icon ion-quote size-32" style="color: #808080"></i>

				<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

				<p><b>Your Name, </b>Lorem Ipsum</p>
			</div>
		</div>

		<div class="col-md-4 center">
			<div class="padding-20">
 				<i class="icon ion-quote size-32" style="color: #808080"></i>

				<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

				<p><b>Your Name, </b>Lorem Ipsum</p>
			</div>
		</div>

		<div class="col-md-4 center">
			<div class="padding-20">
 				<i class="icon ion-quote size-32" style="color: #808080"></i>

				<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

				<p><b>Your Name, </b>Lorem Ipsum</p>
			</div>
		</div>

	</div>

</div>


