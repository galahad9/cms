
<div data-num="83" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w01.png" data-cat="0,21">

	<div class="row">

		<div class="col-md-12">

            <h1 style="text-align: center; font-size:3em">Our Services</h1>

        </div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-monitor-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-gear-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-heart-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-compose-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-world-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-calendar-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

	</div>

</div>



<div data-num="84" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w02.png" data-cat="0,21">

	<div class="row">

		<div class="col-md-12">

            		 <h1 style="text-align: center; font-size:3em">Our Services</h1>

        	</div>

	</div>

	<div class="row">

		<div class="col-md-6">

            <p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-ios-heart-outline size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

			<p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-ios-compose-outline size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

			<p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-ios-gear-outline size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

		</div>

		<div class="col-md-6">

            <p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-ios-calendar-outline size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

			<p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-ios-clock-outline size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

			<p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-laptop size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

		</div>

	</div>

</div>


<div data-num="85" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w04.png" data-cat="0,21">

	<div class="row">

		<div class="col-md-12 center">

			<div class="display">

				<h1>Beautiful content. Responsive.</h1>

			</div>

			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet.</p>

		</div>

	</div>

	<div class="row">

		<div class="col-md-6">

            <img src="<?= $assets_url ?>assets/minimalist-basic/w04-1.png">

        </div>

		<div class="col-md-6">
            		
            <div style="margin:30px 0 0 0">
                <p><i class="icon ion-ios-heart-outline size-48" style="vertical-align: middle;margin-right:30px;"></i> Lorem Ipsum is simply dummy text.</p>

                <p><i class="icon ion-ios-compose-outline size-48" style="vertical-align: middle;margin-right:30px;"></i> Lorem Ipsum is simply dummy text.</p>

                <p><i class="icon ion-ios-gear-outline size-48" style="vertical-align: middle;margin-right:30px;"></i> Lorem Ipsum is simply dummy text.</p>
			</div>

     	</div>

	</div>

</div>



<div data-num="86" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w05.png" data-cat="21">

	<div class="row">

		<div class="col-md-12 center">

                    <h1 style="font-size: 3em; margin: 1.2em 0">What We Offer</h1>

                </div>

	</div>

	<div class="row">

        	<div class="col-md-6">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/w05-1.png">

        	</div>

			<div class="col-md-6">

                		<h3>Beautiful Content. Responsive.</h3>

                		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                		<div style="margin:1em 0 2.5em;">

                			<a href="#" class="btn btn-primary btn-lg edit">Read More</a>

            		</div>

        	</div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
				<i class="icon ion-ios-compose-outline size-80"></i>

				<h3>Lorem Ipsum</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<i class="icon ion-ios-camera-outline size-80"></i>

				<h3>Lorem Ipsum</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

        	</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<i class="icon ion-ios-paperplane-outline size-80"></i>

				<h3>Lorem Ipsum</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

        	</div>
		</div>
	</div>

</div>

<div data-num="87" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w06.png" data-cat="0,21">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size: 3em; margin: 1.2em 0">What We Offer</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3 center">
			<i class="icon ion-ios-heart-outline size-48" style="margin-top: 0.3em"></i>
			<p><b>Beautiful Content</b><br>Lorem Ipsum is simply.</p>
        </div>
        
		<div class="col-md-3 center">
			<i class="icon ion-ios-monitor-outline size-48" style="margin-top: 0.3em"></i>
			<p><b>Responsive</b><br>Lorem Ipsum is simply.</p>
        </div>
        
		<div class="col-md-3 center">
			<i class="icon ion-ios-gear-outline size-48" style="margin-top: 0.3em"></i>
			<p><b>Typesetting</b><br>Lorem Ipsum is simply.</p>
        </div>
        
		<div class="col-md-3 center">
			<i class="icon ion-ios-compose-outline size-48" style="margin-top: 0.3em"></i>
			<p><b>Simply Text</b><br>Lorem Ipsum is simply.</p>
        </div>
	</div>
</div>



<div data-num="88" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w07.png" data-cat="0,21">
	<div class="row">
		<div class="col-md-12 center">
            	<h1 style="font-size:3em">SERVICES</h1>
            	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
        </div>
	</div>

	<div class="row">
		<div class="col-md-4 center">
			<i class="icon ion-ios-heart-outline size-80"></i>
			<h3 style="margin-top:0">Lorem Ipsum</h3>
        </div>

		<div class="col-md-4 center">
			<i class="icon ion-ios-gear-outline size-80"></i>
			<h3 style="margin-top:0">Lorem Ipsum</h3>
        </div>

		<div class="col-md-4 center">
			<i class="icon ion-ios-filing-outline size-80"></i>
			<h3 style="margin-top:0">Lorem Ipsum</h3>
        </div>
	</div>
</div>



<div data-num="89" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w10.png" data-cat="0,21">
	<div class="row">
		<div class="col-md-12 center">
            	<h1 style="text-align: center; font-size:3em;margin:1.5em 0">Our Services</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1" style="border-radius:20px">
				<div class="margin-25 center">
			   		<i class="icon ion-ios-gear-outline size-64"></i>
			    	<h3 style="margin-top:0">Lorem Ipsum</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              </div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1" style="border-radius:20px">
				<div class="margin-25 center">
			    	<i class="icon ion-ios-heart-outline size-64"></i>
			    	<h3 style="margin-top:0">Lorem Ipsum</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        		</div>
       	 	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1" style="border-radius:20px">
				<div class="margin-25 center">
			   		<i class="icon ion-ios-mic-outline size-64"></i>
			   		<h3 style="margin-top:0">Lorem Ipsum</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        		</div>
         	</div>
        </div>
    </div>
</div>