
<div data-num="12" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/g01.png" data-cat="0,6">

	<div class="row">

		<div class="col-md-12">

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>

        </div>

	</div>

</div>

<div data-num="21" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/g02.png" data-cat="0,6">

	<div class="row">

        	<div class="col-md-6">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/g02-1.jpg">

        	</div>
			<div class="col-md-6">

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        	</div>

	</div>

</div>



<div data-num="22" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/g03.png" data-cat="0,6">

	<div class="row">

		<div class="col-md-6">

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

       		 </div>

        	<div class="col-md-6">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/g03-1.jpg">

       		</div>

	</div>

</div>



<div data-num="23" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/h01.png" data-cat="0,6">

	<div class="row">

		<div class="col-md-6">

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        	</div>

		<div class="col-md-6">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h01-1.jpg" style="border-radius:500px;margin-right: 1.5em">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h01-2.jpg" style="border-radius:500px;margin-right: 1.5em">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h01-3.jpg" style="border-radius:500px">

		</div>

	</div>

</div>



<div data-num="24" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/h02.png" data-cat="0,6">

	<div class="row">

		<div class="col-md-6">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h02-1.jpg" style="border-radius:500px;margin-right: 1.5em">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h02-2.jpg" style="border-radius:500px;margin-right: 1.5em">

			<img src="<?= $assets_url ?>assets/minimalist-basic/h02-3.jpg" style="border-radius:500px">

		</div>	

		<div class="col-md-6">

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        	</div>

	</div>

</div>
