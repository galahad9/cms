


<div data-num="113" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ab01.png" data-cat="0,25">

	<div class="row">

		<div class="col-md-12">

			<div class="display center">

            	<h1>Portfolio</h1>

			</div>

			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>

        </div>

	</div>

	<div class="row">

		<div class="col-md-4">

            <img src="<?= $assets_url ?>assets/minimalist-basic/ab01-1.jpg">	

        </div>

        <div class="col-md-4">

		    <img src="<?= $assets_url ?>assets/minimalist-basic/ab01-2.jpg">

        </div>

        <div class="col-md-4">

		    <img src="<?= $assets_url ?>assets/minimalist-basic/ab01-3.jpg">

        </div>

	</div>

	<div class="row">

		<div class="col-md-4">

            <img src="<?= $assets_url ?>assets/minimalist-basic/ab01-4.jpg">	

        </div>

        <div class="col-md-4">

		    <img src="<?= $assets_url ?>assets/minimalist-basic/ab01-5.jpg">

        </div>

        <div class="col-md-4">

		    <img src="<?= $assets_url ?>assets/minimalist-basic/ab01-6.jpg">

        </div>

	</div>

</div>



<div data-num="114" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ab02.png" data-cat="0,25">

	<div class="row">

		<div class="col-md-12 center">

            <h1 style="font-size: 3em">Our Works</h1>

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

        </div>

	</div>

	<div class="row">

		<div class="col-md-4">
            
            <div class="padding-20">
            <img src="<?= $assets_url ?>assets/minimalist-basic/ab02-1.jpg">

		    <p><b>Beautiful Content</b><br>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>

        </div>

		<div class="col-md-4">

            <div class="padding-20">
            <img src="<?= $assets_url ?>assets/minimalist-basic/ab02-2.jpg">

		    <p><b>Printing and Typesetting</b><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>
            </div>

        </div>

		<div class="col-md-4">

            <div class="padding-20">
            <img src="<?= $assets_url ?>assets/minimalist-basic/ab02-3.jpg">

		    <p><b>Simply Dummy Text</b><br>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Vivamus leo ante, consectetur sit amet vulputate vel. </p>
            </div>

        </div>

	</div>

</div>



<div data-num="115" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ab03.png" data-cat="25">

	<div class="row">

		<div class="col-md-12 center">

			<h1 style="font-size: 3em">Products</h1>

		</div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
            	<img src="<?= $assets_url ?>assets/minimalist-basic/ab03-1.jpg" style="border-radius: 500px">

				<h2 style="font-size: 1.7em">Lorem Ipsum</h2>

				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem ipsum dolor sit amet.</p>

				<div style="margin:2em 0 2.5em;">

           	    	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">Buy Now</a>

           	 	</div>
			</div>
    	</div>   	

		<div class="col-md-4 center">
			<div class="padding-20">
            	<img src="<?= $assets_url ?>assets/minimalist-basic/ab03-2.jpg" style="border-radius: 500px">

				<h2 style="font-size: 1.7em">Lorem Ipsum</h2>

				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem ipsum dolor sit amet.</p>

				<div style="margin:2em 0 2.5em;">

            		<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">Buy Now</a>

				</div>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
            	<img src="<?= $assets_url ?>assets/minimalist-basic/ab03-3.jpg" style="border-radius: 500px">

				<h2 style="font-size: 1.7em">Lorem Ipsum</h2>

				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem ipsum dolor sit amet.</p>

				<div style="margin:2em 0 2.5em;">

            		<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">Buy Now</a>

            	</div>
			</div>
        </div>

	</div>

</div>


<div data-num="116" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ab06.png" data-cat="0,25">

	<div class="row">
		<div class="col-md-12">
				<img src="<?= $assets_url ?>assets/minimalist-basic/ab06-1.jpg"><img src="<?= $assets_url ?>assets/minimalist-basic/ab06-2.jpg"><img src="<?= $assets_url ?>assets/minimalist-basic/ab06-3.jpg">
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
            <div class="display">
                <h1 style="font-size:3.3em">Portfolio</h1>
                <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
            </div>
			<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
         </div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
            <div style="margin:1em 0 2.5em;">
            	<a href="#" class="btn btn-primary btn-lg edit">VIEW PROJECT</a>
            </div>
        </div>
	</div>
</div>

<div data-num="117" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ab07.png" data-cat="0,25">

	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em">Our Products</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
	</div>

	<div class="row">
		<div class="col-md-12">
            <br>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
					<img src="<?= $assets_url ?>assets/minimalist-basic/ab07-1.jpg" style="border-radius: 500px">
			    	<h3 style="font-size:1.5em">Product One</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0">
						<a href="#" class="btn btn-primary btn-lg edit">Buy Now</a>
					</div>
            	</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			    	<img src="<?= $assets_url ?>assets/minimalist-basic/ab07-2.jpg" style="border-radius: 500px">
			    	<h3 style="font-size:1.5em">Product Two</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            		<div style="margin:2em 0">
						<a href="#" class="btn btn-primary btn-lg edit">Buy Now</a>
					</div>
        		</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			  		<img src="<?= $assets_url ?>assets/minimalist-basic/ab07-3.jpg" style="border-radius: 500px">				
			    	<h3 style="font-size:1.5em">Product Three</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
     				<div style="margin:2em 0">
						<a href="#" class="btn btn-primary btn-lg edit">Buy Now</a>
					</div>
        		</div>
         	</div>
		</div>
	</div>
</div>

<div data-num="118" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ab10.png" data-cat="0,25">

	<div class="row">
		<div class="col-md-12 center">
			<i class="icon ion-ios-cart size-64" style="color: #333"></i>
            <h1 class="size-64" style="margin-top:0">Our Products</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
	</div>

	<div class="row">
		<div class="col-md-12">
            <br>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            <img src="<?= $assets_url ?>assets/minimalist-basic/ab10-1.jpg">
        </div>
		<div class="col-md-6">
            <h1>Product One</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            <div style="margin:1em 0 2.5em;">
            	<a href="#" class="btn btn-primary btn-lg edit">BUY NOW</a>
            </div>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
            <br>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            		<h1>Product Two</h1>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            		<div style="margin:1em 0 2.5em;">
            			<a href="#" class="btn btn-primary btn-lg edit">BUY NOW</a>
            		</div>
        	</div>
        	<div class="col-md-6">
            		<img src="<?= $assets_url ?>assets/minimalist-basic/ab10-2.jpg">
        	</div>
	</div>
</div>