
<div data-num="13" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/e01.png" data-cat="0,5">

	<div class="row">

        <div class="col-md-12">

            <h1>Heading 1 Text Goes Here</h1>

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        </div>

    </div>

</div>



<div data-num="14" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/e02.png" data-cat="0,5">

	<div class="row">

        <div class="col-md-12">

            <h2>Heading 2 Text Goes Here</h2>

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        </div>

    </div>

</div>


<div data-num="15" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/e09.png" data-cat="0,5">

	<div class="row">

		<div class="col-md-4">

			<h1>Lorem Ipsum</h1>

			<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

		</div>

		<div class="col-md-8">

			<img src="<?= $assets_url ?>assets/minimalist-basic/e09-1.jpg">

		</div>

	</div>

</div>
