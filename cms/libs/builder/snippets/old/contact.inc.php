
<div data-num="47" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/t01.png" data-cat="0,17,22">

    <div class="row">

        <div class="col-md-12">

            <div class="embed-responsive embed-responsive-16by9">

            <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&amp;hl=en&amp;sll=-7.981898,112.626504&amp;sspn=0.009084,0.016512&amp;oq=melbourne&amp;hnear=Melbourne+Victoria,+Australia&amp;t=m&amp;z=10&amp;output=embed"></iframe>

            </div>
        </div>
    </div>
</div>



<div data-num="48" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/t02.png" data-cat="0,17,22">

    <div class="row">

        <div class="col-md-8">

            <div class="embed-responsive embed-responsive-16by9">

            <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&amp;hl=en&amp;sll=-7.981898,112.626504&amp;sspn=0.009084,0.016512&amp;oq=melbourne&amp;hnear=Melbourne+Victoria,+Australia&amp;t=m&amp;z=10&amp;output=embed"></iframe>

            </div>

        </div>

        <div class="col-md-4">

            <h3>Get in Touch</h3>

            <p>

              <strong>Company, Inc.</strong><br>

              123 Street, City<br>

              State 12345<br>

              P: (123) 456 7890 / 456 7891

            </p>



            <p>

              <strong>Full Name</strong><br>

              <a href="mailto:#">first.last@example.com</a>

            </p>

        </div>

    </div>

</div>





<div data-num="90" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x01.png" data-cat="0,22">

	<div class="row">

		<div class="col-md-12 center">

			<div class="display">

				<h1 style="font-size: 3.5em">Contact Us</h1>

			</div>

		</div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel.</p>

        	</div>

	</div>

	<div class="row">

		<div class="col-md-6">

			<h3>Address</h3>

         		<p>

              		<strong>Company, Inc.</strong><br>

              		123 Street, City<br>

              		State 12345<br>

              		P: (123) 456 7890 / 456 7891

            	</p>

	    </div>

	    <div class="col-md-6">

            <div class="row">

			    <div class="col-md-12">

				    <h3>Stay in Touch</h3>

            	    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

                    <div class="clearfix is-boxed-button-medium2">
                        <a href="https://twitter.com/" style="background-color: #00bfff;"><i class="icon ion-social-twitter"></i></a>
                        <a href="https://facebook.com/" style="background-color: #128BDB;"><i class="icon ion-social-facebook"></i></a>		
                        <a href="mailto:you@example.com" style="background-color: #ff69B4;"><i class="icon ion-ios-email-outline"></i></a>
                    </div>

			    </div>

            </div>


	    </div>

	</div>

</div>



<div data-num="91" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x05.png" data-cat="22">

	<div class="row">

		<div class="col-md-12 center">

               <h1 style="font-size: 3em">Contact Us</h1>

               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        </div>

	</div>

	<div class="row">

		<div class="col-md-6">

            <div class="row">

			    <div class="col-md-12">  

				    <h3>OFFICE ONE</h3>

			    </div>

            </div>

		    <div class="row">                        

				<div class="col-md-2">

 					<p style="line-height:1"><i class="icon ion-ios-location size-32"></i></p>

				</div>

				<div class="col-md-10">

                    <p>

              			<strong>Company, Inc.</strong><br>

              			123 Street, City<br>

              			State 12345

            		</p>

				</div>

			</div>

			<div class="row">

				<div class="col-md-2">

 					<p style="line-height:1"><i class="icon ion-ios-telephone size-32"></i></p>

				</div>

				<div class="col-md-10">

                    <p><b>Phone</b><br>(123) 456 7890</p>

				</div>

			</div>

	   		<div class="row">

				<div class="col-md-2">

					<p style="line-height:1"><i class="icon ion-ios-email size-32"></i></p>

				</div>

				<div class="col-md-10">

                     <p>

              			<strong>Email</strong><br>

              			<a href="mailto:#">first.last@example.com</a>

            		</p>

				</div>

             </div>

		</div>

		<div class="col-md-6">

			<div class="row">

                <div class="col-md-12">  

				    <h3>OFFICE TWO</h3>

                </div>

			</div>

			<div class="row">                       

				<div class="col-md-2">

 					<p style="line-height:1"><i class="icon ion-ios-location size-32"></i></p>

				</div>

				<div class="col-md-10">

                   	<p>

              			<strong>Company, Inc.</strong><br>

              			123 Street, City<br>

              			State 12345

            		</p>

				</div>

			</div>

			<div class="row">

				<div class="col-md-2">

 					<p style="line-height:1"><i class="icon ion-ios-telephone size-32"></i></p>

				</div>

				<div class="col-md-10">

                    <p><b>Phone</b><br>(123) 456 7890</p>

				</div>

			</div>

			<div class="row">

				<div class="col-md-2">

					<p style="line-height:1"><i class="icon ion-ios-email size-32"></i></p>

				</div>

				<div class="col-md-10">

                     <p>

              			<strong>Email</strong><br>

              			<a href="mailto:#">first.last@example.com</a>

            		</p>

				</div>

            </div>

		 </div>

	</div>

</div>


<div data-num="92" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x09.png" data-cat="22">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em">Contact Us</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3 center">
            <i class="icon ion-ios-location-outline size-48"></i>
			<h3 style="font-size:1.5em;margin-top:0">Company, Inc.</h3>
			<p>
              123 Street, City<br>
              State 12345
            </p>
        </div>

		<div class="col-md-3 center">
            <i class="icon ion-ios-telephone-outline size-48"></i>
			<h3 style="font-size:1.5em;margin-top:0">Call Us</h3>
  			<p>(123) 456 7890 / 456 7891</p>
        </div>

		<div class="col-md-3 center">
            <i class="icon ion-ios-email-outline size-48"></i>
			<h3 style="font-size:1.5em;margin-top:0">Email</h3>
  			<p>
              <a href="mailto:#">first.last@example.com</a>
            </p>
        </div>

		<div class="col-md-3 center">
            <i class="icon ion-ios-chatboxes-outline size-48"></i>
			<h3 style="font-size:1.5em;margin-top:0">Social Media</h3>
  			<p><a href="https://www.facebook.com">Facebook</a><br><a href="https://twitter.com">Twitter</a><br></p>
        </div>
	</div>
</div>



<div data-num="93" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x10.png" data-cat="0,17,22">

	<div class="row">
		<div class="col-md-12">
            <h1 style="font-size:3em">Contact Us</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
           <p>
              <span style="font-size:1.2em"><b>Company, Inc.</b></span><br>
              123 Street, City<br>
              State 12345<br>
              P: (123) 456 7890 / 456 7891
            </p>
			<div class="clearfix is-rounded-button-medium" style="margin:1em 0">
                	<a href="https://twitter.com/" style="background-color: #00bfff;margin-left:0;"><i class="icon ion-social-twitter"></i></a>
                	<a href="https://www.facebook.com/" style="background-color: #128BDB"><i class="icon ion-social-facebook"></i></a>
					<a href="https://plus.google.com/" style="background-color: #DF311F"><i class="icon ion-social-googleplus"></i></a>          
			</div>
        </div>

		<div class="col-md-6">
            <div class="embed-responsive embed-responsive-16by9">
            	<iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&amp;hl=en&amp;sll=-7.981898,112.626504&amp;sspn=0.009084,0.016512&amp;oq=melbourne&amp;hnear=Melbourne+Victoria,+Australia&amp;t=m&amp;z=10&amp;output=embed"></iframe>
            </div>
        </div>
	</div>	
</div>


<div data-num="94" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x11.png" data-cat="0,22">

	<div class="row">
		<div class="col-md-12">
            <h1 style="font-size:3em">Contact Us</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
            <h3>
				<i class="icon ion-ios-location-outline size-48" style="margin-top:0;line-height:1;vertical-align:middle;margin-right:10px"></i>
				<span style="vertical-align:middle">Address</span>
			</h3>
            <p>
              123 Street, City<br>
              State 12345
            </p>
        </div>

		<div class="col-md-4">
        	<h3>
 				<i class="icon ion-iphone size-48" style="margin-top:0;line-height:1;vertical-align:middle;margin-right:10px"></i>
				<span style="vertical-align:middle">Phone</span>
			</h3>
            <p>(123) 456 7890 / 456 7891</p>
        </div>

		<div class="col-md-4">
            <h3>
 				<i class="icon ion-ios-email-outline size-48" style="margin-top:0;line-height:1;vertical-align:middle;margin-right:10px"></i>
				<span style="vertical-align:middle">Email</span>
			</h3>
            <p>
              <a href="mailto:#">first.last@example.com</a>
            </p>
        </div>
	</div>
</div>

<div data-num="95" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x12.png" data-cat="0,22">
	<div class="row">
		<div class="col-md-12">
            <h1>Contact Us</h1>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            <img src="<?= $assets_url ?>assets/minimalist-basic/x12-1.jpg" style="margin-top: 0px;">
        </div>

		<div class="col-md-6">
            <h3 style="margin-top:0">Company, Inc.</h3>
			<p>
              123 Street, City<br>
              State 12345<br>
              P: (123) 456 7890 / 456 7891
            </p>
			<div class="is-social edit">
                <a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                <a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
				<a href="https://www.pinterest.com/"><i class="icon ion-social-pinterest-outline" style="margin-right: 1em"></i></a>
                <a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
				<a href="https://www.instagram.com/"><i class="icon ion-social-instagram-outline"></i></a>
			</div>
        </div>
	</div>
</div>


<div data-num="2201" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/2201.png" data-cat="0,22">
	<section class="contact-1">
			<div id="map"></div>
				<script src="js/maps.js"></script>
			<!--YOU MUST REPLACE WITH YOUR OWN API KEY FOR THE MAP TO WORK-->
			<script async defer
				src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBptaDKT_ntSoNEytCnSang5JenaNAj_Us&callback=initMap">
			</script>
			<div class="container">
				<div class="row contact-details">
					<div class="col-sm-8 text-center text-md-left mb-4">
						<h3>Ask us a question</h3>
						<form class="contact-form mt-4">
							<div class="row">
								<div class="col-md-5">
									<input type="text" class="form-control-custom mb-4" placeholder="Name" value="Your name">
								</div>
								<div class="col-md-5">
									<input type="text" class="form-control-custom mb-4" placeholder="Email address" value="Email address">
								</div>
								<br />
							</div>
							<div class="row">
								<div class="col-md-10">
									<textarea class="form-control-custom mb-4" rows="3">Your Message</textarea><br />
									<button type="submit" class="btn btn-primary btn-lg mb-4">Send Message</button>
								</div> 
							</div>
						</form>
					</div>
					<div class="col-sm-4 text-center text-md-left">
						<h3>Contact</h3>
						<h4 class="pt-4">Email</h4>
						<p>hello@startup.co</p>
						<h4 class="pt-2">Phone</h4>
						<p>+111 234 567 89</p>
						<h4 class="pt-2">Address</h4>
						<p>1 Street Name, City, Zip Code<br />
						United States</p>
						<ul class="social">
							<li><a href="#" title="Facebook" class="fa fa-facebook"></a></li>
							<li><a href="#" title="Twitter" class="fa fa-twitter"></a></li>
							<li><a href="#" title="Google+" class="fa fa-google"></a></li>
							<li><a href="#" title="Dribbble" class="fa fa-dribbble"></a></li>
							<li><a href="#" title="Instagram" class="fa fa-instagram"></a></li>
							<div class="clear"></div>
						</ul>
					</div>
				</div>
			</div>
		</section>
</div>
<div data-num="2202" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/2202.png" data-cat="0,22">
		<section class="contact-2">
			<div class="container">
				<div class="row contact-details">
					<div class="col-sm-8 m-auto text-center">
						<h2>Contact Us</h2>
						<p class="lead constrain-width mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. At aliquam rhoncus donec magna turpis, dictum sit amet tellus at, commodo elementum sapien.</p>
						<div class="divider"></div>
						<h4>Ask us a question</h4>
						<form class="contact-form mt-4">
							<div class="row">
								<div class="col-md-6">
									<input type="text" class="form-control-custom mb-4" placeholder="Name" value="Your name">
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control-custom mb-4" placeholder="Email address" value="Email address">
								</div>
								<br />
							</div>
							<div class="row">
								<div class="col-md-12">
									<textarea class="form-control-custom mb-4" rows="3">Your Message</textarea><br />
									<button type="submit" class="btn btn-primary btn-lg mb-4">Send Message</button>
								</div> 
							</div>
						</form>
					</div>
					
				</div>
			</div>
		</section>
</div>
<div data-num="2203" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/2203.png" data-cat="0,22">
		<section class="contact-3">
			<div class="container text-center">
				<div class="row contact-details">
					<div class="col-sm-5 text-center text-md-left">
						<h3>Contact</h3>
						<h4 class="pt-4">Email</h4>
						<p>hello@startup.co</p>
						<h4 class="pt-2">Phone</h4>
						<p>+111 234 567 89</p>
						<h4 class="pt-2">Address</h4>
						<p>1 Street Name, City, Zip Code<br />
						United States</p>
						<ul class="social">
							<li><a href="#" title="Facebook" class="fa fa-facebook"></a></li>
							<li><a href="#" title="Twitter" class="fa fa-twitter"></a></li>
							<li><a href="#" title="Google+" class="fa fa-google"></a></li>
							<li><a href="#" title="Dribbble" class="fa fa-dribbble"></a></li>
							<li><a href="#" title="Instagram" class="fa fa-instagram"></a></li>
							<div class="clear"></div>
						</ul>
					</div>
					<div class="col-sm-7 text-center text-md-left mb-4">
						<h3>Ask us a question</h3>
						<form class="contact-form mt-4">
							<div class="row">
								<div class="col-md-5">
									<input type="text" class="form-control-custom mb-4" placeholder="Name" value="Your name">
								</div>
								<div class="col-md-5">
									<input type="text" class="form-control-custom mb-4" placeholder="Email address" value="Email address">
								</div>
								<br />
							</div>
							<div class="row">
								<div class="col-md-10">
									<textarea class="form-control-custom mb-4" rows="3">Your Message</textarea><br />
									<button type="submit" class="btn btn-primary btn-lg mb-4">Send Message</button>
								</div> 
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
</div>
<div data-num="2204" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/2204.png" data-cat="0,22">
		<section class="contact-4">
			<div class="container text-center">
				<h2>Contact Us</h2>
				<div class="divider"></div>
				<div class="row contact-details">
					<div class="col-lg-6 text-center">
						<img class="mb-5 img-fluid" src="images/placeholder-team.jpg" >
					</div>
					<div class="col-lg-6 text-center text-lg-left mb-4">
						<p class="lead mt-0 mb-2">Integer libero nunc, elementum eu venenatis sed, molestie non tortor. Duis suscipit pulvinar justo. Orci varius natoque penatibus et magnis dis parturient.</p>
						<h4 class="pt-4">Email</h4>
						<p>hello@startup.co</p>
						<h4 class="pt-2">Phone</h4>
						<p>+111 234 567 89</p>
						<h4 class="pt-2">Address</h4>
						<p>1 Street Name, City, Zip Code, United States</p>
						<ul class="social">
							<li><a href="#" title="Facebook" class="fa fa-facebook"></a></li>
							<li><a href="#" title="Twitter" class="fa fa-twitter"></a></li>
							<li><a href="#" title="Google+" class="fa fa-google"></a></li>
							<li><a href="#" title="Dribbble" class="fa fa-dribbble"></a></li>
							<li><a href="#" title="Instagram" class="fa fa-instagram"></a></li>
							<div class="clear"></div>
						</ul>
					</div>
				</div>
			</div>
		</section>
  </div>