

<div data-num="106" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z01.png" data-cat="0,24">

	<div class="row">

		<div class="col-md-12 center">

			<div class="display">

				<h1 style="font-size: 3.5em">Meet The Team</h1>

			</div>

		</div>

	</div>

	<div class="row">

		<div class="col-md-4 center">

			<img src="<?= $assets_url ?>assets/minimalist-basic/z01-1.jpg" style="border-radius:500px; margin-top: 1em">

			<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>

			</div>          

		</div>

		<div class="col-md-8">

			<h1>Your Name</h1>

			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>

		</div>

	</div>

	<div class="row">

		<div class="col-md-4 center">

			<img src="<?= $assets_url ?>assets/minimalist-basic/z01-2.jpg" style="border-radius:500px; margin-top: 1em">

			<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>

			</div>         

		</div>

		<div class="col-md-8">

			<h1>Your Name</h1>

			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>

		</div>

	</div>

</div>


<div data-num="107" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z02.png" data-cat="0,24">
	<div class="row">
		<div class="col-md-12 center">
            		<h1 style="font-size: 3em">Meet The Team</h1>
		    	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        	</div>
	</div>	
	<div class="row">
		<div class="col-md-6">
			<div class="row">
			    <div class="col-md-4 center">
				    <img src="<?= $assets_url ?>assets/minimalist-basic/z02-2.jpg" style="border-radius:500px">
			    </div>
			    <div class="col-md-8">
				<p><b>Your Name</b><br>Lorem Ipsum is simply.</p>
				<div class="is-social edit">
                		    <a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>
                		    <a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>
                		    <a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>
                		    <a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			        </div>
			    </div>
		    </div>
	    	</div>
	    <div class="col-md-6">
		    <div class="row">
			    <div class="col-md-4 center">
				    <img src="<?= $assets_url ?>assets/minimalist-basic/z02-3.jpg" style="border-radius:500px">
			    </div>
			    <div class="col-md-8">
				<p><b>Your Name</b><br>Lorem Ipsum is simply.</p>				    
				<div class="is-social edit">
                		    <a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>
                		    <a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>
                		    <a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>
                		    <a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			        </div>
			    </div>
		    </div>
	    </div>
    	</div>
</div>


<div data-num="108" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z03.png" data-cat="0,24">

	<div class="row">

		<div class="col-md-12 center">

			<h1 style="font-size: 3em">Our Team</h1>

			<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

		</div>

	</div>

	<div class="row">

		<div class="col-md-6 center">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/z03-1.jpg" style="border-radius:500px">

				<h3>Your Name</h3>

				<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>

				<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>

				</div>
			</div>
		</div>

		<div class="col-md-6 center">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/z03-2.jpg" style="border-radius:500px">

				<h3>Your Name</h3>

				<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>

				<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>

				</div>
			</div>
		</div>

	</div>

</div>


<div data-num="109" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z05.png" data-cat="0,24">
	<div class="row">
		<div class="col-md-12 center">
            	<h1 style="margin:1.2em 0;font-size:3em">Meet The Team</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
					<img src="<?= $assets_url ?>assets/minimalist-basic/z05-1.jpg" style="border-radius: 500px;" alt="">			
			    	<h3 style="font-size:1.5em">Your Name</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>              
        		</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			    	<img src="<?= $assets_url ?>assets/minimalist-basic/z05-2.jpg" style="border-radius: 500px;" alt="">		
			    	<h3 style="font-size:1.5em">Your Name</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            		<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>    
        		</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			  		<img src="<?= $assets_url ?>assets/minimalist-basic/z05-3.jpg" style="border-radius: 500px;" alt="">
			   	 	<h3 style="font-size:1.5em">Your Name</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>         
        		</div>
         	</div>
		</div>
	</div>
</div>

<div data-num="110" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z06.png" data-cat="0,24">
	<div class="row">
		<div class="col-md-12 center">
            <h1>Our Team</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z06-1.jpg" style="border-radius: 500px;" alt="">
            <p><b>Your Name</b><br>Lorem Ipsum is simply.</p>
			<div class="is-social edit">
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                	<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                	<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div> 
        </div>

		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z06-2.jpg" style="border-radius: 500px;" alt="">
            <p><b>Your Name</b><br>Lorem Ipsum is simply.</p>
			<div class="is-social edit">
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                	<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                	<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div> 
        </div>

		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z06-3.jpg" style="border-radius: 500px;" alt="">
            <p><b>Your Name</b><br>Lorem Ipsum is simply.</p>
			<div class="is-social edit">
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                	<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                	<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div> 
        </div>

		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z06-4.jpg" style="border-radius: 500px;" alt="">
            <p><b>Your Name</b><br>Lorem Ipsum is simply.</p>
			<div class="is-social edit">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div> 
        </div>
	</div>
</div>

<div data-num="111" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z07.png" data-cat="0,24">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
                <h1>Meet Our Team</h1>
			</div>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z07-1.jpg" style="border-radius: 500px;">
		</div>
		
		<div class="col-md-3">
			<p><b>Your Name</b><br>Lorem Ipsum is simply dummy text of the printing industry.</p>
			<div class="is-social edit">
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                	<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                	<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div>   
		</div>
		
		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z07-2.jpg" style="border-radius: 500px;">
		</div>
		
		<div class="col-md-3">
			<p><b>Your Name</b><br>Lorem Ipsum is simply dummy text of the printing industry.</p>
			<div class="is-social edit">
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                	<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                	<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div>   
		</div>
	</div>
</div>

<div data-num="112" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z09.png" data-cat="0,24">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
				<h1>Meet The Team</h1>
			</div>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">
                	<img src="<?= $assets_url ?>assets/minimalist-basic/z09-1.jpg" style="border-radius: 500px;">
			    	<h3 style="font-size:1.5em">Your Name</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>     
        		</div>
        	</div>
		</div>

		<div class="col-md-6">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">
                	<img src="<?= $assets_url ?>assets/minimalist-basic/z09-2.jpg" style="border-radius: 500px;">
			    	<h3 style="font-size:1.5em">Your Name</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>  
            	</div>
        	</div>
		</div>
	</div>
</div>

<div data-num="46" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/s10.png" data-cat="0,24">

	<div class="row">

		<div class="col-md-6">

	        <div class="row">

			    <div class="col-md-4 center">

				    <p><img src="<?= $assets_url ?>assets/minimalist-basic/s10-1.jpg" style="border-radius:500px;margin-top:10px;"></p>

			    </div>

			    <div class="col-md-8">

				    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>

				    <div class="is-social edit">

                			    <a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                			    <a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                			    <a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                			    <a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

				    </div>

			    </div>

			</div>

		</div>

		<div class="col-md-6">

	        <div class="row">

			    <div class="col-md-4 center">

				    <p><img src="<?= $assets_url ?>assets/minimalist-basic/s10-2.jpg" style="border-radius:500px;margin-top:10px;"></p>

			    </div>

			    <div class="col-md-8">

				    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>

				    <div class="is-social edit">

                			    <a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                			    <a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                			    <a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                			    <a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

				    </div>

			    </div>

			</div>

		</div>

	</div>

</div>