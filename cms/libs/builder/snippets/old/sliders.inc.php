

<div data-num="301" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/slider1.png" data-cat="0,35">
    <div class="row">
        <div class="col-md-12" data-module="slider" data-module-desc="Slider" data-html="%3Cdiv%20id%3D%22{id}%22%20class%3D%22slider-on-content%22%20style%3D%22width%3A100%25%3Bheight%3A400px%3B%22%3E%3Cdiv%20class%3D%22is-boxes%20slider-image%22%20style%3D%22background-image%3Aurl(assets%2Fminimalist-basic%2Fslider1-b.jpg)%3B%22%3E%3C%2Fdiv%3E%3Cdiv%20class%3D%22is-boxes%20slider-image%22%20style%3D%22background-image%3Aurl(assets%2Fminimalist-basic%2Fslider1-a.jpg)%3B%22%3E%3C%2Fdiv%3E%3C%2Fdiv%3E%3Cscript%3E%0Avar%20docReady%3Dfunction(fn)%7Bvar%20stateCheck%3DsetInterval(function%20()%7Bif(document.readyState!%3D%3D%22complete%22)return%3BclearInterval(stateCheck)%3Btry%7Bfn()%7Dcatch(e)%7B%7D%7D%2C1)%3B%7D%3B%0AdocReady(function()%20%7B%0AjQuery(%22%23{id}%22).slick(%7B%0Adots%3A%20true%2Carrows%3A%20true%2Cinfinite%3A%20true%2Cspeed%3A%20500%2CcssEase%3A%20%22linear%22%2CslidesToShow%3A%201%2Cautoplay%3A%20true%2CautoplaySpeed%3A%203000%2Cfade%3A%20false%2CadaptiveHeight%3A%20true%2Cresponsive%3A%20%5B%7Bbreakpoint%3A%20480%2Csettings%3A%20%7Barrows%3A%20false%2CslidesToShow%3A%201%7D%7D%5D%0A%7D)%3B%0A%7D)%3B%0A%3C%2Fscript%3E" data-settings="%5B%7B%22auto%22%3Atrue%2C%22arrow%22%3Atrue%2C%22dots%22%3Atrue%2C%22fade%22%3Afalse%2C%22height%22%3A%22400%22%2C%22images%22%3A%5B%7B%22src%22%3A%20%22assets%2Fminimalist-basic%2Fslider1-b.jpg%22%2C%20%22caption%22%3A%20%22%22%2C%20%22link%22%3A%20%22%22%2C%20%22width%22%3A%20%22450%22%2C%20%22position%22%3A%20%22bottom%20left%22%7D%2C%7B%22src%22%3A%20%22assets%2Fminimalist-basic%2Fslider1-a.jpg%22%2C%20%22caption%22%3A%20%22%22%2C%20%22link%22%3A%20%22%22%2C%20%22width%22%3A%20%22450%22%2C%20%22position%22%3A%20%22bottom%20left%22%7D%5D%7D%5D">

        </div>
    </div>
</div>

<div data-num="11" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c01.png" data-cat="0">

	<div class="row">

        <div class="col-md-12">
			 <p class="size-21 is-info2"><i>This is a special report</i></p>
             <h1 class="size-48 is-title1-48 is-title-bold is-upper">Lorem Ipsum is simply dummy text of the printing industry</h1>
        </div>

	</div>

</div>










<div data-num="18" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b01.png" data-cat="0">

	<div class="row">

        <div class="col-md-12">
			 <h1 class="size-48 is-title1-48 is-title-bold is-upper">Beautiful Content. Responsive.</h1>
             <p class="size-21"><i>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</i></p>
        </div>

	</div>

</div>



<div data-num="19" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b12.png" data-cat="0">

	<div class="row">

		<div class="col-md-12">

			<div class="center">

				<img src="<?= $assets_url ?>assets/minimalist-basic/b12-1.jpg" style="border-radius:500px">

			</div>

			<div class="display center">

				<h1>Lorem Ipsum is Simply</h1>

				<p>Lorem Ipsum is simply dummy text</p>

			</div>

			<p style="text-align: center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>

		</div>

	</div>

</div>



<div data-num="20" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/b14.png" data-cat="0">

	<div class="row">

		<div class="col-md-12">

			<div class="display center">

				<h1>Beautiful Content</h1>

				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>


			</div>

			<div class="center">

				<img src="<?= $assets_url ?>assets/minimalist-basic/b14-1.jpg"><img src="<?= $assets_url ?>assets/minimalist-basic/b14-2.jpg"><img src="<?= $assets_url ?>assets/minimalist-basic/b14-3.jpg">

			</div>

		</div>

	</div>

</div>
