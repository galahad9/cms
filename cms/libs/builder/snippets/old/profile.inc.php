<div data-num="44" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/s01.png" data-cat="16">

	<div class="row">

        	<div class="col-md-2 center">

                <p><img src="<?= $assets_url ?>assets/minimalist-basic/s01-1.jpg" style="border-radius:500px;margin-top:5px;"></p>

        	</div>

			<div class="col-md-10">

            		<p>

                	<b>Sara Phillipps</b><br>A freelance web designer &amp; developer based in Melbourne, Australia.

            		</p>

            		<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<!--<a href="#"><i class="icon ion-social-github"></i></a>

                		<a href="#"><i class="icon ion-social-dribbble"></i></a>

                		<a href="#"><i class="icon ion-social-pinterest"></i></a>

                		<a href="#"><i class="icon ion-social-linkedin"></i></a>

                		<a href="#"><i class="icon ion-social-instagram"></i></a>-->

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

           	 	</div>

       	</div>

	</div>

</div>



<div data-num="45" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/s03.png" data-cat="16">

	<div class="row">

		<div class="col-md-8">
            
			<img src="<?= $assets_url ?>assets/minimalist-basic/s03-1.jpg">

 		</div>

		<div class="col-md-4 center">

			<h2>Lorem Ipsum</h2>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

			<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

			</div>

       </div>

	</div>

</div>