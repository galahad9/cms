
<div data-num="128" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ef02.png" data-cat="0,27">

	<div class="row">

		<div class="col-md-12 center">

			<div class="display">

                <h1 style="font-size: 3em">Our Clients</h1>

            </div>

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

		    <div class="center" style="display:inline-block;width:18%">

                <img src="<?= $assets_url ?>assets/minimalist-basic/ef02-1.png">

			    <p>Company One</p>

		    </div>

		    <div class="center" style="display:inline-block;width:18%">

               <img src="<?= $assets_url ?>assets/minimalist-basic/ef02-2.png">

			    <p>Company Two</p>

		    </div>

		    <div class="center" style="display:inline-block;width:18%">

                <img src="<?= $assets_url ?>assets/minimalist-basic/ef02-3.png">

			    <p>Company Three</p>

		    </div>

		    <div class="center" style="display:inline-block;width:18%">

               <img src="<?= $assets_url ?>assets/minimalist-basic/ef02-4.png">

			    <p>Company Four</p>

		    </div>

		    <div class="center" style="display:inline-block;width:18%">

                <img src="<?= $assets_url ?>assets/minimalist-basic/ef02-5.png">

			    <p>Company Five</p>

		    </div>

        </div>

	</div>

</div>



<div data-num="129" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ef03.png" data-cat="0,27">

	<div class="row">

		<div class="col-md-12 center">

            <div class="display">

                <h1 style="font-size: 3.5em; margin:0.2em 0.5em">Our Partners</h1>

            </div>

        </div>

	</div>

	<div class="row">

		<div class="col-md-3 center">

            <img src="<?= $assets_url ?>assets/minimalist-basic/ef03-1.png">

			<p>Company One</p>

		</div>

		<div class="col-md-3 center">

            <img src="<?= $assets_url ?>assets/minimalist-basic/ef03-2.png">

			<p>Company Two</p>

		</div>

		<div class="col-md-3 center">

            <img src="<?= $assets_url ?>assets/minimalist-basic/ef03-3.png">

			<p>Company Three</p>

		</div>

		<div class="col-md-3 center">

            <img src="<?= $assets_url ?>assets/minimalist-basic/ef03-4.png">

			<p>Company Four</p>

		</div>

	</div>	

</div>


<div data-num="130" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ef05.png" data-cat="27">

	<div class="row">

		<div class="col-md-12 center">

            <h1>Our Partners</h1>

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem ipsum dolor sit amet, consectetur elit.</p>

        </div>

	</div>

	<div class="row">

		<div class="col-md-4 center">

             <img src="<?= $assets_url ?>assets/minimalist-basic/ef05-1.png">
             
			<p>Company One</p>

		</div>

		<div class="col-md-4 center">

            <img src="<?= $assets_url ?>assets/minimalist-basic/ef05-2.png">

			<p>Company Two</p>

		</div>

		<div class="col-md-4 center">

            <img src="<?= $assets_url ?>assets/minimalist-basic/ef05-3.png">

			<p>Company Three</p>

		</div>

	</div>	

</div>

<div data-num="131" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ef08.png" data-cat="27">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em;margin:1.5em 0">Our Clients</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12 center">
			<div class="center" style="display:inline-block;width:18%">
                    <img src="<?= $assets_url ?>assets/minimalist-basic/ef08-1.png">
			</div>
			<div class="center" style="display:inline-block;width:18%">
                    <img src="<?= $assets_url ?>assets/minimalist-basic/ef08-2.png">
			</div>
			<div class="center" style="display:inline-block;width:18%">
                    <img src="<?= $assets_url ?>assets/minimalist-basic/ef08-3.png">
			</div>
			<div class="center" style="display:inline-block;width:18%">
                    <img src="<?= $assets_url ?>assets/minimalist-basic/ef08-4.png">
			</div>
			<div class="center" style="display:inline-block;width:18%">
                    <img src="<?= $assets_url ?>assets/minimalist-basic/ef08-5.png">
			</div>
        </div>
	</div>
</div>

<div data-num="132" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ef09.png" data-cat="0,27">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em">Our Partners</h1>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3">
 			 <div class="is-card is-dark-text shadow-1">
				<div class="margin-20 center">
                    <img src="<?= $assets_url ?>assets/minimalist-basic/ef09-1.png">
				</div>
            </div>
		</div>

		<div class="col-md-3">
 			 <div class="is-card is-dark-text shadow-1">
				<div class="margin-20 center">
                    <img src="<?= $assets_url ?>assets/minimalist-basic/ef09-2.png">
				</div>
            </div>
		</div>

		<div class="col-md-3">
 			 <div class="is-card is-dark-text shadow-1">
				<div class="margin-20 center">
                    <img src="<?= $assets_url ?>assets/minimalist-basic/ef09-3.png">
				</div>
            </div>
		</div>

		<div class="col-md-3">
 			 <div class="is-card is-dark-text shadow-1">
				<div class="margin-20 center">
                    <img src="<?= $assets_url ?>assets/minimalist-basic/ef09-4.png">
				</div>
        	</div>
		</div>
	</div>
</div>



<div data-num="127" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ef01.png" data-cat="0,27">

	<div class="row">

		<div class="col-md-4">

			<h1>Our Clients</h1>

            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>

        </div>

		<div class="col-md-8">

			<div class="row">

				<div class="col-md-4 center">

                  	<img src="<?= $assets_url ?>assets/minimalist-basic/ef01-1.png">

					<p>Company One</p>

				</div>

				<div class="col-md-4 center">

                   <img src="<?= $assets_url ?>assets/minimalist-basic/ef01-2.png">

					<p>Company Two</p>

				</div>

				<div class="col-md-4 center">

                   <img src="<?= $assets_url ?>assets/minimalist-basic/ef01-3.png">

					<p>Company Three</p>

				</div>

            </div>

		</div>

	</div>

</div>

