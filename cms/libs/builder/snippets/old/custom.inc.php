

<div data-num="302" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/code.png" data-cat="0,100">
    <div class="row" data-module="code" data-dialog-width="80%" data-html="%3Cdiv%20class%3D%22col-md-12%22%3E%0A%0A%3Ch1%20id%3D%22{id}%22%3ELorem%20ipsum%3C%2Fh1%3E%0A%3Cp%3EThis%20is%20a%20code%20block.%20You%20can%20edit%20this%20block%20using%20the%20source%20dialog.%3C%2Fp%3E%0A%0A%3Cscript%3E%0A%2F*%20Example%20of%20script%20block%20*%2F%0Avar%20docReady%20%3D%20function%20(fn)%20%7B%0A%09var%20stateCheck%20%3D%20setInterval(function%20()%20%7B%0A%09%09if%20(document.readyState%20!%3D%3D%20%22complete%22)%20return%3B%0A%09%09clearInterval(stateCheck)%3B%0A%09%09try%7Bfn()%7Dcatch(e)%7B%7D%0A%09%7D%2C%201)%3B%0A%7D%3B%0A%0AdocReady(function()%20%7B%0A%09%24('%23{id}').html('%3Cb%3EHello%20World!%3C%2Fb%3E')%3B%0A%7D)%3B%0A%3C%2Fscript%3E%0A%0A%3C%2Fdiv%3E">
		<div class="col-md-12">

		</div>
    </div>
</div>


<div data-num="303" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de36.png" data-cat="100">
	<div class="placeholder placeholder-content">{{ CONTENT }}</div>
</div>




