
<div data-num="78" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/u10.png" data-cat="0,18">

	<div class="row">


		<div class="col-md-12 center">

            		    <div class="display">

                		    <h1 style="font-size: 3.5em">Beautiful Content. Responsive.</h1>

            		    </div>

                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

                </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

            		<div class="clearfix is-rounded-button-medium">

                		<a href="https://twitter.com/" style="background-color: #00bfff"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/" style="background-color: #128BDB"><i class="icon ion-social-facebook"></i></a>

						<a href="https://plus.google.com/" style="background-color: #DF311F"><i class="icon ion-social-googleplus"></i></a>           			

				</div>

       </div>

	</div>

</div>



<div data-num="79" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/u11.png" data-cat="0,18">

	<div class="row">

		<div class="col-md-12 center">

			<i class="icon ion-ios-heart-outline size-64"></i>

			<h1 class="size-21">BEAUTIFUL CONTENT</h1>

            		    <div class="display">

                		    <h1 style="font-size: 3.5em; margin:0.5em 0">Lorem Ipsum is simply dummy text</h1>

            		    </div>

                </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

            <div class="clearfix is-rounded-button-medium">

                	<a href="https://twitter.com/" style="background-color: #00bfff"><i class="icon ion-social-twitter"></i></a>

                	<a href="https://www.facebook.com/" style="background-color: #128BDB"><i class="icon ion-social-facebook"></i></a>

					<a href="https://plus.google.com/" style="background-color: #DF311F"><i class="icon ion-social-googleplus"></i></a>           			

			</div>

       </div>

	</div>

</div>



<div data-num="80" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/u12.png" data-cat="0,18">

	<div class="row">

		<div class="col-md-12">

			<div class="display">

                            <h1>Lorem Ipsum is simply text</h1>

			</div>

                        <p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

                </div>

	</div>

	<div class="row">

		<div class="col-md-12">
            <div class="clearfix is-boxed-button-big2">
                <a href="https://twitter.com/" style="background-color: #00bfff;"><i class="icon ion-social-twitter"></i></a>
                <a href="https://facebook.com/" style="background-color: #128BDB;"><i class="icon ion-social-facebook"></i></a>
                <a href="https://www.pinterest.com/" style="background-color: #E20000;"><i class="icon ion-social-pinterest-outline"></i></a>		
                <a href="mailto:you@example.com" style="background-color: #ff69B4;"><i class="icon ion-ios-email-outline"></i></a>
            </div>
        </div>

	</div>

</div>



<div data-num="81" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/u13.png" data-cat="0,18">
	<div class="row">
		<div class="col-md-12">
			<div class="display">
            	<h1 style="font-size:3.5em">Beautiful content. Responsive.</h1>
            	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
            <div class="is-social edit">
				<div class="size-24">
                	<a href="https://dribbble.com/"><i class="icon ion-social-dribbble-outline" style="margin-right: 1em"></i></a>
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
					<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://www.instagram.com/"><i class="icon ion-social-instagram-outline" style="margin-right: 1em"></i></a>
					<a href="https://pinterest.com/"><i class="icon ion-social-pinterest-outline"></i></a>
				</div>
			</div>
        </div>
	</div>
</div>



<div data-num="82" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/u14.png" data-cat="18">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em;letter-spacing:8px">BEAUTIFUL CONTENT. RESPONSIVE.</h1>
            <p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet.</p>
			<br>
			 <div class="is-social edit">
				<div class="size-24">
                	<a href="https://dribbble.com/"><i class="icon ion-social-dribbble-outline" style="margin-right: 1em"></i></a>
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
					<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://www.instagram.com/"><i class="icon ion-social-instagram-outline" style="margin-right: 1em"></i></a>
					<a href="https://pinterest.com/"><i class="icon ion-social-pinterest-outline"></i></a>
				</div>
			</div>
		</div>	
	</div>
</div>


<div data-num="52" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/u04.png" data-cat="0,18">

	<div class="row">

		<div class="col-md-12 center">


            <div class="clearfix is-boxed-button-big">
                <a href="https://twitter.com/" style="background-color: #00bfff;"><i class="icon ion-social-twitter"></i></a>
                <a href="https://facebook.com/" style="background-color: #128BDB;"><i class="icon ion-social-facebook"></i></a>
                <a href="https://www.youtube.com/" style="background-color: #E20000;"><i class="icon ion-social-youtube"></i></a>	
                <a href="http://www.website.com/" style="background-color: #0569AA;"><i class="icon ion-android-home"></i></a>		
                <a href="mailto:you@example.com" style="background-color: #ff69B4;"><i class="icon ion-ios-email-outline"></i></a>
            </div>

        </div>

	</div>

</div>
