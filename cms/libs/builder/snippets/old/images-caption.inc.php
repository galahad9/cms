
<div data-num="26" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/k06.png" data-cat="0,9">

	<div class="row">

		<div class="col-md-3">

			<img src="<?= $assets_url ?>assets/minimalist-basic/k06-1.jpg">

		</div>

		<div class="col-md-3">

			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

		</div>

		<div class="col-md-3">

			<img src="<?= $assets_url ?>assets/minimalist-basic/k06-2.jpg">

		</div>

		<div class="col-md-3">

			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

		</div>

	</div>

</div>



<div data-num="27" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/k02.png" data-cat="0,9">

	<div class="row">

		<div class="col-md-4">

            		<figure>

						<img src="<?= $assets_url ?>assets/minimalist-basic/k02-1.jpg">

                		<figcaption>

							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

						</figcaption>	

				</figure>

        	</div>

        	<div class="col-md-4">

            		<figure>

						<img src="<?= $assets_url ?>assets/minimalist-basic/k02-2.jpg">

                		<figcaption>

							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

						</figcaption>	

				</figure>

        	</div>

        	<div class="col-md-4">

            		<figure>

						<img src="<?= $assets_url ?>assets/minimalist-basic/k02-3.jpg">

                		<figcaption>

							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

						</figcaption>	

				</figure>

        	</div>

	</div>

</div>



<div data-num="28" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/k01.png" data-cat="0,9">

	<div class="row">

		<div class="col-md-6">

            		<figure>

						<img src="<?= $assets_url ?>assets/minimalist-basic/k01-1.jpg">

                		<figcaption>

							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

						</figcaption>	

					</figure>

        	</div>

        	<div class="col-md-6">

            		<figure>

						<img src="<?= $assets_url ?>assets/minimalist-basic/k01-2.jpg">

               	 		<figcaption>

							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

						</figcaption>	

					</figure>

        	</div>

	</div>

</div>
