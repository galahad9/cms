
<div data-num="213" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c02.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<p class="size-24 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-96 is-title1-96 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="214" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c03.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-64 is-title1-64 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="215" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c04.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			 <p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
             <h1 class="size-48 is-title1-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="216" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c05.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			 <p class="is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
             <h1 class="size-32 is-title1-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="217" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c06.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-96" style="line-height:1"></i>
            <p class="size-24 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title2-96 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="218" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c07.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-64" style="line-height:1"></i>
            <p class="size-21 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title2-64 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="219" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c08.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-64" style="line-height:1"></i>
            <p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title2-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="220" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c09.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-48" style="line-height:1"></i>
            <p class="is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title2-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="221" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c10.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title3-96 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="222" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c11.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title3-64 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="223" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c12.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title3-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="224" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c13.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title3-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="225" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c14.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div>
            	<h1 class="size-96 is-title4-96 is-title-lite" style="display:inline-block">Lorem Ipsum</h1>
			</div>
        </div>
	</div>
</div>

<div data-num="226" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c15.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div>
            	<h1 class="size-64 is-title4-64 is-title-lite" style="display:inline-block">Lorem Ipsum</h1>
        	</div>
        </div>
	</div>
</div>

<div data-num="227" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c16.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <div>
            	<h1 class="size-48 is-title4-48 is-title-lite" style="display:inline-block">LOREM IPSUM IS DUMMY TEXT</h1>
			</div>
        </div>
	</div>
</div>

<div data-num="228" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c17.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <div>
           		<h1 class="size-32 is-title4-32 is-title-lite" style="display:inline-block">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        	</div>
        </div>
	</div>
</div>

<!-- -->

<div data-num="229" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c18.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title5-96 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="230" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c19.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title5-64 is-title-lite">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="231" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c20.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title5-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="232" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c21.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title5-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="233" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c22.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<p class="size-24 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-96 is-title1-96 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="234" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c23.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-64 is-title1-64 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="235" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c24.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			 <p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
             <h1 class="size-48 is-title1-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="236" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c25.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			 <p class="is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
             <h1 class="size-32 is-title1-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="237" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c26.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-96" style="line-height:1"></i>
            <p class="size-24 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title2-96 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="238" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c27.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-64" style="line-height:1"></i>
            <p class="size-21 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title2-64 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="239" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c28.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-64" style="line-height:1"></i>
            <p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title2-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="240" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c29.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
			<i class="icon ion-ios-home-outline size-48" style="line-height:1"></i>
            <p class="is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title2-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="241" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c30.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title3-96 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="242" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c31.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title3-64 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="243" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c32.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title3-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="244" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c33.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title3-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>

<!-- -->

<div data-num="245" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c34.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div>
            	<h1 class="size-96 is-title4-96 is-title-bold" style="display:inline-block">Lorem Ipsum</h1>
        	</div>
        </div>
	</div>
</div>

<div data-num="246" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c35.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div>
            	<h1 class="size-64 is-title4-64 is-title-bold" style="display:inline-block">Lorem Ipsum</h1>
        	</div>
        </div>
	</div>
</div>

<div data-num="247" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c36.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <div>
            	<h1 class="size-48 is-title4-48 is-title-bold" style="display:inline-block">LOREM IPSUM IS DUMMY TEXT</h1>
			</div>
        </div>
	</div>
</div>

<div data-num="248" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c37.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <div>
            	<h1 class="size-32 is-title4-32 is-title-bold" style="display:inline-block">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        	</div>
        </div>
	</div>
</div>

<!-- -->

<div data-num="249" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c38.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-24 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-96 is-title5-96 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="250" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c39.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <h1 class="size-64 is-title5-64 is-title-bold">Lorem Ipsum</h1>
        </div>
	</div>
</div>

<div data-num="251" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c40.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="size-21 is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-48 is-title5-48 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
        </div>
	</div>
</div>

<div data-num="252" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/c41.png" data-cat="3">
	<div class="row">
		<div class="col-md-12">
            <p class="is-info2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <h1 class="size-32 is-title5-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
        </div>
	</div>
</div>