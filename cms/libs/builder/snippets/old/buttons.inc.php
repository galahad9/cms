

<!-- Buttons  -->

<div data-num="253" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce01.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost2 is-upper edit">Read More</a> &nbsp;

            <a href="#" class="is-btn is-btn-ghost1 is-upper edit">Buy Now</a>

        </div>
	</div>
</div>

<div data-num="254" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce02.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost2 is-upper edit">Read More</a>

        </div>
	</div>
</div>

<div data-num="255" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce03.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost1 is-upper edit">Buy Now</a>

        </div>
	</div>
</div>

<!-- -->

<div data-num="256" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce04.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost2 is-upper is-btn-small edit">Read More</a> &nbsp;

            <a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Buy Now</a>

        </div>
	</div>
</div>

<div data-num="257" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce05.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost2 is-upper is-btn-small edit">Read More</a>

        </div>
	</div>
</div>

<div data-num="258" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce06.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Buy Now</a>

        </div>
	</div>
</div>

<!-- -->

<div data-num="259" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce07.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost2 is-upper is-rounded-30 edit">Read More</a> &nbsp;

            <a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 edit">Buy Now</a>

        </div>
	</div>
</div>

<div data-num="260" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce08.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost2 is-upper is-rounded-30 edit">Read More</a>

        </div>
	</div>
</div>

<div data-num="261" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce09.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 edit">Buy Now</a>

        </div>
	</div>
</div>

<!-- -->

<div data-num="262" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce10.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost2 is-upper is-rounded-30 is-btn-small edit">Read More</a> &nbsp;

            <a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small edit">Buy Now</a>

        </div>
	</div>
</div>

<div data-num="263" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce11.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost2 is-upper is-rounded-30 is-btn-small edit">Read More</a>

        </div>
	</div>
</div>

<div data-num="264" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/ce12.png" data-cat="33">
	<div class="row">
		<div class="col-md-12">

            <a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small edit">Buy Now</a>

        </div>
	</div>
</div>