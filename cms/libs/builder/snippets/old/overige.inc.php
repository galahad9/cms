<div data-num="150" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/kl01.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">
			<i class="icon ion-android-time size-64"></i>
            <h1 class="size-64" style="font-weight:800;margin:0.2em 0">COMING SOON</h1>
        </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

            <p style="margin: 1.5em 0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

			<div class="clearfix is-rounded-button-big">

			    <a href="https://twitter.com/" style="background-color: #00bfff"><i class="icon ion-social-twitter"></i></a>

			    <a href="https://facebook.com/" style="background-color: #128BDB"><i class="icon ion-social-facebook"></i></a>

			    <a href="https://www.youtube.com/" style="background-color: #E20000"><i class="icon ion-social-youtube"></i></a>

			    <a href="http://www.website.com/" style="background-color: #0569AA"><i class="icon ion-ios-home"></i></a>

			    <a href="mailto:you@example.com" style="background-color: #ff69B4"><i class="icon ion-ios-email"></i></a>

			</div>

        </div>

	</div>

</div>



<div data-num="151" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/kl02.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">

			<i class="icon ion-ios-paperplane-outline size-80"></i>

            <h1 class="size-80" style="font-weight:800">UNDER CONSTRUCTION</h1>

        </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

            <div style="margin:1em 0 2.5em;">

            	<a href="#" class="btn btn-primary btn-lg edit">GET UPDATES</a>

            </div>

        </div>

	</div>

</div>



<div data-num="152" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/kl03.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">

			<h3>Company Name</h3>

            <h1 class="size-80" style="font-weight:800;">COMING SOON</h1>

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

        </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

            <div class="clearfix is-rounded-button-medium">

                <a href="https://twitter.com/" style="background-color: #00bfff"><i class="icon ion-social-twitter"></i></a>

                <a href="https://www.facebook.com/" style="background-color: #128BDB"><i class="icon ion-social-facebook"></i></a>

				<a href="https://plus.google.com/" style="background-color: #DF311F"><i class="icon ion-social-googleplus"></i></a>

			</div>

        </div>

	</div>

</div>



<div data-num="153" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/kl04.png" data-cat="12">

	<div class="row">

		<div class="col-md-12 center">

			<div class="display">

                <h1 style="font-size: 3.5em">We are coming very soon</h1>

			</div>

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

			<div style="margin:2em 0">

            	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">GET UPDATES</a>

            </div>

        </div>

	</div>

	<div class="row">

		<div class="col-md-4">

    		<div class="row">

				<div class="col-md-3">

 					<p style="line-height:1"><i class="icon ion-ios-location-outline size-32"></i></p>

				</div>

				<div class="col-md-9">

                    <p>

              			<strong>Company, Inc.</strong><br>

              			123 Street, City<br>

              			State 12345

            		</p>

				</div>

     		</div>

		</div>

		<div class="col-md-4">

			<div class="row">

				<div class="col-md-3">

 					<p style="line-height:1"><i class="icon ion-ios-telephone-outline size-32"></i></p>

				</div>

				<div class="col-md-9">

                    <p><b>Call us</b><br>(123) 456 7890<br>456 7891</p>

				</div>

         	</div>

		</div>

		<div class="col-md-4">

			<div class="row">

				<div class="col-md-3">

					<p style="line-height:1"><i class="icon ion-ios-email-outline size-32"></i></p>

				</div>

				<div class="col-md-9">

                    <p>

              		    <strong>Email</strong><br>

              		    <a href="mailto:#">first.last@example.com</a>

            		</p>

				</div>

         	</div>

 		</div>

	</div>

</div>



<div data-num="154" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/kl05.png" data-cat="12">

	<div class="row">

		<div class="col-md-12 center">

            <div class="display">

                <h1 style="font-size: 4.3em">Under Construction</h1>

		        <p>Lorem Ipsum is simply dummy text of the printing industry.</p>

            </div>

        </div>	

	</div>

	<div class="row">

		<div class="col-md-12 center">

			<div>

            	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">Get Notified</a>

            </div>

		</div>

	</div>

	<div class="row">

		<div class="col-md-12">

           	<br>

        </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

			<div class="is-social edit">
				<div class="size-24">
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>

                	<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>

                	<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>

                	<a href="https://www.pinterest.com/"><i class="icon ion-social-pinterest"></i></a>
				</div>
			</div>


		</div>

	</div>

</div>


<div data-num="155" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/kl07.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1" style="width:100%;max-width:450px;margin: 0 auto">
				<div class="margin-30 center">
					<i class="icon ion-ios-alarm-outline size-64"></i>
                	<h1 style="margin-top:0">Coming Soon!</h1>
                	<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<br>
					<div class="is-social edit">
						<div class="size-21">
                			<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                			<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                			<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                			<a href="https://www.instagram.com/"><i class="icon ion-social-instagram-outline"></i></a>					
						</div>      
					</div>   
				</div>
        	</div>
		</div>
	</div>
</div>

<div data-num="156" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/kl08.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
                    <h1 style="font-size:3.5em">WE ARE CURRENTLY UNDER CONSTRUCTION</h1>
			</div>
            <p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12 center">
            <div class="clearfix is-boxed-button-small" style="margin:1.5em 0">
                <a href="https://twitter.com/" style="background-color: #00bfff;"><i class="icon ion-social-twitter"></i></a>
                <a href="https://facebook.com/" style="background-color: #128BDB;"><i class="icon ion-social-facebook"></i></a>		
                <a href="https://www.pinterest.com/" style="background-color: #E20000;"><i class="icon ion-social-pinterest"></i></a>
            </div>
        </div>
	</div>
</div>

<div data-num="157" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/kl09.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1" style="width:100%;max-width:700px;margin:0 auto;">
				<div class="margin-30 center">
					<h1 style="font-size:3em">COMING SOON</h1>
                	<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<div style="margin:2em 0;">
            			<a href="#" class="btn btn-default btn-lg edit" style="border-radius:50px">Read More</a>
            		</div>
					<div style="margin:1em 0">
            			<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Get Updates</a>          		
            		</div>
				</div>
        	</div>
		</div>
	</div>
</div>

<div data-num="158" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/kl10.png" data-cat="12">
	<div class="row">
		<div class="col-md-12 center">
			<i class="icon ion-android-bicycle size-64"></i>
            <h1 class="size-64" style="margin:0.2em 0">WE ARE COMING VERY SOON</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
            <br><br>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12 center">
            <h3>Contact Us</h3>
        	<p>Company, Inc. 123 Street, City. State 12345. <br>(123) 456 7890 / 456 7891</p>
        </div>
	</div>
</div>


<div data-num="159" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/mn01.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">

			<i class="icon ion-android-alert size-64"></i>
            <h1 class="size-64" style="font-weight:800">PAGE NOT FOUND</h1>
            <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

       </div>	

	</div>

	<div class="row">

		<div class="col-md-12 center">

			<div>

            	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">Back to Home Page</a>

           </div>

		</div>	

	</div>

</div>



<div data-num="160" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/mn02.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">

            <div class="display">

                <h1 style="font-size: 9em">404</h1>

            </div>

			 <h1>PAGE NOT FOUND</h1>

            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

        </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">
			<div class="clearfix is-rounded-button-medium">

				<a href="http://www.website.com/" style="background-color: #fff;"><i style="color:#000" class="icon ion-ios-home"></i></a>
			</div>

		</div>

	</div>

</div>



<div data-num="161" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/mn03.png" data-cat="0,12">

	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1" style="border-radius:30px;width:100%;max-width:400px;margin: 0 auto;">
				<div class="margin-30 center">
					<div class="display">
						<h1><span style="font-size: 3em">404</span></h1>
					</div>
            		<h5>The page you are looking for doesn't exist.</h5>
 					<div style="margin:2.2em 0">
            			<a href="#" class="btn btn-primary btn-lg edit">Back Home</a>
            		</div>
        		</div>
			</div>
		</div>
	</div>

</div>



<div data-num="162" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/mn04.png" data-cat="0,12">

    <div class="row">

        <div class="col-md-12 center">

             <i class="icon ion-ios-pulse size-80"></i>

            <div class="display">

                <h1 style="font-size: 4em">Oops...</h1>

            </div>

            <h1 style="font-size: 3em">Page Not Found</h1>

        </div>

    </div>

</div>



<div data-num="163" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/mn05.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">

            <div class="display">

                <h1 style="font-size: 9em">404</h1>

            </div>

			 <h1>PAGE NOT FOUND</h1>

            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

        </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

            <div style="margin:2em 0">

            	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">BACK HOME</a>

            </div>

        </div>

	</div>

</div>

<div data-num="164" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/mn08.png" data-cat="12">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:6em">Error <span style="font-weight:800">404</span></h1>
            <h3 style="margin-top:0">The page you are looking for doesn't exist.</h3>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12 center">
				<a href="#"><i class="icon ion-ios-home size-64"></i></a>
		</div>
	</div>
</div>

<div data-num="165" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/mn09.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
             <i class="icon ion-android-sad size-80"></i>
            <h1 style="font-size:3em;margin-top:0">Sorry, this page doesn't exist.</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12 center">
            <div style="margin:1em 0 2.5em;">
            	<a href="#">BACK TO HOMEPAGE</a>
            </div>
        </div>
	</div>
</div>

<div data-num="25" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/o01.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12">

            <img src="<?= $assets_url ?>assets/minimalist-basic/o01-1.jpg">

        </div>

	</div>

</div>








<div data-num="119" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/cd01.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">

            <h1 style="font-size: 3em">How it Works</h1>

        </div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
				<i class="icon ion-ios-paperplane size-80"></i>

				<h3>Lorem Ipsum</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

				<div style="margin-top: 2em">

            		<a href="#" class="btn btn-default btn-lg edit" style="border-radius: 50px">Read More</a>

				</div>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<i class="icon ion-ios-compose-outline size-80"></i>

				<h3>Lorem Ipsum</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

				<div style="margin-top: 2em">

            		<a href="#" class="btn btn-default btn-lg edit" style="border-radius: 50px">Read More</a>

				</div>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<i class="icon ion-archive size-80"></i>

				<h3>Lorem Ipsum</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

				<div style="margin-top: 2em">

            		<a href="#" class="btn btn-default btn-lg edit" style="border-radius: 50px">Read More</a>

				</div>
			</div>
        </div>

	</div>

</div>



<div data-num="120" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/cd03.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">

            <h1 style="font-size: 3em">How it Works</h1>

        </div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">

				<h4>Step<span style="font-size: 4em"><b>1</b></span></h4>

				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
		</div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<h4>Step <span style="font-size: 4em"><b>2</b></span></h4>

				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
		</div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<h4>Step <span style="font-size: 4em"><b>3</b></span></h4>

				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
		</div>

	</div>

</div>



<div data-num="121" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/cd04.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">

            <h1 style="font-size: 3em">The Process</h1>

        </div>

	</div>

	<div class="row">

		<div class="col-md-6 center">
			<div class="padding-20">
				<i class="icon ion-ios-chatboxes-outline size-96"></i>

				<h3>STEP 1</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
			</div>
        </div>

		<div class="col-md-6 center">
			<div class="padding-20">
				<i class="icon ion-edit size-96"></i>

				<h3>STEP 2</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
			</div>
        </div>

	</div>

	<div class="row">

		<div class="col-md-6 center">
			<div class="padding-20">
				<i class="icon ion-ios-gear-outline size-96"></i>

				<h3>STEP 3</h3>

           	 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
			</div>
        </div>

		<div class="col-md-6 center">
			<div class="padding-20">
				<i class="icon ion-ios-heart-outline size-96"></i>

				<h3>STEP 4</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
			</div>
        </div>

	</div>

</div>


<div data-num="122" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/cd06.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em;margin:1.5em 0">How it Works</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25">
			   		<i class="icon ion-ios-compose-outline size-64"></i>			
			    	<h3 style="margin-top:0">Step One</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            	</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25">
			    	<i class="icon ion-ios-gear-outline size-64"></i>				
			    	<h3 style="margin-top:0">Step Two</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        		</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25">
			   		<i class="icon ion-ios-paperplane-outline size-64"></i>			
			   		<h3 style="margin-top:0">Step Three</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        		</div>
        	</div>
		</div>
	</div>
</div>

<div data-num="123" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/cd07.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
				<h1>Work Steps</h1>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-3">
			<h3 style="font-size:3em;font-weight:800">1</h3>
            <p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>

		<div class="col-md-3">
			<h3 style="font-size:3em;font-weight:800">2</h3>
            <p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>

		<div class="col-md-3">
			<h3 style="font-size:3em;font-weight:800">3</h3>
            <p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>

		<div class="col-md-3">
			<h3 style="font-size:3em;font-weight:800">4</h3>
            <p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>
	</div>
</div>

<div data-num="124" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/cd08.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em;margin:1.5em 0">How it Works</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-2">
 					<p><i class="icon ion-android-bulb size-48" style="margin-top:0;line-height:1"></i></p>
				</div>
				<div class="col-md-10">
                    <p style="margin-top:0"><span style="font-size:1.8em">Step One</span><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div>
			</div>
        </div>

		<div class="col-md-6">
			<div class="row">
				<div class="col-md-2">
					<p><i class="icon ion-android-settings size-48" style="margin-top:0;line-height:1"></i></p>
				</div>
				<div class="col-md-10">
                    <p style="margin-top:0"><span style="font-size:1.8em">Step Two</span><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-2">
 					<p><i class="icon ion-android-favorite-outline size-48" style="margin-top:0;line-height:1"></i></p>
				</div>
				<div class="col-md-10">
                    <p style="margin-top:0"><span style="font-size:1.8em">Step Three</span><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div>
			</div>
        </div>

		<div class="col-md-6">
			<div class="row">
				<div class="col-md-2">
					<p><i class="icon ion-paper-airplane size-48" style="margin-top:0;line-height:1"></i></p>
				</div>
				<div class="col-md-10">
                    <p style="margin-top:0"><span style="font-size:1.8em">Step Four</span><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div data-num="125" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/cd09.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
			<i class="icon ion-ios-gear-outline size-64"></i>
            <h1 class="size-64" style="margin-top:0">How it Works</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4 center">
            <h3 style="font-size:2.7em;font-weight:800">1</h3>
            <p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>

		<div class="col-md-4 center">
            <h3 style="font-size:2.7em;font-weight:800">2</h3>
            <p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>

		<div class="col-md-4 center">
            <h3 style="font-size:2.7em;font-weight:800">3</h3>
            <p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
	</div>
</div>

<div data-num="126" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/cd10.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="margin:1.5em 0;font-size:3.2em">How it Works</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">
			    	<h1>Step 01</h1>
 					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
 			    	<div style="margin:2em 0">
            	    	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Read More</a>
                	</div>
            	</div>
        	</div>
		</div>

		<div class="col-md-6">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">
			    	<h1>Step 02</h1>
 					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
 			    	<div style="margin:2em 0">
            	   	 	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Read More</a>
                	</div>
              	</div>
        	</div>
		</div>
	</div>
</div>





<div data-num="133" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/gh01.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">

			<h1>As Featured On</h1>

        </div>	

	</div>	

	<div class="row">

		<div class="col-md-4">

			<div class="row">

				<div class="col-md-6 center">

                    <p style="line-height:1;margin-top:0">
                        <img src="<?= $assets_url ?>assets/minimalist-basic/gh01-1.png">
                    </p>

				</div>

				<div class="col-md-6">

					<p style="line-height:1;margin-top:30px; font-size:15px;">Company One</p>

				</div>

			</div>

		</div>

		<div class="col-md-4">

			<div class="row">

				<div class="col-md-6 center">

                    <p style="line-height:1;margin-top:0">
                        <img src="<?= $assets_url ?>assets/minimalist-basic/gh01-2.png">
                    </p>

				</div>

				<div class="col-md-6">

					<p style="line-height:1;margin-top:30px; font-size:15px;">Company Two</p>

				</div>

			</div>

		</div>

		<div class="col-md-4">

			<div class="row">

				<div class="col-md-6 center">

                    <p style="line-height:1;margin-top:0">
                        <img src="<?= $assets_url ?>assets/minimalist-basic/gh01-3.png">
                    </p>

				</div>

				<div class="col-md-6">

					<p style="line-height:1;margin-top:30px; font-size:15px;">Company Three</p>

				</div>

			</div>

		</div>

	</div>

</div>



<div data-num="134" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/gh02.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">

			<h1>As Featured On</h1>

        </div>

	</div>

	<div class="row">

		<div class="col-md-3 center">

            <img src="<?= $assets_url ?>assets/minimalist-basic/gh02-1.png">

			<p>Company One</p>

		</div>

		<div class="col-md-3 center">

            <img src="<?= $assets_url ?>assets/minimalist-basic/gh02-2.png">

			<p>Company Two</p>

		</div>

		<div class="col-md-3 center">

            <img src="<?= $assets_url ?>assets/minimalist-basic/gh02-3.png">

		    <p>Company Three</p>

		</div>

		<div class="col-md-3 center">

            <img src="<?= $assets_url ?>assets/minimalist-basic/gh02-4.png">

		    <p>Company Four</p>

		</div>

	</div>

</div>



<div data-num="135" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/gh03.png" data-cat="0,12">

	<div class="row">

		<div class="col-md-12 center">

			<h1>As Featured On</h1>

        </div>

	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
          			<img src="<?= $assets_url ?>assets/minimalist-basic/gh03-1.png">
				</div>
				<div class="col-md-8">
						<h3 style="margin-top:20px">Company One</h3>
				</div>
			</div>
		</div>	
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
          			<img src="<?= $assets_url ?>assets/minimalist-basic/gh03-2.png">
				</div>
				<div class="col-md-8">
					<h3 style="margin-top:20px">Company Two</h3>
				</div>
			</div>
		</div>
	</div>

	<div class="row">

		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
          			<img src="<?= $assets_url ?>assets/minimalist-basic/gh03-3.png">
				</div>
				<div class="col-md-8">
						<h3 style="margin-top:20px">Company Three</h3>
				</div>
			</div>
		</div>	
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
          			<img src="<?= $assets_url ?>assets/minimalist-basic/gh03-4.png">
				</div>
				<div class="col-md-8">
					<h3>Company Four</h3>
				</div>
			</div>
		</div>
	</div>
</div>



<div data-num="136" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/gh05.png" data-cat="12">

	<div class="row">

		<div class="col-md-12 center">

			<h1>As Featured On</h1>

		</div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/gh05-1.png">

				<h3>Company One</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/gh05-2.png">

				<h3>Company Two</h3>

        		<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/gh05-3.png">

				<h3>Company Three</h3>

    			<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
			</div>
    	</div>

	</div>

</div>

<div data-num="137" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/gh07.png" data-cat="12">
	<div class="row">
		<div class="col-md-12">
            <h1 style="font-size:3em;margin:1.2em 0">As Featured On</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
			<img src="<?= $assets_url ?>assets/minimalist-basic/gh07-1.png">
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>

		<div class="col-md-4">
			<img src="<?= $assets_url ?>assets/minimalist-basic/gh07-2.png">
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>

		<div class="col-md-4">
			<img src="<?= $assets_url ?>assets/minimalist-basic/gh07-3.png">
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>
	</div>
</div>

<div data-num="138" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/gh08.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em;margin:1.2em 0">As Featured On</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            <img src="<?= $assets_url ?>assets/minimalist-basic/gh08-1.png">
			<h3 style="font-size:1.5em;margin-top:0">Company One</h3>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>

		<div class="col-md-6">
            <img src="<?= $assets_url ?>assets/minimalist-basic/gh08-2.png">
			<h3 style="font-size:1.5em;margin-top:0">Company Two</h3>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            <img src="<?= $assets_url ?>assets/minimalist-basic/gh08-3.png">
			<h3 style="font-size:1.5em;margin-top:0">Company Three</h3>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>

		<div class="col-md-6">
            <img src="<?= $assets_url ?>assets/minimalist-basic/gh08-4.png">
			<h3 style="font-size:1.5em;margin-top:0">Company Four</h3>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>
	</div>
</div>

<div data-num="139" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/gh09.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em;margin:1.2em 0">As Featured On</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			   		<img src="<?= $assets_url ?>assets/minimalist-basic/gh09-1.png">			
			    	<h3>Company One</h3> 
            	</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			    	<img src="<?= $assets_url ?>assets/minimalist-basic/gh09-2.png">			
			    	<h3>Company Two</h3>			  
        		</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			  		<img src="<?= $assets_url ?>assets/minimalist-basic/gh09-3.png">		
			 		<h3>Company Three</h3>			 
        		</div>
         	</div>
		</div>
	</div>
</div>

<div data-num="166" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/op01.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em;margin:1.5em 0">OUR SKILLS</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
			<div class="is-card is-card-circle is-dark-text shadow-1" style="width:140px;height:140px;padding:15px">
  				<div class="is-card-content-centered">
                	<h2 style="font-size:40px;font-weight:800;">98%</h2>
				</div>
			</div>
            <h3 style="font-size:1.5em;text-align:center">Web Design</h3>
        </div>

		<div class="col-md-4">
			<div class="is-card is-card-circle is-dark-text shadow-1" style="width:140px;height:140px;padding:15px">
  				<div class="is-card-content-centered">
                	<h2 style="font-size:40px;font-weight:800;">87%</h2>
				</div>
			</div>
            <h3 style="font-size:1.5em;text-align:center">Web Development</h3>
        </div>

		<div class="col-md-4">
			<div class="is-card is-card-circle is-dark-text shadow-1" style="width:140px;height:140px;padding:15px">
  				<div class="is-card-content-centered">
                	<h2 style="font-size:40px;font-weight:800;">100%</h2>
				</div>
			</div>
            <h3 style="font-size:1.5em;text-align:center">Customer Support</h3>
        </div>
	</div>
</div>

<div data-num="167" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/op02.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
            <h1>Professional Skills</h1>
            <p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3 center">
            <h1 style="font-weight:800">93%</h1>
            <h3 style="font-size:1.2em">HTML &amp; CSS</h3>
        </div>

		<div class="col-md-3 center">
            <h1 style="font-weight:800">71%</h1>
            <h3 style="font-size:1.2em">JavaScript</h3>
        </div>

		<div class="col-md-3 center">
            <h1 style="font-weight:800">90%</h1>
            <h3 style="font-size:1.2em">PHP</h3>
        </div>

		<div class="col-md-3 center">
            <h1 style="font-weight:800">85%</h1>
            <h3 style="font-size:1.2em">Photoshop</h3>
        </div>
	</div>
</div>

<div data-num="168" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/op03.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 display center">
            <h1>Professional Skills</h1>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
			<div class="row">
            	<div class="col-md-4 center">
            		<p style="line-height:1;margin-top:0;">
						<i class="icon ion-android-favorite-outline size-64"></i>
					</p>
				</div>
				<div class="col-md-8 center">
					<p style="line-height:1;margin-top:0"><span style="font-size: 2.5em"><b>89%</b></span><br><br>Web Design</p>
				</div>
        	</div>
		</div>
		<div class="col-md-4">
			<div class="row">
            	<div class="col-md-4 center">
            		<p style="line-height:1;margin-top:0;">
						<i class="icon ion-code size-64"></i>
					</p>
				</div>
				<div class="col-md-8 center">
					<p style="line-height:1;margin-top:0"><span style="font-size: 2.5em"><b>92%</b></span><br><br>HTML &amp; CSS</p>
				</div>
        	</div>
		</div>
		<div class="col-md-4">
			<div class="row">
            	<div class="col-md-4 center">
            		<p style="line-height:1;margin-top:0;">
						<i class="icon ion-android-settings size-64"></i>
					</p>
				</div>
				<div class="col-md-8 center">
					<p style="line-height:1;margin-top:0"><span style="font-size: 2.5em"><b>81%</b></span><br><br>Marketing</p>
				</div>
        	</div>
		</div>
	</div>
</div>

<div data-num="169" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/op04.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
				<h1 style="margin:1.2em 0">TEAM SKILLS</h1>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-4 center">
			<h2><span style="font-weight:800;font-size:2.2em">89</span>%</h2>
			<h3>Web Design</h3>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>
        
        <div class="col-md-4 center">
			<h2><span style="font-weight:800;font-size:2.2em">91</span>%</h2>
			<h3>PHP</h3>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>
        
		<div class="col-md-4 center">
			<h2><span style="font-weight:800;font-size:2.2em">95</span>%</h2>
			<h3>HTML &amp; CSS</h3>
            <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
        </div>
	</div>
</div>

<div data-num="170" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/op05.png" data-cat="0,12">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em">OUR SKILLS</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1" style="margin:0 auto">
				<div class="margin-25 center">
					<h1><span style="font-weight:800;font-size:2.5em">92</span>%</h1>
					<h3>UX/UI Design</h3>
				</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1" style="margin:0 auto">
				<div class="margin-25 center">
					<h1><span style="font-weight:800;font-size:2.5em">88</span>%</h1>
					<h3>Development</h3>
				</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1" style="margin:0 auto">
				<div class="margin-25 center">
					<h1><span style="font-weight:800;font-size:2.5em">65</span>%</h1>
					<h3>Branding</h3>
				</div>
        	</div>
		</div>
	</div>
</div>

<div data-num="171" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/op07.png" data-cat="12">
	<div class="row">
		<div class="col-md-4">
            <h2 style="font-size:2.2em;margin-top:0.2em">Our Skills</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>
        </div>

		<div class="col-md-8">
			<div class="row">
				<div class="col-md-6">
					<div class="is-card is-card-circle is-dark-text shadow-1" style="width:120px;height: 120px;padding:15px">
  						<div class="is-card-content-centered">
                			<h2 style="font-size:35px;font-weight:800;">85%</h2>
						</div>
					</div>
                    <h3 style="text-align:center">Web Design</h3>
				</div>
				<div class="col-md-6">
					<div class="is-card is-card-circle is-dark-text shadow-1" style="width:120px;height: 120px;padding:15px">
  						<div class="is-card-content-centered">
                			<h2 style="font-size:35px;font-weight:800;">91%</h2>
						</div>
					</div>
                	<h3 style="text-align:center">HTML &amp; CSS</h3>
				</div>
			</div>
		</div>
	</div>
</div>

<div data-num="172" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/op08.png" data-cat="12">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em">Knowledge</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4 center">
                <i class="icon ion-ios-heart-outline size-64"></i>
                <h2 style="font-size:1.3em;margin-top:0"><strong>92%</strong>, Web Design</h2>
        </div>

		<div class="col-md-4 center">
            <i class="icon ion-ios-film-outline size-64"></i>
            <h2 style="font-size:1.3em;margin-top:0"><strong>78%</strong>, Video Editing</h2>
        </div>

		<div class="col-md-4 center">
            <i class="icon ion-ios-gear-outline size-64"></i>
            <h2 style="font-size:1.3em;margin-top:0"><strong>89%</strong>, Web Development</h2>
        </div>
	</div>
</div>
