<div data-num="<?= $cat ?>0001" data-thumb="<?= $assets_url ?>thumbnails/services-0001.png" data-cat="<?= $cat ?>">
	<div class="feature2">
        <div class="container">
            <!-- Row  -->
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <span class="label label-info label-rounded">Feature 2</span>
                    <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                    <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
                </div>
            </div>
            <!-- Row  -->
            <div class="row m-t-40">
                <!-- Column -->
                <div class="col-md-4 wrap-feature2-box">
                    <div class="card card-shadow" data-aos="flip-left" data-aos-duration="1200">
                        <img class="card-img-top" src="/cms/assets/contentbox/assets/images/owl.jpg" alt="wrappixel kit" />
                        <div class="card-body text-center">
                            <h5 class="font-medium">Retargeting Market</h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-md-4 wrap-feature2-box">
                    <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                        <img class="card-img-top" src="/cms/assets/contentbox/assets/images/owl.jpg" alt="wrappixel kit" />
                        <div class="card-body text-center">
                            <h5 class="font-medium">Fruitful Results</h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-md-4 wrap-feature2-box">
                    <div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
                        <img class="card-img-top" src="/cms/assets/contentbox/assets/images/owl.jpg" alt="wrappixel kit" />
                        <div class="card-body text-center">
                            <h5 class="font-medium">Instant Solutions</h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 m-t-20 text-center">
                    <a class="btn btn-primary-gradiant btn-md btn-arrow"href="#"><span>View Feature2 code <i class="ti-arrow-right"></i></span></a>
                </div>
            </div>
        </div>
    </div>
</div>


<div data-num="<?= $cat ?>0002" data-thumb="<?= $assets_url ?>thumbnails/services-0002.png" data-cat="<?= $cat ?>">
	<div class="feature3">
        <div class="container">
            <!-- Row  -->
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <span class="label label-success label-rounded">Feature 3</span>
                    <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                    <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
                </div>
            </div>
            <!-- Row  -->
            <div class="row m-t-40">
                <!-- Column -->
                <div class="col-md-6 wrap-feature3-box">
                    <div class="card card-shadow" data-aos="fade-right" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space align-self-center"><i class="icon-Double-Circle display-2 text-success-gradiant"></i></div>
                            <div class="align-self-center">
                                <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Instant Solutions <i class="ti-arrow-right"></i></a></h5>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-6 wrap-feature3-box">
                    <div class="card card-shadow" data-aos="fade-left" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space align-self-center"><i class="icon-Stopwatch display-2 text-success-gradiant"></i></div>
                            <div class="align-self-center">
                                <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Powerful Techniques <i class="ti-arrow-right"></i></a></h5>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-6 wrap-feature3-box">
                    <div class="card card-shadow" data-aos="fade-right" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space align-self-center"><i class="icon-Thumbs-UpSmiley display-2 text-success-gradiant"></i></div>
                            <div class="align-self-center">
                                <h5 class="font-medium"><a href="javascript:void(0)" class="linking">100% Satisfaction <i class="ti-arrow-right"></i></a></h5>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-6 wrap-feature3-box">
                    <div class="card card-shadow" data-aos="fade-left" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space align-self-center"><i class="icon-Window-2 display-2 text-success-gradiant"></i></div>
                            <div class="align-self-center">
                                <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Retargeting Market <i class="ti-arrow-right"></i></a></h5>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-md-12 m-t-20 text-center">
                    <a class="btn btn-primary-gradiant btn-md btn-arrow" href="#"><span>View Feature3 code <i class="ti-arrow-right"></i></span></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div data-num="<?= $cat ?>0003" data-thumb="<?= $assets_url ?>thumbnails/services-0003.png" data-cat="<?= $cat ?>">
	<div class="feature4">
        <div class="container">
            <!-- Row  -->
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <span class="label label-danger label-rounded">Feature 4</span>
                    <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                    <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
                </div>
            </div>
            <!-- Row  -->
            <div class="row m-t-40">
                <!-- Column -->
                <div class="col-md-6 wrap-feature4-box">
                    <div class="card" data-aos="zoom-out-right" data-aos-duration="1200">
                        <div class="card-body">
                            <div class="icon-round bg-light-info"><i class="icon-Clown"></i></div>
                            <h5 class="font-medium">Instant Solutions</h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tristique pellentesque ipsum.</p>
                            <a href="#" class="linking text-themecolor">Check the Feature4 Code <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-6 wrap-feature4-box">
                    <div class="card" data-aos="zoom-out-left" data-aos-duration="1200">
                        <div class="card-body">
                            <div class="icon-round bg-light-info"><i class="icon-Mouse-3"></i></div>
                            <h5 class="font-medium">Powerful Techniques </h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tristique pellentesque ipsum. </p>
                            <a class="linking text-themecolor" href="#">Check the Feature4 Code <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div data-num="<?= $cat ?>0004" data-thumb="<?= $assets_url ?>thumbnails/services-0004.png" data-cat="<?= $cat ?>">
	<div class="feature5">
        <div class="container">
            <!-- Row  -->
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <span class="label label-success label-rounded">Feature 5</span>
                    <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                    <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
                </div>
            </div>
            <!-- Row  -->
            <div class="row m-t-40">
                <!-- Column -->
                <div class="col-md-4 wrap-feature5-box">
                    <div class="card card-shadow" data-aos="fade-right" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space"><i class="text-success-gradiant icon-Stopwatch"></i></div>
                            <div class="">
                                <h6 class="font-medium"><a href="javascript:void(0)" class="linking">Instant Solutions</a></h6>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-4 wrap-feature5-box">
                    <div class="card card-shadow" data-aos="fade-down" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space"><i class="text-success-gradiant icon-Information"></i></div>
                            <div class="">
                                <h6 class="font-medium"><a href="javascript:void(0)" class="linking">Powerful Tech </a></h6>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-4 wrap-feature5-box">
                    <div class="card card-shadow" data-aos="fade-left" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space"><i class="text-success-gradiant icon-Leo-2"></i></div>
                            <div class="">
                                <h6 class="font-medium"><a href="javascript:void(0)" class="linking">100% Satisfaction </a></h6>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-4 wrap-feature5-box">
                    <div class="card card-shadow" data-aos="fade-right" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space"><i class="text-success-gradiant icon-Target-Market"></i></div>
                            <div class="">
                                <h6 class="font-medium"><a href="javascript:void(0)" class="linking">Targeting Market</a></h6>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-4 wrap-feature5-box">
                    <div class="card card-shadow" data-aos="fade-up" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space"><i class="text-success-gradiant icon-Sunglasses-Smiley"></i></div>
                            <div class="">
                                <h6 class="font-medium"><a href="javascript:void(0)" class="linking">Goal Achievement </a></h6>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-4 wrap-feature5-box">
                    <div class="card card-shadow" data-aos="fade-left" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space"><i class="text-success-gradiant  icon-Laptop-Phone"></i></div>
                            <div class="">
                                <h6 class="font-medium"><a href="javascript:void(0)" class="linking">Fully Responsive</a></h6>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-md-12 m-t-20 text-center">
                    <a class="btn btn-success-gradiant btn-md btn-arrow" href="#"><span>View Feature5 code <i class="ti-arrow-right"></i></span></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div data-num="<?= $cat ?>0005" data-thumb="<?= $assets_url ?>thumbnails/services-0005.png" data-cat="<?= $cat ?>">
	<div class="bg-light spacer feature8">
        <div class="container">
            <!-- Row -->
            <div class="row">
                <!-- Column -->
                <div class="col-lg-6">
                    <small class="text-info">Success Stories</small>
                    <h3>Re-Targeting gets easier with WrapKit</h3>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                    <ul class="list-block m-b-30">
                        <li><i class="sl-icon-check text-info"></i> Powerful and Faster Results for your site</li>
                        <li><i class="sl-icon-check text-info"></i> Make your site in no-time with your Bootstrap WrapKit</li>
                        <li><i class="sl-icon-check text-info"></i> Tons of Features and Elements available here</li>
                        <li><i class="sl-icon-check text-info"></i> What are you wait for? Go and Get it.</li>
                    </ul>
                    <a class="btn btn-info-gradiant btn-md btn-arrow" href="#"><span>View Feature8 code <i class="ti-arrow-right"></i></span></a>
                </div>
                <!-- Column -->
                <div class="col-lg-6">
                    <div class="p-20">
                        <img src="/cms/assets/contentbox/assets/images/owl.jpg" alt="" class="img-responsive img-shadow" data-aos="flip-right" data-aos-duration="1200" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div data-num="<?= $cat ?>0006" data-thumb="<?= $assets_url ?>thumbnails/services-0006.png" data-cat="<?= $cat ?>">
	<div class="feature9">
        <div class="container">
            <!-- Row  -->
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <span class="label label-success label-rounded">Feature 9</span>
                    <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                    <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
                </div>
            </div>
            <!-- Row  -->
            <div class="row m-t-40">
                <!-- Column -->
                <div class="col-md-6 wrap-feature9-box b-r b-b">
                    <div class="card" data-aos="fade-right" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="align-self-center">
                                <h2 class="font-medium">Wrap Seo Digital Marketing Agency</h2>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-6 wrap-feature9-box b-b">
                    <div class="card" data-aos="fade-left" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space align-self-center"><i class="icon-Computer-Secure display-4 text-success-gradiant"></i></div>
                            <div class="align-self-center">
                                <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Online Marketer <i class="ti-arrow-right"></i></a></h5>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-6 wrap-feature9-box b-r">
                    <div class="card" data-aos="fade-right" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space align-self-center"><i class="icon-Cloud-Smartphone display-4 text-success-gradiant"></i></div>
                            <div class="align-self-center">
                                <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Digital World <i class="ti-arrow-right"></i></a></h5>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-md-6 wrap-feature9-box">
                    <div class="card" data-aos="fade-left" data-aos-duration="1200">
                        <div class="card-body d-flex">
                            <div class="icon-space align-self-center"><i class="icon-Business-ManWoman display-4 text-success-gradiant"></i></div>
                            <div class="align-self-center">
                                <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Meet the Team <i class="ti-arrow-right"></i></a></h5>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-md-12 m-t-20 text-center">
                    <a class="btn btn-success-gradiant btn-md btn-arrow" href="#"><span>View Feature9 code <i class="ti-arrow-right"></i></span></a>
                </div>
            </div>
        </div>
    </div>
</div>


<div data-num="83" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w01.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12">

            <h1 style="text-align: center; font-size:3em">Our Services</h1>

        </div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-monitor-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-gear-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-heart-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-compose-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-world-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
					<i class="icon ion-ios-calendar-outline size-48"></i>

					<h4>Lorem Ipsum</h4>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
        </div>

	</div>

</div>



<div data-num="84" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w02.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12">

            		 <h1 style="text-align: center; font-size:3em">Our Services</h1>

        	</div>

	</div>

	<div class="row">

		<div class="col-md-6">

            <p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-ios-heart-outline size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

			<p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-ios-compose-outline size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

			<p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-ios-gear-outline size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

		</div>

		<div class="col-md-6">

            <p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-ios-calendar-outline size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

			<p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-ios-clock-outline size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

			<p style="padding-left:90px;margin-bottom:3em;position:relative;"><i class="icon ion-laptop size-48" style="position:absolute;top:-15px;left:0;"></i> <b>Lorem Ipsum</b><br>Lorem Ipsum is simply dummy text.</p>

		</div>

	</div>

</div>


<div data-num="85" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w04.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

			<div class="display">

				<h1>Beautiful content. Responsive.</h1>

			</div>

			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet.</p>

		</div>

	</div>

	<div class="row">

		<div class="col-md-6">

            <img src="<?= $assets_url ?>assets/minimalist-basic/w04-1.png">

        </div>

		<div class="col-md-6">
            		
            <div style="margin:30px 0 0 0">
                <p><i class="icon ion-ios-heart-outline size-48" style="vertical-align: middle;margin-right:30px;"></i> Lorem Ipsum is simply dummy text.</p>

                <p><i class="icon ion-ios-compose-outline size-48" style="vertical-align: middle;margin-right:30px;"></i> Lorem Ipsum is simply dummy text.</p>

                <p><i class="icon ion-ios-gear-outline size-48" style="vertical-align: middle;margin-right:30px;"></i> Lorem Ipsum is simply dummy text.</p>
			</div>

     	</div>

	</div>

</div>



<div data-num="86" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w05.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

                    <h1 style="font-size: 3em; margin: 1.2em 0">What We Offer</h1>

                </div>

	</div>

	<div class="row">

        	<div class="col-md-6">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/w05-1.png">

        	</div>

			<div class="col-md-6">

                		<h3>Beautiful Content. Responsive.</h3>

                		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                		<div style="margin:1em 0 2.5em;">

                			<a href="#" class="btn btn-primary btn-lg edit">Read More</a>

            		</div>

        	</div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
				<i class="icon ion-ios-compose-outline size-80"></i>

				<h3>Lorem Ipsum</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<i class="icon ion-ios-camera-outline size-80"></i>

				<h3>Lorem Ipsum</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

        	</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<i class="icon ion-ios-paperplane-outline size-80"></i>

				<h3>Lorem Ipsum</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

        	</div>
		</div>
	</div>

</div>

<div data-num="87" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w06.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size: 3em; margin: 1.2em 0">What We Offer</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3 center">
			<i class="icon ion-ios-heart-outline size-48" style="margin-top: 0.3em"></i>
			<p><b>Beautiful Content</b><br>Lorem Ipsum is simply.</p>
        </div>
        
		<div class="col-md-3 center">
			<i class="icon ion-ios-monitor-outline size-48" style="margin-top: 0.3em"></i>
			<p><b>Responsive</b><br>Lorem Ipsum is simply.</p>
        </div>
        
		<div class="col-md-3 center">
			<i class="icon ion-ios-gear-outline size-48" style="margin-top: 0.3em"></i>
			<p><b>Typesetting</b><br>Lorem Ipsum is simply.</p>
        </div>
        
		<div class="col-md-3 center">
			<i class="icon ion-ios-compose-outline size-48" style="margin-top: 0.3em"></i>
			<p><b>Simply Text</b><br>Lorem Ipsum is simply.</p>
        </div>
	</div>
</div>



<div data-num="88" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w07.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            	<h1 style="font-size:3em">SERVICES</h1>
            	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
        </div>
	</div>

	<div class="row">
		<div class="col-md-4 center">
			<i class="icon ion-ios-heart-outline size-80"></i>
			<h3 style="margin-top:0">Lorem Ipsum</h3>
        </div>

		<div class="col-md-4 center">
			<i class="icon ion-ios-gear-outline size-80"></i>
			<h3 style="margin-top:0">Lorem Ipsum</h3>
        </div>

		<div class="col-md-4 center">
			<i class="icon ion-ios-filing-outline size-80"></i>
			<h3 style="margin-top:0">Lorem Ipsum</h3>
        </div>
	</div>
</div>



<div data-num="89" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/w10.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            	<h1 style="text-align: center; font-size:3em;margin:1.5em 0">Our Services</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1" style="border-radius:20px">
				<div class="margin-25 center">
			   		<i class="icon ion-ios-gear-outline size-64"></i>
			    	<h3 style="margin-top:0">Lorem Ipsum</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              </div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1" style="border-radius:20px">
				<div class="margin-25 center">
			    	<i class="icon ion-ios-heart-outline size-64"></i>
			    	<h3 style="margin-top:0">Lorem Ipsum</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        		</div>
       	 	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1" style="border-radius:20px">
				<div class="margin-25 center">
			   		<i class="icon ion-ios-mic-outline size-64"></i>
			   		<h3 style="margin-top:0">Lorem Ipsum</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        		</div>
         	</div>
        </div>
    </div>
</div>