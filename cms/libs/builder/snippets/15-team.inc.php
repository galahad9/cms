<div 
data-num="<?= $cat ?>0001" 
data-thumb="<?= $assets_url ?>thumbnails/team-0001.png" 
data-cat="<?= $cat ?>">
	<div class="team1">
        <div class="container">
            <div class="row">
                <!-- column  -->
                <div class="col-lg-6">
                    <div class="card card-shadow">
                        <!-- Row -->
                        <div class="row no-gutters card-shadow">
                            <div class="col-md-5 pro-pic" style="background:url(/cms/assets/contentbox/assets/images/team.jpg) center center no-repeat / cover">
                                <div class="card-img-overlay">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ti-twitter"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ti-instagram"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ti-behance"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-7 bg-white">
                                <div class="p-30">
                                    <h5 class="title m-t-0 font-medium">Michael Doe</h5>
                                    <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Row -->
                    </div>
                </div>
                <!-- column  -->
                <div class="col-lg-6">
                    <div class="card card-shadow">
                        <!-- Row -->
                        <div class="row no-gutters card-shadow">
                            <div class="col-md-5 pro-pic" style="background:url(/cms/assets/contentbox/assets/images/team.jpg) center center no-repeat / cover">
                                <div class="card-img-overlay">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ti-twitter"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ti-instagram"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ti-behance"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-7 bg-white">
                                <div class="p-30">
                                    <h5 class="title m-t-0 font-medium">Michael Doe</h5>
                                    <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Row -->
                    </div>
                </div>
                <!-- column  -->
            </div>
        </div>
    </div>
</div>

<div 
data-num="<?= $cat ?>0002" 
data-thumb="<?= $assets_url ?>thumbnails/team-0002.png" 
data-cat="<?= $cat ?>">
	<div class="team2">
        <div class="container">
            <div class="row m-t-30">
                <!-- column  -->
                <div class="col-lg-3 m-b-30">
                    <!-- Row -->
                    <div class="row no-gutters">
                        <div class="col-md-12 pro-pic" style="background:url(/cms/assets/contentbox/assets/images/team.jpg) center center no-repeat / cover">
                            <div class="card-img-overlay">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="p-t-10">
                                <h5 class="title font-medium">Michael Doe</h5>
                                <h6 class="subtitle">Property Specialist</h6>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>
                <!-- column  -->
                <!-- column  -->
                <div class="col-lg-3 m-b-30">
                    <!-- Row -->
                    <div class="row no-gutters">
                        <div class="col-md-12 pro-pic" style="background:url(/cms/assets/contentbox/assets/images/team.jpg) center center no-repeat / cover">
                            <div class="card-img-overlay">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="p-t-10">
                                <h5 class="title font-medium">Michael Doe</h5>
                                <h6 class="subtitle">Property Specialist</h6>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>
                <!-- column  -->
                <!-- column  -->
                <div class="col-lg-3 m-b-30">
                    <!-- Row -->
                    <div class="row no-gutters">
                        <div class="col-md-12 pro-pic" style="background:url(/cms/assets/contentbox/assets/images/team.jpg) center center no-repeat / cover">
                            <div class="card-img-overlay">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="p-t-10">
                                <h5 class="title font-medium">Michael Doe</h5>
                                <h6 class="subtitle">Property Specialist</h6>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>
                <!-- column  -->
                <!-- column  -->
                <div class="col-lg-3 m-b-30">
                    <!-- Row -->
                    <div class="row no-gutters">
                        <div class="col-md-12 pro-pic" style="background:url(/cms/assets/contentbox/assets/images/team.jpg) center center no-repeat / cover">
                            <div class="card-img-overlay">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="p-t-10">
                                <h5 class="title font-medium">Michael Doe</h5>
                                <h6 class="subtitle">Property Specialist</h6>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>
                <!-- column  -->
            </div>
        </div>
    </div>
</div>

<div 
data-num="<?= $cat ?>0003" 
data-thumb="<?= $assets_url ?>thumbnails/team-0003.png" 
data-cat="<?= $cat ?>">
	<div class="team3">
        <div class="container">
            <div class="row m-t-30">
                <!-- column  -->
                <div class="col-lg-4 m-b-30">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <img src="/cms/assets/contentbox/assets/images/team.jpg" alt="" class="img-responsive" />
                        </div>
                        <div class="col-md-12">
                            <div class="p-t-10">
                                <h5 class="title font-medium">Michael Doe</h5>
                                <h6 class="subtitle">Property Specialist</h6>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                <ul class="list-inline">
                                     <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>
                <!-- column  -->
                <!-- column  -->
                <div class="col-lg-4 m-b-30">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-12 pro-pic">
                            <img src="/cms/assets/contentbox/assets/images/team.jpg" alt="" class="img-responsive" />
                        </div>
                        <div class="col-md-12">
                            <div class="p-t-10">
                                <h5 class="title font-medium">Michael Doe</h5>
                                <h6 class="subtitle">Property Specialist</h6>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                <ul class="list-inline">
                                     <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>
                <!-- column  -->
                <!-- column  -->
                <div class="col-lg-4 m-b-30">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-12 pro-pic">
                            <img src="/cms/assets/contentbox/assets/images/team.jpg" alt="" class="img-responsive" />
                        </div>
                        <div class="col-md-12">
                            <div class="p-t-10">
                                <h5 class="title font-medium">Michael Doe</h5>
                                <h6 class="subtitle">Property Specialist</h6>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>
                <!-- column  -->
            </div>
        </div>
    </div>
</div>



<div 
data-num="<?= $cat ?>0004" 
data-thumb="<?= $assets_url ?>thumbnails/team-0004.png" 
data-cat="<?= $cat ?>">
	<div class="team4">
        <div class="container">
            <div class="row m-t-30">
                <!-- column  -->
                <div class="col-lg-3 m-b-30">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-12 pro-pic">
                            <img src="/cms/assets/contentbox/assets/images/team.jpg" alt="" class="img-responsive" />
                        </div>
                        <div class="col-md-12">
                            <div class="p-t-10">
                                <h5 class="title font-medium">Michael Doe</h5>
                                <h6 class="subtitle">Property Specialist</h6>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>
                <!-- column  -->
                <!-- column  -->
                <div class="col-lg-3 m-b-30">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-12 pro-pic">
                            <img src="/cms/assets/contentbox/assets/images/team.jpg" alt="" class="img-responsive" />
                        </div>
                        <div class="col-md-12">
                            <div class="p-t-10">
                                <h5 class="title font-medium">Michael Doe</h5>
                                <h6 class="subtitle">Property Specialist</h6>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>
                <!-- column  -->
                <!-- column  -->
                <div class="col-lg-3 m-b-30">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-12 pro-pic">
                            <img src="/cms/assets/contentbox/assets/images/team.jpg" alt="" class="img-responsive" />
                        </div>
                        <div class="col-md-12">
                            <div class="p-t-10">
                                <h5 class="title font-medium">Michael Doe</h5>
                                <h6 class="subtitle">Property Specialist</h6>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                <ul class="list-inline">
                                   <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>
                <!-- column  -->
                <!-- column  -->
                <div class="col-lg-3 m-b-30">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-12 pro-pic">
                            <img src="/cms/assets/contentbox/assets/images/team.jpg" alt="" class="img-responsive" />
                        </div>
                        <div class="col-md-12">
                            <div class="p-t-10">
                                <h5 class="title font-medium">Michael Doe</h5>
                                <h6 class="subtitle">Property Specialist</h6>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-instagram"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ti-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>
            </div>
        </div>
    </div>
</div>




<div data-num="106" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z01.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

			<div class="display">

				<h1 style="font-size: 3.5em">Meet The Team</h1>

			</div>

		</div>

	</div>

	<div class="row">

		<div class="col-md-4 center">

			<img src="<?= $assets_url ?>assets/minimalist-basic/z01-1.jpg" style="border-radius:500px; margin-top: 1em">

			<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>

			</div>          

		</div>

		<div class="col-md-8">

			<h1>Your Name</h1>

			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>

		</div>

	</div>

	<div class="row">

		<div class="col-md-4 center">

			<img src="<?= $assets_url ?>assets/minimalist-basic/z01-2.jpg" style="border-radius:500px; margin-top: 1em">

			<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>

			</div>         

		</div>

		<div class="col-md-8">

			<h1>Your Name</h1>

			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>

		</div>

	</div>

</div>


<div data-num="107" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z02.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            		<h1 style="font-size: 3em">Meet The Team</h1>
		    	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        	</div>
	</div>	
	<div class="row">
		<div class="col-md-6">
			<div class="row">
			    <div class="col-md-4 center">
				    <img src="<?= $assets_url ?>assets/minimalist-basic/z02-2.jpg" style="border-radius:500px">
			    </div>
			    <div class="col-md-8">
				<p><b>Your Name</b><br>Lorem Ipsum is simply.</p>
				<div class="is-social edit">
                		    <a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>
                		    <a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>
                		    <a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>
                		    <a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			        </div>
			    </div>
		    </div>
	    	</div>
	    <div class="col-md-6">
		    <div class="row">
			    <div class="col-md-4 center">
				    <img src="<?= $assets_url ?>assets/minimalist-basic/z02-3.jpg" style="border-radius:500px">
			    </div>
			    <div class="col-md-8">
				<p><b>Your Name</b><br>Lorem Ipsum is simply.</p>				    
				<div class="is-social edit">
                		    <a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>
                		    <a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>
                		    <a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>
                		    <a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			        </div>
			    </div>
		    </div>
	    </div>
    	</div>
</div>


<div data-num="108" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z03.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

			<h1 style="font-size: 3em">Our Team</h1>

			<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

		</div>

	</div>

	<div class="row">

		<div class="col-md-6 center">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/z03-1.jpg" style="border-radius:500px">

				<h3>Your Name</h3>

				<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>

				<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>

				</div>
			</div>
		</div>

		<div class="col-md-6 center">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/z03-2.jpg" style="border-radius:500px">

				<h3>Your Name</h3>

				<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>

				<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>

				</div>
			</div>
		</div>

	</div>

</div>


<div data-num="109" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z05.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            	<h1 style="margin:1.2em 0;font-size:3em">Meet The Team</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
					<img src="<?= $assets_url ?>assets/minimalist-basic/z05-1.jpg" style="border-radius: 500px;" alt="">			
			    	<h3 style="font-size:1.5em">Your Name</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>              
        		</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			    	<img src="<?= $assets_url ?>assets/minimalist-basic/z05-2.jpg" style="border-radius: 500px;" alt="">		
			    	<h3 style="font-size:1.5em">Your Name</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            		<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>    
        		</div>
        	</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			  		<img src="<?= $assets_url ?>assets/minimalist-basic/z05-3.jpg" style="border-radius: 500px;" alt="">
			   	 	<h3 style="font-size:1.5em">Your Name</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>         
        		</div>
         	</div>
		</div>
	</div>
</div>

<div data-num="110" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z06.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            <h1>Our Team</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z06-1.jpg" style="border-radius: 500px;" alt="">
            <p><b>Your Name</b><br>Lorem Ipsum is simply.</p>
			<div class="is-social edit">
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                	<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                	<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div> 
        </div>

		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z06-2.jpg" style="border-radius: 500px;" alt="">
            <p><b>Your Name</b><br>Lorem Ipsum is simply.</p>
			<div class="is-social edit">
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                	<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                	<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div> 
        </div>

		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z06-3.jpg" style="border-radius: 500px;" alt="">
            <p><b>Your Name</b><br>Lorem Ipsum is simply.</p>
			<div class="is-social edit">
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                	<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                	<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div> 
        </div>

		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z06-4.jpg" style="border-radius: 500px;" alt="">
            <p><b>Your Name</b><br>Lorem Ipsum is simply.</p>
			<div class="is-social edit">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div> 
        </div>
	</div>
</div>

<div data-num="111" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z07.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
                <h1>Meet Our Team</h1>
			</div>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z07-1.jpg" style="border-radius: 500px;">
		</div>
		
		<div class="col-md-3">
			<p><b>Your Name</b><br>Lorem Ipsum is simply dummy text of the printing industry.</p>
			<div class="is-social edit">
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                	<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                	<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div>   
		</div>
		
		<div class="col-md-3 center">
			<img src="<?= $assets_url ?>assets/minimalist-basic/z07-2.jpg" style="border-radius: 500px;">
		</div>
		
		<div class="col-md-3">
			<p><b>Your Name</b><br>Lorem Ipsum is simply dummy text of the printing industry.</p>
			<div class="is-social edit">
                	<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                	<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                	<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                	<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
			</div>   
		</div>
	</div>
</div>

<div data-num="112" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/z09.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
				<h1>Meet The Team</h1>
			</div>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">
                	<img src="<?= $assets_url ?>assets/minimalist-basic/z09-1.jpg" style="border-radius: 500px;">
			    	<h3 style="font-size:1.5em">Your Name</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>     
        		</div>
        	</div>
		</div>

		<div class="col-md-6">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">
                	<img src="<?= $assets_url ?>assets/minimalist-basic/z09-2.jpg" style="border-radius: 500px;">
			    	<h3 style="font-size:1.5em">Your Name</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>  
            	</div>
        	</div>
		</div>
	</div>
</div>

<div data-num="46" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/s10.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-6">

	        <div class="row">

			    <div class="col-md-4 center">

				    <p><img src="<?= $assets_url ?>assets/minimalist-basic/s10-1.jpg" style="border-radius:500px;margin-top:10px;"></p>

			    </div>

			    <div class="col-md-8">

				    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>

				    <div class="is-social edit">

                			    <a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                			    <a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                			    <a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                			    <a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

				    </div>

			    </div>

			</div>

		</div>

		<div class="col-md-6">

	        <div class="row">

			    <div class="col-md-4 center">

				    <p><img src="<?= $assets_url ?>assets/minimalist-basic/s10-2.jpg" style="border-radius:500px;margin-top:10px;"></p>

			    </div>

			    <div class="col-md-8">

				    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>

				    <div class="is-social edit">

                			    <a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                			    <a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                			    <a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                			    <a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

				    </div>

			    </div>

			</div>

		</div>

	</div>

</div>


<div data-num="44" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/s01.png" data-cat="<?= $cat ?>">

	<div class="row">

        	<div class="col-md-2 center">

                <p><img src="<?= $assets_url ?>assets/minimalist-basic/s01-1.jpg" style="border-radius:500px;margin-top:5px;"></p>

        	</div>

			<div class="col-md-10">

            		<p>

                	<b>Sara Phillipps</b><br>A freelance web designer &amp; developer based in Melbourne, Australia.

            		</p>

            		<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<!--<a href="#"><i class="icon ion-social-github"></i></a>

                		<a href="#"><i class="icon ion-social-dribbble"></i></a>

                		<a href="#"><i class="icon ion-social-pinterest"></i></a>

                		<a href="#"><i class="icon ion-social-linkedin"></i></a>

                		<a href="#"><i class="icon ion-social-instagram"></i></a>-->

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

           	 	</div>

       	</div>

	</div>

</div>



<div data-num="45" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/s03.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-8">
            
			<img src="<?= $assets_url ?>assets/minimalist-basic/s03-1.jpg">

 		</div>

		<div class="col-md-4 center">

			<h2>Lorem Ipsum</h2>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

			<div class="is-social edit">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

			</div>

       </div>

	</div>

</div>