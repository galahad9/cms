<div data-num="<?= $cat ?>0001" data-thumb="<?= $assets_url ?>thumbnails/cta-0001.png" data-cat="<?= $cat ?>">
	<div class="c2a1">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <h2 class="title text-white font-medium">Become a Volunteer</h2>
                    <p class="font-light text-white op-8">Lorem ipsum dolor sit amet, consectetur adipiscelit. Nam malesu dolor sit amet, consectetur adipiscelit. consectetur adipiscelit. Nam malesu dolor.</p>
                    <a class="btn btn-primary-gradient btn-md btn-arrow m-t-20 text-uppercase" href="#"><span>join us now<i class="ti-arrow-right"></i></span></a> 
                </div>
            </div>
        </div>
    </div>
</div>

<div data-num="<?= $cat ?>0002" data-thumb="<?= $assets_url ?>thumbnails/cta-0002.png" data-cat="<?= $cat ?>">
	<div class="c2a2">
        <div class="container">
            <!-- Row -->
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <h2 class="title font-light">Are you happy with what we offer? Grab your wrapkit copy now</h2>
                    <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
                    <div class="m-t-40">
                        <a class="btn btn-primary-gradient btn-rounded btn-md btn-arrow m-t-20 m-r-10" href="#"><span>Buy Wrapkit <i class="ti-arrow-right"></i></span></a>
                        <a class="btn btn-outline-inverse btn-rounded btn-md btn-arrow m-t-20" href="#"><span>Live Preview <i class="ti-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div data-num="<?= $cat ?>0003" data-thumb="<?= $assets_url ?>thumbnails/cta-0003.png" data-cat="<?= $cat ?>">
	<div class="c2a3">
        <div class="container">
            <div class="d-flex">
                <div class="display-7 align-self-center">
                    <h3 class="">We are leading Manufacturing and Exporting company</h3>
                    <h6 class="font-16 subtitle">You will feel great using our quality products and services</h6>
                </div>
                <div class="ml-auto m-t-10 m-b-10"><a href="" class="btn btn-primary-gradient btn-md">Contact Us</a></div>
            </div>
        </div>
    </div>
</div>

<div data-num="<?= $cat ?>0004" data-thumb="<?= $assets_url ?>thumbnails/cta-0004.png" data-cat="<?= $cat ?>">
	<div class="c2a4">
        <div class="container">
            <div class="row p-b-30">
                <div class="col-lg-9 col-md-7">
                    <h2 class="font-bold">Happy with what we offer? Grab it Now!</h2>
                    <h6 class="subtitle">Yeah ! Its pretty cool about what you are offering and i starterd to feel in love with your Amazing UI Kit.</h6>
                </div>
                <div class="col-lg-3 col-md-5 align-self-center">
                    <a class="btn btn-primary-gradiant btn-md btn-arrow btn-rounded m-b-20 pull-right" data-toggle="collapse" href="#f1"><span>BUY BASIC <i class="ti-arrow-right"></i></span></a>
                </div>
            </div>
        </div>    
    </div>
</div>

<div data-num="<?= $cat ?>0005" data-thumb="<?= $assets_url ?>thumbnails/cta-0005.png" data-cat="<?= $cat ?>">
	<div class="c2a8">
        <div class="container">
            <!-- Row -->
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7 text-center both-space">
                    <div class="card bg-danger-gradiant aos-init aos-animate" data-aos="flip-left" data-aos-duration="1200">
                        <div class="card-body">
                            <div class="text-box"> 
                                <h3 class="title text-white font-stylish">Make your Reservation for Delicious Food</h3>
                                <h6 class="subtitle text-white op-5">You can relay on our amazing features list and also our customer services will be great experience for you without doubt</h6> <a class="btn bg-white text-danger btn-rounded btn-md btn-arrow m-t-20" href="#"><span>Reserve Your Table <i class="ti-arrow-right"></i></span></a> </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            </div>
        </div>
    </div>
</div>




<div data-num="29" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p01.png" data-cat="<?= $cat ?>">

	<div class="row">

        	<div class="col-md-6">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/p01-1.jpg">

       	 	</div>

		<div class="col-md-6">

           		<div class="display">

               			<h1>Beautiful content. Responsive.</h1>

                		<p>Lorem Ipsum is simply dummy text.</p>

                		<div style="margin:1em 0 2.5em;">

                			<a href="#" class="btn btn-primary btn-lg edit">Read More</a>

                		</div>

            		</div>

        	</div>

	</div>

</div>



<div data-num="30" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p02.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-6">

            		<div class="display">

                		<h1>Beautiful content. Responsive.</h1>

                		<p>Lorem Ipsum is simply dummy text.</p>

                		<div style="margin:1em 0 2.5em;">

                			<a href="#" class="btn btn-primary btn-lg edit">Read More</a>

                		</div>

            		</div>

        	</div>

        	<div class="col-md-6">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/p02-1.jpg">

        	</div>

	</div>

</div>



<div data-num="31" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p03.png" data-cat="<?= $cat ?>">

 	<div class="row">

        	<div class="col-md-6">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/p03-1.jpg">

        	</div>

		<div class="col-md-6">

            		<h1>Beautiful content. Responsive.</h1>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

            		<div style="margin:1em 0 2.5em;">

            			<a href="#" class="btn btn-primary btn-lg edit">Read More</a>

            		</div>

       </div>

	</div>

</div>



<div data-num="32" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p04.png" data-cat="<?= $cat ?>">

 	<div class="row">

		<div class="col-md-6">

            		<h1>Beautiful content. Responsive.</h1>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

            		<div style="margin:1em 0 2.5em;">

            			<a href="#" class="btn btn-primary btn-lg edit">Read More</a>

            		</div>

        	</div>

        	<div class="col-md-6">

            		<img src="<?= $assets_url ?>assets/minimalist-basic/p04-1.jpg">

        	</div>

	</div>

</div>



<div data-num="33" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p05.png" data-cat="<?= $cat ?>">

  <div class="row">

        <div class="col-md-12">

            <div class="display center">

                <h1>Beautiful content. Responsive.</h1>

                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

            </div>

         </div>

    </div>

    <div class="row">

        <div class="col-md-12">

            <div class="center" style="margin:1em 0 2.5em;">

            <a href="#" class="btn btn-primary btn-lg edit">Read More</a>

            </div>

        </div>

    </div>

</div>



<div data-num="34" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p06.png" data-cat="<?= $cat ?>">

 <div class="row">

        <div class="col-md-12">

            <div class="display center">

                <h1 style="font-size:4em">Lorem Ipsum is simply dummy text of the printing and typesetting industry</h1>

            </div>

            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>

        </div>

    </div>

    <div class="row">

        <div class="col-md-12">

            <div class="center" style="margin:1em 0 2.5em;">

            <a href="#" class="btn btn-primary btn-lg edit">Read More</a>

            </div>

        </div>

    </div>

</div>



<div data-num="35" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p07.png" data-cat="<?= $cat ?>">

 <div class="row">

        <div class="col-md-12">

            <div class="center" style="margin:1em 0 2.5em;">

            <a href="#" class="btn btn-default btn-lg edit">Read More</a> &nbsp;

            <a href="#" class="btn btn-primary btn-lg edit">Download</a>

            </div>

        </div>

    </div>

</div>




<div data-num="55" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p34.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

			<div class="display">

				<h1 style="font-size: 3.5em">Beautiful Content</h1>

			</div>

 			<p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet.</p>

		</div>

	</div>

	<div class="row">

        	<div class="col-md-6">

			
                    <img src="<?= $assets_url ?>assets/minimalist-basic/p34-1.png">
        

			</div>

			<div class="col-md-6">

                		<h2>Responsive.</h2>

                		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

                		<div style="margin:1.3em 0 2.5em;">

                			<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px;">Read More</a>

            			</div>

        	</div>

	</div>

</div>

	

<div data-num="56" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p35.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

			<i class="icon ion-leaf size-64"></i>

			<h1 class="size-21">BEAUTIFUL CONTENT</h1>

            	<div class="display">

                	<h1 style="font-size: 3.5em; margin:0.2em 0">Lorem Ipsum is simply dummy text</h1>

            	</div>

           <p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

       </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

            		    <div style="margin:1em 0">

           			 <a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">READ MORE</a>

            		    </div>

                </div>

	</div>

</div>



<div data-num="57" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p36.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

			<div class="is-social edit">
				<div class="size-21">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>

						<a href="https://www.pinterest.com/"><i class="icon ion-social-pinterest-outline" style="margin-right: 1em"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>

						<a href="https://www.instagram.com/"><i class="icon ion-social-instagram-outline"></i></a>
				</div>
			</div>

            		<div class="display">

                		    <h1 style="margin:0.3em 0; font-size: 4em">Beautiful Content. Responsive.</h1>

            		 </div>

                    <p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                </div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

            		<div style="margin:1em 0 2.5em;">

            			<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">Read More</a>

            		</div>

        	</div>

	</div>

</div>



<div data-num="58" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p38.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12">
				<div class="display center">

            		<h1>Beautiful content.</h1>

            		<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
            		
				</div>
        	</div>

	</div>

	<div class="row">

		<div class="col-md-6">

            		<h3>Lorem Ipsum is simply dummy text</h3>

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>



			<div style="margin:1em 0 2.5em;">

           		   <a href="#" class="btn btn-default btn-lg edit">Read More</a> &nbsp;

            		   <a href="#" class="btn btn-primary btn-lg edit">Download</a>

            		</div>

        	</div>

        	<div class="col-md-6">

            		<div class="embed-responsive embed-responsive-4by3">

            			<iframe width="560" height="315" src="//www.youtube.com/embed/P5yHEKqx86U?rel=0" frameborder="0" allowfullscreen=""></iframe>

            		</div>

        	</div>

	</div>

</div>


<div data-num="59" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p42.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<img src="<?= $assets_url ?>assets/minimalist-basic/p42-1.jpg">
			<h3>BEAUTIFUL CONTENT. RESPONSIVE.</h3>
			<div class="display">
				<h1>Lorem Ipsum is simply text of the printing industry</h1>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
            <div style="margin:0.5em 0 2.5em;">
            	<a href="#" class="btn btn-default btn-lg edit" style="border-radius:50px">Read More</a> &nbsp;
            	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Download</a>
            </div>
        </div>
	</div>
</div>

<div data-num="60" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p43.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            	<div class="clearfix is-rounded-button-medium">
                		<a href="https://twitter.com/" style="background-color: #00bfff;margin-left:0"><i class="icon ion-social-twitter"></i></a>
                		<a href="https://www.facebook.com/" style="background-color: #128BDB"><i class="icon ion-social-facebook"></i></a>
		             	<a href="https://plus.google.com/" style="background-color: #DF311F"><i class="icon ion-social-googleplus"></i></a>    
                        <a href="mailto:you@example.com" style="background-color: #ff69B4"><i class="icon ion-ios-email-outline"></i></a>           			
				</div>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="display">
				<h1 style="margin-top:0.5em;font-size:3.7em">Lorem Ipsum is simply dummy text</h1>
			</div>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
            <div style="margin:1em 0;">        
            	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">GET STARTED</a>
            </div>
        </div>
	</div>
</div>

<div data-num="61" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p44.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
				<h1 style="font-size:3.4em">Lorem Ipsum is simply text</h1>
			</div>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12 center">
            	<div class="clearfix is-rounded-button-medium" style="margin:1em 0">
                	<a href="https://twitter.com/" style="background-color: #00bfff;"><i class="icon ion-social-twitter"></i></a>
                	<a href="https://www.facebook.com/" style="background-color: #128BDB"><i class="icon ion-social-facebook"></i></a>
		            <a href="https://plus.google.com/" style="background-color: #DF311F"><i class="icon ion-social-googleplus"></i></a>    
                    <a href="mailto:you@example.com" style="background-color: #ff69B4"><i class="icon ion-ios-email-outline"></i></a>   
                    <a href="https://www.instagram.com/" style="background-color: #0569AA"><i class="icon ion-social-instagram-outline"></i></a>         			
				</div>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12 center">
            <div style="margin:2em 0">
            <a href="#" class="btn btn-default btn-lg edit" style="border-radius:50px">Read More</a> &nbsp;
            <a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Download</a>
            </div>
        </div>
	</div>
</div>

<div data-num="62" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p46.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<i class="icon ion-ios-film-outline size-64"></i>
            <h1 style="font-size:3.5em;margin-top:0">Beautiful Content. Responsive.</h1>
            <p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12 center">
            <div style="margin:1em 0">
            	<a href="#" class="btn btn-primary btn-lg edit">Watch Video</a>
            </div>
        </div>
	</div>
</div>

<div data-num="63" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p47.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
				<h1 style=" font-size: 4em">Lorem Ipsum is simply dummy text</h1>
			</div>
			<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12 center">
           <div style="margin:1em 0 2.5em;">
            <a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">GET STARTED TODAY</a>
            </div>
        </div>
	</div>

	<div class="row">
		<div class="col-md-12 center">
           <div class="clearfix is-rounded-button-big">
            			<a href="#" class="btn btn-default btn-lg edit" style="padding:0; width:80px; height:80px;"><i class="icon ion-android-arrow-dropright"></i> </a>
            	</div>
        </div>
	</div>
</div>

<div data-num="64" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/p50.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            	<div class="clearfix is-rounded-button-big">
            			<a href="#" class="btn btn-primary" style="padding:0; width:90px; height:90px;"><i class="icon ion-android-arrow-dropright"></i> </a>
            	</div>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12 center">
                <h1 style="font-size:3em">Beautiful content. Responsive</h1>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet.</p>
         </div>
	</div>
	
	<div class="row">
		<div class="col-md-12 center">
            <div style="margin:2em 0">
            	<a href="#" class="btn btn-default btn-lg edit" style="border-radius:50px">Read More</a> &nbsp;
            	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Get Started</a>
            </div>
        </div>
	</div>
</div>	