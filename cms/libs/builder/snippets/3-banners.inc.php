<div data-num="<?= $cat ?>0001" data-thumb="<?= $assets_url ?>thumbnails/banner-0001.png" data-cat="<?= $cat ?>">
	<div class="static-slider10">
        <div class="container">
            <!-- Row  -->
            <div class="row justify-content-center ">
                <!-- Column -->
                <div class="col-md-6 align-self-center text-center aos-init aos-animate" data-aos="fade-down" data-aos-duration="1200">
                    <h1 class="title">Build Your Site in Record time</h1>
                    <h6 class="subtitle op-8 m-t-30">make in record time with wrapkit</h6>
                    <a class="btn btn-primary btn-rounded btn-md btn-arrow m-t-20 m-r-10" href="#"><span>Features <i class="ti-arrow-right"></i></span></a>
                    <a class="btn btn-outline-light btn-rounded m-t-20 btn-md" data-toggle="modal" data-target="#static-slide7" href=""><i class="ti-control-play m-r-10"></i> Intro </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div data-num="<?= $cat ?>0002" data-thumb="<?= $assets_url ?>thumbnails/banner-0002.png" data-cat="<?= $cat ?>">
	<div class="static-slider2">
        <div class="container">
            <!-- Row  -->
            <div class="row">
                <!-- Column -->
                <div class="col-md-5 align-self-center" data-aos="fade-right" data-aos-duration="1200">
                    <h1 class="title"><span class="typewrite">Website</span><br/>Done<span class="text-megna">.</span></h1>
                    <h6 class="subtitle">I’m John Doe, a Web & Mobile App Designer,Specialize in Website Design, Illustration & Mobile Application Work.</h6>
                    <a class="btn btn-primary-gradiant btn-md btn-arrow m-t-20 m-b-10" data-toggle="collapse" href=""><span>Hire Me <i class="ti-arrow-right"></i></span></a>
                    <a class="btn btn-link btn-md btn-arrow m-t-20 m-b-10 font-medium" data-toggle="collapse" href=""><span class="underline">Check my work <i class="ti-arrow-right"></i></span></a>
                </div>
                <!-- Column -->
                <div class="col-md-7 img-anim" data-aos="fade-up" data-aos-duration="2200">
                    <img src="/cms/assets/contentbox/assets/images/img1.jpg" alt="" class="img-fluid m-t-20" />
                </div>
            </div>
        </div>
    </div>
</div>