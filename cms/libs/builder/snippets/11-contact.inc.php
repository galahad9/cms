<div 
data-num="<?= $cat ?>0001" 
data-thumb="<?= $assets_url ?>thumbnails/contact-00001.png" 
data-cat="<?= $cat ?>">
	<div class="contact1">
        <div class="row">
            <div class="col-md-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1619902.0054433027!2d-122.68851282163044!3d37.534535608111824!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1507725785789" width="100%" height="450" frameborder="0" style="border:0"></iframe>
            </div>
            <div class="container">
                <div class="spacer">
                        <div class="row m-0">
                            <div class="col-lg-8">
                                <div class="contact-box p-r-40">
                                    <h4 class="title">Quick Contact</h4>
                                    {{ FORM::1 }}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="detail-box p-40 bg-info aos-init aos-animate" data-aos="fade-right" data-aos-duration="1200">
                                    <h2 class="text-white">Company Headquarters</h2>
                                    <p class="text-white m-t-30 op-8">251 546 9442
                                        <br> info@wrappixel.com</p>
                                    <p class="text-white op-8">601 Sherwood Ave.
                                        <br> San Bernandino, CA 92404</p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div data-num="47" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/t01.png" data-cat="<?= $cat ?>">

    <div class="row">

        <div class="col-md-12">

            <div class="embed-responsive embed-responsive-16by9">

            <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&amp;hl=en&amp;sll=-7.981898,112.626504&amp;sspn=0.009084,0.016512&amp;oq=melbourne&amp;hnear=Melbourne+Victoria,+Australia&amp;t=m&amp;z=10&amp;output=embed"></iframe>

            </div>
        </div>
    </div>
</div>



<div data-num="48" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/t02.png" data-cat="<?= $cat ?>">

    <div class="row">

        <div class="col-md-8">

            <div class="embed-responsive embed-responsive-16by9">

            <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&amp;hl=en&amp;sll=-7.981898,112.626504&amp;sspn=0.009084,0.016512&amp;oq=melbourne&amp;hnear=Melbourne+Victoria,+Australia&amp;t=m&amp;z=10&amp;output=embed"></iframe>

            </div>

        </div>

        <div class="col-md-4">

            <h3>Get in Touch</h3>

            <p>

              <strong>Company, Inc.</strong><br>

              123 Street, City<br>

              State 12345<br>

              P: (123) 456 7890 / 456 7891

            </p>



            <p>

              <strong>Full Name</strong><br>

              <a href="mailto:#">first.last@example.com</a>

            </p>

        </div>

    </div>

</div>





<div data-num="90" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x01.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

			<div class="display">

				<h1 style="font-size: 3.5em">Contact Us</h1>

			</div>

		</div>

	</div>

	<div class="row">

		<div class="col-md-12 center">

            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel.</p>

        	</div>

	</div>

	<div class="row">

		<div class="col-md-6">

			<h3>Address</h3>

         		<p>

              		<strong>Company, Inc.</strong><br>

              		123 Street, City<br>

              		State 12345<br>

              		P: (123) 456 7890 / 456 7891

            	</p>

	    </div>

	    <div class="col-md-6">

            <div class="row">

			    <div class="col-md-12">

				    <h3>Stay in Touch</h3>

            	    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

                    <div class="clearfix is-boxed-button-medium2">
                        <a href="https://twitter.com/" style="background-color: #00bfff;"><i class="icon ion-social-twitter"></i></a>
                        <a href="https://facebook.com/" style="background-color: #128BDB;"><i class="icon ion-social-facebook"></i></a>		
                        <a href="mailto:you@example.com" style="background-color: #ff69B4;"><i class="icon ion-ios-email-outline"></i></a>
                    </div>

			    </div>

            </div>


	    </div>

	</div>

</div>



<div data-num="91" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x05.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

               <h1 style="font-size: 3em">Contact Us</h1>

               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

        </div>

	</div>

	<div class="row">

		<div class="col-md-6">

            <div class="row">

			    <div class="col-md-12">  

				    <h3>OFFICE ONE</h3>

			    </div>

            </div>

		    <div class="row">                        

				<div class="col-md-2">

 					<p style="line-height:1"><i class="icon ion-ios-location size-32"></i></p>

				</div>

				<div class="col-md-10">

                    <p>

              			<strong>Company, Inc.</strong><br>

              			123 Street, City<br>

              			State 12345

            		</p>

				</div>

			</div>

			<div class="row">

				<div class="col-md-2">

 					<p style="line-height:1"><i class="icon ion-ios-telephone size-32"></i></p>

				</div>

				<div class="col-md-10">

                    <p><b>Phone</b><br>(123) 456 7890</p>

				</div>

			</div>

	   		<div class="row">

				<div class="col-md-2">

					<p style="line-height:1"><i class="icon ion-ios-email size-32"></i></p>

				</div>

				<div class="col-md-10">

                     <p>

              			<strong>Email</strong><br>

              			<a href="mailto:#">first.last@example.com</a>

            		</p>

				</div>

             </div>

		</div>

		<div class="col-md-6">

			<div class="row">

                <div class="col-md-12">  

				    <h3>OFFICE TWO</h3>

                </div>

			</div>

			<div class="row">                       

				<div class="col-md-2">

 					<p style="line-height:1"><i class="icon ion-ios-location size-32"></i></p>

				</div>

				<div class="col-md-10">

                   	<p>

              			<strong>Company, Inc.</strong><br>

              			123 Street, City<br>

              			State 12345

            		</p>

				</div>

			</div>

			<div class="row">

				<div class="col-md-2">

 					<p style="line-height:1"><i class="icon ion-ios-telephone size-32"></i></p>

				</div>

				<div class="col-md-10">

                    <p><b>Phone</b><br>(123) 456 7890</p>

				</div>

			</div>

			<div class="row">

				<div class="col-md-2">

					<p style="line-height:1"><i class="icon ion-ios-email size-32"></i></p>

				</div>

				<div class="col-md-10">

                     <p>

              			<strong>Email</strong><br>

              			<a href="mailto:#">first.last@example.com</a>

            		</p>

				</div>

            </div>

		 </div>

	</div>

</div>


<div data-num="92" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x09.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size:3em">Contact Us</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3 center">
            <i class="icon ion-ios-location-outline size-48"></i>
			<h3 style="font-size:1.5em;margin-top:0">Company, Inc.</h3>
			<p>
              123 Street, City<br>
              State 12345
            </p>
        </div>

		<div class="col-md-3 center">
            <i class="icon ion-ios-telephone-outline size-48"></i>
			<h3 style="font-size:1.5em;margin-top:0">Call Us</h3>
  			<p>(123) 456 7890 / 456 7891</p>
        </div>

		<div class="col-md-3 center">
            <i class="icon ion-ios-email-outline size-48"></i>
			<h3 style="font-size:1.5em;margin-top:0">Email</h3>
  			<p>
              <a href="mailto:#">first.last@example.com</a>
            </p>
        </div>

		<div class="col-md-3 center">
            <i class="icon ion-ios-chatboxes-outline size-48"></i>
			<h3 style="font-size:1.5em;margin-top:0">Social Media</h3>
  			<p><a href="https://www.facebook.com">Facebook</a><br><a href="https://twitter.com">Twitter</a><br></p>
        </div>
	</div>
</div>



<div data-num="93" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x10.png" data-cat="<?= $cat ?>">

	<div class="row">
		<div class="col-md-12">
            <h1 style="font-size:3em">Contact Us</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
           <p>
              <span style="font-size:1.2em"><b>Company, Inc.</b></span><br>
              123 Street, City<br>
              State 12345<br>
              P: (123) 456 7890 / 456 7891
            </p>
			<div class="clearfix is-rounded-button-medium" style="margin:1em 0">
                	<a href="https://twitter.com/" style="background-color: #00bfff;margin-left:0;"><i class="icon ion-social-twitter"></i></a>
                	<a href="https://www.facebook.com/" style="background-color: #128BDB"><i class="icon ion-social-facebook"></i></a>
					<a href="https://plus.google.com/" style="background-color: #DF311F"><i class="icon ion-social-googleplus"></i></a>          
			</div>
        </div>

		<div class="col-md-6">
            <div class="embed-responsive embed-responsive-16by9">
            	<iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&amp;hl=en&amp;sll=-7.981898,112.626504&amp;sspn=0.009084,0.016512&amp;oq=melbourne&amp;hnear=Melbourne+Victoria,+Australia&amp;t=m&amp;z=10&amp;output=embed"></iframe>
            </div>
        </div>
	</div>	
</div>


<div data-num="94" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x11.png" data-cat="<?= $cat ?>">

	<div class="row">
		<div class="col-md-12">
            <h1 style="font-size:3em">Contact Us</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
            <h3>
				<i class="icon ion-ios-location-outline size-48" style="margin-top:0;line-height:1;vertical-align:middle;margin-right:10px"></i>
				<span style="vertical-align:middle">Address</span>
			</h3>
            <p>
              123 Street, City<br>
              State 12345
            </p>
        </div>

		<div class="col-md-4">
        	<h3>
 				<i class="icon ion-iphone size-48" style="margin-top:0;line-height:1;vertical-align:middle;margin-right:10px"></i>
				<span style="vertical-align:middle">Phone</span>
			</h3>
            <p>(123) 456 7890 / 456 7891</p>
        </div>

		<div class="col-md-4">
            <h3>
 				<i class="icon ion-ios-email-outline size-48" style="margin-top:0;line-height:1;vertical-align:middle;margin-right:10px"></i>
				<span style="vertical-align:middle">Email</span>
			</h3>
            <p>
              <a href="mailto:#">first.last@example.com</a>
            </p>
        </div>
	</div>
</div>

<div data-num="95" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/x12.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <h1>Contact Us</h1>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            <img src="<?= $assets_url ?>assets/minimalist-basic/x12-1.jpg" style="margin-top: 0px;">
        </div>

		<div class="col-md-6">
            <h3 style="margin-top:0">Company, Inc.</h3>
			<p>
              123 Street, City<br>
              State 12345<br>
              P: (123) 456 7890 / 456 7891
            </p>
			<div class="is-social edit">
                <a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                <a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
				<a href="https://www.pinterest.com/"><i class="icon ion-social-pinterest-outline" style="margin-right: 1em"></i></a>
                <a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
				<a href="https://www.instagram.com/"><i class="icon ion-social-instagram-outline"></i></a>
			</div>
        </div>
	</div>
</div>

