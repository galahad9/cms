<div data-num="310001" data-thumb="<?= $assets_url ?>thumbnails/navigation-0001.png" data-cat="<?= $cat ?>">
	<div class="topbar">
	    <div class="header">
	        <div class="h6-topbar">
	            <div class="container">
	                <div class="d-flex justify-content-between font-14 authentication-box">
	                    <div class="hidden-md-down align-self-center">
	                        <span class="font-bold">Phone:</span> +31 | <span class="font-bold">Email:</span> info@
	                    </div>
	                    <div class="">
	                        Vandaag <a href="" class="font-bold"> geopend</a>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="container po-relative">
	            <nav class="navbar navbar-expand-lg h6-nav-bar">
	                <a href="javascript:void(0)" class="navbar-brand">{{ LOGO }}</a>
	                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h6-info1" aria-controls="h6-info" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>
	                <div class="collapse navbar-collapse hover-dropdown font-14" id="h6-info1">
	                    <ul class="navbar-nav">
	                    	{{ MENU }}
	                    </ul>
	                    <div class="ml-auto act-buttons">
	                        <a href="/" class="btn btn-secondary font-13">Action 1</a>
	                        <a href="/" class="btn btn-danger-gradiant font-14">Action 2</a>
	                    </div>
	                </div>
	            </nav>
	        </div>
	    </div>
	</div>
</div>


<div data-num="310003" data-thumb="<?= $assets_url ?>thumbnails/navigation-0003.png" data-cat="<?= $cat ?>">
	<div class="topbar bg-transparent">
        <div class="header5">
            <div class="container po-relative">
                <nav class="navbar navbar-expand-lg hover-dropdown h5-nav-bar">
                    <a href="javascript:void(0)" class="navbar-brand">{{ LOGO }}</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h5-info" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>
                    <div class="collapse navbar-collapse" id="h5-info">
                        <ul class="navbar-nav ml-auto">
                            {{ MENU }}
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>

<div data-num="310004" data-thumb="<?= $assets_url ?>thumbnails/navigation-0004.png" data-cat="<?= $cat ?>">
	<div class="topbar">
        <div class="header7">
            <div class="container po-relative">
                <!-- Topbar -->
                <div class="h7-topbar">
                    <div class="d-flex justify-content-between font-14">
                        <div class=" hidden-md-down align-self-center">
                            Welcome to Company, Your Solution for All Web Needs
                        </div>
                        <div class="con-btn">
                            <a class="btn btn-success-gradiant btn-rounded btn-sm" data-toggle="collapse" href="">Contact</a>
                        </div>
                    </div>
                </div>
                <!-- End Topbar -->
                <!-- Navbar -->
                <div class="h7-nav-bar">
                    <div class="logo-box"><a href="javascript:void(0)" class="navbar-brand">{{ LOGO </a></div>
                    <button class="btn btn-success btn-circle hidden-md-up op-clo"><i class="ti-menu"></i></button>
                    <nav class="h7-nav-box">
                        <div class="h7-mini-bar">
                            <div class="d-flex justify-content-between">
                                <div class="gen-info font-14">
                                    <span><i class="fa fa-envelope text-success-gradiant"></i> info@company.com</span>
                                    <span><i class="fa fa-phone-square text-success-gradiant"></i> +01-234-6789</span>
                                    <span class="hidden-lg-down"><i class="fa fa-clock-o text-success-gradiant"></i> 8.00 AM - 6:00PM</span>
                                </div>
                                <div class="social-info hidden-lg-down">
                                    <a href="/"><i class="ti-facebook"></i></a>
                                    <a href="/"><i class="ti-twitter"></i></a>
                                    <a href="/"><i class="ti-google"></i></a>
                                    <a href="/"><i class="ti-youtube"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="main-nav">
                            <ul>
                                {{ MENU }}
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
 </div>

 <div data-num="310005" data-thumb="<?= $assets_url ?>thumbnails/navigation-0005.png" data-cat="<?= $cat ?>">
	<div class="topbar">
        <div class="header4 po-relative">
            <div class="h4-topbar">
                <div class="container">
                    <nav class="navbar navbar-expand-lg h4-nav">
                        <a class="hidden-lg-up">Navigation</a>
                        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#header4" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="ti-menu"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="header4">
                            <div class="hover-dropdown">
                                <ul class="navbar-nav">
                                    {{ MENU }}
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="h4-navbar">
                <div class="container">
                    <nav class="navbar navbar-expand-lg h4-nav-bar">
                        <a class="navbar-brand">{{ LOGO }}</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h4-info" aria-controls="h4-info" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sl-icon-options-vertical"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="h4-info">
                            <ul class="navbar-nav ml-auto text-uppercase">
                                <li class="nav-item">
                                    <a class="nav-link b-r">
                                        <div class="display-6 m-r-10"><i class="iconmind icon-Mail"></i></div>
                                        <div><small>Email us at</small>
                                            <h6 class="font-bold font-14">info@company.com</h6></div>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link">
                                        <div class="display-6 m-r-10"><i class="iconmind icon-Phone-2"></i></div>
                                        <div><small>CALL US NOW</small>
                                            <h6 class="font-bold ">703 (123) 4567</h6></div>
                                    </a>
                                </li>
                                <li class="nav-item donate-btn"><a href="/" class="btn btn-outline-primary">Contact</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
 </div>

 <div data-num="310006" data-thumb="<?= $assets_url ?>thumbnails/navigation-0006.png" data-cat="<?= $cat ?>">

	<div class="topbar">
        <div class="header16 po-relative">
            <!-- Topbar  -->
            <div class="h16-topbar">
                <div class="container">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand hidden-lg-up" href="/">Top Menu</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header16a" aria-controls="header16a" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sl-icon-options"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="header16a">
                            <ul class="navbar-nav font-14">
                                <li class="nav-item">Welcome to  <span class="text-dark">Companyname</span></li>
                            </ul>
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item"><a class="nav-link" href="/"><i class="ti-email"></i> info@company.com</a></li>
                                <li class="nav-item"><a class="nav-link" href="/"><i class="ti-twitter"></i></a></li>
                                <li class="nav-item"><a class="nav-link" href="/"><i class="ti-facebook"></i></a></li>
                                <li class="nav-item"><a class="nav-link" href="/"><i class=" ti-linkedin"></i></a></li>
                                <li class="nav-item"><a class="nav-link" href="/"><i class="ti-google"></i></a></li>
                                <li class="nav-item"><a class="nav-link" href="/"><i class="ti-instagram"></i></a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Infobar  -->
            <div class="h16-infobar">
                <div class="container">
                    <nav class="navbar navbar-expand-lg h16-info-bar">
                        <a class="navbar-brand">{{ LOGO }}</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h16-info" aria-controls="h16-info" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sl-icon-options-vertical"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="h16-info">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link">
                                        <div class="display-6 m-r-10"><i class="icon-Over-Time text-primary"></i></div>
                                        <div><small>Mon to Sat - <span class="text-dark">9:00-18:00</span> <br/>Sunday - Closed</small></div>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link">
                                        <div class="display-6 m-r-10"><i class="icon-Phone-2 text-primary"></i></div>
                                        <div><small>Book Table</small>
                                        <h5 class="font-bold">703 (123) 4567</h5></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Navbar  -->
            <div class="h16-navbar">
                <div class="container">
                    <nav class="navbar navbar-expand-lg h16-nav">
                        <a class="hidden-lg-up">Navigation</a>
                        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#header16" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="ti-menu"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="header16">
                            <div class="hover-dropdown">
                                <ul class="navbar-nav">
                                   {{ MENU }}
                                </ul>
                            </div>
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item search"><a class="nav-link" href="javascript:void(0)">CONTACT</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>


 <div data-num="310007" data-thumb="<?= $assets_url ?>thumbnails/navigation-0007.png" data-cat="<?= $cat ?>">
 	<div class="topbar">
        <div class="header3">
            <div class="po-relative">
                <div class="h3-topbar">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5">
                                <ul class="list-inline">
                                    <li><a href="mailto:info@company.com"><i class="icon-Mail info-icon"></i> info@company.com</a></li>
                                </ul>    
                            </div>
                            <div class="col-md-7 t-r">
                                <ul class="list-inline">
                                    <li><a href="/"><i class="ti-twitter"></i></a></li>
                                    <li><a href="/"><i class="ti-facebook"></i></a></li>
                                    <li><a href="/"><i class="ti-google"></i></a></li>
                                    <li><a href="/"><i class="ti-youtube"></i></a></li>
                                    <li><a href="/"><i class="ti-instagram"></i></a></li>
                                    <li><a><span class="vdevider"></span></a></li>
                                    <li><a href="/"><i class="icon-Phone-2 info-icon"></i> +205 123 4567</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="h3-navbar">
                    <div class="container">
                        <nav class="navbar navbar-expand-lg h3-nav">
                          <a class="navbar-brand" href="/">{{ LOGO }}</a>
                          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header3" aria-controls="header3" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>
                          
                            <div class="collapse navbar-collapse hover-dropdown" id="header3">
                                <ul class="navbar-nav">
                                 	{{ MENU }}
                                </ul>
                            </div>
                        </nav>
                    </div>    
                </div>
            </div>
        </div>    
    </div>
 </div>

 <div data-num="310008" data-thumb="<?= $assets_url ?>thumbnails/navigation-0008.png" data-cat="<?= $cat ?>">
	<div class="topbar">
        <div class="header11 po-relative">
            <div class="container">
                <!-- Header 1 code -->
                <nav class="navbar navbar-expand-lg h11-nav">
                    <a class="navbar-brand" href="/">{{ LOGO }}</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header11" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>
                    <div class="collapse navbar-collapse hover-dropdown flex-column" id="header11">
                        <div class="ml-auto h11-topbar">
                            <ul class="list-inline ">
                                <li><a href="/"><i class="ti-facebook"></i></a></li>
                                <li><a href="/"><i class="ti-twitter"></i></a></li>
                                <li><a href="/"><i class="ti-google"></i></a></li>
                                <li><a><i class="icon-Phone-2"></i> 215 123 4567</a></li>
                            </ul>
                        </div>
                        <ul class="navbar-nav ml-auto font-13">
                           	{{ MENU }}
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
 </div>


 <div data-num="310009" data-thumb="<?= $assets_url ?>thumbnails/navigation-0009.png" data-cat="<?= $cat ?>">
	<div class="topbar">
        <div class="header1 po-relative">
            <div class="container">
                <nav class="navbar navbar-expand-lg h1-nav">
                    <a class="navbar-brand" href="/">{{ LOGO }}</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mheader1" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="ti-menu"></span>
                    </button>
                    <div class="collapse navbar-collapse hover-dropdown" id="mheader1">
                        <ul class="navbar-nav">
                            {{ MENU }}
                        </ul>
                        <ul class="navbar-nav social-icon ml-auto">
                            <li class="nav-item"><a class="nav-link" href="/"><i class="ti-facebook"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="/"><i class="ti-twitter"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="/"><i class="ti-instagram"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="/"><i class="ti-youtube"></i></a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
 </div> 


<div data-num="310010" data-thumb="<?= $assets_url ?>thumbnails/navigation-0010.png" data-cat="<?= $cat ?>">
	<div class="topbar">
		<div class="header14 po-relative">
            <!-- Topbar  -->
            <div class="h14-topbar">
                <div class="container">
                    <nav class="navbar navbar-expand-lg font-14">
                        <a class="navbar-brand hidden-lg-up" href="/">Top Menu</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header14a" aria-controls="header14a" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sl-icon-options"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="header14a">
                            <ul class="navbar-nav">
                                <li class="nav-item"><a class="nav-link">We build awesome kits like this one here!</a></li>
                            </ul>
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item"><a class="nav-link"><i class="fa fa-clock-o"></i> Mon to Sat - 9:00-18:00 | Sunday - Closed</a></li>
                                <li class="nav-item"><a class="nav-link" href="/"><i class="ti-facebook"></i></a></li>
                                <li class="nav-item"><a class="nav-link" href="/"><i class="ti-twitter"></i></a></li>
                                <li class="nav-item"><a class="nav-link" href="/"><i class="ti-linkedin"></i></a></li>
                                <li class="nav-item"><a class="nav-link" href="/"><i class="ti-youtube"></i></a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Infobar  -->
            <div class="h14-infobar">
                <div class="container">
                    <nav class="navbar navbar-expand-lg h14-info-bar">
                        {{ LOGO }}
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h14-info" aria-controls="h14-info" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sl-icon-options-vertical"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="h14-info">
                            <ul class="navbar-nav ml-auto text-uppercase">
                                <li class="nav-item">
                                    <a class="nav-link">
                                        <div class="display-6 m-r-10"><i class="icon-Mail"></i></div>
                                        <div><small>Email us at</small>
                                        <h6 class="font-bold">info@wrappixel.com</h6></div>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link">
                                        <div class="display-6 m-r-10"><i class="icon-Phone-2"></i></div>
                                        <div><small>CALL US NOW</small>
                                            <h6 class="font-bold">703 (123) 4567</h6></div>
                                    </a>
                                </li>
                                <li class="nav-item donate-btn"><a href="/" class="btn btn-outline-primary">Request a Quote</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Navbar  -->
            <div class="h14-navbar">
                <div class="container">
                    <nav class="navbar navbar-expand-lg h14-nav">
                        <a class="hidden-lg-up">Navigation</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header14" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="ti-menu"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="header14">
                            <div class="hover-dropdown">
                                <ul class="navbar-nav">
                                	{{ MENU }}
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

