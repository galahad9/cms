
<div data-num="96" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/y01.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

			<h1 style="font-size: 4em">Ready to Purchase?</h1>

			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

		</div>

	</div>

	<div class="row">
		<div class="col-md-12">
            <br>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1" style="border-radius:30px;width:100%;max-width:400px;margin: 0 auto;">
				<div class="margin-30 center">
					<div class="display">
						<h1>$<span style="font-size: 2.7em;">59</span></h1>
					</div>
            		<p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
 					<div style="margin:2.5em 0">
            			<a href="#" class="btn btn-primary btn-lg edit">Purchase</a>
            		</div>
        		</div>
			</div>
		</div>
	</div>

</div>



<div data-num="97" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/y02.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

			<h1 style="font-size: 4em">Pricing</h1>

			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

		</div>

	</div>

	<div class="row">
		<div class="col-md-12">
            <br>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
           <div class="is-card is-dark-text shadow-1" style="border-radius:30px;"> 
				<div class="margin-30 center">
			    	<div class="display">
				    	<h1>$<span style="font-size: 2.5em">59</span></h1>
			    	</div>
                	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
 			   		<div style="margin:2.7em 0">
            	    	<a href="#" class="btn btn-primary btn-lg edit">Purchase</a>
                	</div>
            	</div>
        	</div>
		</div>

		<div class="col-md-6">
           <div class="is-card is-dark-text shadow-1" style="border-radius:30px;"> 
				<div class="margin-30 center">
			   	 	<div class="display">
				    	<h1>$<span style="font-size: 2.5em">99</span></h1>
			    	</div>
                	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
 			    	<div style="margin:2.7em 0">
            	    	<a href="#" class="btn btn-primary btn-lg edit">Purchase</a>	
                	</div>
           		</div>    
        	</div>
		</div>
	</div>
	
</div>



<div data-num="98" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/y03.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

			<div class="display">

                    		<h1 style="font-size: 3.5em">Pricing</h1>

			</div>

                </div>

	</div>

	<div class="row">
		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1" style="border-radius:20px;"> 
				<div class="margin-25 center">
			   	 	<div class="display">
				    	<h1>$<span style="font-size: 2.3em;">29</span></h1>
			   	 	</div>
			    	<h3>Personal</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
 			    	<div style="margin:2.2em 0">
            	    	<a href="#" class="btn btn-primary btn-lg edit">Purchase</a>
                	</div>
              	</div>
        	</div>
		</div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1" style="border-radius:20px;"> 
				<div class="margin-25 center">
			    	<div class="display">
				    	<h1>$<span style="font-size: 2.3em">59</span></h1>
			    	</div>
			    	<h3>Professional</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
 			    	<div style="margin:2.2em 0">
            			<a href="#" class="btn btn-primary btn-lg edit">Purchase</a>
                	</div>
        		</div>
        	</div>
		</div>


		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1" style="border-radius:20px;"> 
				<div class="margin-25 center">
			    	<div class="display">
				    	<h1>$<span style="font-size: 2.3em">89</span></h1>
			    	</div>
			    	<h3>Business</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
 			    	<div style="margin:2.2em 0">
            			<a href="#" class="btn btn-primary btn-lg edit">Purchase</a>           		
                	</div>
        		</div>
         	</div>
		</div>
	</div>

</div>


<div data-num="99" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/y04.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12 center">

			<h1 style="font-size: 4em">Pricing Plans</h1>

		</div>

	</div>

	<div class="row">

		<div class="col-md-4 center">
			<div class="padding-20">
				<div class="display" style="margin-bottom: -2em">

					<h1>$<span style="font-size: 2.5em">19</span></h1>

				</div>

				<p><b>Per Month</b></p>

				<h3>Standard</h3>

				<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

 				<div style="margin:2em 0">

            			<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">Purchase</a>

            	</div>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<div class="display" style="margin-bottom: -2em">

					<h1>$<span style="font-size: 2.5em">39</span></h1>

				</div>

				<p><b>Per Month</b></p>

				<h3>Professional</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

 				<div style="margin:2em 0">

            		<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">Purchase</a>

            	</div>
			</div>
        </div>

		<div class="col-md-4 center">
			<div class="padding-20">
				<div class="display" style="margin-bottom: -2em">

					<h1>$<span style="font-size: 2.5em">79</span></h1>

				</div>

				<p><b>Per Month</b></p>

				<h3>Business</h3>

            	<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

 				<div style="margin:2em 0">

            		<a href="#" class="btn btn-primary btn-lg edit" style="border-radius: 50px">Purchase</a>

            	</div>
			</div>
        </div>

	</div>

</div>



<div data-num="100" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/y05.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
                <h1 style="font-size: 3.5em">Pricing</h1>
			</div>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1"> 
				<div class="margin-25 center">					    
					<h3>Personal</h3>
			    	<div class="display">
				    	<h1>$<span style="font-size: 1.7em;">29</span></h1>
			   	 	</div>
					<ul style="text-align:left">
						<li>Lorem Ipsum is simply</li>
						<li>Printing and Typesetting</li>
						<li>Vivamus leo ante</li>
					</ul>
 			    	<div style="margin:2em 0">
            	    	<a href="#" class="btn btn-primary btn-lg edit">Buy Now</a>
                	</div>
        		</div>
        	</div>
		</div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1"> 
				<div class="margin-25 center">		    
					<h3>Professional</h3>
			    	<div class="display">
				    	<h1>$<span style="font-size: 1.7em;">39</span></h1>
			    	</div>
					<ul style="text-align:left">
						<li>Lorem Ipsum is simply</li>
						<li>Printing and Typesetting</li>
						<li>Vivamus leo ante</li>
					</ul>
 			    	<div style="margin:2em 0">
            	    	<a href="#" class="btn btn-primary btn-lg edit">Buy Now</a>
                	</div>
            	</div>
        	</div>
		</div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1"> 
				<div class="margin-25 center">			    
					<h3>Business</h3>
			    	<div class="display">
				    	<h1>$<span style="font-size: 1.7em;">49</span></h1>
			    	</div>
					<ul style="text-align:left">
						<li>Lorem Ipsum is simply</li>
						<li>Printing and Typesetting</li>
						<li>Vivamus leo ante</li>
					</ul>
 			    	<div style="margin:2em 0">
            	    	<a href="#" class="btn btn-primary btn-lg edit">Buy Now</a>
                	</div>
            	</div>
        	</div>
		</div>
	</div>
</div>



<div data-num="101" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/y06.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
				<h1 style="font-size: 3.5em;margin:0.5em 0">Pricing</h1>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-4 center">
			<div class="display">
				<h1>$<span style="font-size: 2.5em">19</span></h1>
			</div>
            <h3>Basic</h3>
            <ul style="text-align:left">
				<li>Lorem Ipsum is simply text</li>
				<li>Printing and typesetting</li>
				<li>Lorem Ipsum dolor sit amet</li>
			</ul>
			<div style="margin:2em 0 2.5em;">
            	<a href="#" class="btn btn-primary btn-lg edit">Choose Plan</a>
            </div>
        </div>

		<div class="col-md-4 center">
			<div class="display">
				<h1>$<span style="font-size: 2.5em">39</span></h1>
			</div>
            <h3>Professional</h3>
            <ul style="text-align:left">
				<li>Lorem Ipsum is simply text</li>
				<li>Printing and typesetting</li>
				<li>Lorem Ipsum dolor sit amet</li>
			</ul>
			<div style="margin:2em 0 2.5em;">
           		<a href="#" class="btn btn-primary btn-lg edit">Choose Plan</a>
            </div>
        </div>

		<div class="col-md-4 center">
			<div class="display">
				<h1>$<span style="font-size: 2.5em">59</span></h1>
			</div>
            <h3>Business</h3>
            <ul style="text-align:left">
				<li>Lorem Ipsum is simply text</li>
				<li>Printing and typesetting</li>
				<li>Lorem Ipsum dolor sit amet</li>
			</ul>
			<div style="margin:2em 0 2.5em;">
            	<a href="#" class="btn btn-primary btn-lg edit">Choose Plan</a>
            </div>
        </div>
	</div>
</div>



<div data-num="102" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/y07.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
				<h1 style="font-size: 3.5em;margin:1em 0">Pricing</h1>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1"> 
				<div class="margin-25 center">
			    	<h2>PRO</h2>
					<p style="font-size:1.3em;color:#808080"><i>$29 per month</i></p>
            		<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
 			    	<div style="margin:2em 0">
            	    	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Buy Now</a>
                	</div>
            	</div>
        	</div>
		</div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1"> 
				<div class="margin-25 center">
			    	<h2>BUSINESS</h2>
            		<p style="font-size:1.3em;color:#808080"><i>$39 per month</i></p>
            		<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
 			    	<div style="margin:2em 0">
            			<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Buy Now</a>
                	</div>
        		</div>
        	</div>
		</div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1"> 
				<div class="margin-25 center">
			    	<h2>DEVELOPER</h2>
            		<p style="font-size:1.3em;color:#808080"><i>$59 per month</i></p>
            		<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
 			    	<div style="margin:2em 0">
            			<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Buy Now</a>
                	</div>
        		</div>
        	</div>
		</div>
	</div>
</div>



<div data-num="103" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/y08.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
                <h1 style="font-size: 3.5em">Pricing Plans</h1>
			</div>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4 center">
			<h1>Basic</h1>
            <div class="display">
				<h1>$<span style="font-size: 1.8em">19</span></h1>
			</div>
			<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
			<div style="margin:2em 0">
            		<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Select Plan</a>
            </div>
        </div>

		<div class="col-md-4 center">
			<h1>Professional</h1>
            <div class="display">
				<h1>$<span style="font-size: 1.8em">39</span></h1>
			</div>
			<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
			<div style="margin:2em 0">
            	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Select Plan</a>
            </div>
		</div>

		<div class="col-md-4 center">
			<h1>Business</h1>
            <div class="display">
				<h1>$<span style="font-size: 1.8em">49</span></h1>
			</div>
			<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
			<div style="margin:2em 0">
            		<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Select Plan</a>
            </div>
		</div>
	</div>
</div>


<div data-num="104" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/y09.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
                <h1 style="font-size: 3.7em;margin:1em 0">Pricing</h1>
			</div>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
           <div class="is-card is-dark-text shadow-1"> 
				<div class="margin-30 center">
			    	<h1>Basic</h1>
			    	<div class="display">
				    	<h1>$<span style="font-size: 2em;">29</span></h1>
			    	</div>
  					<ul style="text-align:left">
						<li>Lorem Ipsum is simply text of the printing.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipiscing.</li>
						<li>Vivamus leo ante, consectetur sit amet.</li>
					</ul>
 			    	<div style="margin:2em 0">
            	    	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Get Started</a>
                	</div>
            	</div>
        	</div>
		</div>

		<div class="col-md-6">
           <div class="is-card is-dark-text shadow-1"> 
				<div class="margin-30 center">
			    	<h1>Pro</h1>
			    	<div class="display">
				    	<h1>$<span style="font-size: 2em;">39</span></h1>
			    	</div>
  					<ul style="text-align:left">
						<li>Lorem Ipsum is simply text of the printing.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipiscing.</li>
						<li>Vivamus leo ante, consectetur sit amet.</li>
					</ul>
 			    	<div style="margin:2em 0">
            	    	<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Get Started</a>
                	</div>
            	</div>
        	</div>
		</div>
	</div>
</div>

<div data-num="105" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/y10.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<h1 style="font-size: 4em;margin:1em 0">Pricing Plans</h1>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1"> 
				<div class="margin-25 center">
			    	<h2>Basic</h2>
            		<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
 			    	<h3>$ <span style="font-size:3em;font-weight:600">39</span>/mo</h3>
					<div style="margin:2em 0">
            			<a href="#" class="btn btn-primary btn-lg edit">Choose Plan</a>
            		</div>
        		</div>
            </div>
		</div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1"> 
				<div class="margin-25 center">
			    	<h2>Professional</h2>
            		<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
 			    	<h3>$ <span style="font-size:3em;font-weight:600">49</span>/mo</h3>
					<div style="margin:2em 0">
            			<a href="#" class="btn btn-primary btn-lg edit">Choose Plan</a>
            		</div>
        		</div>
        	</div>
		</div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1"> 
				<div class="margin-25 center">
			    	<h2>Business</h2>
            		<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
 			    	<h3>$ <span style="font-size:3em;font-weight:600">59</span>/mo</h3>
					<div style="margin:2em 0">
            			<a href="#" class="btn btn-primary btn-lg edit">Choose Plan</a>
            		</div>
        		</div>
         	</div>
		</div>
	</div>
</div>
