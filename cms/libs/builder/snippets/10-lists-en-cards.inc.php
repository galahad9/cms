<div data-num="36" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q01.png" data-cat="<?= $cat ?>">

    <div class="row">

        <div class="col-md-6">

            <div class="list">

                <i class="icon ion-checkmark"></i>

                <h3>Feature Item</h3>

                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

            </div>

        </div>

        <div class="col-md-6">

            <div class="list">

                <i class="icon ion-checkmark"></i>

                <h3>Feature Item</h3>

                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

            </div>

        </div>

    </div> 	

</div>



<div data-num="37" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q02.png" data-cat="<?= $cat ?>">

    <div class="row">

        <div class="col-md-4">

            <div class="list">

                <i class="icon ion-checkmark"></i>

                <h3>Feature Item</h3>

                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

            </div>

        </div>

        <div class="col-md-4">

            <div class="list">

                <i class="icon ion-checkmark"></i>

                <h3>Feature Item</h3>

                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

            </div>

        </div>

        <div class="col-md-4">

            <div class="list">

                <i class="icon ion-checkmark"></i>

                <h3>Feature Item</h3>

                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

            </div>

        </div>

    </div> 	

</div>


<div data-num="38" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q30.png" data-cat="<?= $cat ?>">

    <div class="row">
        <div class="col-md-6">
				<i class="icon ion-android-microphone size-48"></i>

				<h3 style="margin: 0.5em 0">Feature Item</h3>

            	<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
       </div>

       <div class="col-md-6">
				<i class="icon ion-social-codepen-outline size-48"></i>

				<h3 style="margin: 0.5em 0">Feature Item</h3>

           		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
        </div>
    </div>

</div>

<div data-num="39" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q31.png" data-cat="<?= $cat ?>">

    <div class="row">
        <div class="col-md-4">
			
				<i class="icon ion-android-favorite-outline size-48"></i>

				<h3 style="margin: 0.5em 0">Feature Item</h3>

           		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
       </div>

       <div class="col-md-4">
				<i class="icon ion-android-create size-48"></i>

				<h3 style="margin: 0.5em 0">Feature Item </h3>

           		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>		
       </div><div class="col-md-4">
			
				<i class="icon ion-ios-paperplane size-48"></i>

				<h3 style="margin: 0.5em 0">Feature Item </h3>

           		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
       </div>
    </div>

</div>


<div data-num="66" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q21.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12">

                    <h1 style="text-align: center; font-size: 3em">Product Features</h1>

                </div>

	</div>


	<div class="row">

		<div class="col-md-4">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/q21-1.jpg" style="border-radius:500px">
				<br><br>
            		<div class="list">

                		<i class="icon ion-checkmark"></i>

                		<h3>Feature Item</h3>

                		<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

            		</div>
			</div>
        </div>

        <div class="col-md-4">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/q21-2.jpg" style="border-radius:500px">
				<br><br>
            		<div class="list">

                		<i class="icon ion-checkmark"></i>

                		<h3>Feature Item</h3>

                		<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

            		</div>
			</div>
        </div>

        <div class="col-md-4">
			<div class="padding-20">
				<img src="<?= $assets_url ?>assets/minimalist-basic/q21-3.jpg" style="border-radius:500px">
				<br><br>
            		<div class="list">

                		<i class="icon ion-checkmark"></i>

                		<h3>Feature Item</h3>

                		<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

            		</div>
			</div>
        </div>

	</div>

</div>




<div data-num="67" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q24.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-6">
			<div class="padding-20">
				<i class="icon ion-android-microphone size-64"></i>

				<h3 style="margin: 0.5em 0">FEATURE ITEM</h3>

            	<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
			</div>
       </div>

       <div class="col-md-6">
			<div class="padding-20">
				<i class="icon ion-social-codepen-outline size-64"></i>

				<h3 style="margin: 0.5em 0">FEATURE ITEM</h3>

           		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
			</div>
        </div>

	</div>

	<div class="row">

		<div class="col-md-6">
			<div class="padding-20">
				<i class="icon ion-android-favorite-outline size-64"></i>

				<h3 style="margin: 0.5em 0">FEATURE ITEM</h3>

           		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
           </div>
       </div>

       <div class="col-md-6">
			<div class="padding-20">
				<i class="icon ion-android-create size-64"></i>

				<h3 style="margin: 0.5em 0">FEATURE ITEM</h3>

           		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
           </div>			
       </div>

	</div>

</div>

<div data-num="68" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q25.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size: 3em">Best Features</h1> 
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
            <br>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3 center">
			<p style="margin-top:0;line-height:1"><i class="icon ion-ios-heart-outline size-64"></i></p>
		</div>
		
		<div class="col-md-3">
			<p style="margin-top:0;line-height:2"><b>Feature Item</b><br>Lorem Ipsum is simply dummy text.</p>
		</div>
		
		<div class="col-md-3 center">
			<p style="margin-top:0;line-height:1"><i class="icon ion-ios-mic-outline size-64"></i></p>
		</div>
		
		<div class="col-md-3">
			<p style="margin-top:0;line-height:2"><b>Feature Item</b><br>Lorem Ipsum is simply dummy text.</p>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
            <br>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-3 center">
			<p style="margin-top:0;line-height:1"><i class="icon ion-ios-paperplane size-64"></i></p>
		</div>

		<div class="col-md-3">
			<p style="margin-top:0;line-height:2"><b>Feature Item</b><br>Lorem Ipsum is simply dummy text.</p>
		</div>

		<div class="col-md-3 center">
			<p style="margin-top:0;line-height:1"><i class="icon ion-ios-film-outline size-64"></i></p>
		</div>

		<div class="col-md-3">
			<p style="margin-top:0;line-height:2"><b>Feature Item</b><br>Lorem Ipsum is simply dummy text.</p>
		</div>
	</div>
</div>

<div data-num="69" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q26.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size: 3em">Product Features</h1>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
				<img src="<?= $assets_url ?>assets/minimalist-basic/q26-1.jpg">
				<div class="list">
                		<i class="icon ion-checkmark"></i>
                		<h3>Feature Item</h3>
                		<p>Lorem Ipsum is simply dummy text of the typesetting industry.</p>
            	</div>
				<div class="list">
                		<i class="icon ion-checkmark"></i>
                		<h3>Feature Item</h3>
                		<p>Lorem Ipsum is simply dummy text of the typesetting industry.</p>
            	</div>
		</div>
		<div class="col-md-6">
				<img src="<?= $assets_url ?>assets/minimalist-basic/q26-2.jpg">
				<div class="list">
                		<i class="icon ion-checkmark"></i>
                		<h3>Feature Item</h3>
                		<p>Lorem Ipsum is simply dummy text of the typesetting industry.</p>
            	</div>
				<div class="list">
                		<i class="icon ion-checkmark"></i>
                		<h3>Feature Item</h3>
                		<p>Lorem Ipsum is simply dummy text of the typesetting industry.</p>
            	</div>
		</div>
	</div>
</div>

<div data-num="70" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q27.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            <h1 style="font-size: 3em">Product Features</h1>     
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-4 center">
			<i class="icon ion-ios-monitor-outline size-64"></i>
			<h3 style="margin-top:0;">Feature Item</h3>
			<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
		</div>

		<div class="col-md-4 center">
			<i class="icon ion-android-download size-64"></i>
			<h3 style="margin-top:0;">Feature Item</h3>
			<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
		</div>

		<div class="col-md-4 center">
			<i class="icon ion-ios-heart-outline size-64"></i>
			<h3 style="margin-top:0;">Feature Item</h3>
			<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
		</div>
	</div>
</div>

<div data-num="71" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q28.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
            	<h1 style="font-size: 3em">Product Features</h1>     
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6 center">
                <i class="icon ion-compose size-64" style="margin-top:0"></i>
				<h3 style="margin-top:0">Feature Item</h3>
                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				<div style="margin:2em 0 2.5em;">
            		<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Read More</a>
            	</div>
        </div>

		<div class="col-md-6 center">
                <i class="icon ion-android-favorite-outline size-64" style="margin-top:0"></i>
				<h3 style="margin-top:0">Feature Item</h3>
                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				<div style="margin:2em 0 2.5em;">
            		<a href="#" class="btn btn-primary btn-lg edit" style="border-radius:50px">Read More</a>
            	</div>
        </div>
	</div>
</div>

<div data-num="72" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q29.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12 center">
			<div class="display">
				<h1>Product Features</h1>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            <h3>
 				<i class="icon ion-ios-heart-outline size-48" style="margin-top:0;line-height:1;vertical-align:middle;margin-right:10px"></i>
				<span style="vertical-align:middle">Feature Item</span>
			</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>

		<div class="col-md-6">
            <h3>
 				<i class="icon ion-ios-camera-outline size-48" style="margin-top:0;line-height:1;vertical-align:middle;margin-right:10px"></i>
				<span style="vertical-align:middle">Feature Item</span>
			</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
            <h3>
 				<i class="icon ion-ios-compose-outline size-48" style="margin-top:0;line-height:1;vertical-align:middle;margin-right:10px"></i>
				<span style="vertical-align:middle">Feature Item</span>
			</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>

		<div class="col-md-6">
            <h3>
 				<i class="icon ion-ios-paperplane-outline size-48" style="margin-top:0;line-height:1;vertical-align:middle;margin-right:10px"></i>
				<span style="vertical-align:middle">Feature Item</span>
			</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
	</div>
</div>



<div data-num="65" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/q20.png" data-cat="<?= $cat ?>">

	<div class="row">

		<div class="col-md-12">

                    <h1 style="text-align: center; font-size: 3em">Product Features</h1>        

                </div>

	</div>

	<div class="row">

		<div class="col-md-6">

			<img src="<?= $assets_url ?>assets/minimalist-basic/q20-1.png">


        	</div>

		<div class="col-md-6">

			<div class="list">

                		<i class="icon ion-checkmark"></i>

                		<h3>Feature Item</h3>

                		<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

            		</div>

			<div class="list">

               	 		<i class="icon ion-checkmark"></i>

                		<h3>Feature Item</h3>

                		<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

            		</div>

			<div class="list">

                		<i class="icon ion-checkmark"></i>

                		<h3>Feature Item</h3>

                		<p>Lorem Ipsum is simply dummy text of the printing industry.</p>

            		</div>

        	</div>

	</div>

</div>


<div data-num="265" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de01.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
					<img src="<?= $assets_url ?>assets/minimalist-basic/z05-1.jpg" style="border-radius: 500px;" alt="">
			    	<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM </h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>              
        		</div>
			</div>
        </div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			    	<img src="<?= $assets_url ?>assets/minimalist-basic/z05-2.jpg" style="border-radius: 500px;" alt="">
			    	<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            		<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>    
        		</div>
			</div>
        </div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			  		<img src="<?= $assets_url ?>assets/minimalist-basic/z05-3.jpg" style="border-radius: 500px;" alt="">
			   		<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>         
        		</div>
			</div>
         </div>
	</div>
</div>

<div data-num="266" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de02.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-6">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">
                	<img src="<?= $assets_url ?>assets/minimalist-basic/k06-1.jpg" style="border-radius: 500px;" alt="">
			   		<h3 class="size-32 is-title1-32 is-title-lite">LOREM IPSUM </h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>     
        		</div>
			</div>
        </div>

		<div class="col-md-6">
     		<div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">
                	<img src="<?= $assets_url ?>assets/minimalist-basic/k06-2.jpg" style="border-radius: 500px;" alt="">
			     	<h3 class="size-32 is-title1-32 is-title-lite">LOREM IPSUM </h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>  
            	</div>
			</div>
        </div>
	</div>
</div>

<div data-num="267" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de03.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <div class="is-card max-390 is-dark-text shadow-1">
				<div class="margin-30 center">
                	<img src="<?= $assets_url ?>assets/minimalist-basic/k06-1.jpg" style="border-radius: 500px;" alt="">
			   		<h3 class="size-32 is-title1-32 is-title-lite">LOREM IPSUM </h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
					<div class="is-social edit" style="margin:2em 0">
                		<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus" style="margin-right: 1em"></i></a>
                		<a href="mailto:you@example.com"><i class="icon ion-ios-email"></i></a>
					</div>     
        		</div>
			</div>
        </div>
	</div>
</div>

<div data-num="268" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de04.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">
			   		<i class="icon ion-ios-lightbulb-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            	</div>
			</div>
        </div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">				
			    	<i class="icon ion-ios-lightbulb-outline size-48" style="line-height:1.5"></i>				
			   		<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        		</div>
			</div>
        </div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">			
			   		<i class="icon ion-ios-lightbulb-outline size-48" style="line-height:1.5"></i>					
			   		<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        		</div>
			</div>
        </div>
	</div>
</div>

<div data-num="269" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de05.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-6">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">				
			   		<i class="icon ion-ios-lightbulb-outline size-48" style="line-height:1.5"></i>				
			   		<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            	</div>
			</div>
        </div>

		<div class="col-md-6">
			 <div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">				
			    	<i class="icon ion-ios-lightbulb-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        		</div>
			</div>
        </div>
	</div>
</div>

<div data-num="270" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de06.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <div class="is-card max-390 is-dark-text shadow-1">
				<div class="margin-30 center">				
			   		<i class="icon ion-ios-lightbulb-outline size-48" style="line-height:1.5"></i>				
			   		<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            	</div>
			</div>
        </div>
	</div>
</div>

<div data-num="271" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de07.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-4">
 			<div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">				
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> 		
            	</div>
			</div>
        </div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">			
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>	 		
              </div>
			</div>
        </div>
        
		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">				
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>		
              	</div>
			</div>
        </div>
	</div>
</div>

<div data-num="272" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de08.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-6">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">				
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> 		
            	</div>
			</div>
        </div>

		<div class="col-md-6">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">				
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> 			
              </div>
			</div>
        </div>
	</div>
</div>

<div data-num="273" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de09.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card max-390 is-dark-text shadow-1">
				<div class="margin-30 center">				
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> 		
            	</div>
			</div>
        </div>
	</div>
</div>

<div data-num="274" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de10.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">				
			    	<i class="icon ion-ios-location-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
            	</div>
			</div>
        </div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">			
			    	<i class="icon ion-ios-location-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
              	</div>
			</div>
        </div>
        
		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">	
			    	<i class="icon ion-ios-location-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
              </div>
			</div>
        </div>	
	</div>
</div>

<div data-num="275" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de11.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-6">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">			
			    	<i class="icon ion-ios-location-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
              </div>
			</div>
        </div>
        
		<div class="col-md-6">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">				
			    	<i class="icon ion-ios-location-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
              </div>
			</div>
        </div>
	</div>
</div>

<div data-num="276" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de12.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card max-390 is-dark-text shadow-1">
				<div class="margin-30 center">			
			    	<i class="icon ion-ios-location-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-lite">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
              </div>
			</div>
        </div>
	</div>
</div>


<div data-num="277" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de13.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-4">
 			<div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">				
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
            	</div>
			</div>
        </div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">			
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
              	</div>
			</div>
        </div>
        
		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1">
				<div class="margin-25 center">				
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
              	</div>
			</div>
        </div>
	</div>
</div>

<div data-num="278" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de14.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-6">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">				
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
            	</div>
			</div>
        </div>

		<div class="col-md-6">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-30 center">				
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>			
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
              	</div>
			</div>
        </div>
	</div>
</div>

<div data-num="279" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de15.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card max-390 is-dark-text shadow-1">
				<div class="margin-30 center">				
			    	<i class="icon ion-ios-heart-outline size-48" style="line-height:1.5"></i>				
			    	<h3 class="size-28 is-title1-28 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 1em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
            	</div>
			</div>
        </div>
	</div>
</div>

<div data-num="280" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de16.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/p02-1.jpg" class="margin-0" alt="">				
				<div class="margin-25">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
				</div>
             </div>
        </div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/p02-1.jpg" class="margin-0" alt="">				
				<div class="margin-25">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
				</div>
            </div>
        </div>
        
		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/p02-1.jpg" class="margin-0" alt="">				
				<div class="margin-25">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
				</div>
            </div>
        </div>
	</div>
</div>

<div data-num="281" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de17.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-6">
    		<div class="is-card max-390 is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/g03-1.jpg" class="margin-0" alt="">				
				<div class="margin-30">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
				</div>
            </div>
        </div>

		<div class="col-md-6">
           <div class="is-card max-390 is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/g03-1.jpg" class="margin-0" alt="">				
				<div class="margin-30">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
				</div>
            </div>
        </div>
	</div>
</div>

<div data-num="282" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de18.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
    		<div class="is-card max-390 is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/g03-1.jpg" class="margin-0" alt="">				
				<div class="margin-30">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
				</div>
            </div>
        </div>
	</div>
</div>

<div data-num="283" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de19.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">				
			    <img src="<?= $assets_url ?>assets/minimalist-basic/p02-1.jpg" class="margin-0" alt="">				
				<div class="margin-25">				
			    	<h3 class="size-28 margin-0 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
            	</div>
			</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">				
			    <img src="<?= $assets_url ?>assets/minimalist-basic/p02-1.jpg" class="margin-0" alt="">				
				<div class="margin-25">				
			    	<h3 class="size-28 margin-0 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
            	</div>
			</div>
		</div>

		<div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">				
			    <img src="<?= $assets_url ?>assets/minimalist-basic/p02-1.jpg" class="margin-0" alt="">				
				<div class="margin-25">				
			    	<h3 class="size-28 margin-0 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
            	</div>
			</div>
		</div>
	</div>
</div>

<div data-num="284" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de20.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-6">
            <div class="is-card max-390 is-dark-text shadow-1">				
			    <img src="<?= $assets_url ?>assets/minimalist-basic/g03-1.jpg" class="margin-0" alt="">				
				<div class="margin-30">				
			    	<h3 class="size-28 margin-0 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
            	</div>
			</div>
		</div>

		<div class="col-md-6">
            <div class="is-card max-390 is-dark-text shadow-1">				
			    <img src="<?= $assets_url ?>assets/minimalist-basic/g03-1.jpg" class="margin-0" alt="">				
				<div class="margin-30">				
			    	<h3 class="size-28 margin-0 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
            	</div>
			</div>
		</div>
	</div>
</div>

<div data-num="285" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de21.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
            <div class="is-card max-390 is-dark-text shadow-1">				
			    <img src="<?= $assets_url ?>assets/minimalist-basic/g03-1.jpg" class="margin-0" alt="">				
				<div class="margin-30">				
			    	<h3 class="size-28 margin-0 is-title-bold">LOREM IPSUM</h3>
            		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
 					<div style="margin:2em 0 0.5em">
						<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Read More</a>
					</div>
            	</div>
			</div>
		</div>
	</div>
</div>

<div data-num="286" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de22.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-40 center">			
            		<h1 class="size-48 is-title5-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
            		<p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<div style="margin:2em 0 0.5em">
 						<a href="#" class="is-btn is-btn-ghost2 is-upper edit">Read More</a> &nbsp;
            			<a href="#" class="is-btn is-btn-ghost1 is-upper edit">Buy Now</a>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>

<div data-num="287" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de23.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-40 center">	
            		<h1 class="size-48 is-title5-48 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT</h1>
            		<p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<div style="margin:2em 0 0.5em">
             			<a href="#" class="is-btn is-btn-ghost1 is-upper edit">Read More</a>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>

<div data-num="288" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de24.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-40 center">	
            		<h1 class="size-32 is-title5-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<div style="margin:2em 0 0.5em">
 						<a href="#" class="is-btn is-btn-small is-btn-ghost2 is-upper edit">Read More</a> &nbsp;
            			<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Buy Now</a>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>

<div data-num="289" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de25.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-40 center">			
            		<h1 class="size-32 is-title5-32 is-title-lite">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<div style="margin:2em 0 0.5em">
            			<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Buy Now</a>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>

<div data-num="290" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de26.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-40 center">		
            		<h1 class="size-48 is-title1-48 is-title-bold">LOREM IPSUM IS DUMMY TEXT</h1>
            		<p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<div style="margin:2em 0 0.5em">
 						<a href="#" class="is-btn is-btn-ghost2 is-upper edit">Read More</a> &nbsp;
            			<a href="#" class="is-btn is-btn-ghost1 is-upper edit">Buy Now</a>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>

<div data-num="291" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de27.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-40 center">		
            		<h1 class="size-48 is-title1-48 is-title-bold">LOREM IPSUM IS DUMMY TEXT</h1>
            		<p class="size-21">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<div style="margin:2em 0 0.5em">
 						<a href="#" class="is-btn is-btn-ghost1 is-upper edit">Read More</a>
					</div>           
				</div>
			</div>
        </div>
	</div>
</div>

<div data-num="292" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de28.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-40 center">			
            		<h1 class="size-32 is-title2-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<div style="margin:2em 0 0.5em">
 						<a href="#" class="is-btn is-btn-small is-btn-ghost2 is-upper edit">Read More</a> &nbsp;
            			<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Buy Now</a>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>

<div data-num="293" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de29.png" data-cat="<?= $cat ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="is-card is-dark-text shadow-1">
				<div class="margin-40 center">	
            		<h1 class="size-32 is-title2-32 is-title-bold">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY</h1>
            		<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<div style="margin:2em 0 0.5em">
            			<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Buy Now</a>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>


<div data-num="294" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de30.png" data-cat="<?= $cat ?>">
	<div class="row">

        <div class="col-md-4">
            <div class="is-card is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/de01-1.jpg" class="margin-0" alt="">				
				<div class="margin-25">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0 0.5em">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

			</div>

				</div>
             </div>
        </div>

		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/de01-2.jpg" class="margin-0" alt="">				
				<div class="margin-25">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0 0.5em">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

			</div>

				</div>
            </div>
        </div>
        
		<div class="col-md-4">
           <div class="is-card is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/de01-3.jpg" class="margin-0" alt="">				
				<div class="margin-25">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0 0.5em">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

			</div>

				</div>
            </div>
        </div>

	</div>
</div>

<div data-num="295" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de31.png" data-cat="<?= $cat ?>">
	<div class="row">

        <div class="col-md-6">
    		<div class="is-card max-390 is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/de02-1.jpg" class="margin-0" alt="">				
				<div class="margin-30">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0 0.5em">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

			</div>

				</div>
            </div>
        </div>

		<div class="col-md-6">
           <div class="is-card max-390 is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/de02-2.jpg" class="margin-0" alt="">				
				<div class="margin-30">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0 0.5em">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

			</div>

				</div>
            </div>
        </div>

	</div>
</div>

<div data-num="296" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de32.png" data-cat="<?= $cat ?>">
	<div class="row">

        <div class="col-md-12">
    		<div class="is-card max-390 is-dark-text shadow-1">
				<img src="<?= $assets_url ?>assets/minimalist-basic/de02-1.jpg" class="margin-0" alt="">				
				<div class="margin-30">				
					<h3 class="size-28 margin-0 is-title-lite">LOREM IPSUM</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="is-social edit" style="margin:2em 0 0.5em">

                		<a href="https://twitter.com/"><i class="icon ion-social-twitter"></i></a>

                		<a href="https://www.facebook.com/"><i class="icon ion-social-facebook"></i></a>

                		<a href="https://plus.google.com/"><i class="icon ion-social-googleplus"></i></a>

                		<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>

					</div>

				</div>
            </div>
        </div>

	</div>
</div>

<div data-num="297" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de33.png" data-cat="<?= $cat ?>">
	<div class="row">

        <div class="col-md-12">
            <div class="is-card is-card-circle is-dark-text shadow-1">
                <div class="is-card-content-centered">
                    <h1 class="size-32 is-title1-32 is-title-lite">LOREM IPSUM IS DUMMY TEXT</h1>
                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                    <div style="margin:1.2em 0 0">
                        <a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Read More</a>	
                    </div>	   
            	</div>                     
            </div>
        </div>

	</div>
</div>

<div data-num="298" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de34.png" data-cat="<?= $cat ?>">
	<div class="row">

        <div class="col-md-12">
            <div class="is-card is-card-circle is-dark-text shadow-1">
                <div class="is-card-content-centered">
                    <h1 class="size-32 is-title1-32 is-title-bold">LOREM IPSUM IS DUMMY TEXT</h1>
                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                    <div style="margin:1.2em 0 0">
                        <a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Read More</a>	
                    </div>	   
            	</div>                     
            </div>
        </div>

	</div>
</div>

<div data-num="299" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de35.png" data-cat="<?= $cat ?>">
	<div class="row">

        <div class="col-md-12">
            <div class="is-card is-card-circle is-light-text" style="background:#000;">
                <div class="is-card-content-centered" style="opacity:0.85;">
                    <h1 class="size-32 is-title1-32 is-title-lite">LOREM IPSUM IS DUMMY TEXT</h1>
                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                    <div style="margin:1.2em 0 0">
                        <a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Read More</a>	
                    </div>	   
            	</div>                     
            </div>
        </div>

	</div>
</div>

<div data-num="300" data-thumb="<?= $assets_url ?>assets/minimalist-basic/thumbnails/de36.png" data-cat="<?= $cat ?>">
	<div class="row">

        <div class="col-md-12">
            <div class="is-card is-card-circle is-light-text" style="background:#000;">
                <div class="is-card-content-centered" style="opacity:0.85;">
                    <h1 class="size-32 is-title1-32 is-title-bold">LOREM IPSUM IS DUMMY TEXT</h1>
                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                    <div style="margin:1.2em 0 0">
                        <a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Read More</a>	
                    </div>	   
            	</div>                     
            </div>
        </div>

	</div>
</div>