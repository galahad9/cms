#!/bin/sh

domain="`cat .domain`"
apiurl="`cat .domain.dev`"
apiurl+="/rest/api/version";

domain="${domain/http:\/\//}";
domain="${domain/https:\/\//}";


function update(){
	
	current_version=$(curl -sS $apiurl | jq -r '.current');
	last_version=$(curl -sS $apiurl | jq -r '.last');
	next_version=$(curl -sS $apiurl | jq -r '.next');
	has_update=$(curl -sS $apiurl | jq -r '.update');

	if [ "$has_update" ]; then

		if [ "$next_version" != "false" ] && [ "$next_version" != "null" ]; then

		    echo -e "New update available";
		    echo -e "Current:\t$current_version";
		    echo -e "Next:\t\t$next_version";
		    echo -e "Last:\t\t$last_version";

		    /usr/sbin/plesk ext git --update -domain $domain -name cms.git -active-branch $next_version

	        echo -n "$next_version" > .version



		else 
		    echo "Already up-to-date: $current_version";
		fi;
	else 
		    echo "Already up-to-date: $current_version";
	fi;

	for path in update/*; do
        if [ -a "$path/.updated" ]; then
             continue
        else 
            echo "$path not updated. Updating $path...."

            /usr/sbin/plesk php "$PWD/$path/update-before.inc.php"
            /usr/sbin/plesk php "$PWD/$path/migrate.inc.php"
            composer update > composer.log 2> /dev/null
            /usr/sbin/plesk php "$PWD/$path/update-after.inc.php"

            echo "$currentdate" > "$PWD/$path/.updated";
        fi;
    done;
}

# check if shell is fired from root directory
if [ -f .domain ]; then

	# update current branche first
	/usr/sbin/plesk ext git --fetch -domain $domain -name cms.git
	/usr/sbin/plesk ext git --deploy -domain $domain -name cms.git

	last_commit=$(/usr/sbin/plesk ext git --get-last-commit -domain $domain -name cms.git);
	echo -n "$last_commit" > .last_commit


	# check for newer release branche
	has_update=$(curl -sS $apiurl | jq -r '.update');
	next_version=$(curl -sS $apiurl | jq -r '.next');

	# new update found in newer release branche
	if [ "$next_version" != "false" ] && [ "$next_version" != "null" ]; then

		# go to 1 newer release branche at the time
		for (( i = 0; i < 100; i++ )); do
			if [ "$has_update" ]; then
				update
			else 
				break;
			fi;
		done
	
		echo "Update completed to: $current_version";
	else
		echo "Already up-to-date: $current_version";
	fi;
else
	echo "Wrong directory or run the installer first";
fi;


