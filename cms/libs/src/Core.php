<?php
/**
 *  CMS Core functions
 *      * Core update
 *      * Plugin update
 */

use GuzzleHttp\Client;

class Core {

	private $composer;

	private function composer(){
		if(! $this->composer){
			$this->composer = json_decode(file_get_contents(ROOT_PATH.'/composer.json'));
		}
		return $this->composer;
	}

	public function git(){
		if(! $this->git){
			$this->git = new Git;
		}
		return $this->git;
	}

	public function lastUpdate(){
		return date('d-m-Y H:i:s', strtotime('-5 minutes'));
	}

	public function getVersion(){
		return $this->composer()->version;
	}

	public function hasUpdate(){
		if($this->git()->branche('current') != $this->git()->branche('last')){
			return true;
		}
		return false;
	}

	public function updateCount(){
		return (($this->hasUpdate()) ? 1 : 0);
	}

	public static function version(){
		$instance = new static;
		return $instance->getVersion();
	}

	public static function count(){
		 $instance = new static;
		return $instance->updateCount();
	}

	public function setting($type='SITE'){
		return loadSettings($type);
	}
}


