<?php



class Plugin {

	const ACTIVE = 'active';
	const NONACTIVE = 'nonactive';
	const NOT_INSTALLED = 'not installed';
	const ERROR = 'unknown error';

	protected $plugin;

	public $snippets;
	public $modules;
	public $menu;

	/**
	 * Constructor. 
	 * Plugin activates always on start of request
	 */
	public function __construct(){
		
	}

	/**
	 * Retreive plugin status
	 * @return array 
	 */
	private function plugin(){
		if(! $this->plugin){
			$query = DB::CMS()->query("SELECT * FROM plugins WHERE name=:name")->bind(':name', $this->name());
			$this->plugin = $query->single();
		}

		return $this->plugin;
	}



	/**
	 * Activate plugin
	 * @return null
	 */
	public function activate(){
		if($this->plugin()){
			DB::CMS()->query("UPDATE plugins SET status=:status WHERE id=:id", [
				':status' => self::ACTIVE,
				':id' => $this->plugin()['id']
			])->execute();
		} else {
			DB::CMS()->query("INSERT INTO plugins (name, status) VALUES (:name, :status)", [
				':name' => $this->name(),
				':status' => self::ACTIVE,
			])->execute();
		}
	}

	/**
	 * Deactive plugin
	 * @return null
	 */
	public function deactivate(){
		DB::CMS()->query("UPDATE plugins SET status=:status WHERE id=:id", [
			':status' => self::NONACTIVE,
			':id' => $this->plugin()['id']
		])->execute();
	}

	/**
	 * Retreive module name
	 * @return string
	 */
	public function name(){
		if($this->isPostType()){
			return ucfirst($this->posttype);	
		} 
		
		return get_class($this);
	}

	/**
	 * Get Posttype ID
	 * @return string posttype ID
	 */
	public function posttype(){
		return $this->posttype;
	}

	/**
	 * Check if module is posttype
	 * @return string posttype ID
	 */
	public function isPostType(){
		$reflection = new \ReflectionObject($this);
    	return in_array('Post\Posttype', $reflection->getTraitNames());
	}

	/**
	 * Retreive module slug
	 * @return string
	 */
	public function url(){
		return strtourl($this->name());
	}

	/**
	 * Retreive module description
	 * @return string
	 */
	public function description(){
		return '';
	}

	public function modules(){
		$modules = glob_recursive($this->modules);
		return $modules;
	
	}


	public function version(){
		return 1.0;
	}
	

	/**
	 * Retreive module status
	 * @return string
	 */
	public function status(){
		if($this->plugin()['id']){
			if($this->plugin()['status'] == self::ACTIVE){
				return self::ACTIVE;
			} elseif($this->plugin()['status'] == self::NONACTIVE){
				return self::NONACTIVE;
			}
		}

		return self::NOT_INSTALLED;
	}

	/**
	 * Retreive site setting by key
	 * @return string
	 */
	static public function setting($setting){
		$settings = loadSettings('SITE');
		return $settings[$setting];
	}

	/**
	 * Convert template
	 * @return array
	 */
	static public function template($template, $data){

		preg_match_all("/{{(.+)}}/U", $template, $output_array, PREG_SET_ORDER, 0);
		foreach ($output_array as $key => $module_key) {
			$key = $module_key[0];
			$value = trim($module_key[1]);
			$key_val = $data[$value];
			if(strpos($value, ':') !== false){
				$value = explode(':', $value);
				$type = $value[1];
				$value = $value[0];

				if($type == 'image' && $data[$value]){
					$key_val = '<img class="img-responsive" src="/'.$data[$value].'" alt="'.$data['title'].'">';
				// } elseif($type == 'imageurl' && $data[$value]){
					// $key_val = 
				} elseif(strpos($type, 'cutParagraph') !== false){
					$length = between($type,'(', ')');
					$key_val = cutParagraph($data[$value], $length);
				} elseif($type == 'text'){
					$key_val = nl2br($data[$value]);
				} else {
					$key_val = $data[$value];
				}
			} 


			$template = str_replace($key, $key_val, $template);	
	    }

		return ['template' => $template];
	}
}