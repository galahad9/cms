<?php
namespace Page;

Class Builder {

	protected $template;
	protected $data = [];

	public function template($template){
		if(! file_exists($template)){
			throw new BuilderException('template file not found');
		}
		$this->template = $template;
	}

	public function data($data){
		$this->data =  array_merge($this->data, $data);
	}

	public function build(){
		if(! $this->template){
			throw new BuilderException('no template specified');
		}

		ob_start();

		$data = $this->data;
		foreach ($data as $key => $value) {
			${$key} = $value;
		}

		include($this->template);

		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}
}