<?php 
namespace Post;

use DB;
use Page\Builder;

trait Category {

	/**
	 * Fetch all data
	 */
	public function fetchCategories($filters=[]){

		$data = DB::SITE()->query("SELECT * FROM posttype_category WHERE posttype=:posttype")->bind(':posttype', $this->url())->fetch();
		
		return $data;
	}	

	/**
	 * Fetch category detail
	 * @param  int $id 
	 * @return array   category data
	 */
	public function getCategory($id){
		$data = DB::SITE()->query("SELECT * FROM posttype_category WHERE posttype=:posttype AND id=:id LIMIT 1", [
			':posttype' => $this->url(),
			':id' => $id
		])->bind(':posttype', $this->url())->single();
		
		return $data;
	}


	public function category(){
		$builder = new Builder();
		$builder->template(CMS_ROOT_PATH.'pages/posttype/category.inc.php');
		$builder->data($this->posttypeData());

		return $builder->build();
	}

}