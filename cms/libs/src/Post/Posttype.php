<?php 
namespace Post;

use DB;
use Page\Builder;

trait Posttype { 

	/**
	 * Check if trait Postcategory is used
	 * @return boolean 
	 */
	public function hasCategories(){
		if($this->disableCategory !== true){
			$reflection = new \ReflectionObject($this);
    		return in_array('Post\Category', $reflection->getTraitNames());
    	}
    	return false;
	}

	/**
	 * Fetch all data
	 */
	public function fetchAll($filters=[]){
		$query = " `deleted_at` IS NULL ";
		$limit = 999;
		if(is_array($filters)){
			foreach ($filters as $key => $value) {
				$query .= " AND `$key`='".addslashes($value)."'";
			}
		} elseif(is_int($filters)){
			$limit = $filters;
		}

		$data = DB::SITE()->query("SELECT * FROM posttype_data WHERE posttype=:posttype AND $query LIMIT 0,$limit")->bind(':posttype', $this->url())->fetch();
		foreach ($data as $key => $value) {

			$data[$key] = array_merge($value, json_decode($value['data'], true));
			if($this->hasCategories()){
				$data[$key]['category'] = $this->getCategory($value['category']);
			}
		}

		return $data;
	}

	/**
	 * Fetch all data
	 */
	public function find($id){
		$query = " `deleted_at` IS NULL ";
		
		$data = DB::SITE()->query("SELECT * FROM posttype_data WHERE posttype=:posttype AND $query AND id=:id", [
			':posttype' => $this->url(),
			':id' => $id
		])->fetch();
		foreach ($data as $key => $value) {

			$data[$key] = array_merge($value, json_decode($value['data'], true));
			if($this->hasCategories()){
				$data[$key]['category'] = $this->getCategory($value['category']);
			}
		}

		return $data[0];
	}	

	/**
	 * Dashboard CMS page
	 */
	public function dashboard(){

		$builder = new Builder();
		$builder->template(CMS_ROOT_PATH.'pages/posttype/dashboard.inc.php');
		$builder->data($this->posttypeData());
		
		return $builder->build();
	}

	/**
	 * Edit CMS Page
	 */
	public function edit(){
		$builder = new Builder();
		$builder->template(CMS_ROOT_PATH.'pages/posttype/edit.inc.php');
		$builder->data($this->posttypeData());

		return $builder->build();
	}

	public function fields(){
		$postfields = [];
		if(plugin('customfields')->status() == \Plugin::ACTIVE){
			$fields = plugin('customfields')->where($this->url());
			if(is_array($fields) && count($fields) > 0){
				$fields = $fields[0];
				if(is_array($fields['data']) && count($fields['data']) > 0){
					foreach ($fields['data'] as $key => $value) {
						$postfields[$value['label']] = $value;
					}
				}
			}
		}

		return $postfields;
	}

	/**
	 * CMS DATA
	 */
	private function posttypeData(){

		$fields = $this->fields;

		if(plugin('customfields')->status() == \Plugin::ACTIVE){
			$customfields = plugin('customfields')->where($this->url());
			foreach ($customfields as $key => $group) {
				foreach ($group['data'] as $key => $value) {
					$value['customfield'] = 1;
					$fields[$value['label']] = $value;
				}
			}
		}

		$data = [
			'fields' => $fields,
			'posttype' => $this,
			'plugin' => $this,
		];

		if($this->hasCategories()){
			$categories = DB::SITE()->query("SELECT * FROM posttype_category WHERE posttype=:posttype")->bind(':posttype', $this->url())->fetch();
			foreach ($categories as $key => $value) {
				$value['rows'] = [];
				$data['categories'][$value['id']] = $value;
			}
		}

		$data['rows'] = DB::SITE()->query("SELECT * FROM posttype_data WHERE posttype=:posttype")->bind(':posttype', $this->url())->fetch();
		foreach ($data['rows'] as $key => $value) {
			$value['rows'] = json_decode($value['data'], true);
			$data['rows'][$key] = array_merge($value['rows'], $value);

			if($value['category']){
				$data['categories'][$value['category']]['rows'][] = $value;
			}
		}

		return $data;
	}
}