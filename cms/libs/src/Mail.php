<?php

class Mail {

	private $transport;
	private $mailer;

	function __construct(){	
		$this->transport = new Swift_SendmailTransport('/usr/sbin/sendmail -bs');
		$this->mailer = new Swift_Mailer($this->transport);
		return $this;
	}

	public function message(){
		$message = new Swift_Message();
		return $message;
	}

	public function send(Swift_Message $message){
		try { 
			return $this->mailer->send($message);
		} catch(Swift_TransportException $e){
			if(DEBUG){
				throw new \Exception('Mail not send', 0, $e);
			}
			return false;
		}
	}
}