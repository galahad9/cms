<?php

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

Class Git {

	private $rest = 'http://stash.wux.nl/rest/api/1.0/projects/SIENN/repos/CMS/';
	private $remote = 'live';
	private $client;
	private $repo;



	public function client(){
		$this->client = new GuzzleHttp\Client(['base_uri' => $this->rest]);
		return $this->client;
	}

	public function branche($status='current'){
		if($status == 'current'){
			return file_get_contents(ROOT_PATH.'/.version');
		} elseif($status == 'last'){

			$tags = $this->branches();
			foreach ($tags as $key => $tag) {
				$id = str_replace('refs/heads/release/v', '', $tag['id']);
				$tags[$key]['orderId'] = $id;
			}

			usort($tags, function($a, $b) {
			    return $a['orderId'] <=> $b['orderId'];
			});

			return end($tags)['displayId'];

		} elseif($status == 'next'){
			
			$tags = $this->branches();
			foreach ($tags as $key => $tag) {
				$id = str_replace('refs/heads/release/v', '', $tag['id']);
				$tags[$key]['orderId'] = $id;
			}

			usort($tags, function($a, $b) {
			    return $a['orderId'] <=> $b['orderId'];
			});
			
			foreach ($tags as $key => $tag) {
				if($tag['displayId'] == $this->branche('current')){
					return $tags[$key+1]['displayId'];
				}
			}
			
			return false;
		}
	}

	public function tags(){
		$tags = $this->api('tags');
		return $tags['values'];
	}

	public function branches(){
		$tags = $this->api('branches');
		$tags = $tags['values'];
		$new_tags = [];
		foreach ($tags as $key => $tag) {
			if(strpos($tag['displayId'], 'release') !== false){
				$new_tags[] = $tag;
			}
		}

		return $new_tags;
	}

	private function api($command, $type='GET'){
		$res = $this->client()->request($type, $command, [

		]);
		$body = $res->getBody();
		return json_decode($body->getContents(), true);
	}


	
}

Class GitException extends Exception { }