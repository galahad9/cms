<%@ Page Language="VB"%>
<%@ OutputCache Location="None" VaryByParam="none"%>

<script runat="server">

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        If Not Page.IsPostBack Then
            If Not IsNothing(Session("content-bootstrap")) Then
                litContent.Text = Session("content-bootstrap")
            Else
                'Optional initial content
                litContent.Text = "<div class=""is-section is-light-text is-box is-bg-grey is-section-100"">" & _
                        "<div class=""is-overlay"">" & _
                            "<div class=""is-overlay-bg"" style=""background-image: url(contentbox/images/sample.jpg);""></div>" & _
                            "<div class=""is-overlay-color""></div>" & _
                            "<div class=""is-overlay-content""></div>" & _
                        "</div>" & _
                        "<div class=""is-boxes"">" & _
                            "<div class=""is-box-centered is-opacity-90"">" & _
                                "<div class=""is-container is-builder container-fluid"">" & _
                                    "<div class=""row"">" & _
                                        "<div class=""col-md-6"">" & _
                                            "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>" & _
                                        "</div>" & _
                                        "<div class=""col-md-6"">" & _
                                            "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>" & _
                                        "</div>" & _
                                    "</div>" & _
                                "</div>" & _
                            "</div>" & _
                        "</div>" & _
                    "</div>"
            End If

        End If

    End Sub
    
    Protected Sub btnPost_Click(sender As Object, e As System.EventArgs) Handles btnPost.Click

        'Get Submitted Content
        Dim sContent As String = System.Uri.UnescapeDataString(hidContent.Value)
        
        'You can save the content into a database. in this example we just display the content back.
        Session("content-bootstrap") = sContent
        
        Response.Redirect(Request.RawUrl)
       
    End Sub
  
</script>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Default Example</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">  
    
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- Include the required css -->    
    <link href="box/box.css" rel="stylesheet" type="text/css" />
    <link href="assets/minimalist-basic/content-bootstrap.css" rel="stylesheet" type="text/css" />

    <!--
    Include ContentBuilder.js and ContentBox.js (all js included before the body ends). -->
    <link href="contentbuilder/contentbuilder.css" rel="stylesheet" type="text/css" />
    <link href="contentbox/contentbox.css" rel="stylesheet" type="text/css" />
    <link href="assets/scripts/simplelightbox/simplelightbox.css" rel="stylesheet" type="text/css" />

    <style>
        /* Bottom panel to place "Save" button */
        .bottomBar {position:fixed;bottom:0;left:0;width:100%;height:57px;background:rgba(255,255,255,0.95);border-top:#eee 1px solid;;text-align:center;padding:10px 0 0;box-sizing:border-box;z-index:1000}
        
        /* Adjustment due to bottom panel */
        .is-wrapper > div:last-child {margin-bottom: 57px;} 
        .is-wrapper > div.is-section-100 {height: -moz-calc(100% - 57px); height: -webkit-calc(100% - 57px); height: -o-calc(100% - 57px); height:calc(100% - 57px);} 
        @media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) { /* for IE */
            .is-wrapper > div:last-child {border-bottom:rgba(0,0,0,0) 57px solid;height:calc(100% - 0px);}
        }        
    </style>        
</head>
<body>

<div class="is-wrapper">
    <asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>

<div class="bottomBar">
    <button id="btnAddSection" class="btn btn-default" style="padding: 3px 15px;font-size:0.9em">+ Add Section</button>
    <button id="btnViewHTML" class="btn btn-default" style="padding: 3px 15px;font-size:0.9em">HTML</button>
    <button id="btnSave" class="btn btn-default" style="padding: 3px 15px;font-size:0.9em">Save</button>
</div>

<!-- Hidden Form Fields to post content -->
<form id="form1" runat="server" style="display:none">
    <asp:HiddenField ID="hidContent" ClientIDMode="Static" runat="server" />
    <asp:Button ID="btnPost" runat="server" Text="Button" />
</form>

<script src="contentbuilder/jquery.min.js" type="text/javascript"></script>  
<script src="contentbuilder/jquery-ui.min.js" type="text/javascript"></script>
<script src="contentbuilder/contentbuilder.js" type="text/javascript"></script>
<script src="contentbuilder/saveimages.js" type="text/javascript"></script>
<script src="contentbox/contentbox.js" type="text/javascript"></script>
<script src="assets/scripts/simplelightbox/simple-lightbox.min.js" type="text/javascript"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {

        //Call contentbox() to enable editing
        $(".is-wrapper").contentbox({
            snippetFile: 'assets/minimalist-basic/snippets-bootstrap.html', /* Specify the snippets file */
            coverImageHandler: 'savecover.ashx', /* or savecover.php - for uploading image */
            snippetOpen: true,
            contentHtmlStart: '<div class="is-container is-builder container-fluid"><div class="row"><div class="col-md-12">',
            contentHtmlEnd: '</div></div></div>',
            largerImageHandler: 'saveimage-large.ashx', /* for uploading larger image */
            moduleConfig: [{
                "moduleSaveImageHandler": "saveimage-module.ashx" /* for module purpose image saving (ex. slider) */
            }],
            onRender: function () {
                $('a.is-lightbox').simpleLightbox({ closeText: '<i style="font-size:35px" class="icon ion-ios-close-empty"></i>', navText: ['<i class="icon ion-ios-arrow-left"></i>', '<i class="icon ion-ios-arrow-right"></i>'], disableScroll: false });
            },
            snippetCustomCode: true
        });

        //Call addSection() to add new section
        $("#btnAddSection").click(function () {
            $('.is-wrapper').data('contentbox').addSection();
            return false;
        });

        //Call viewHtml() to view HTML
        $("#btnViewHTML").click(function () {
            $('.is-wrapper').data('contentbox').viewHtml();
            return false;
        });

        $("#btnSave").click(function () {


            //Save all base64 images into files on the server
            $("body").saveimages({
                handler: 'saveimage.ashx', /* or saveimage.php - for saving embedded base64 image to image file */
                onComplete: function () {

                    //Save Content
                    var sHTML = $('.is-wrapper').data('contentbox').html();

                    $('#hidContent').val(encodeURIComponent(sHTML));
                    $('#btnPost').click();

                }
            });
            $("body").data('saveimages').save();

            $("html").fadeOut(1000);

            return false;
        });
    });
</script>
<script src="box/box.js" type="text/javascript"></script> <!-- Box Framework js include -->

</body>
</html>
