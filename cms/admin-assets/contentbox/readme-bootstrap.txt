﻿ContentBox.js ver. 1.3.4


*** USAGE ***

1. Include the required css:

    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="box/box.css" rel="stylesheet" type="text/css" />
    <link href="assets/minimalist-basic/content-bootstrap.css" rel="stylesheet" type="text/css" />


	Note:
	box.css is a small css framework for content structure.
	content.css is needed by the content blocks (defined in snippets.html).


2. Include JQuery & JQuery UI

	<script src="contentbuilder/jquery.min.js" type="text/javascript"></script>  
	<script src="contentbuilder/jquery-ui.min.js" type="text/javascript"></script>


3. Include ContentBuilder.js plugin and its saveimages.js plugin

    <link href="contentbuilder/contentbuilder.css" rel="stylesheet" type="text/css" />
	<script src="contentbuilder/contentbuilder.js" type="text/javascript"></script>
	<script src="contentbuilder/saveimages.js" type="text/javascript"></script>


	Note:
	For quick setup & usage, ContentBox.js package already includes ContentBuilder.js (limited version) which can only be used within ContentBox.js. 
	For more flexibility, you can get ContentBuilder.js full package, available at:	http://innovastudio.com/content-builder.aspx


4. Include ContentBox.js plugin

    <link href="contentbox/contentbox.css" rel="stylesheet" type="text/css" />
	<script src="contentbox/contentbox.js" type="text/javascript"></script>


5. Run:

    $(".is-wrapper").contentbox({
        snippetFile: 'assets/minimalist-basic/snippets.html',
        snippetOpen: true,
        coverImageHandler: 'savecover.php',
		moduleConfig: [{
                "moduleSaveImageHandler": "saveimage-module.php" /* for module purpose image saving (ex. slider) */
        }]
    });

	PARAMETERS:
	- snippetFile: Specify html snippets file.
	- snippetOpen: Open snippet panel on first load. Default: false
	- coverImageHandler: Specify PHP or ASP.NET handler for saving background images. Ex: savecover.php or savecover.aphx
	- moduleConfig: Containing params for future use. Now only one param is needed ("moduleSaveImageHandler") that is used for slider image upload. Ex. saveimage-module.php or saveimage-module.ashx
	- iconselect: Specify icon selection dialog page. Default: assets/ionicons/selecticon.html
	- customval: Optional custom parameter if you want to pass custom value to the image handler (can be used to specify custom upload folder)


6. To view HTML in a dialog:

	$('.is-wrapper').data('contentbox').viewHtml();


7. To open "Add Section" dialog:

	$('.is-wrapper').data('contentbox').addSection();


8. To get HTML:

    var sHTML = $('.is-wrapper').data('contentbox').html();


9. For Saving content, all embedded base64 images will be saved first (with the help of a simple handler on the server). 
	Then after all images are saved, get the HTML content and you're ready to submit it to the server for saving purpose (eg. in your database, etc).
	To save all embedded images, use saveimages JQuery plugin (saveimages.js). 
	Then onComplete event, we can get the HTML result.


    //Save all base64 images into files on the server
    $("body").saveimages({
        handler: 'saveimage.php', /* or saveimage.ashx - for saving embedded base64 image to image file */
        onComplete: function () {

            //Get HTML content
            var sHTML = $('.is-wrapper').data('contentbox').html();

            //Here, you're ready to submit sHTML to the server for saving

        }
    });
    $("body").data('saveimages').save();


	PARAMETERS for saveimages.js plugin:
	- handler: Specify PHP or ASP.NET handler for saving embedded base64 images. Ex: saveimage.php or saveimage.aphx
	- onComplete: Event triggered after all images are saved.	
	- customval: Optional custom parameter if you want to pass custom value to the image handler (can be used to specify custom upload folder)


10. Specify upload folder.

	For PHP: open savecover.php & saveimage.php. Change $path variable if needed.

	For ASP.NET: open savecover.ashx & saveimage.ashx. Change sPath variable if needed.

	The default upload folder is "uploads"
	

*** OTHERS ***

To activate custom tags insert button (on editing toolbar), specify "customTags" parameter with your own custom tags. Custom tags is commonly used in a CMS for adding dynamic content (ex. slider, form, etc) within the content (by replacing the tags in production).
Example:

    $(".is-wrapper").contentbox({
        customTags: [["Contact Form", "{%CONTACT_FORM%}"],
			["Slider", "{%SLIDER%}"],
			["My Plugin", "{%MY_PLUGIN%}"]],         
        ...
    });

To enable/disable Custom Code snippet/block, set "snippetCustomCode" parameter (default: false):

	$(".is-wrapper").contentbox({            
            snippetCustomCode: true,
			...
        });

	Then, make sure you have applied the latest snippet (folder: assets/minimalist-basic/) from the package.
	Or if you use your own custom snippet, add the following block into your snippets.html (Just make sure that it has data-cat="100". You can change the data-num.):

		<div data-num="1000" data-thumb="assets/minimalist-basic/thumbnails/code.png" data-cat="100">
			<div class="row clearfix" data-mode="code" data-html="%3Cdiv%20class%3D%22column%20full%22%3E%0A%0A%3Cp%20id%3D%22{id}%22%3E%0ALorem%20ipsum%0A%3C%2Fp%3E%0A%3Cp%3E%0AThis%20is%20a%20code%20block.%20You%20can%20edit%20this%20block%20using%20the%20source%20dialog.%0A%3C%2Fp%3E%0A%0A%3Cscript%3E%0A%2F*%20Example%20of%20script%20block%20*%2F%0A%24('%23{id}').html('%3Cb%3EHello%20World!%3C%2Fb%3E')%3B%0A%3C%2Fscript%3E%0A%0A%3C%2Fdiv%3E">
				<div class="column full">

				</div>
			</div>
		</div>

	If you're using Bootstrap snippets (snippets-bootstrap.html):

		<div data-num="301" data-thumb="assets/minimalist-basic/thumbnails/code.png" data-cat="100">
			<div class="row" data-mode="code" data-html="%3Cdiv%20class%3D%22column%20full%22%3E%0A%0A%3Cp%20id%3D%22{id}%22%3E%0ALorem%20ipsum%0A%3C%2Fp%3E%0A%3Cp%3E%0AThis%20is%20a%20code%20block.%20You%20can%20edit%20this%20block%20using%20the%20source%20dialog.%0A%3C%2Fp%3E%0A%0A%3Cscript%3E%0A%2F*%20Example%20of%20script%20block%20*%2F%0A%24('%23{id}').html('%3Cb%3EHello%20World!%3C%2Fb%3E')%3B%0A%3C%2Fscript%3E%0A%0A%3C%2Fdiv%3E">
				<div class="col-md-12">

				</div>
			</div>
		</div>

To change Code Block message (displayed on source dialog for Custom Code snippet/block), use "snippetCustomCodeMessage" parameter:

	$(".is-wrapper").contentbox({            
            snippetCustomCodeMessage: '<b>IMPORTANT</b>: This is a code block. Custom javascript code (&lt;script&gt; block) is allowed here but may not always work or compatible with the content builder, so proceed at your own risk. We do not support problems with custom code.',
			...
        });

*** Lightbox extension ***

Starting from ver.1.2.5, ContentBox is extended with a simple lightbox plugin. You can disable this if you want.
Lightbox plugin allows users to embed image with option to enlarge image on click. The implementation is as follows: (already implemented in the examples)

1. Include the required lightbox plugin script:
	<link href="assets/scripts/simplelightbox/simplelightbox.css" rel="stylesheet" type="text/css" />
	<script src="assets/scripts/simplelightbox/simple-lightbox.min.js" type="text/javascript"></script>

2. Initiate the lightbox plugin within contentbox using onRender param and specify upload handler for uploading image:

	In PHP:
	
        $(".is-wrapper").contentbox({
            ...
            largerImageHandler: 'saveimage-large.php',
            onRender: function () {
                $('a.is-lightbox').simpleLightbox({ closeText: '<i style="font-size:35px" class="icon ion-ios-close-empty"></i>', navText: ['<i class="icon ion-ios-arrow-left"></i>', '<i class="icon ion-ios-arrow-right"></i>'], disableScroll: false });
            }
        });

	In ASP.NET:

        $(".is-wrapper").contentbox({
            ...
            largerImageHandler: 'saveimage-large.aspx',
            onRender: function () {
                $('a.is-lightbox').simpleLightbox({ closeText: '<i style="font-size:35px" class="icon ion-ios-close-empty"></i>', navText: ['<i class="icon ion-ios-arrow-left"></i>', '<i class="icon ion-ios-arrow-right"></i>'], disableScroll: false });
            }
        });

NOTE:
- If you need to customize the image upload handler, the files are saveimage-large.php (for PHP) or saveimage-large.aspx (for ASP.NET).
  Customization may needed if you want to add custom security for image upload or to change the default upload folder. Default upload folder is: uploads/
- If you don't want to use the lightbox plugin, just remove the lines from the above implementation steps.



*** EXAMPLES ***


- example-bootstrap.html (Basic Example. Content is saved in browser's Local Storage)

- example-bootstrap.php (Complete Example in PHP)

- example-bootstrap.aspx (Complete example in ASP.NET)



*** SUPPORT ***

Email us at: support@innovastudio.com



---- IMPORTANT NOTE : ---- 
1. Custom Development is beyond of our support scope.
 
Once you get the HTML content, then it is more of to user's custom application (eg. posting it to the server for saving into a file, database, etc).
PHP programming, ASP.NET programming or server side implementation is beyond of our support scope. 
We also do not provide free custom development of extra features or functionalities.

2. Our support doesn't cover custom integration into users' applications. It is users' responsibility.
------------------------------------------
