/*
Template Name: Wrapkit Ui Kit
Author: Themedesigner
Email: niravjoshi87@gmail.com
File: js
*/
$(function () {
    "use strict";
    // ============================================================== 
    //This is for preloader
    // ============================================================== 
    $(function () {
        $(".preloader").fadeOut();
    });
    // ============================================================== 
    //Tooltip
    // ============================================================== 
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    // ============================================================== 
    //Popover
    // ============================================================== 
    $(function () {
        $('[data-toggle="popover"]').popover()
    })
    // ============================================================== 
    // For mega menu
    // ============================================================== 
    jQuery(document).on('click', '.mega-dropdown', function (e) {
        e.stopPropagation()
    });
    jQuery(document).on('click', '.navbar-nav > .dropdown', function(e) {
         e.stopPropagation();
    });
    $(".dropdown-submenu").click(function(){
        $(".dropdown-submenu > .dropdown-menu").toggleClass("show");                     
    });
    // ============================================================== 
    // Resize all elements
    // ============================================================== 
    $("body").trigger("resize");
    // ============================================================== 
    //Fix header while scroll
    // ============================================================== 
    var wind = $(window);
         wind.on("load", function() {
            var bodyScroll = wind.scrollTop(),
                navbar = $(".topbar");
            if (bodyScroll > 100) {
                navbar.addClass("fixed-header animated slideInDown")
            } else {
                navbar.removeClass("fixed-header animated slideInDown")
            }
        });
        $(window).scroll(function () {
            if ($(window).scrollTop() >= 200) {
                $('.topbar').addClass('fixed-header animated slideInDown');
                $('.bt-top').addClass('visible');
            } else {
                $('.topbar').removeClass('fixed-header animated slideInDown');
                $('.bt-top').removeClass('visible');
            }
        });
    // ============================================================== 
    // Animation initialized
    // ============================================================== 
    AOS.init();
    // ============================================================== 
    // Back to top
    // ============================================================== 
    $('.bt-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
    // ============================================================== 
    // menu scroll
    // ============================================================== 
    

      // filter items on button click
    $('.portfolio4 .filterby').on('click', 'a', function() {
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({
            filter: filterValue
        });
    });
    // init Isotope
    var $grid = $('.portfolio-box4').isotope({
        itemSelector: '.filter',
        percentPosition: true,
        masonry: {
            // use outer width of grid-sizer for columnWidth
            columnWidth: '.filter',
        }
    });
    //****************************
      // Isotope Load more button
      //****************************
      var initShow = 6; //number of images loaded on init & onclick load more button
      var counter = initShow; //counter for load more button
      var iso = $grid.data('isotope'); // get Isotope instance

      setTimeout(function(){
        loadMore(initShow); //execute function onload
      }, 500);

      function loadMore(toShow) {
        if(iso){
            $grid.find(".hidden").removeClass("hidden");

            var hiddenElems = iso.filteredItems.slice(toShow, iso.filteredItems.length).map(function(item) {
              return item.element;
            });
            $(hiddenElems).addClass('hidden');
            $grid.isotope('layout');

            //when no more to load, hide show more button
            if (hiddenElems.length == 0) {
              $("#load-more").hide();
            } 
            else {
              $("#load-more").show();
            };
        }
      }

      //append load more button
      $grid.after('<div class="text-center"><a id="load-more" class="btn btn-info btn-md btn-arrow m-t-20" href="javascript:void(0)"> <span>Load More <i class="ti-arrow-right"></i></span></a></div>');

      //when load more button clicked
      $(".portfolio4 #load-more").click(function() {
        if ($('#filters').data('clicked')) {
          //when filter button clicked, set initial value for counter
          counter = initShow;
          j$('#filters').data('clicked', false);
        } else {
          counter = counter;
        };

        counter = counter + initShow;

        loadMore(counter);
      });


    /**--------------------------------**/
    // portfolio 5
    /**--------------------------------**/
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return item.el.attr('title') + '<small>by Jon Doe</small>';
            }
        }
    });
});
