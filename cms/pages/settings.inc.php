<?php

$plugin = 0;
$upload_dir = UPLOAD_DIR;
$upload_path = $settings['Website Url'].UPLOAD_FOLDER;
if (isset($_GET['type']) && $_GET['type'] == 'cms') {
	$database = DB::CMS();
	$type = 'CMS';
	$upload_dir = CMS_ROOT_PATH.'/assets/img/';
	$upload_path = CMS_PUBLIC_URL.'/assets/img/';

	if(! $_SESSION['user']['admin']){
		header('Location: ' .CMS_PUBLIC_URL);
	}

} elseif($_GET['type'] == 'plugin'){
	$database = DB::SITE();
	$type = $_PLUGINS[$_GET['plugin']]->name();
	$plugin = 1;
} else {
	$database = DB::SITE();
	$type = 'Website';
}

if(! is_writable($upload_dir)){
	$error = 'Upload dir not writable!';
	if(DEBUG){
		$error .= ' path: '.$upload_dir;
	}
}


if (!empty($_FILES['settings']['name'])){
	foreach ($_FILES['settings']['name'] as $key => $value) {

		if ($_FILES['settings']['error'][$key] === UPLOAD_ERR_OK) { 
			$ext = pathinfo($_FILES['settings']['name'][$key], PATHINFO_EXTENSION);
			$filename = strtourl($key).'.'.$ext;
			$dest = $upload_dir.$filename;
			if(move_uploaded_file($_FILES['settings']['tmp_name'][$key], $dest)){
				$settings[$key] = $filename;
				$_POST['settings'][$key] = $filename;
			} else {
				$error = 'Coundn\'t upload file. Unknown error';
			}
		} elseif($_FILES['settings']['error'][$key] === UPLOAD_ERR_NO_FILE){
			// No file uploaded
		} else { 
			throw new UploadException($_FILES['settings']['error'][$key]); 
		} 
	}
}

if (!empty($_POST['action']) && $_POST['action'] == 'updateSettings') {
	
	foreach ($_POST['settings'] as $name => $value) {
		$found = $database->query("SELECT id FROM settings WHERE name=:name", [':name' => $name])->get('id');

		if ($found) {
			$database->query("UPDATE settings SET value=:value WHERE name=:name", array(
				':name' => $name,
				':value' => $value
			))->execute();
		}
		else {
			$database->query("INSERT INTO settings (name, value, plugin) VALUES (:name, :value, :plugin)", array(
				':name' => $name,
				':value' => $value,
				':plugin' => $plugin
			))->execute();
		}
		
		$settings[$name] = $value;
		if($name == 'Favicon' && $value){
			$faverror = generateFavIcon($value);
		} elseif($name == 'Image Favicon' && $value){ // CMS Favicon
			$faverror = generateFavIconCMS($value);
		} elseif($name == 'Favicon'){
			generateFavIcon('delete');
		} elseif($name == 'Image Favicon'){ // CMS Favicon
			generateFavIconCMS('delete');
		} elseif($name == 'Website Url'){
			file_put_contents(ROOT_PATH.'/.domain', str_replace(['http://', 'https://', 'www.'], '', $value));
		} elseif($name == 'Website Dev Url'){
			file_put_contents(ROOT_PATH.'/.domain.dev', $value);
		}

		if($faverror !== true){
			$error = $faverror;
		}
	}

}

if (!empty($_POST['action']) && $_POST['action'] == 'updateCmsSettings') {
	
	foreach ($_POST['settings'] as $name => $value) {
		DB::CMS()->query("UPDATE settings SET value=:value WHERE name=:name", array(
				':name' => $name,
				':value' => $value
			))->execute();
		$cmsSettings[$name] = $value;
	}

}

if($type == 'CMS' || $type == 'Website'){
	$settingGroups = $database->query("SELECT category FROM settings WHERE plugin=0 GROUP BY category")->fetch();
	foreach ($settingGroups as $key => $group) {
		$settingGroups[$key]['settings'] = $database->query("SELECT * FROM settings WHERE category=:category AND plugin=0",[
			':category' => $group['category']
		])->fetch();
	}
	
} elseif($_GET['type'] == 'plugin'){
	$settingGroups = $_PLUGINS[$_GET['plugin']]->settingGroups;
	foreach ($settingGroups as $key => $group) {
		foreach ($group['settings'] as $key2 => $set) {
			$group['settings'][$key2]['value'] = $_PLUGINS[$_GET['plugin']]->setting($set['name']);
		
		}
		
		$settingGroups[$key]['settings'] = $group['settings'];
	}
}


?>



<!--START PAGE HEADER -->
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator"><?= $type ?> <?= $trans['Settings'] ?></h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.php"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0)"><?= $trans['Settings'] ?></a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= $type ?> <?= $trans['Settings'] ?></li>
				</ol>
			</nav>
		</div>
		
	</div>
</header>
<?php
if($error){ 
	echo '<div class="alert alert-danger alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">'.$error.'</div></div>';
}
?>
<section class="page-content">

	<div class="row">
		
		<?php
		
		
		
		foreach ($settingGroups as $group) {
		?>
		
			<div class="col-md-6">
				<div class="card" data-widget="dropzone">
					<h5 class="card-header"><?= ucfirst($group['category']) ?> <?= $trans['Settings'] ?></h5>
					<form id="formSettings<?= ucfirst(strtourl($group['category'])) ?>" method="post" action="" enctype="multipart/form-data">
						<input type="hidden" name="action" value="updateSettings">
						<div class="card-body">
							<?php
							
									
							foreach ($group['settings'] as $settingFromDatabase) {										
							?>
									<div class="form-group row">
										<label for="input<?= ucfirst(strtourl($settingFromDatabase['name'])) ?>" class="col-sm-3 col-form-label"><?= $settingFromDatabase['name'] ?></label>
										<div class="col-sm-9">
											<?php
											if ($settingFromDatabase['type'] == 'boolean') {
											?>
												<input class="tgl tgl-light tgl-primary" id="checkbox<?= ucfirst(strtourl($settingFromDatabase['name'])) ?>" name="settings[<?= $settingFromDatabase['name'] ?>]" type="checkbox" <?= ($settingFromDatabase['value'] == 'true') ? 'checked=""' : '' ?> value="true">
												
												<label class="tgl-btn" for="checkbox<?= ucfirst(strtourl($settingFromDatabase['name'])) ?>"></label>
												<input id="checkbox<?= ucfirst(strtourl($settingFromDatabase['name'])) ?>Hidden" type="hidden" value="false" name="settings[<?= $settingFromDatabase['name'] ?>]">
											<?php
											} elseif($settingFromDatabase['type'] == 'image'){

												?>

												<div class="dz-message dropzone-fake needsclick singleFileUpload" style="cursor:pointer;" data-for="#input<?= ucfirst(strtourl($settingFromDatabase['name'])) ?>">
													 <h6 class="card-title text-center"><?= $trans['Click to upload'] ?>.</h6>
													 <?php
													 if(! $settingFromDatabase['value']){
													 	echo '<i class="icon dripicons-cloud-upload gradient-1"></i>';
													 } else {
													 	echo '<div class="d-block text-center"><img src="'.$upload_path.'/'.$settingFromDatabase['value'].'" class="img-responsive" style="max-height:80px"></div>';
													 }?>
													 <div class="d-block text-center">
													  <button type="button" class="btn btn-primary btn-rounded btn-floating btn-md"><?= $trans['Upload'] ?></button>
													  <button type="button" class="btn btn-default btn-rounded btn-floating btn-md"><?= $trans['Remove'] ?></button>
														</div>
													 </div>
												<input style="visibility:hidden;" type="file" id="input<?= ucfirst(strtourl($settingFromDatabase['name'])) ?>" name="settings[<?= $settingFromDatabase['name'] ?>]" value="<?= $settingFromDatabase['value'] ?>">
												<?php
											} elseif($settingFromDatabase['type'] == 'builder'){ ?>
												<a href="/cms/index.php?page=builder&name=<?= $settingFromDatabase['name'] ?>&plugin=<?= $_GET['plugin'] ?>" class="btn btn-primary"><?= $trans['Edit template'] ?></a>
											<?php
											} elseif ($settingFromDatabase['type'] == 'color') { ?>
												<div id="cp2" class="input-group colorpicker-component">
													<input type="text" class="form-control" id="input<?= ucfirst(strtourl($settingFromDatabase['name'])) ?>" name="settings[<?= $settingFromDatabase['name'] ?>]" value="<?= $settingFromDatabase['value'] ?>">
													<span class="input-group-addon"><i></i></span>
												</div>

											<?php } else {
											?>
												<input type="text" class="form-control" id="input<?= ucfirst(strtourl($settingFromDatabase['name'])) ?>" name="settings[<?= $settingFromDatabase['name'] ?>]" value="<?= $settingFromDatabase['value'] ?>">
											<?php
											}
											
											
											if ($settingFromDatabase['name'] == 'Google Analytics ID Data Display') {
												echo '<small>' . $trans['Add analytics-api@zylon-cms.iam.gserviceaccount.com to user management in Analytics'] . '</small>';
											}
											
											//analytics-api@zylon-cms.iam.gserviceaccount.com
										
											?>
										</div>
									</div>
							<?php
							}										
							?>
						</div>
							
						<div class="card-footer bg-light">
							<div class="form-group m-b-0 row">
								<div class="col-sm-10">
									<button type="submit" class="btn btn-primary"><?= $trans['Save'] ?></button>
								</div>
							</div>
						</div>
					</form>
					<script>
						$(document).ready(function() {
							$("#formSettings<?= ucfirst(strtourl($group['category'])) ?>").submit(function(event) {
								
								//sets a hidden input on disabled for 'checkbox' switches to send properly with a post request
								$('#formSettings<?= ucfirst(strtourl($group['category'])) ?> input[type=checkbox]').each(function () {
									if (this.checked) {
										$("#" + $(this).attr('id') + "Hidden").prop('disabled', true);
									}
								});
							});
							$('.colorpicker-component').colorpicker();
						});
					</script>
				</div>
			</div>
		<?php
		
		}
		
		?>
		
	</div>
	
</section>

<script type="text/javascript">
	$(".dropzone-fake").click(function(e){
		e.preventDefault(); e.stopPropagation();
		var $id = $(this).data('for');
		console.log($($id));
		$($id).click();
	})

</script>
	