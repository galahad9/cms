<?php

/*


#################### ER ZIT NOG EEN BUG IN HET VERWIJDEREN VAN PAGINA'S, DEZE KIJKT NOG NIET NAAR DE TAAL (DAAROM GEDISABLED) ####################


*/

$menuId = 1;

$settings = loadSettings('SITE');
$defaultLanguage = $settings['Default Language'];
$additionalLanguages = explode(',', $settings['Additional Languages']);
$additionalLanguages = array_filter($additionalLanguages);
$selectedLanguage = (isset($_GET['language']) && in_array($_GET['language'], $additionalLanguages)) ? $_GET['language'] : $defaultLanguage;

?>
<link rel="stylesheet" href="admin-assets/js/jquery-ui-1.12.1.custom/jquery-ui.min.css">
<script src="admin-assets/js/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="admin-assets/js/jquery.serialize-list.js"></script>
<script>
$(document).ready(function() {		
	
	$( "ul.menuList").sortable({
		placeholder: "ui-state-highlight",
		/*connectWith:['.submenuList', '.subsubmenuList'],
		dropOnEmpty: true,
		isValidTarget: function ($item, container) {
			var depth = 1;
			var maxDepth = 2;
			var children = $item.find('ul').first().find('li');

			depth += container.el.parents('ul').length;

			while (children.length) {
				depth++;
				children = children.find('ul').first().find('li');
			}

			return depth <= maxDepth;
		},*/
		stop: function() {
			$.post("ajax/menu/setMenuSequence.ajax.php",$(".menuList").serializeTree("data-id","menu"),function(data){
				//saved
			},"text");
        }
	});

	$( "ul.submenuList").sortable({
		connectWith:['.submenuList', '.subsubmenuList'],
		placeholder: "ui-sub-state-highlight",
		dropOnEmpty: true,
		stop: function() {
			$.post("ajax/menu/setMenuSequence.ajax.php",$(".menuList").serializeTree("data-id","menu"),function(data){
				//saved
			},"text"); 
        }
	});

	$( "ul.subsubmenuList").sortable({
		connectWith:['.submenuList', '.subsubmenuList'],
		placeholder: "ui-sub-state-highlight",
		dropOnEmpty: true,
		stop: function() {
			$.post("ajax/menu/setMenuSequence.ajax.php",$(".menuList").serializeTree("data-id","menu"),function(data){
				//saved
			},"text");
        }
	});

	
	$(document).on("click", ".remove-menu-item", function () {
		var menuItemId = $(this).data('menu-item-id');
		if (menuItemId !== undefined) {
			deleteMenuItem(menuItemId);
		}
	});


	$(document).on('input', '.menu-item-url', function() {
		var val = $(this).val();
		val = removeDiacritics(val);
		val = val.toLowerCase();
		val = replaceAndSymbol(val);
		val = val.replace(/\s+/g, '-');
		$(this).val(val);
	}).on('blur', function() {
		var val = $(this).val();
		val = val.trim();
		$(this).val(val);
	});

	$(document).on('input', '.menu-item-name', function() {
		var prevTitleVal = $(this).attr('data-titleval');
		if (prevTitleVal == $(this).closest('table').find('.menu-item-title').val()) {
			$(this).closest('table').find('.menu-item-title').val($(this).val());
			$(this).attr('data-titleval', $(this).val());
		}

		var prevSlugVal = removeDiacritics($(this).attr('data-slugval'));
		prevSlugVal = replaceAndSymbol(prevSlugVal);
		prevSlugVal = prevSlugVal.toLowerCase().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,'');
		prevSlugVal = '/' + prevSlugVal;

		var val = removeDiacritics($(this).val());
		val = replaceAndSymbol(val);
		val = val.toLowerCase().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,'');
		val = removeDiacritics(val).toLowerCase();
		val = '/' + val;

		var currrentSlug = $(this).closest('table').find('.menu-item-url').val();
		if (prevSlugVal == currrentSlug || currrentSlug == '') {
			$(this).closest('table').find('.menu-item-url').val(val);
		}

		$(this).attr('data-slugval', val);
	});

	$(document).on('blur', '.menu-item-url', function() {
		var url = $(this).val();
		if ( (url.charAt(0) == '/') || (url.includes('https://')) || (url.includes('http://')) || (url.includes('mailto:')) || (url.includes('tel:')) ) {
			return true;
		}

		$(this).val('/'+url);
	});




		
	loadMenu();
	
});




function deleteMenuItem(menuItemId) {
	
	swal({
		title: "Are you sure?", 
		text: "You will not be able to recover this item!", 
		type: "warning", 
		showCancelButton: true,   
		confirmButtonColor: "#f35958",   
		confirmButtonText: "Yes, delete it!",   
	}).then((result) => {
		$.post('ajax/menu/deleteMenuItem.ajax.php', {id:menuItemId}, function (result) {
			window.location.href = '<?= CMS_PUBLIC_PATH ?>?page=menu&language=<?= $selectedLanguage ?>';
		});
	});
	

}

function saveMenuItem(reload) {
	// console.log("save");
	

	if (reload) {
		$btn = $(".ajax-holder").find('button[onclick="saveMenuItem(1)"]');
	}
	else {
		$btn = $(".ajax-holder").find('button[onclick="saveMenuItem(0)"]');
	}	

	var old_html = $btn.html();
	$btn.html('<i class="fa fa-circle-o-notch fa-spin"></i>');


	var id = window.hash.id;
	var language = window.hash.language;
	var under = <?= intval($_GET['under']) ?>;
	var sequence = <?= intval($_GET['sequence']) ?>;
	var type = ((window.hash.type == 'category' || window.hash.type == 'dynamic') ? window.hash.type : ((window.hash.under == 1) ? "item" : "item"));
    if (window.hash.type == 'category') {
        var typeid = $(".ajax-holder").find("#typeid").val();
    } else {
        var typeid = '';
    }
	var name = $(".ajax-holder").find(".menu-item-name").val();
	var title = $(".ajax-holder").find(".menu-item-title").val();
	var url = $(".ajax-holder").find(".menu-item-url").val();
	var description = $(".ajax-holder").find(".menu-item-description").val();
	var show = $(".ajax-holder").find(".menu-item-show").is(':checked');

	$('.page-content').html('<div class="text-center" style="font-size: 8rem;"><span class="la la-spin la-circle-o-notch"><span></div>');
	
	$.post("ajax/menu/setMenuItem.ajax.php", {id:id, under:under, sequence:sequence, type:type, typeid:typeid, name:name, title:title, url:url, description:description, show:show, language:language}, function (result) {
		// $('#menu-save-success').show();
		// if (reload) {
		// 	loadMenu();
		// } else {
		// 	$btn.html(old_html);
		// }
		window.location.reload();
	});
}

var parseQueryString = function( queryString ) {
    var params = {}, queries, temp, i, l;
 
    // Split into key/value pairs
    queries = queryString.split("&");
 
    // Convert the array of strings into an object
    for ( i = 0, l = queries.length; i < l; i++ ) {
        temp = queries[i].split('=');
        params[temp[0]] = temp[1];
    }
 
    return params;
};

window.onhashchange = function(){
	loadMenu();
}

function loadMenu() {
	var hash = parseQueryString(window.location.hash.replace('#', ''));
	window.hash = hash;
	$(".ajax-holder").html('');
	$("#loader").show();

	if(! hash.id){
		$(".ajax-holder").html('');
		$("#loader").hide();
	}
	else {
		data = hash;
		data.page_language = '<?= $selectedLanguage ?>';
		$.get('ajax/menu/getMenu.ajax.php', data , function(data, textStatus, xhr) {
			$(".ajax-holder").html(data);
			$("#loader").hide();
			menuOnChange();
		});
	}
}

function menuOnChange() {
	if ( ($('.menu-item-name').val() == '') || ($('.menu-item-title').val() == '') ) {
		$('.btn-save-menu').prop('disabled', true);
	}
	else {
		$('.btn-save-menu').prop('disabled', false);
	}
}

$(document).on('input change', '.ajax-holder :input', function() {
	menuOnChange();
});

// function reloadMenu(){
// 	$(".menuList-ajax").html('<i class="fa fa-circle-o-notch fa-spin"></i> Loading...');
// 	$.post('ajax/menu/getMenu.ajax.php', {}, function(data, textStatus, xhr) {
// 		$(".menuList-ajax").html(data);
// 	});
// }

</script>

<style>

.menuList, .addItemList {
	list-style-type: none;
	padding-left: 0px;
	width: 100%;
}

.ui-sortable-helper {
	background: #f0f0f0!important;
	opacity: 0.9;
}

.ui-state-highlight {
	background: #edf7fc!important;
	height: 50px;
}
.ui-sub-state-highlight {
	background: #edf7fc!important;
	height: 20px;
}

.menuList li, .addItemList li {
	display: block;
    margin: 0px 0;
    padding: 8px;
    color: #6F7B8A;
    text-decoration: none;
    font-weight: bold;
    background: #fff;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    box-sizing: border-box;
	cursor: grab;
}
.submenuList li .icons {
	display: none;
}
.submenuList li:hover > .icons {
	display: block!important;
}

.submenuList li:hover, .menuList .addSubItemList li:hover {;
    font-weight: bold;
    background: #fbfbfb;
}

.muted {
	color: lightgrey!important;
}
.muted i {
	color: lightgrey!important;
}

.menuList .btn-sm{
	font-size: 1rem;
	padding: 0.1rem !important;
	height: 1.3rem !important;
}

.menuList .btn-sm i{
	font-size: 1rem;
	color: #fff !important;
}

.btn{
	color: #fff !important;
}

.ajax-holder > .card{
	margin-bottom: 0;
}

</style>


<!--START PAGE HEADER -->
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1>Menu &amp; content</h1>
		</div>
	</div>
</header>


<div class="page-content">

	<div class="row">
		<div class="col-md-6">
		
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-6">
							<?php
								if (!empty($additionalLanguages)) {
							?>
							<select name="language" id="language" class="form-control">
							<?php
								$languages = array_merge([$defaultLanguage], $additionalLanguages);
								foreach($languages as $language) {
									$selected = ($language == $selectedLanguage) ? 'selected' : '';
									echo '<option value="'.$language.'" '.$selected.'>'.$language.'</option>';
								}
							?>
							</select>
							<?php
								}
							?>
						</div>
						<div class="col-6 menuList text-right">
							<?php
							$base = DB::SITE()->query("SELECT id, template, under, language FROM menu WHERE name='base' AND language_id=0")->single();
							
							if ($base && $base['language'] != $selectedLanguage) {
								$languageBase = DB::SITE()->query("SELECT id FROM menu WHERE language=:language AND language_id=:language_id", [
									':language' => $selectedLanguage,
									':language_id' => $base['id']
								])->single();

								if (!$languageBase['id']) {
									DB::SITE()->query("
										INSERT INTO menu (name, type, template, under, language, language_id, sequence, typeid, url, title, description) 
										VALUES ('base', 'item', :template, :under, :language,  :language_id, 1, '', '', '', '')
									", [
										':template' => $base['template'],
										':under' => $base['under'],
										':language' => $selectedLanguage,
										':language_id' => $base['id'],
									])->execute();
									$languageBase['id'] = DB::SITE()->lastInsertId();
								}

								$base = $languageBase;
							}

							if ($base['id']) {
							?>
							<a class="btn btn-sm btn-info" href="<?= $builderSettings['public_url'].'&id='.$base['id'] ?>"><i class="la la-paint-brush"></i></a>&nbsp;
							<?php
							}
							?>
						</div>
					</div>
				</div>
				<div class="card-content">
						
						<ul class="menuList menuList-ajax">
							<?php
							
							$menuItems = DB::SITE()->query("SELECT * FROM menu WHERE type='item' AND under=:under AND language_id=0 ORDER BY sequence ASC", [':under' => $menuId])->fetch();
							
							$sequenceCounter = 0;
							foreach ($menuItems as $menuItem) {
								if ($menuItem['language'] != $selectedLanguage) {
									$mainID = $menuItem['id'];
									$menuItem = DB::SITE()->query("SELECT * FROM menu WHERE language=:language AND language_id=:language_id", [':language' => $selectedLanguage, ':language_id' => $mainID])->single();
									if (!$menuItem) {
										continue;
									}
									$menuItem['main_id'] = $mainID;
								}
								else {
									$menuItem['main_id'] = $menuItem['id'];
								}

								$sequenceCounter++;
							
								$icon = ($menuItem['show']) ? '<i class="la la-eye"></i>' : '<i class="la la-eye-slash"></i>';

								echo '<li id="menuItem' . $menuItem['main_id'] . '" data-id="' . $menuItem['main_id'] . '">'.$icon.' &nbsp;' . $menuItem['name'];
								
									echo '<span class="pull-right">';
										$prefix = ($defaultLanguage == $menuItem['language']) ? '' : '/'.$menuItem['language'];
										echo '<a class="btn btn-sm btn-info" href="'.$builderSettings['public_url'].'&id='.escape($menuItem['id']).'"><i class="la la-paint-brush"></i></a>';
										echo '&nbsp; &nbsp;';
										echo '<a class="btn btn-sm btn-info" href="index.php?page=menu&language='.$selectedLanguage.'&type=item#id=' . $menuItem['main_id'] . '&language='.$selectedLanguage.'"><i class="la la-gear"></i></a>';
										echo '&nbsp; &nbsp;';
										echo '<a class="btn btn-sm btn-info" href="' . $prefix.$menuItem['url'] . '" target="_blank"><i class="la la-eye"></i></a>';
									echo '</span>';

									$subMenuItems = DB::SITE()->query("SELECT * FROM menu WHERE under=:under ORDER BY sequence ASC", array(
										':under' => $menuItem['main_id']
									))->fetch();


									$subSequenceCounter = 0;
									if ($subMenuItems) {

										echo '<ul class="submenuList">';

										
											foreach ($subMenuItems as $subMenuItem) {
												if ($subMenuItem['language'] != $selectedLanguage) {
													$mainID = $subMenuItem['id'];
													$subMenuItem = DB::SITE()->query("SELECT * FROM menu WHERE language=:language AND language_id=:language_id", [':language' => $selectedLanguage, ':language_id' => $mainID])->single();
													if (!$subMenuItem) {
														continue;
													}
													$subMenuItem['main_id'] = $mainID;
												}
												else {
													$subMenuItem['main_id'] = $subMenuItem['id'];
												}

												$subSequenceCounter++;

												echoMenu($subMenuItem);


											}

										echo '</ul>';

									}

									echo '<ul class="addSubItemList">';

										echo '<li ><a class="d-block muted" href="index.php?page=menu&language='.$selectedLanguage.'&under=' . $menuItem['main_id'] . '&sequence=' . $subSequenceCounter++ . '#id=new&language='.$selectedLanguage.'"><i class="la la-plus"></i> &nbsp;Add</a></li>';

									echo '</ul>';


								echo '</li>';
								
							}
							
							?>
						</ul>
						<ul class="addItemList">
							<li><a class="d-block muted" href="index.php?page=menu&language=<?= $selectedLanguage ?>&under=1&sequence=<?= $sequenceCounter++ ?>&type=item#id=new&language=<?= $selectedLanguage ?>"><i class="la la-plus"></i> &nbsp;Add</a></li>
						</ul>
						

				</div>
			</div>
		</div>
		 
		<div class="col-md-6 addOrChangeMenuItem" style="position:relative;">
			<div class="alert alert-success mb-2" style="display:none;" id="menu-save-success">
				Menu saved successfully!
			</div>
			<div class="card">
				<div class="card-content">
					<div id="loader" style="text-align: center; padding: 60px 0px;">
						<i class="fa fa-circle-o-notch fa-spin"></i> Loading...
					</div>
					<div class="ajax-holder" style="position: relative; width: 100%; min-height:100%;"></div>
				</div>
			</div>
		</div>
	
	</div>
</div>

<script>
	$('#language').change(function() {
		var language = $(this).val();
		window.location.href = '<?= CMS_PUBLIC_PATH ?>?page=menu&language='+language;
	});
</script>

<?php

function echoMenu($subMenuItem) {
	global $builderSettings, $defaultLanguage, $selectedLanguage;

    $edit = false;
    $editTemplate = false;
    $delete = false;
	$preview = false;
	$icon = '';

    if ($subMenuItem['type'] == 'item') {
        $icon = ($subMenuItem['show']) ? '<i class="la la-eye"></i>' : '<i class="la la-eye-slash"></i>';
        $edit = true;
        // $editUrl = 'menu?id=' . $subMenuItem['main_id'] . '&type=sub';
        $delete = true;
        $preview = true;
        $editTemplate = true;
	}

    echo '<li id="menuItem' . $subMenuItem['main_id'] . '" data-id="' . $subMenuItem['main_id'] . '">' . $icon . ' &nbsp;' . ucfirst($subMenuItem['name']);
        echo '<span class="icons pull-right">';
		
		if ($editTemplate) {
            echo '&nbsp; &nbsp;';
			echo '<a class="btn btn-sm btn-info" href="'.$builderSettings['public_url'].'&id='.escape($subMenuItem['id']).'"><i class="la la-paint-brush"></i></a>';
		}
        if ($edit) {
            echo '&nbsp; &nbsp;';
			echo '<a class="btn btn-sm btn-info" href="index.php?page=menu&language='.$selectedLanguage.'&type=item#id=' . $subMenuItem['main_id'] . '&language='.$selectedLanguage.'"><i class="la la-gear"></i></a>';
		}
		if ($preview) {
			$prefix = ($defaultLanguage == $subMenuItem['language']) ? '' : '/'.$subMenuItem['language'];
			echo '&nbsp; &nbsp;';
			echo '<a class="btn btn-sm btn-info" href="' . $prefix.$subMenuItem['url'] . '" target="_blank"><i class="la la-eye"></i></a>';
		}
        echo '</span>';


        if ($subMenuItem['type'] == 'sub') {
            $subSubMenuItems = DB::SITE()->query("SELECT * FROM menu WHERE under=:under ORDER BY sequence ASC", array(
                ':under' => $subMenuItem['main_id']
            ))->fetch();

            $subSubSequenceCounter = 0;
            echo '<ul class="subsubmenuList" >';
                if ($subSubMenuItems) {


                    foreach ($subSubMenuItems as $subSubMenuItem) {
                        $subSubSequenceCounter++;
                        echoMenu($subSubMenuItem);
                    }

                }
            echo '</ul>';

            echo '<ul class="addSubSubItemList">';

                echo '<li ><a style="display:block" class="muted" href="index.php?page=menu&language='.$selectedLanguage.'#id=new&under=' . $subMenuItem['main_id'] . '&sequence=' . $subSubSequenceCounter++ . '&sub=1&language='.$selectedLanguage.'"><i class="la la-plus"></i> &nbsp;Add/a></li>';


            echo '</ul>';

        }

    echo '</li>';
}


	
	
	
	