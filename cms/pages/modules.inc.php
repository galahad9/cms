<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">Modules</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.php"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item active" aria-current="page">Modules</li>
				</ol>
			</nav>
		</div>
	</div>
</header>

<?php
if($_POST['module']){
	$module = plugin($_POST['module']);
	if($_POST['action'] == 'activate'){
		$module->activate();
		header('Location: index.php?page=modules');
	} elseif($_POST['action'] == 'deactivate'){
		$module->deactivate();
		header('Location: index.php?page=modules');

	}
}

?>
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<h5 class="card-header">Modules</h5>
			<div class="card-body">
				<p>Activate or Deactive modules here</p>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>Module</th>
								<th>Description</th>
								<th>Version</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach (adminPluginPages() as $key => $module) {
								echo '<tr>
									<td>'.$module->name().'</td>
									<td>'.$module->description().'</td>
									<td>'.$module->version().'</td>
									<td>';
									if($module->status() == Plugin::ACTIVE){
										echo '<span class="badge badge-success">'.$module->status().'</span>';
									} else {
										echo '<span class="badge badge-danger">'.$module->status().'</span>';
									}
									echo '</td>
									<td><form method="post">
										<input type="hidden" name="module" value="'.$module->url().'">
										';
										if($module->status() == Plugin::ACTIVE){
											echo '<button type="submit" name="action" value="deactivate" class="btn btn-sm btn-danger">Deactivate</button>';	
										} else {
											echo '<button type="submit" name="action" value="activate" class="btn btn-sm  btn-primary">Activate</button>';	
										}
										echo '
									</form></td>
								</tr>';
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>