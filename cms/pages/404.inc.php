<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">Page not found</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
					<li class="breadcrumb-item active" aria-current="page">Analytics Dashboard</li>
				</ol>
			</nav>
		</div>
	</div>
</header>
<header class="text-center m-b-30 m-t-30">
	<h1>The page you are looking for don't exists</h1>
	<p>Search for help</p>
</header>
<div class="row">
	<div class="col-lg-6 offset-lg-3 col-xl-6 offset-xl-3">
		<form>
			<div class="search-wrapper page-search">
				<button class="search-button-submit" type="submit"><i class="icon dripicons-search"></i></button>
				<input type="text" class="search-input" placeholder="Search our knowledge base...">
			</div>
		</form>
		<p class="text-center m-t-50 m-b-50">
			Or choose a category below to find what you're looking for.
		</p>
	</div>
</div>
<div class="col-12">
	<div class="card-deck m-b-100">
		<div class="card card-elevate text-center">
			<a href="#gettingStartedTitle" class="smooth-scroll">
				<div class="card-body">
					<i class="icon dripicons-meter font-size-80 text-primary"></i>
					<h5 class="card-title m-t-20">Dashboard</h5>
					<!-- <small class="text-muted">.</small> -->
				</div>
			</a>
		</div>
		<div class="card card-elevate card-hover text-center">
			<a href="#faqTitle" class="smooth-scroll">
				<div class="card-body">
					<i class="icon la-file-text font-size-80 text-primary"></i>
					<h5 class="card-title m-t-20">Content</h5>
					<!-- <small class="text-muted">Ad vegan excepteur butcher vice lomo.</small> -->
				</div>
			</a>
		</div>
		<div class="card card-elevate card-hover text-center">
			<a href="#communityTitle" class="smooth-scroll">
				<div class="card-body">
					<i class="icon dripicons-gear font-size-80 text-primary"></i>
					<h5 class="card-title  m-t-20">Settings</h5>
					<!-- <small class="text-muted">Craft beer labore wes anderson cred nesciunt.</small> -->
				</div>
			</a>
		</div>
	</div>
</div>


