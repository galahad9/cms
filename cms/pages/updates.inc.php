<?php

$core = new Core;

?>


	<header class="page-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h1 class="separator"><?= $trans['Updates'] ?></h1>
				<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/"><i class="icon dripicons-home"></i></a></li>
						<li class="breadcrumb-item"><a href="javascript:void(0)"><?= $trans['Settings'] ?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><?= $trans['Updates'] ?></li>
					</ol>
				</nav>
			</div>
			<ul class="actions top-right">
				<!-- <button class="btn btn-primary"><?= $trans['Save'] ?></button> -->
			
				<!--<li class="dropdown">
					<a href="index.php?page=user-edit&id=new" class="btn btn-fab" >
						<i class="la la-plus"></i>
					</a>
				</li>-->
			</ul>
		</div>
	</header>


	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<?php
					echo '<h2 class="pull-left">'.$cmsSettings['Name'].' Updaten</h2>';
					echo '<p class="pull-right">Latest check on '.$core->lastUpdate().'</p>';
					echo '<div class="clearfix"></div>';
					echo '<p>Current version <strong>'.$core->git()->branche('current').'</strong></p>';
					

					if($core->hasUpdate()){
						echo '<div class="alert alert-warning">
							<h3 class="text-white">There is a new version of the CMS available!</h3>
							<p>This update wil automaticly update. <a href="" class="text-white">See Changes</a>
						</div>';

						// echo '<pre>';
						// 	dump($core->git()->changes());
						// echo '</pre>';
					} else {
						echo '<div class="alert alert-success">The most recent version of the CMS has been installed</div>';

						if(file_exists(ROOT_PATH.'/.last_commit')){
							$commit = file_get_contents(ROOT_PATH.'/.last_commit');
							$exploded = explode("\n", $commit);
							$hash = str_replace('commit ', '', $exploded[0]);
							echo '
							<p>&nbsp;</p>
							<div class="accordion m-t-20" id="accordion">
								<div class="card">
									<div class="card-header" id="headingOne">
										<h5 class="mb-0">
											<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
												Latest update changes <small style="    color: #adadad; font-weight: 300; font-size: 13px; float: right; padding-right: 40px;">'.$hash.'</small>
											</button>
										</h5>
									</div>
									<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
										<div class="card-body">
											<pre>'.$commit.'</pre>
										</div>
									</div>
								</div>
							</div>';
						}
					}
					?>
				</div>
			</div>
		</div>
	</div>
	
	<?php
	if(count(getPlugins()) > 0){ ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<?php
						echo '<h2>Modules</h2>';
						echo '<p>All modules are up to date.</p>';

						?>
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th><input type="checkbox"></th>
										<th>Module</th>
										<th>Update</th>
									</tr>
								</thead>
								<tbody>
									<?php
									foreach (getPlugins() as $key => $plugin) {
										echo '<tr>';
											echo '<td><input type="checkbox"></td>';
											echo '<td>'.$plugin->name().'</td>';
											echo '<td><div class="badge badge-success">Up-to-date</div></td>';
										echo '</tr>';
									}
									?>
								</tbody>
							</table>
						</div>


						
					</div>
				</div>
			</div>
		</div>
	<?php } ?>