<?php

if(! $_SESSION['user']['admin']){
	header('Location: ' .CMS_PUBLIC_URL);
}
?>

<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator"><?= $trans['CMS'] ?> <?= $trans['Users'] ?></h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0)"><?= $trans['Settings'] ?></a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= $trans['CMS'] ?> <?= $trans['Users'] ?></li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<li class="dropdown">
				<a href="index.php?page=user-edit&id=new" class="btn btn-fab" >
					<i class="la la-plus"></i>
				</a>
			</li>
		</ul>
	</div>
</header>




<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th><?= $trans['Name'] ?></th>
								<th><?= $trans['Email address'] ?></th>
								<th><?= $trans['Role'] ?></th>
								<th><?= $trans['Language'] ?></th>
								<th><?= $trans['Admin'] ?></th>
								<th><?= $trans['Active'] ?></th>
								<th><?= $trans['Edit'] ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							
							$users = DB::CMS()->query("SELECT * FROM users ORDER BY id ASC")->fetch();
							
							foreach ($users as $user) {
							?>
						
								<tr>
									<td><?= $user['firstname'] ?> <?= $user['lastname'] ?></td>
									<td><?= $user['emailaddress'] ?></td>
									<td><?= $user['role'] ?></td>
									<td><?= $user['language'] ?></td>
									<td><?= echoCheckbox($user['admin']) ?></td>
									<td><?= echoCheckbox($user['active']) ?></td>
									<td><a href="index.php?page=user-edit&id=<?= $user['id'] ?>"><i class="la la-edit"></i> <?= $trans['Edit'] ?></a></td>
								</tr>
							
							<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>