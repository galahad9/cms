<?php


//If the id is empty we show the users profile page (should work for non-admin users)
if ((!$_SESSION['user']['admin']) && (isset($_GET['id']))) {
	header('Location: ' . CMS_PUBLIC_URL);
	exit;
}


$admin = ($_SESSION['user']['admin']);

if ($_GET['id'] != 'new') {
	if (isset($_GET['id'])) {
		$_GET['id'] = intval($_GET['id']);
	}

	$id = ($_GET['id']) ? $_GET['id'] : $_SESSION['user']['id'];
	$user = DB::CMS()->query("SELECT * FROM users WHERE id=:id", array(
		":id" => $id
	))->single();

	if (!$user) {
		redirect(CMS_PUBLIC_PATH);
	}

	$user['created'] = strtotime($user['created']);
	$user['created'] = date("d-m-Y H:i:s", $user['created']);
	
	$active = ($user['active']) ? ' checked="checked" ' : ' ';
	$admin_checked = ($user['admin']) ? ' checked="checked" ' : ' ';


} else {
	$active = ' checked="checked" ';
}
?>

<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator"><?= ($_GET['id'] != 'new') ? 'Edit' : 'Add'?> CMS <?= $trans['User'] ?></h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0)"><?= $trans['Settings'] ?></a></li>
					<li class="breadcrumb-item"><a href="index.php?page=users">CMS <?= $trans['Users'] ?></a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= ($_GET['id'] != 'new') ? $trans['Edit'] : $trans['Add'] ?></li>
				</ol>
			</nav>
		</div>
	</div>
</header>

<div class="row">
	<div class="col-md-6">
		<div class="card">
			<form method="POST" action="">
				<input name="id" value="<?= $_GET['id'] ?>" type="hidden">
				<div class="card-body">
					
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="firstname"><?= $trans['First name'] ?>*</label>
							<input type="text" id="firstname" required name="firstname" value="<?= $user['firstname'] ?>" class="form-control">
						</div>
						<div class="form-group col-md-12">
							<label for="lastname"><?= $trans['Last name'] ?></label>
							<input type="text" id="lastname" name="lastname" value="<?= $user['lastname'] ?>" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<label for="emailaddress"><?= $trans['Email address'] ?>*</label>
                            <input type="text" id="emailaddress" required name="emailaddress" value="<?= $user['emailaddress'] ?>" class="form-control">
						</div>
						<div class="form-group col-md-12">
							<label for="password"><?= $trans['Password'] ?><?= ($_GET['id'] == 'new') ? '*' : ''?></label>
                            <input type="password" id="password" <?= ($_GET['id'] == 'new') ? 'required' : ''?> name="password" value="" class="form-control">
						</div>
						<?php
						if ($admin && isset($_GET['id'])) {
						?>
						<div class="form-group col-md-12">
							<label for="role"><?= $trans['Role'] ?></label>
							<input type="text" id="role" name="role" value="<?= $user['role'] ?>" class="form-control">
						</div>
						<?php
						}
						?>
						<div class="form-group col-md-12">
							<label for="language"><?= $trans['Language'] ?></label>
							<select id="language" name="language" class="form-control">
								<?php
								$user['language'] = (empty($user['language'])) ? DEFAULT_LANGUAGE : $user['language'];
								
								$availableLanguages = getCMSLanguages();
								foreach ($availableLanguages as $language) {
									echo '<option value="' . $language . '"' . ($language == $user['language'] ? ' SELECTED' : '') . '>' . $language . '</option>';
								}							
								?>
							</select>
							
						</div>
					</div>	
					
					<?php
					if ($admin && isset($_GET['id'])) {
					?>
					<div class="form-group">
						<div class="custom-control custom-checkbox checkbox-primary form-check">
							<input type="checkbox" class="custom-control-input" id="admin" name="admin" value="1" <?= $admin_checked ?>>
							<label class="custom-control-label" for="admin"><?= $trans['Admin'] ?></label>
						</div>
					</div>
					<div class="form-group">
						<div class="custom-control custom-checkbox checkbox-primary form-check">
							<input type="checkbox" class="custom-control-input" id="active" name="active" <?= $active ?>>
							<label class="custom-control-label" for="active"><?= $trans['Active'] ?></label>
						</div>
					</div>
					<?php
					}
					?>
				</div>

				<div class="card-footer text-right">
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<?php
								if($admin) { 
								?>
								<a href="index.php?page=users"><button type="button" class="btn btn-light btn-rounded btn-outline"><?= $trans['Cancel'] ?></button></a>
								<?php
								
									if( (intval($_GET['id']) > 0) ) { 
									?>
										<button class="btn btn-danger btn-rounded" type="button" onclick="removeUser()"><?= $trans['Remove'] ?></button>
										<button class="btn btn-primary btn-rounded" type="button" onclick="saveUser(0)"><?= $trans['Save and stay'] ?></button>
									<?php 
									}
								}
								?>
								<button class="btn btn-primary btn-rounded" type="button" onclick="saveUser(<?= intval(($_GET['id'] > 0)) ?>)"><?= $trans['Save'] ?></button>
							</div>
						</div>
					</div>
				</div>

				<div class="alert alert-danger m-2" style="display:none;" id="user-save-error"></div>
				<div class="alert alert-success m-2" style="display:none;" id="user-save-success">
					<?php
					if (isset($_GET['id'])) {
						echo 'User saved successfully';
					}
					else {
						echo 'Profile saved successfully';
					}
					?>
				</div>
				
			</form>
		</div>
	</div>
</div>


<script type="text/javascript">


	<?php
		if ($admin) {
	?>
    function removeUser(){
        var btn = $(".delete-button");
        $(btn).attr("data-html", $(btn).html());
        $(btn).attr("data-onclick", $(btn).attr("onclick"));
        $(btn).html('<i class="fa fa-circle-o-notch fa-spin "></i>');
        $(btn).removeAttr('onclick');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this user!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#f35958",
            confirmButtonText: "Yes, delete it!",
            showLoaderOnConfirm: true
        }).then((result) => {
            if (result.value){
                $.post('ajax/users/deleteUser.ajax.php', { id: '<?= $_GET["id"] ?>' }, function(data, textStatus, xhr) {
                    if (data == "1"){
                        // Messenger().post("User removed!");
                        window.location.href = "index.php?page=users";
                    }
                    else{
                        // Messenger().post({message: "Something went wrong!", type: 'error'});
                    }
                });
            } else {
                $(btn).attr("onclick", $(btn).attr("data-onclick"));
                $(btn).html($(btn).attr("data-html"));
            }
        });
    }
	<?php
		}
	?>

    function saveUser(goBack) {
		
        if (goBack) {
            $(".save-button").html('<i class="fa fa-circle-o-notch fa-spin "></i>');
            $(".save-button").removeAttr('onclick');
        }

		$(':input[required]:visible').each(function(index, el) {
			var newerror = false;
			if(! $(el).val()){
				newerror = true;
				// $(el).parentsUntil('form-grou')
				$(el).closest('.form-group').addClass('has-error');
			}
			else {
				$(el).closest('.form-group').removeClass('has-error');
			}
			error = newerror;
		});

		if(error){
			// Messenger().post({ type: 'error', message:'Fix the errors first!' });
			$(".save-button").text('Save');
			$(".save-button").attr('onclick', 'saveData(1)');
			return false;
		}
		
		var id = '<?= $_GET["id"] ?>';
		var firstname = $("#firstname").val();
		var lastname = $("#lastname").val();
		var emailaddress = $("#emailaddress").val();
		var role = $("#role").val();
		var language = $("#language").val();
		var password = $("#password").val();
		var active = ($("#active").is(':checked')) ? 1 : 0;
		var admin = ($("#admin").is(':checked')) ? 1 : 0;

		var input = {firstname:firstname, lastname:lastname, emailaddress:emailaddress, role:role, language:language, password:password, active:active, admin:admin};

		if (parseInt(id) > 0 || id == 'new') {
			input.id = id;
		}

		if (id == 'new') {
			goBack = true;
		}
		
        $.post('ajax/users/setUser.ajax.php', input, function(data){
			$('#user-save-error').slideUp();
			$('#user-save-success').slideUp();

			data = JSON.parse(data);

			console.log(data);
			
			if (data.success){
				$('#user-save-success').slideDown();
				if (goBack) {
					window.location.href = "index.php?page=users";
				}
			}
			else if (data.error){
				$('#user-save-error').text(data.error).slideDown();
			}
			else{
				$('#user-save-error').text('Something went wrong!').slideDown();
			}
        });

    }


</script>
