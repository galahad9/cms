<div class="iframeHolder">
	<iframe id="builderFrame" src="<?= CMS_PUBLIC_PATH ?>builder.php?<?= $_SERVER['QUERY_STRING'] ?>" onload="iframeLoaded()"></iframe>
</div>

<script type="text/javascript">
	function iframeLoaded() {
		var iFrameID = document.getElementById('builderFrame');
		if(iFrameID) {
			// here you can make the height, I delete it first, then I make it again
			iFrameID.height = "";
			iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
		}   

		var iframe = $('#builderFrame').contents();
		iframe.find(".back-action").click(function(e){
			e.preventDefault();
			window.location.href='/cms/index.php?page=menu';
		});
	}
</script>   

<style type="text/css">
	#app { min-height:100vh; overflow:hidden; }
	.iframeHolder { width:100%; height: calc(100vh - 80px); }
	.iframeHolder iframe { width:100%; height:100%; border:none;}
	.header-wrapper { height:80px !important}
	.header-bottom { display:none !important; }

	@media (min-width: 576px){
		.container {
			max-width: 540px;
		}
	}
	@media (min-width: 768px){
		.container {
			max-width: 720px;
		}
	}
	@media (min-width: 992px){
		.container {
			max-width: 960px;
		}
	}
	@media (min-width: 1200px){
		.container {
			max-width: 1140px;
		}
	}
</style>