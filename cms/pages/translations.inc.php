<?php
use Symfony\Component\Yaml\Yaml;

if(! $_SESSION['user']['admin']){
	header('Location: ' .CMS_PUBLIC_URL);
}

if ($_POST && $_POST['action'] == 'saveTranslations') {
	unset($_POST['action']);
	foreach ($_POST as $language => $translations) {
		
		$yaml = Yaml::dump($translations);

		file_put_contents(__DIR__ . '/../translations/' . $language . '.yaml', $yaml);
	}
}
?>

<form method="POST" action="">
	<input type="hidden" name="action" value="saveTranslations">
	<header class="page-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h1 class="separator"><?= $trans['Translations'] ?></h1>
				<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
						<li class="breadcrumb-item"><a href="javascript:void(0)"><?= $trans['Admin'] ?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><?= $trans['Translations'] ?></li>
					</ol>
				</nav>
			</div>
			<ul class="actions top-right">
				<button class="btn btn-primary"><?= $trans['Save'] ?></button>
			
				<!--<li class="dropdown">
					<a href="index.php?page=user-edit&id=new" class="btn btn-fab" >
						<i class="la la-plus"></i>
					</a>
				</li>-->
			</ul>
		</div>
	</header>




	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>String</th>
									<?php
									
									$allTranslationStrings = array();
									
									$languages = getCMSLanguages();
									foreach ($languages as $language) {
										$translations[$language] = Yaml::parseFile(__DIR__ . '/../translations/' . $language . '.yaml');
										
										foreach ($translations[$language] as $index => $nothing) {
											$allTranslationStrings[$index]++;
										}
										
										echo '<th>' . strtoupper($language) . '</th>';
									}
									?>
								</tr>
							</thead>
							<tbody>
								<?php
								asort($allTranslationStrings);
								foreach ($allTranslationStrings as $index => $nothing) {
								?>
							
									<tr>
										<td><?= $index ?></td>
										<?php
										foreach ($languages as $language) {
											echo '<td><input class="form-control" type="text" name="' . $language . '[' . $index . ']" value="' . $translations[$language][$index] . '" style="width: 90%;"></td>';
										
										}
										?>
									</tr>
								
								<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</form>