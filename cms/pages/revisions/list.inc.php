<?php
    $tables = DB::CMS()->query("SELECT `database`, `table`, COUNT(`id`) AS `counter` FROM `revisions` GROUP BY `database`, `table` ORDER BY `database` ASC, `table` ASC")->fetch();
?>
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">Revisions</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.php"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0)">Revisions</a></li>
				</ol>
			</nav>
		</div>
	</div>
</header>

<section class="page-content">
	<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Table</th>
                                <th>Revisions</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($tables as $table) {
                                    if ( (!$_SESSION['user']['admin']) && ($table['database'] == 'cms') ) {
                                        continue;
                                    }
                                    echo '<tr>';
                            ?>
                            <tr>
                                <td><?= $table['database'] ?> <span class="icon dripicons-chevron-right"></span> <?= $table['table'] ?></td>
                                <td><?= $table['counter'] ?></td>
                                <td><a href="<?= CMS_PUBLIC_PATH ?>index.php?page=revisions/table&database=<?=$table['database'] ?>&table=<?= $table['table'] ?>" class="btn btn-primary">Open</a></td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>