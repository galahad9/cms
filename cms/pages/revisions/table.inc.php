<?php
    if ((!$_SESSION['user']['admin']) && ($_GET['table'] == 'cms')) {
        header('Location: ' . CMS_PUBLIC_URL);
        exit;
    }

    $rows = DB::CMS()->query("SELECT `table_id`, `date`, `data`, COUNT(`id`) AS `counter` FROM `revisions` WHERE `database`=:database AND `table`=:table GROUP BY table_id ORDER BY id DESC", [':database' => $_GET['database'], ':table' => $_GET['table']])->fetch();
    foreach($rows as $rowKey => $row) {
        $data = json_decode($row['data'],true);
        foreach($data as $dataKey => $dataValue) {
            if (strlen($dataValue) > 150) {
                $dataValue = substr($dataValue, 0, 150).'###DEBUG-END###';
            }
            $dataValue = str_replace(["\n", "\t", "\r"], ' ', $dataValue);
            while(strpos($dataValue, '  ') !== false) {
                $dataValue = str_replace('  ', ' ', $dataValue);
            }
            $dataValue = trim($dataValue);
            if (!empty($dataValue)) {
                $data[$dataKey] = htmlentities($dataValue);
            }
            else {
                unset($data[$dataKey]);
            }
        }
        unset($data['id']);
        $data = print_r($data, true);
        $data = str_replace('###DEBUG-END###', '<span class="text-danger bg-light">[...]</span>', $data);
        $rows[$rowKey]['data'] = $data;
    }
?>
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">Revisions - <?= htmlentities($_GET['table']) ?></h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.php"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="/cms/index.php?page=revisions/list">Revisions</a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0)"><?= htmlentities($_GET['table']) ?></a></li>
				</ol>
			</nav>
		</div>
	</div>
</header>

<section class="page-content">
	<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Values</th>
                                <th>Revisions</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($rows as $row) {
                            ?>
                            <tr>
                                <td><?= $row['table_id'] ?></td>
                                <td><?= $row['date'] ?></td>
                                <td><pre><?= $row['data'] ?></pre></td>
                                <td><?= $row['counter'] ?></td>
                                <td><a href="/cms/index.php?page=revisions/row&database=<?=htmlentities($_GET['database']) ?>&table=<?= htmlentities($_GET['table']) ?>&row=<?= $row['table_id'] ?>" class="btn btn-primary">Open</a></td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>