<?php
    if ( (!$_SESSION['user']['admin']) && ($_GET['table'] == 'cms') ) {
        header('Location: ' . CMS_PUBLIC_URL);
        exit;
    }

    if ( (isset($_POST['action'])) && ($_POST['action'] == 'restore') && (isset($_POST['id'])) ) {
        $row = DB::CMS()->query("SELECT * FROM `revisions` WHERE `database`=:database AND `table`=:table AND `id`=:id", [':database' => $_GET['database'], ':table' => $_GET['table'], ':id' => $_POST['id']])->single();
        $data = json_decode($row['data'],true);
        
        $dbName = strtoupper($row['database']);
        $db = DB::$dbName();

        $id = $data['id'];
        unset($data['id']);

        $fields = [];
        $values = [];
        foreach($data as $field => $value) {
            $fields[] = $field.'=:'.$field;
            $values[':'.$field] = $value;
        }
        $fields = implode(', ', $fields);
        $db->query("UPDATE {$row['table']} SET {$fields} WHERE id={$id}", $values)->execute(true);
        $restoreSuccess = true;
    }

    //Group by data so we will skip duplicate changes
    $rows = DB::CMS()->query("SELECT *, COUNT(`id`) AS `counter` FROM `revisions` WHERE `database`=:database AND `table`=:table AND `table_id`=:id GROUP BY data ORDER BY id DESC", [':database' => $_GET['database'], ':table' => $_GET['table'], ':id' => $_GET['row']])->fetch();
    
    $firstData = json_decode($rows[0]['data'], true);
    unset($rows[0]);

    $duplicateRows = 0;

    foreach($rows as $rowKey => $rowValue) {
        $data =json_decode($rowValue['data'], true);
        $oldData = [];


        foreach($data as $valueKey => $value) {
            if ($firstData[$valueKey] == $value) {
                unset($data[$valueKey]);
            }
            else {
                $oldData[$valueKey] = $firstData[$valueKey];
            }
        }

        $rows[$rowKey]['data'] = $data;
        $rows[$rowKey]['old_data'] = $oldData;

        $duplicateRows += $rowValue['counter']-1;
    }
?>

<style>
    .html-display iframe{
        width: 100%;
        border-width: 0;
        outline: 0;
        height: 500px;
    }
</style>

<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">Revisions - <?= htmlentities($_GET['table']) ?></h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.php"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="/cms/index.php?page=revisions/list">Revisions</a></li>
                    <li class="breadcrumb-item"><a href="/cms/index.php?page=revisions/table&database=<?= htmlentities($_GET['database']) ?>&table=<?= htmlentities($_GET['table']) ?>"><?= htmlentities($_GET['table']) ?></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)"><?= htmlentities($_GET['row']) ?></a></li>
				</ol>
			</nav>
		</div>
	</div>
</header>

<section class="page-content">
	<div class="row">
        <div class="col-md-12">
            <?php
                if (isset($restoreSuccess)) {
                    echo '<div class="alert alert-success mb-4">Data restored successfully</div>';
                }
                if ($duplicateRows) {
                    echo '<div class="alert alert-info mb-4">Skipped '.$duplicateRows.' duplicate rows.</div>';
                }
            ?>
        </div>
        <div class="col-12">
                <?php
                    if (count($rows) == 0) {
                ?>
                <div class="alert alert-info mb-4">All revisions are identical to the current data. &nbsp; &nbsp; No revisions to restore.</div>
                <a class="btn btn-primary" href="/cms/index.php?page=revisions/table&database=<?= htmlentities($_GET['database']) ?>&table=<?= htmlentities($_GET['table']) ?>">Back</a>
                <?php
                    }
                ?>

                <?php
                    foreach($rows as $row) {
                ?>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6"><h4><?= $row['date'] ?></h4></div>
                            <div class="col-6 text-right">
                                <form method="POST" action="">
                                    <input type="hidden" name="id" value="<?= $row['id'] ?>">
                                    <button class="btn btn-primary" name="action" value="restore">Restore</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr class="table-dark">
                                    <th width="10%">Field</th>
                                    <th width="35%">Current value</th>
                                    <th width="45%">Revision value</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    unset($row['data']['id']);
                                    foreach($row['data'] as $field => $value) {
                                        $oldData = '<b class="text-warning bg-light p-1">No previous value</b>';
                                        if (isset($row['old_data'][$field])) {
                                            $oldData = $row['old_data'][$field];
                                        }

                                        if ( (strip_tags($oldData) != $oldData) || (strip_tags($value) != $value) ) {
                                            if ( ($field == 'template') && ($row['table'] == 'menu') && ($row['name'] != 'base') ) {
                                                // $oldData = processTemplate($oldData);
                                                // $value = processTemplate($value);
                                            }
                                            $template = ($row['database'] == 'site') ? 'yes' : 'no';
                                            echo '<tr>
                                                <th width="120px">'.$field.'</th>
                                                <td><div class="html-display" data-template="'.$template.'" data-html="'.htmlspecialchars($oldData).'"></div></td>
                                                <td><div class="html-display" data-template="'.$template.'" data-html="'.htmlspecialchars($value).'"></div></td>
                                            </tr>';
                                        }
                                        else {
                                            echo '<tr>
                                                <th width="120px">'.$field.'</th>
                                                <td>'.$oldData.'</td>
                                                <td>'.$value.'</td>
                                            </tr>';
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php
                    }
                ?>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('.html-display').each(function(index, obj) {
            var html = $(obj).attr('data-html');
            if ($(obj).attr('data-template') == 'yes') {
                html = $('#head-html').attr('data-html') + html + $('#foot-html').attr('data-html');
            }
            $(obj).append('<iframe>');
            setTimeout(function() {
                $(obj).find('iframe').contents().find('html').html(html);
            },250);
        });
    });
</script>


<?php
    ob_start();
    try{
        include(WEBSITE_ROOT_PATH.'/head.inc.php');
    }
    catch(Exception $e) { }
    $headHTML = ob_get_contents();
    ob_clean();

    ob_start();
    try{
        include(WEBSITE_ROOT_PATH.'/foot.inc.php');
    }
    catch(Exception $e) { }
    $footHTML = ob_get_contents();
    ob_clean();
?>
<div id="head-html" data-html="<?= htmlspecialchars($headHTML) ?>"></div>
<div id="foot-html" data-html="<?= htmlspecialchars($footHTML) ?>"></div>