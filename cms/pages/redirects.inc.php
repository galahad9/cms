
<!--START PAGE HEADER -->
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">REDIRECTS</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.php"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0)"><?= $trans['Settings'] ?></a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= $type ?> Redirects</li>
				</ol>
			</nav>
		</div>
		
	</div>
</header>

<section class="page-content">
	<?php
	if($_POST['action'] == 'add'){
		$from = trim($_POST['from'], '/');
		$to = trim($_POST['to'], '/');
		$responsecode = intval($_POST['responsecode']);
		DB::SITE()->query("INSERT INTO redirects 
			(`from`, `to`, `responsecode`) VALUES 
			(:from, :to, :responsecode)
		", [
			':from' => $from,
			':to' => $to,
			':responsecode' => $responsecode,
		])->execute();

		echo '<div class="alert alert-success">Redirect created</div>';
	} elseif($_POST['action'] == 'remove'){
		DB::SITE()->query("DELETE FROM redirects WHERE id=:id")->bind(':id', $_POST['id'])->execute();
		echo '<div class="alert alert-success">Redirect deleted</div>';
	}

	$redirects = DB::SITE()->query("SELECT * FROM redirects")->fetch();
	$website_url = trim($settings['Website Url'],'/').'/';
	?>
	<div class="row">
		
		<div class="col-md-12">
			<div class="card">
				<h5 class="card-header">Add Redirect</h5>
				<div class="card-body">
					<p>Create a new redirect here.</p>
					<form method="post">
						<div class="row">
							<div class="col-sm-4 col-md-4">
								<label for="from">From</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon3">
											<?= $website_url ?>
										</span>
									</div>
									<input type="text" class="form-control" required name="from" id="from">
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<label for="to">To</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon3">	<?= $website_url ?>
										</span>
									</div>
									<input type="text" class="form-control" required name="to" id="to">
								</div>
							</div>
							<div class="col-sm-2 col-md-2">
								<label for="to">Response Code</label>
								<input type="number" class="form-control" required name="responsecode" id="responsecode" value="301">
							</div>
							<div class="col-sm-2 col-md-2">
								<label>&nbsp;</label>
								<div class="clearfix">
									<button type="submit" name="action" value="add" class="btn btn-primary btn-sm pull-right"><i class="la la-plus" style="color:#fff"></i> Add</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<?php
				?>

			</div>
		</div>

		<div class="col-md-12">
			<div class="card">
				<h5 class="card-header">Active Redirects</h5>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-redirects">
							<thead>
								<tr>
									<th>From</th>
									<th>To</th>
									<th style="width:400px">Response Code</th>
									<th style="width:200px">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(count($redirects) > 0){
									foreach ($redirects as $key => $redirect) {
										echo '<tr>';
											echo '<td><span class="muted">'.$website_url.'</span>'.$redirect['from'].'</td>';
											echo '<td><span class="muted">'.$website_url.'</span>'.$redirect['to'].'</td>';
											echo '<td>'.$redirect['responsecode'].'</td>';
											echo '<td><form method="post"><input type="hidden" name="id" value="'.$redirect['id'].'"><button type="submit" class="btn btn-primary btn-sm" name="action" value="remove"><span class="la la-trash"></span> Delete</button></form></td>';
										echo '</tr>';
									}
								} else {
									echo '<tr><td colspan="4">There are no redirects.</td></tr>';
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<style>
.table-redirects span.muted {
	color:#d0d3d8;
}
</style>