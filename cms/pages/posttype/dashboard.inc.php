
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator"><?= $posttype->name() ?></h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.php"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="index.php?page=modules">Modules</a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= $posttype->name() ?></li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<li class="dropdown">
				<a href="index.php?module=<?= $plugin->url() ?>&page=edit&id=new" class="btn btn-fab" >
					<i class="la la-plus"></i>
				</a>
			</li>
			<?php
			if($plugin->settingGroups){ ?>
				<li class="dropdown">
					<a href="index.php?page=settings&type=plugin&plugin=<?= $plugin->url() ?>" class="btn btn-fab" >
						<i class="la la-cog"></i>
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
</header>

<?php
if($_POST['action'] == 'delete'){
	DB::SITE()->query("DELETE FROM posttype_data WHERE id=:id")->bind(':id', $_POST['id'])->execute();
	$msg = '<div class="alert alert-success alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">'.$posttype->name().' item removed</div></div>';
	foreach ($rows as $key => $value) {
		if($value['id'] == $_POST['id']){
			unset($rows[$key]);
		}
	}
}

if($msg){
	echo $msg;
}


if($posttype->hasCategories()){
	$fields = ['category' => $categories] + $fields;
}


?>
<section class="page-content">
	<div class="row">
		<?php
		if($posttype->hasCategories()){ ?>
			<div class="col-sm-3">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Categories</h4>
					</div>
					<div class="card-body p-0">
						<ul class="list-group list-group-flush">
							
							<?php
							if(is_array($categories) && count($categories) > 0){
								foreach ($categories as $key => $category) {
									echo '
									<li class="list-group-item d-flex justify-content-between align-items-center">
										'.$category['name'].'
										<div>
											<span class="badge badge-primary badge-circle">'.count($category['rows']).'</span>
											<a href="index.php?module='.$posttype->url().'&page=category&id='.$category['id'].'" class="badge badge-danger badge-circle"><i class="la la-paint-brush" style="color:#fff"></i></a>
										</div>
										
									</li>';
								}
							}
							?>
							<li class="list-group-item"><a style="display:block" href="index.php?module=<?= $posttype->url() ?>&page=category&id=new" ><i class="la la-plus"></i> Toevoegen</a></li>
							
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-9">
		<?php } else {
			echo '<div class="col-12">';
		}
		?>
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Data</h4>
				</div>
				<div class="card-body">
					<table id="bs4-table" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>#</th>
								<?php
								foreach ($fields as $key => $value) {
									echo '<th>'.$key.'</th>';
								}
								?>
								<th style="width:200px">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php

							foreach ($rows as $datakey => $row) {
								echo '<tr>';
									echo '<td>'.$row['id'].'</td>';
									foreach ($fields as $name => $type) {
										echo '<td>'.$row[$name].'</td>';
									}
									echo '
									<td>
										<form method="post">
											<input type="hidden" name="id" value="'.$row['id'].'">
											<input type="hidden" name="action" value="delete">
											<a href="index.php?module='.$plugin->url().'&page=edit&id='.$row['id'].'" class="btn btn-sm btn-primary">Edit</a>
											<button type="submit" name="action" value="delete" class="btn btn-sm btn-danger btn-confirm">Remove</button>
										</form>
									</td>';
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$('#bs4-table').DataTable();

	$(".btn-confirm").click(function(e){
		e.preventDefault();
		$btn = $(this);

		swal({
	        title: "Are you sure?",
	        text: "",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: '#DD6B55',
	        confirmButtonText: 'Yes, I am sure!',
	        cancelButtonText: "No, cancel it!",
	    }).then(function() {
	      $btn.closest('form').submit();

	  });
	})
	
</script>