<?php

if($posttype->hasCategories()){
	$categories = [];
	if(is_array($data['categories']) && count($data['categories']) > 0){
		foreach ($data['categories'] as $key => $value) {
			$categories[] = ['id' => $value['id'], 'name' => $value['name']];
		}
	}

	$fields = ['Category' => ['type' => 'select', 'category' => $categories]] + $fields;
}


if($_GET['id'] != 'new'){
	$posttype_data = DB::SITE()->query("SELECT * FROM posttype_data WHERE id=:id LIMIT 1")->bind(':id', $_GET['id'])->single();
	$data = json_decode($posttype_data['data'], true);
}



if($_POST['action'] == 'update'){

	
	foreach ($fields as $name => $field) {
		
		if(@ $field['type'] == 'image' || @ $field['type'] == 'picture'){
			if ($_FILES[$name]['error'] === UPLOAD_ERR_OK) { 
				$ext = pathinfo($_FILES[$name]['name'], PATHINFO_EXTENSION);
				$filename = strtourl($plugin->url().'-'.$name).'.'.$ext;
				$dest = UPLOAD_DIR.$filename;

				if(move_uploaded_file($_FILES[$name]['tmp_name'], $dest)){
					$postdata[$name] = UPLOAD_FOLDER.$filename;
				} else {
					$error = 'Coundn\'t upload file. Unknown error';
				}
			} elseif($_FILES[$name]['error'] === UPLOAD_ERR_NO_FILE){
				$postdata[$name] = $data[$name];
			} else {
				$msg = '<div class="alert alert-danger alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">Couldn\'t upload image. Error code: '.$_FILES[$name]['error'].'</div></div>';
			}
		} else {
			$postdata[$name] = $_POST[$name];
		}
	}

	$insert_fields = '';
	$insert_params = '';
	$columns = '';
	$binds = [];
	
	$postdata = json_encode($postdata);

	$insert_fields = trim($insert_fields, ', ');
	$insert_params = trim($insert_params, ', ');
	$columns = trim($columns, ', ');

	if($_GET['id'] != 'new'){
		DB::SITE()->query("UPDATE posttype_data SET data=:data, category=:category, updated_at=date('now') WHERE id=:id", [
			':id' => $_GET['id'],
			':data' => $postdata,
			':category' => $_POST['category']
		])->execute();
		
		$msg = '<div class="alert alert-success alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">'.$posttype->name().' updated</div></div>';
	} else {
		DB::SITE()->query("INSERT INTO posttype_data (posttype, data, category) VALUES (:posttype, :data, :category)", [
			':posttype' => $posttype->url(),
			':data' => $postdata,
			':category' => $_POST['category']
		])->execute();

		$id = DB::SITE()->lastInsertId();
		header("Location: index.php?module={$plugin->url()}&page=edit&id=".$id);
	}
} elseif($_POST['action'] == 'remove'){
	DB::SITE()->query("UPDATE posttype_data SET deleted_at=date('now') WHERE id=:id", [
		':id' => $_GET['id']
	])->execute();
	flash_message($plugin->name().' Item Removed');
	header("Location: index.php?module={$plugin->url()}");
}

if($_GET['id'] != 'new'){
	$posttype_data = DB::SITE()->query("SELECT * FROM posttype_data WHERE id=:id LIMIT 1")->bind(':id', $_GET['id'])->single();
	$data = json_decode($posttype_data['data'], true);
}

?>
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator"><?= $posttype->name() ?></h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.php"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="index.php?page=modules">Modules</a></li>
					<li class="breadcrumb-item"><a href="index.php?module=<?= $plugin->url() ?>"><?= $posttype->name() ?></a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= $_GET['id'] ?></li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<li class="dropdown">
				<a href="index.php?module=<?= $plugin->url() ?>" class="btn btn-secondary btn-rounded btn-outline" >
					< Cancel
				</a>
			</li>
			<?php
			if($_GET['id'] != 'new'){ ?>
				<li class="dropdown">
					<form method="post">
						<button type="submit" name="action" value="remove" class="btn btn-danger btn-rounded">
							DELETE
						</button>
					</form>
				</li>
			<?php } ?>
		</ul>
	</div>
</header>

<?php


function label($name, $type){
	$label = '<label for="'.$name.'" class="col-sm-3 col-form-label">'.ucfirst($name);
		if(isset($type['instructions']) && $type['instructions']){
			@ $label .= '<br><small>'.$type['instructions'].'</small>';
		}
	$label .= '</label>';
	return $label;
}
if($msg){
	echo $msg;
}

?>

<section class="page-content">
	<div class="row">
		<div class="col-12">
			<div class="card" data-widget="dropzone">
				<!-- <h5 class="card-header">Tracking settings</h5> -->
				<form  method="post" action="" enctype="multipart/form-data">
					<input type="hidden" name="action" value="save">
					<div class="card-body">
						<?php


						foreach ($fields as $name => $type) {
							$customfield = $type;
							$attr = '';
							if(is_array($type) && $type['customfield']){
								$type = $type['type'];
							}

							if(isset($customfield['required']) && $customfield['required']){
								$attr .= ' required="required" ';
							}

							if(isset($customfield['default']) && $customfield['default'] && ! $data[$name]){
								$data[$name] = $customfield['default'];
							}
							

							if($type == 'image' || $type == 'picture'){

								echo '<div class="form-group row">
									'.label($name, $customfield).'
									<div class="col-sm-9">
											<div class="dz-message dropzone-fake needsclick singleFileUpload" style="cursor:pointer;" data-for="#'.$name.'">
											 <h6 class="card-title text-center">Click to upload.</h6>';
											 
											 if(! $data[$name]){
											 	echo '<i class="icon dripicons-cloud-upload gradient-1"></i>';
											 	echo '<div class="d-block text-center d-none"><img src="" class="img-responsive" style="max-height:80px"></div>';
											 } else {
											 	echo '<i class="icon dripicons-cloud-upload gradient-1 d-none"></i>';
											 	echo '<div class="d-block text-center"><img src="'.$settings['Website Url'].'/'.$data[$name].'?cache='.time().'" class="img-responsive" style="max-height:80px"></div>';
											 }
											 echo '
											 <div class="d-block text-center">
											  	<button type="button" class="btn btn-primary btn-rounded btn-floating btn-md">Selecteer bestand</button>
											  	<button type="button" class="btn btn-default btn-rounded btn-floating btn-md btn-remove">Remove</button>
											</div>
										 </div>
										<input style="visibility:hidden;" '.$attr.' type="file" id="'.$name.'" name="'.strtourl($name).'" value="">
									</div>
								</div>';
							} elseif($type == 'tinymce'){
								echo '<div class="form-group row">
									'.label($name, $customfield).'
									<div class="col-sm-9">
										<textarea '.$attr.' class="tinymce" name="'.strtourl($name).'" id="'.$name.'">'.$data[$name].'</textarea>
									</div>
								</div>';
							} elseif($type == 'textarea'){
								echo '<div class="form-group row">
									'.label($name, $customfield).'
									<div class="col-sm-9">
										<textarea '.$attr.' rows="20" class="form-control" name="'.strtourl($name).'" id="'.$name.'">'.$data[strtourl($name)].'</textarea>
									</div>
								</div>';

							} elseif(is_array($type)){

								echo '<div class="form-group row">
									'.label($name, $customfield).'
									<div class="col-sm-9">
										<select class="form-control" '.$attr.' id="'.$name.'" name="'.strtourl($name).'">
											<option value=""></option>';
											foreach ($categories as $category) {
												echo '<option '.(($data[$name] == $category['id']) ? 'selected' : '').' value="'.$category['id'].'">'.$category['name'].'</option>';
											}
										echo '</select>
									</div>
								</div>';

							} else {

								echo '<div class="form-group row">
									'.label($name, $customfield).'
									<div class="col-sm-9">
										<input type="'.$type.'" '.$attr.' class="form-control" id="'.$name.'" name="'.strtourl($name).'" value="'.$data[$name].'">
									</div>
								</div>';
							}

						}
						?>
						
					</div>
					<div class="card-footer bg-light">
						<div class="form-group m-b-0 row">
							<div class="col-sm-10">
								<button type="submit" name="action" value="update" class="btn btn-primary">Save</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript">
	$(".dropzone-fake").click(function(e){
		e.preventDefault(); e.stopPropagation();
		var $id = $(this).data('for');
		console.log($($id));
		$($id).click();
	});

	$.extend( Sienn.tinymceSettings, {

	});
	tinymce.init(Sienn.tinymceSettings);

	function readURL(input) {
		if (input.files && input.files[0]) {
		    var reader = new FileReader();

		    reader.onload = function(e) {
		    	$(input).closest('.form-group').find('img').attr('src', e.target.result);
		    	$(input).closest('.form-group').find('.d-block').removeClass('d-none');
		    	$(input).closest('.form-group').find('.icon').addClass('d-none');
		    }

		    reader.readAsDataURL(input.files[0]);
		}
	}

	$("input[type=file]").change(function() {
  		readURL(this);
	});

	$(".btn-remove").click(function(e){
		e.preventDefault();
		$(this).closest('.form-group').find('.d-block').addClass('d-none');
		$(this).closest('.form-group').find('.icon').removeClass('d-none');
	})
</script>
<style>
	.mce-tinymce iframe { height:600px !important }
</style>