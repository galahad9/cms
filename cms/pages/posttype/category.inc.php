<?php

$fields = [
	'name' => 'text',
	'slug' => 'text'
];


if($_POST['action'] == 'update'){
	foreach ($fields as $name => $type) {
		
		if($type == 'image'){
			if ($_FILES[$name]['error'] === UPLOAD_ERR_OK) { 
				$ext = pathinfo($_FILES[$name]['name'], PATHINFO_EXTENSION);
				$filename = strtourl($plugin->url().'-'.$name).'.'.$ext;
				$dest = UPLOAD_DIR.$filename;
				if(move_uploaded_file($_FILES[$name]['tmp_name'], $dest)){
					$postdata[$name] = $_POST[$name];
				} else {
					$error = 'Coundn\'t upload file. Unknown error';
				}
			}
		} else {
			$postdata[$name] = $_POST[$name];
		}
	}

	


	if($_GET['id'] != 'new'){
		DB::SITE()->query("UPDATE posttype_category SET name=:name, slug=:slug  WHERE id=:id", [
			':id' => $_GET['id'],
			':name' => $postdata['name'],
			':slug' => $postdata['slug']
		])->execute();
		
		$msg = '<div class="alert alert-success alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">'.$posttype->name().' updated</div></div>';
	} else {
		DB::SITE()->query("INSERT INTO posttype_category (posttype, name, slug) VALUES (:posttype, :name, :slug)", [
			':posttype' => $posttype->url(),
			':name' => $postdata['name'],
			':slug' => $postdata['slug']
		])->execute();

		$id = DB::SITE()->lastInsertId();
		header("Location: index.php?module={$plugin->url()}");
	}
} elseif($_POST['action'] == 'remove'){
	DB::SITE()->query("DELETE FROM posttype_category WHERE id=:id", [
		':id' => $_GET['id']
	])->execute();
	flash_message('Category Removed');
	header("Location: index.php?module={$plugin->url()}");
}

$data = $data['categories'][$_GET['id']];

?>
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator"><?= $posttype->name() ?> Category</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="index.php?page=modules">Modules</a></li>
					<li class="breadcrumb-item"><a href="index.php?module=<?= $plugin->url() ?>"><?= $posttype->name() ?></a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= $_GET['id'] ?></li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<li class="dropdown">
				<a href="index.php?module=<?= $plugin->url() ?>" class="btn btn-secondary btn-rounded btn-outline" >
					< Cancel
				</a>
			</li>
			<?php
			if($_GET['id'] != 'new'){ ?>
				<li class="dropdown">
					<form method="post">
						<button type="submit" name="action" value="remove" class="btn btn-danger btn-rounded">
							DELETE
						</button>
					</form>
				</li>
			<?php } ?>
		</ul>
	</div>
</header>

<?php

if($msg){
	echo $msg;
}
?>

<section class="page-content">
	<div class="row">
		<div class="col-12">
			<div class="card" data-widget="dropzone">
				<!-- <h5 class="card-header">Tracking settings</h5> -->
				<form  method="post" action="" enctype="multipart/form-data">
					<input type="hidden" name="action" value="save">
					<div class="card-body">
						<?php
						foreach ($fields as $name => $type) {

							if($type == 'image'){

								echo '<div class="form-group row">
									<label for="'.$name.'" class="col-sm-3 col-form-label">Image</label>
									<div class="col-sm-9">
											<div class="dz-message dropzone-fake needsclick singleFileUpload" style="cursor:pointer;" data-for="#'.$name.'">
											 <h6 class="card-title text-center">Click to upload.</h6>';
											 
											 if(! $data[$name]){
											 	echo '<i class="icon dripicons-cloud-upload gradient-1"></i>';
											 	echo '<div class="d-block text-center d-none"><img src="" class="img-responsive" style="max-height:80px"></div>';
											 } else {
											 	echo '<i class="icon dripicons-cloud-upload gradient-1 d-none"></i>';
											 	echo '<div class="d-block text-center"><img src="'.$settings['Website Url'].'/'.UPLOAD_FOLDER.''.$data[$name].'?cache='.time().'" class="img-responsive" style="max-height:80px"></div>';
											 }
											 echo '
											 <div class="d-block text-center">
											  	<button type="button" class="btn btn-primary btn-rounded btn-floating btn-md">Selecteer bestand</button>
											  	<button type="button" class="btn btn-default btn-rounded btn-floating btn-md btn-remove">Remove</button>
											</div>
										 </div>
										<input style="visibility:hidden;" type="file" id="'.$name.'" name="'.$name.'" value="">
									</div>
								</div>';
							} elseif($type == 'tinymce'){
								echo '<div class="form-group row">
									<label for="'.$name.'" class="col-sm-3 col-form-label">Content</label>
									<div class="col-sm-9">
										<textarea class="tinymce" name="'.$name.'" id="'.$name.'">'.$data[$name].'</textarea>
									</div>
								</div>';
							} else {

								echo '<div class="form-group row">
									<label for="'.$name.'" class="col-sm-3 col-form-label">'.ucfirst($name).'</label>
									<div class="col-sm-9">
										<input type="'.$type.'" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$data[$name].'">
									</div>
								</div>';
							}

						}
						?>
						
					</div>
					<div class="card-footer bg-light">
						<div class="form-group m-b-0 row">
							<div class="col-sm-10">
								<button type="submit" name="action" value="update" class="btn btn-primary">Save</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript">
	$(".dropzone-fake").click(function(e){
		e.preventDefault(); e.stopPropagation();
		var $id = $(this).data('for');
		console.log($($id));
		$($id).click();
	});

	$.extend( Sienn.tinymceSettings, {

	});
	tinymce.init(Sienn.tinymceSettings);

	function readURL(input) {
		if (input.files && input.files[0]) {
		    var reader = new FileReader();

		    reader.onload = function(e) {
		    	$(input).closest('.form-group').find('img').attr('src', e.target.result);
		    	$(input).closest('.form-group').find('.d-block').removeClass('d-none');
		    	$(input).closest('.form-group').find('.icon').addClass('d-none');
		    }

		    reader.readAsDataURL(input.files[0]);
		}
	}

	$("input[type=file]").change(function() {
  		readURL(this);
	});

	$(".btn-remove").click(function(e){
		e.preventDefault();
		$(this).closest('.form-group').find('.d-block').addClass('d-none');
		$(this).closest('.form-group').find('.icon').removeClass('d-none');
	})
</script>
<style>
	.mce-tinymce iframe { height:600px !important }
</style>