<script>
	(function(w,d,s,g,js,fs){
	  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
	  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
	  js.src='https://apis.google.com/js/platform.js';
	  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
	}(window,document,'script'));

	
	gapi.analytics.ready(function() {
		var data = JSON.parse('<?= getAnalyticsToken() ?>');

		gapi.analytics.auth.authorize({
			'serverAuth': {
				'access_token': data.access_token
			}
		});
		
		changeCharts('30daysAgo');
		
		
		
	});
	
		
	startDate = '';
		
	function changeCharts(newStartDate, obj = '') {
		if (jQuery.type(obj) == 'object') {
			$( ".nav-chart-visitors li a" ).each(function() {
				$(this).removeClass("active");
			});
			$(obj).addClass("active");
		}
		
		startDate = newStartDate;
	
		drawVisitors();
		drawTopPages();
		drawChannels();
		drawChannelsTable();
		
		getAnalyticsSingleData('ga:sessions', 'total-visitors');
		getAnalyticsSingleData('ga:pageviews', 'total-pageviews');
		getAnalyticsSingleData('ga:pageviewsPerSession', 'total-pageviewspersession');
		getAnalyticsSingleData('ga:avgTimeOnPage', 'total-timeonpage');
	}
	
	//ga:sessions
	//total-visitors
	
	function getAnalyticsSingleData(metric, id) {
		var dataChart1 = new gapi.analytics.googleCharts.DataChart({
			query: {
				'ids': 'ga:<?= $settings['Google Analytics ID Data Display'] ?>', // <-- Replace with the ids value for your view.
				'start-date': startDate,
				'end-date': 'yesterday',
				'metrics': metric,
				'max-results': '5',
			},
			chart: {
				'container': id,
				'type': 'TABLE',
				'options': {
					'width': '100%',
				}
			}
		});
		dataChart1.execute();
		
		dataChart1.on('success', function(response) {
			content = $("#" + id + " .google-visualization-table-table tr").children("td:first").html();
			console.log(content);
			$("#" + id).html(content);
		});
	};

	
</script>

<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">Analytics Dashboard</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
					<li class="breadcrumb-item active" aria-current="page">Analytics Dashboard</li>
				</ol>
			</nav>
		</div>
		<ul class="actions top-right">
			<ul class="nav nav-chart-visitors nav-pills nav-pills-primary justify-content-end" id="pills-demo-1" role="tablist">
				<li class="nav-item">
					<a class="nav-link" onclick="changeCharts('7daysAgo', this)" style="cursor: pointer;">Week</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" onclick="changeCharts('30daysAgo', this)" style="cursor: pointer;">Month</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" onclick="changeCharts('365daysAgo', this)" style="cursor: pointer;">Year</a>
				</li>
			</ul>

			
		</ul>
	</div>
</header>
<section class="page-content">
	<div class="row">
		<div class="col-lg-7">
			<div class="card">
				<h5 class="card-header p-t-25 p-b-20"><span class="">Visitors</span></h5>
				<div class="card-toolbar top-right">
					
				</div>
				<div class="card-body">
					<div class="tab-content" id="pills-tabContent-1">
						<div class="tab-pane fade show active" id="pills-1" role="tabpanel" aria-labelledby="pills-1">
							<div id="chart-visitors"></div>
							
							<script>
							
								function drawVisitors() {
									
									
									var chartVisitorsWeek = new gapi.analytics.googleCharts.DataChart({
										query: {
											'ids': 'ga:<?= $settings['Google Analytics ID Data Display'] ?>', // <-- Replace with the ids value for your view.
											'start-date': startDate,
											'end-date': 'yesterday',
											'metrics': 'ga:sessions,ga:pageviews',
											'dimensions': 'ga:date'
										},
										chart: {
											'container': 'chart-visitors',
											'type': 'LINE',
											'options': {
												'width': '100%'
											}
										}
									});
									chartVisitorsWeek.execute();
								}
								
							</script>
							
							
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="card bg-primary" id="totalVisitsChart">
				<div class="card-body p-0">
					<div class="row">
						<div class="col-lg-4">
							<h5 class="card-title border-none text-white p-l-20 p-t-20 m-b-0">Total Visitors</h5>
						</div>
						<div class="col-lg-8">
							<div class="tab-content" id="total-visits-tab-content">
								<div class="tab-pane fade show active" id="total-visits-tab-1" role="tabpanel" aria-labelledby="total-visits-tab-1">
									<span class="card-title text-white font-size-40 font-w-300 p-l-20 counter" id="total-visitors">0</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card bg-primary" id="totalUniqueVisitsChart">
				<div class="card-body p-0">
					<div class="row">
						<div class="col-lg-4">
							<h5 class="card-title border-none text-white p-l-20 p-t-20 m-b-0">Total Pageviews</h5>
						</div>
						<div class="col-lg-8">
							<div class="tab-content" id="total-visits-tab-content">
								<div class="tab-pane fade show active" id="total-visits-tab-1" role="tabpanel" aria-labelledby="total-visits-tab-1">
									<span class="card-title text-white font-size-40 font-w-300 p-l-20 counter" id="total-pageviews">0</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="card bg-accent" id="totalUniqueVisitsChart">
				<div class="card-body p-0">
					<div class="row">
						<div class="col-lg-4">
							<h5 class="card-title border-none text-white p-l-20 p-t-20 m-b-0">Pageviews per visitor</h5>
						</div>
						<div class="col-lg-8">
							<div class="tab-content" id="total-visits-tab-content">
								<div class="tab-pane fade show active" id="total-visits-tab-1" role="tabpanel" aria-labelledby="total-visits-tab-1">
									<span class="card-title text-white font-size-40 font-w-300 p-l-20 counter" id="total-pageviewspersession">0</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card bg-accent" id="totalUniqueVisitsChart">
				<div class="card-body p-0">
					<div class="row">
						<div class="col-lg-4">
							<h5 class="card-title border-none text-white p-l-20 p-t-20 m-b-0">AVG Time On Site</h5>
						</div>
						<div class="col-lg-8">
							<div class="tab-content" id="total-visits-tab-content">
								<div class="tab-pane fade show active" id="total-visits-tab-1" role="tabpanel" aria-labelledby="total-visits-tab-1">
									<span class="card-title text-white font-size-40 font-w-300 p-l-20 counter" id="total-timeonpage">0</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="row m-b-30">
		<div class="col">
			<div class="card-deck">
				<div class="card">
					<h5 class="card-header p-t-25 p-b-20">Top Pages</h5>
											
					
					<div class="card-body">
					
						<div class="table-responsive" id="analytics-container-top-pages"></div>
						
						<script>
							
							function drawTopPages() {
								var chartTopPages = new gapi.analytics.googleCharts.DataChart({
									query: {
										'ids': 'ga:<?= $settings['Google Analytics ID Data Display'] ?>', // <-- Replace with the ids value for your view.
										'start-date': startDate,
										'end-date': 'yesterday',
										'dimensions': 'ga:pagePath',
										'sort': '-ga:pageviews',
										'metrics': 'ga:pageviews',
										'max-results': '12',
									},
									chart: {
										'container': 'analytics-container-top-pages',
										'type': 'TABLE',
										'options': {
											'width': '100%',
										}
									}
								});
								chartTopPages.execute();

								chartTopPages.on('success', function(response) {
									$(".google-visualization-table-table tr").removeClass('gapi-analytics-data-chart-styles-table-tr-odd');
									$(".google-visualization-table-table td").removeClass('gapi-analytics-data-chart-styles-table-td');
								
									$(".google-visualization-table-table").removeClass('google-visualization-table-table').addClass('table table-striped');
								});
																				
																				

							}

						</script>
					
					
						
						
					</div>
				</div>
				<div class="card">
					<h5 class="card-header p-t-25 p-b-20">Channels</h5>

					<div class="card-body">
						<div class="tab-content" id="pills-tabContent-1">
							<div class="tab-pane fade show active" id="pills-1" role="tabpanel" aria-labelledby="pills-1">
						
								<div class="table-responsive" id="analytics-container-channels"></div>
								
								<script>
									
									function drawChannels() {
										var chartChannels = new gapi.analytics.googleCharts.DataChart({
											query: {
												'ids': 'ga:<?= $settings['Google Analytics ID Data Display'] ?>', // <-- Replace with the ids value for your view.
												'start-date': startDate,
												'end-date': 'yesterday',
												'dimensions': 'ga:channelGrouping',
												'sort': '-ga:users',
												'metrics': 'ga:users'
											},
											chart: {
												'container': 'analytics-container-channels',
												'type': 'COLUMN',
												'options': {
													'width': '80%'
												}
											}
										});
										chartChannels.execute();

																						
																						

									}

								</script>
								<br>
								<div class="table-responsive" id="analytics-container-channels-table"></div>
								
								<script>
									
									function drawChannelsTable() {
										var chartChannelsTable = new gapi.analytics.googleCharts.DataChart({
											query: {
												'ids': 'ga:<?= $settings['Google Analytics ID Data Display'] ?>', // <-- Replace with the ids value for your view.
												'start-date': startDate,
												'end-date': 'yesterday',
												'dimensions': 'ga:channelGrouping',
												'sort': '-ga:users',
												'metrics': 'ga:users'
											},
											chart: {
												'container': 'analytics-container-channels-table',
												'type': 'TABLE',
												'options': {
													'width': '100%',
												}
											}
										});
										chartChannelsTable.execute();

												
										chartChannelsTable.on('success', function(response) {
											$(".google-visualization-table-table tr").removeClass('gapi-analytics-data-chart-styles-table-tr-odd');
											$(".google-visualization-table-table td").removeClass('gapi-analytics-data-chart-styles-table-td');
										
											$(".google-visualization-table-table").removeClass('google-visualization-table-table').addClass('table table-striped');
										});
										
									}

								</script>
					
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!--
	<div class="card-deck">
		<div class="card">
			<h5 class="card-header">Browser Stats</h5>
			<div class="card-body p-0">
				<ul class="list-group list-group-flush">
					<li class="list-group-item">
						<div class="media">
							<img class="align-self-center mr-3 rounded-circle" src="http://via.placeholder.com/48x48" alt=" ">
							<div class="media-body p-t-10">
								<p class="mb-0 d-inline">Google Chrome</p>
								<span class="badge badge-pill badge-primary float-right">32% <i class="zmdi zmdi-trending-up font-size-14 text-white"></i></span>
							</div>
						</div>
					</li>
					<li class="list-group-item">
						<div class="media">
							<img class="align-self-center mr-3 rounded-circle" src="http://via.placeholder.com/48x48" alt=" ">
							<div class="media-body p-t-10">
								<p class="mb-0 d-inline">Apple Safari</p>
								<span class="badge badge-pill badge-accent float-right">27% <i class="zmdi zmdi-trending-down font-size-14 text-white"></i></span>
							</div>
						</div>
					</li>
					<li class="list-group-item">
						<div class="media">
							<img class="align-self-center mr-3 rounded-circle" src="http://via.placeholder.com/48x48" alt=" ">
							<div class="media-body p-t-10">
								<p class="mb-0 d-inline">Mozila Firefox</p>
								<span class="badge badge-pill badge-success float-right">17% <i class="zmdi zmdi-trending-up font-size-14 text-white"></i></span>
							</div>
						</div>
					</li>
					<li class="list-group-item">
						<div class="media">
							<img class="align-self-center mr-3 rounded-circle" src="http://via.placeholder.com/48x48" alt=" ">
							<div class="media-body p-t-10">
								<p class="mb-0 d-inline">Microsoft edge</p>
								<span class="badge badge-pill badge-warning float-right">13% <i class="zmdi zmdi-trending-up font-size-14 text-white"></i></span>
							</div>
						</div>
					</li>
					<li class="list-group-item">
						<div class="media">
							<img class="align-self-center mr-3 rounded-circle" src="http://via.placeholder.com/48x48" alt=" ">
							<div class="media-body p-t-10">
								<p class="mb-0 d-inline">UC</p>
								<span class="badge badge-pill badge-info float-right">10% <i class="zmdi zmdi-trending-down font-size-14 text-white"></i></span>
							</div>
						</div>
					</li>
					<li class="list-group-item">
						<div class="media">
							<img class="align-self-center mr-3 rounded-circle" src="http://via.placeholder.com/48x48" alt=" ">
							<div class="media-body p-t-10">
								<p class="mb-0 d-inline">Brave</p>
								<span class="badge badge-pill badge-danger float-right">3% <i class="zmdi zmdi-trending-up font-size-14 text-white"></i></span>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>


		<div class="card" id="barChartLineThree">
			<h5 class="card-header">Overview</h5>
			<div class="card-body">
				<div class="row h-100">
					<div class="col-4">
						<div class="counter counter-md text-left">
							<div class="f-w-400 ">USERS</div>
							<div class="counter-number-group">
								<span class="text-success">627,852 <i class="zmdi zmdi-trending-up font-size-14 text-success"></i></span>
							</div>
						</div>
					</div>
					<div class="col-4">
						<div class="counter counter-sm text-left inline-block">
							<div class="f-w-400">NEW SESSIONS</div>
							<div class="counter-number-group">
								<span class="text-info">412,346 <i class="zmdi zmdi-trending-up font-size-14 text-info"></i></span>
							</div>
						</div>
					</div>
					<div class="col-4">
						<div class="counter counter-sm text-left inline-block">
							<div class="f-w-400">BOUNCE RATE</div>
							<div class="counter-number-group">
								<span class="text-danger">17,555 <i class="zmdi zmdi-trending-down font-size-14 text-danger"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="ct-chart line-chart h-300 mt-30"></div>
			</div>
		</div>

	</div>
	-->
</section>

<!-- ================== PAGE LEVEL VENDOR SCRIPTS ==================-->
<script src="admin-assets/vendor/countup.js/dist/countUp.min.js"></script>
<script src="admin-assets/vendor/jvectormap-next/jquery-jvectormap.min.js"></script>
<script src="admin-assets/vendor/jvectormap-next/jquery-jvectormap-world-mill.js"></script>
<script src="admin-assets/vendor/chartist/dist/chartist.js"></script>
<script src="admin-assets/vendor/flot/jquery.flot.js"></script>
<script src="admin-assets/vendor/jquery.flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="admin-assets/vendor/flot/jquery.flot.resize.js"></script>
<script src="admin-assets/vendor/flot/jquery.flot.time.js"></script>
<script src="admin-assets/vendor/flot.curvedlines/curvedLines.js"></script>
<!-- ================== PAGE LEVEL SCRIPTS ==================-->
<script src="admin-assets/js/cards/sessions-by-location.js"></script>
<script src="admin-assets/js/components/countUp-init.js"></script>
<script src="admin-assets/js/cards/total-visits-chart.js"></script>
<script src="admin-assets/js/cards/total-unique-visits-chart.js"></script>
<script src="admin-assets/js/cards/bar-chart-line-three.js"></script>
<script src="admin-assets/js/cards/traffic-sources.js"></script>			