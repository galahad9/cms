

<!--START PAGE HEADER -->
<header class="page-header">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h1 class="separator">CSS</h1>
			<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.php"><i class="icon dripicons-home"></i></a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0)"><?= $trans['Settings'] ?></a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= $type ?> CSS</li>
				</ol>
			</nav>
		</div>
		
	</div>
</header>
<?php
if($error){ 
	echo '<div class="alert alert-danger alert-outline alert-dismissible fade show"><div class="icon"><i class="la la-warning"></i></div> <div class="text">'.$error.'</div></div>';
}
?>
<section class="page-content">

	<div class="row">
		
	
		<div class="col-md-6">
			<div class="card" data-widget="dropzone">
				<h5 class="card-header">Stylesheet</h5>
				<?php
				$file = CMS_ROOT_PATH . '/assets/css/site.css';
				@ $content = file_get_contents($file);
				?>
				<form class="assetForm" method="post" action="" enctype="multipart/form-data">
					<input type="hidden" name="file" value="<?= $file ?>">
					<div class="card-body" style="height:60vh;">
						<div id="editor"><?= $content ?></div>
					</div>
						
					<div class="card-footer bg-light">
						<div class="form-group m-b-0 row">
							<div class="col-sm-10">
								<button type="submit" class="btn btn-primary"><?= $trans['Save'] ?></button>
							</div>
						</div>
					</div>
				</form>

			</div>
		</div>

		<div class="col-md-6">
			<div class="card" data-widget="dropzone">
				<h5 class="card-header">Javascript</h5>
				<?php
				$file = CMS_ROOT_PATH . '/assets/js/js.js';
				@ $content = file_get_contents($file);
				?>
				<form class="assetForm" method="post" action="" enctype="multipart/form-data">
					<input type="hidden" name="file" value="<?= $file ?>">
					<div class="card-body" style="height:60vh;">
						<div id="editor2"><?= $content ?></div>
					</div>
						
					<div class="card-footer bg-light">
						<div class="form-group m-b-0 row">
							<div class="col-sm-10">
								<button type="submit" class="btn btn-primary"><?= $trans['Save'] ?></button>
							</div>
						</div>
					</div>
				</form>

			</div>
		</div>

	</div>
	
</section>

<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/ace/ace.js" type="text/javascript" charset="utf-8"></script>
<script>
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.session.setMode("ace/mode/css");
    editor.setShowPrintMargin(false);
    $("#editor").data('editor', editor);

    var editor2 = ace.edit("editor2");
    editor2.setTheme("ace/theme/monokai");
    editor2.session.setMode("ace/mode/javascript");
    editor2.setShowPrintMargin(false);
    $("#editor2").data('editor', editor2);

    $(".assetForm").on('submit', function(e){
    	e.preventDefault();
    	var $form = $(this);
    	var value = $form.find('.ace_editor').data('editor').getValue();
    	var file = $form.find('input[name=file]').val();
		$.post("ajax/assets/setAsset.ajax.php", { file: file, value: value } ,function(data){
			if(data == 'success'){
				var $msg = $('<div class="alert alert-success">Opgeslagen</div>');
			} else {
				var $msg = $('<div class="alert alert-danger">'+data+'</div>');
			}

			$form.before($msg);
			setTimeout(function(){
				$msg.slideUp('slow', function() {
					$msg.remove();
				});
			}, 1000);
    	});
	});


</script>
<style type="text/css" media="screen">
    #editor, #editor2 { 
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
    }
</style>