<?php

function flash_message($msg){
	$_SESSION['flash_message'] = $msg;
}

function getCustomPostTypes(){
	$posttypes = [];
	foreach (getPlugins() as $key => $plugin) {
		if($plugin->isPostType()){
			$posttypes[] = $plugin->posttype();
		}
	}
	return $posttypes;
}


function getUserGroups(){
	return DB::CMS()->query("SELECT role FROM users GROUP BY role ORDER BY role ASC")->fetch();
}

function setWebsiteSetting($name, $value, $category='', $type='', $plugin=0){
    $found = DB::SITE()->query("SELECT id FROM settings WHERE name=:name", [':name' => $name])->get('id');
	if ($found) {
		DB::SITE()->query("UPDATE settings SET value=:value WHERE name=:name", array(
			':name' => $name,
			':value' => $value
		))->execute();
	}
	else {
		DB::SITE()->query("INSERT INTO settings (name, value, category, type, plugin) VALUES (:name, :value, :category, :type, :plugin)", array(
			':name' => $name,
			':value' => $value,
			':category' => $category,
			':type' => $type,
			':plugin' => $plugin
		))->execute();
	}
}

function removeWebsiteSetting($name){
    DB::SITE()->query("DELETE FROM settings WHERE name=:name")->bind(':name', $name)->execute();
}

function checkOperator($value1, $operator, $value2) {
    switch ($operator) {
        case '<': // Less than
            return $value1 < $value2;
        case '<=': // Less than or equal to
            return $value1 <= $value2;
        case '>': // Greater than
            return $value1 > $value2;
        case '>=': // Greater than or equal to
            return $value1 >= $value2;
        case '==': // Equal
            return $value1 == $value2;
        case '===': // Identical
            return $value1 === $value2;
        case '!==': // Not Identical
            return $value1 !== $value2;
        case '!=': // Not equal
        case '<>': // Not equal
            return $value1 != $value2;
        case '||': // Or
        case 'or': // Or
           return $value1 || $value2;
        case '&&': // And
        case 'and': // And
           return $value1 && $value2;
        case 'xor': // Or
           return $value1 xor $value2;  
        default:
            return FALSE;
    } // end switch
}