<?php
require(__DIR__ . '/../../cmsFirstment.inc.php');

if($_REQUEST['plugin']){
	$query = DB::SITE()->query("SELECT COUNT(id) as `count` FROM settings WHERE name=:name AND `type`='builder'")->bind(':name', $_REQUEST['name']);

	if($query->get('count') > 0){
		DB::SITE()->query("UPDATE settings SET `value`=:value WHERE `name`=:name AND `type`='builder'", [
			':value' => $_POST['html'],
			':name' => $_REQUEST['name']
		])->execute();
	} else {

		$category = '';
	    foreach ($_PLUGINS['current']->settingGroups as $key => $group) {
	    	foreach ($group['settings'] as $key => $setting) {
	            if($setting['name'] == $_REQUEST['name']){
	                $category = $group['category'];
	            }
	    	}
	    }

		DB::SITE()->query("INSERT INTO settings (value, name, type, category) VALUES (:value, :name, :type, :category)", [
			':value' => $_POST['html'],
			':name' => $_REQUEST['name'],
			':type' => 'builder',
			':category' => $category
		])->execute();
	}
} else {
	$id = $_GET['id'];
	$html = $_POST['html'];

	$html = everything_in_tags($html, 'body');
	
	DB::SITE()->query("UPDATE menu SET template=:template WHERE id=:id", [
		':template' => $html,
		':id' => $id
	])->execute();
	echo DB::SITE()->affected();
}