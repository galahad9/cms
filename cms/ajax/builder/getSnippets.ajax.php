<?php
require(__DIR__ . '/../../cmsFirstment.inc.php');

$assets_url = $builderSettings['assets_base'];
$plugin_url = PLUGINS_URL;

ob_start();

if($_REQUEST['snippets'] && $_REQUEST['snippets'] != 'null'){
	$snippets = json_decode($_REQUEST['snippets'], true);
	$files = [];
	foreach ($snippets as $key => $snippet_path) {
		$files = array_merge($files, glob_recursive($snippet_path));
	}

	foreach ($files as $key => $file) {
		include($file);
	}

} else {

	// Default CMS snippets
	include(CMS_ROOT_PATH.'libs/builder/snippets.inc.php');

	// Plugin snippets
	foreach(getPlugins() as $plugin){
		$files = [];
			
		if($plugin->snippets){
			$snippets = $plugin->snippets;

			if(! is_array($plugin->snippets)){
				$snippets = [$plugin->snippets];
			}
			
			foreach ($snippets as $key => $snippet_path) {
				$files = array_merge($files, glob_recursive($snippet_path));
			}
		}

		foreach ($files as $key => $file) {
			include($file);
		}
		
	}
	
}

$snippets = ob_get_contents();
ob_end_clean();

echo $snippets;