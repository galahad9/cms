<?php
require(__DIR__ . '/../../cmsFirstment.inc.php');

$file = $_POST['file'];
$value = $_POST['value'];

if(file_exists($file) && !is_writable($file)){
	die('File not writable');
}

if(file_put_contents($file, $value)){
	die('success');
}

die('Writing file failed!');

?>