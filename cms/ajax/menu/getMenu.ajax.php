<?php
require(__DIR__ . '/../../cmsFirstment.inc.php');

$settings = loadSettings('SITE');
$defaultLanguage = $settings['Default Language'];
$additionalLanguages = explode(',', $settings['Additional Languages']);
$additionalLanguages = array_filter($additionalLanguages);
$selectedLanguage = (isset($_GET['language'])) ? escape($_GET['language']) : $defaultLanguage;

if (!empty($_GET['id'])) {

    $menu = DB::SITE()->query("SELECT * FROM `menu` WHERE `id`=:id", [':id' => $_GET['id']])->single();
    if ($menu['language'] != $_GET['language']) {
        $parentMenu = $menu;
        $menu = DB::SITE()->query("SELECT * FROM `menu` WHERE `language_id`=:language_id AND `language`=:language", [':language_id' => $_GET['id'], ':language' => $_GET['language']])->single();
        if (empty($menu) && !empty($parentMenu)) {
            $menu['show'] = $parentMenu['show'];
        }
        else {
            $menu['show'] = 1;
        }
    }

    $languagesFound = [];
    if ($menu['id'] || $parentMenu['id']) {
        $findID = $menu['id'];
        if ($parentMenu['id']) {
            $findID = $parentMenu['id'];
        }
        $languagesFound = DB::SITE()->query("SELECT language FROM `menu` WHERE `language_id`=:language_id OR id=:id", [':language_id' => $findID, ':id' => $findID])->fetch();
        $languagesFound = array_column($languagesFound, 'language');
    }

    $show = (intval($menu['show'])) ? 'checked' : '';

    $reload = (isset($menu['id'])) ? 0 : 1;
    
    if (empty($_GET['type'])) {
        ?>
        <form method="POST" onsubmit="event.preventDefault(); return false;" class="card">
        
        
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <?php
                            if (!empty($additionalLanguages)) {
                                $languages = array_merge([$defaultLanguage], $additionalLanguages);
                                foreach($languages as $language) {
                                    $btnClass = (in_array(strtolower($language), $languagesFound)) ? 'btn-primary' : 'btn-dark';
                                    if ($selectedLanguage == $language) {
                                        echo '<button type="button" class="btn btn-sm '.$btnClass.'" disabled>'.$language.'</button> &nbsp; ';
                                    }
                                    else {
                                        echo '<a class="btn btn-sm '.$btnClass.'" href="'.CMS_PUBLIC_PATH.'?page=menu&language='.escape($_GET['page_language']).'#id='.escape($_GET['id']).'&language='.$language.'">'.$language.'</a> &nbsp; ';
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="col-6 text-right">
                        <?php
                        if ($menu['id']) {
                            $multiLanguage = (count($languagesFound) > 1) ? 1 : 0;
                        ?>
                        <button class="btn btn-danger remove-menu-item" type="button" data-menu-item-id="<?= $menu['id'] ?>"><span class="la la-trash"></span>Delete</button>
                        &nbsp; 
                        <a href="<?= $builderSettings['public_url'] ?>&id=<?= escape($menu['id']) ?>" class="btn btn-primary"><span class="la la-paint-brush"></span> Edit template</a>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th colspan="3"></th>
                        </tr>
                    </thead>

                    <tbody style="border-top: 0px;">
                        <tr>
                            <td class="v-align-middle">
                                Name
                            </td><td class="v-align-middle">
                                <input type="text" class="form-control menu-item-name" name="name" value="<?= $menu['name'] ?>" data-titleval="<?= $menu['name'] ?>" data-slugval="<?= $menu['name'] ?>" required>
                            </td>
                            <td class="v-align-middle"><a
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>
                    
                    <tbody style="border-top: 0px;">
                        <tr>
                            <td class="v-align-middle">
                                Title
                            </td><td class="v-align-middle">
                                <input type="text" class="form-control menu-item-title" name="title" value="<?= $menu['title'] ?>" required>
                            </td>
                            <td class="v-align-middle"><a
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>

                    <tbody style="border-top: 0px;">
                        <tr>
                            <td class="v-align-middle">
                                Url
                            </td><td class="v-align-middle">
                                <input type="text" class="form-control menu-item-url" name="url" value="<?= $menu['url'] ?>" required>
                            </td>
                            <td class="v-align-middle"><a
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>
                    
                    <tbody style="border-top: 0px;">
                        <tr>
                            <td class="v-align-middle">
                                Show in menu
                            </td><td class="v-align-middle">
                                <div class="custom-control custom-checkbox checkbox-primary">
                                    <?php $cid = uniqid(); ?>
                                    <input type="checkbox" id="show_<?= $cid ?>" name="show" class="custom-control-input menu-item-show" <?= $show ?>>
                                    <label for="show_<?= $cid ?>" class="custom-control-label">Show in menu</label>
                                </div>
                            </td>
                            <td class="v-align-middle"><a
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>

                    <tbody style="border-top: 0px;">
                        <tr>
                            <td class="v-align-middle">
                                Description
                            </td><td class="v-align-middle">
                                <textarea class="form-control menu-item-description" name="url"><?= $menu['description'] ?></textarea>
                            </td>
                            <td class="v-align-middle"><a
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>
                    
                    
                    <tbody style="border-top: 0px;">
                        <tr>
                            <td class="v-align-middle">
                                &nbsp;
                            </td><td class="v-align-middle">
                                &nbsp;
                            </td>
                            <td class="v-align-middle">
                                <button type="button" class="btn btn-primary btn-save-menu" onclick="saveMenuItem(<?= $reload ?>)">Save</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    
        
        <script type="text/javascript">


        $("#text").click(function() {
            $("#text .fa").toggleClass('fa-angle-left');
            $("#text .fa").toggleClass('fa-angle-down');
            $("#texts").toggle();
        });
        
        $("#script").click(function() {
            $("#script .fa").toggleClass('fa-angle-left');
            $("#script .fa").toggleClass('fa-angle-down');
            $("#scripts").toggle();
            
        });

        $("#customScript").click(function() {
            $("#customScript .fa").toggleClass('fa-angle-left');
            $("#customScript .fa").toggleClass('fa-angle-down');
            $("#customScripts").toggle();

        });


        function select(type, typeid) {
            console.log("save");
            
            var id = 'new';
            var under = <?= (empty($_GET['under'])) ? "''" : (int)$_GET['under'] ?>;
            var sequence = <?= (empty($_GET['sequence'])) ? "''" : (int)$_GET['sequence'] ?>;

            var name = '';
            var title = '';
            var url = '';
            var description = '';
            
            $.post("ajaxs/setMenuItem.ajax.php", {id:id, under:under, sequence:sequence, type:type, typeid:typeid, name:name, title:title, url:url, description:description}, function (result) {
                
                var data = $.parseJSON(result);
                
                $(".content").find("#menuItem<?= $_GET['under'] ?> > ul").first().append('<li id="menuItem'+data.id+'" data-id="'+data.id+'"><i class="fa fa-file-code-o"></i> &nbsp;'+ucfirst(typeid)+'<span class="icons pull-right">&nbsp; &nbsp;<i class="fa fa-trash-o" data-menu-item-id="'+data.id+'"></i></span></li>');
                // window.location.href = "menu";
            });
        }
        </script>
        
        <?php
        
    }
    
    
    
    
    if (@ $_GET['type'] == 'head' || @ $_GET['type'] == 'sub') {
    
        if ($_GET['id'] != 'new') {
            $menuItem = DB::SITE()->query("SELECT * FROM menu WHERE id=:id", array(
                ':id' => $_GET['id']
            ))->single();
        } else {
            $menuItem = array();
        }
        
    ?>
        
        
            <div class="grid simple ">
                <div class="grid-title no-border">
                    <h4>Menu item - <span class="semi-bold"><?= ($_GET['id'] != 'new') ? 'Edit' : 'Add' ?></span></h4>
                </div>
                <div class="grid-body no-border">
                    <div class="form-group">
                        <label class="form-label">Name</label>
                        <div class="controls">
                            <input type="text" id="name" name="name" value="<?= $menuItem['name'] ?>" class="form-control">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="form-label">Url</label>
                        <div class="controls">
                            <input type="text" id="url" name="url" value="<?= $menuItem['url'] ?>" class="form-control">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="form-label">Title</label>
                        <div class="controls">
                            <input type="text" id="title" name="title" value="<?= $menuItem['title'] ?>" class="form-control">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="form-label">Meta Description</label>
                        <div class="controls">
                            <textarea id="description" rows="4" name="description" class="form-control"><?= $menuItem['description'] ?></textarea>
                        </div>
                    </div>
                    
                    <button class="btn btn-primary btn-cons-md" onclick="saveMenuItem(1)">Save</button>
                    <a href="menu"><button class="btn btn-white btn-cons-md">Cancel</button></a>
                    
                    <?php
                    if ($_GET['id'] != 'new') {
                    ?>
                        &nbsp;&nbsp;&nbsp;<span class="muted">|</span>&nbsp;&nbsp;&nbsp;
                    
                        <button class="btn btn-primary btn-cons-md" onclick="saveMenuItem(0)">Save and stay</button>
                    <?php
                    }
                    ?>
                    
                    
                </div>
            </div>
        
<?php
    }


    if (@ $_GET['type'] == 'category') {

        if ($_GET['id'] != 'new') {
            $menuItem = DB::SITE()->query("SELECT * FROM menu WHERE id=:id", array(
                ':id' => $_GET['id']
            ))->single();
        } else {
            $menuItem = array();
        }

        ?>


        <div class="grid simple ">
            <div class="grid-title no-border">
                <h4>Category menu - <span class="semi-bold"><?= ($_GET['id'] != 'new') ? 'Edit' : 'Add' ?></span></h4>
            </div>
            <div class="grid-body no-border">
                <div class="form-group">
                    <label class="form-label">Selection table</label>
                    <div class="controls">
                        <select id="typeid" name="typeid" class="form-control">
                            <option value="categories_master" <?= ($menuItem['typeid'] == 'categories_master') ? 'SELECTED' : '' ?>>Category master</option>
                            <option value="categories" <?= ($menuItem['typeid'] == 'categories') ? 'SELECTED' : '' ?>>Category</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="form-label">Selection</label>
                    <div class="controls">
                        <input type="text" id="name" name="name" value="<?= $menuItem['name'] ?>" class="form-control">
                    </div>
                </div>


                <input type="hidden" id="title" name="description" value="" class="form-control">
                <input type="hidden" id="description" name="description" value="" class="form-control">
                <input type="hidden" id="url" name="url" value="" class="form-control">

                <button class="btn btn-primary btn-cons-md" onclick="saveMenuItem(1)">Save</button>
                <a href="menu"><button class="btn btn-white btn-cons-md">Cancel</button></a>

                <?php
                if ($_GET['id'] != 'new') {
                    ?>
                    &nbsp;&nbsp;&nbsp;<span class="muted">|</span>&nbsp;&nbsp;&nbsp;

                    <button class="btn btn-primary btn-cons-md" onclick="saveMenuItem(0)">Save and stay</button>
                    <?php
                }
                ?>


            </div>
        </div>

        <?php
    }


        if (@ $_GET['type'] == 'dynamic') {

        if ($_GET['id'] != 'new') {
            $menuItem = DB::SITE()->query("SELECT * FROM menu WHERE id=:id", array(
                ':id' => $_GET['id']
            ))->single();
        } else {
            $menuItem = array();
        }

        ?>


        <div class="grid simple ">
            <div class="grid-title no-border">
                <h4>Dynamic menu - <span class="semi-bold"><?= ($_GET['id'] != 'new') ? 'Edit' : 'Add' ?></span></h4>
            </div>
            <div class="grid-body no-border">
                <div class="form-group">
                    <label class="form-label">Select Snippet</label>
                    <div class="controls">
                        <select id="name" name="name" class="form-control">
                            <option value=""></option>
                            <?php
                            $snippets = getSnippets();
                            foreach ($snippets as $key => $snippet) {
                                if(strpos($snippet, 'custom') !== false){
                                    $name = explode('/', $snippet);
                                    $name = end($name);
                                    echo '<option value="'.$snippet.'"  '.(($menuItem['name'] == $snippet) ? 'SELECTED' : '').'>'.$name.'</option>';
                                }
                                
                            }
                            ?>
                        </select>
                    </div>
                </div>

                
                <input type="hidden" id="typeid" name="typeid" value="snippet" class="form-control">
                <input type="hidden" id="title" name="description" value="" class="form-control">
                <input type="hidden" id="description" name="description" value="" class="form-control">
                <input type="hidden" id="url" name="url" value="" class="form-control">

                <button class="btn btn-primary btn-cons-md" onclick="saveMenuItem(1)">Save</button>
                <a href="menu"><button class="btn btn-white btn-cons-md">Cancel</button></a>

                <?php
                if ($_GET['id'] != 'new') {
                    ?>
                    &nbsp;&nbsp;&nbsp;<span class="muted">|</span>&nbsp;&nbsp;&nbsp;

                    <button class="btn btn-primary btn-cons-md" onclick="saveMenuItem(0)">Save and stay</button>
                    <?php
                }
                ?>


            </div>
        </div>

        <?php
    }




}
