<?php
require(__DIR__ . '/../../cmsFirstment.inc.php');

$show = ( (!isset($_POST['show'])) || (intval($_POST['show']) == 1) || $_POST['show'] == 'true' ) ? 1 : 0;
$language = $_POST['language'];

$newLanguage = false;
if (intval($_POST['id']) > 0) {
	$updateID = $_POST['id'];
	$languageSearch = DB::SITE()->query("SELECT `language` FROM menu WHERE id=:id", [':id' => $_POST['id']])->get('language');
	if ($languageSearch != $_POST['language']) {
		$check = DB::SITE()->query("SELECT `id` FROM menu WHERE language_id=:language_id AND language=:language", [':language_id' => $_POST['id'], ':language' => $_POST['language']])->get('id');
		if (!$check) {
			$newLanguage = true;
		}
		else {
			$updateID = $check;
		}
	}
}


$overrule = [];
if ($_POST['id'] == 'new' || $newLanguage) {

	$_POST['under'] = intval($_POST['under']);
	if (!$_POST['under']) {
		$_POST['under'] = DB::SITE()->query("SELECT `id` FROM `menu` WHERE `type`='menu'")->get('id');
	}
	if ($_POST['under'] == 1) {
		$_POST['type'] = 'item';
	}

	$_POST['sequence'] = 1 + DB::SITE()->query("SELECT MAX(`sequence`) AS `amount` FROM `menu` WHERE `under`=:under", [':under' => $_POST['under']])->get('amount');
	
	$languageParent = (intval($_POST['id']) > 0) ? $_POST['id'] : 0;

	$template = '';
	if ($languageParent) {
		$template = DB::SITE()->query("SELECT `template` FROM `menu` WHERE `id`=:id", [':id' => $languageParent])->get('template');
	}

	$query_addMenuItem = DB::SITE()->query("
		INSERT INTO menu (under, sequence, type, typeid, name, title, url, description, show, language, language_id, template) 
		VALUES (:under, :sequence, :type, :typeid, :name, :title, :url, :description, :show, :language, :language_id, :template)", array(
		":under" => $_POST['under'],
		":sequence" => $_POST['sequence'],
		":type" => $_POST['type'],
		":typeid" => $_POST['typeid'],
		":name" => $_POST['name'],
		":title" => $_POST['title'],
		":url" => $_POST['url'],
		":description" => $_POST['description'],
		":show" => $show,
		":language" => $_POST['language'],
		":language_id" => $languageParent,
		":template" => $template
	))->execute();

	$id = DB::SITE()->lastInsertId();

	if($_POST['type'] == 'text' && $_POST['typeid']){
		$overrule['name'] = DB::SITE()->query("SELECT name FROM " . $_SESSION['website']['code'] . "_texts WHERE id=:typeid")->bind(':typeid', $_POST['typeid'])->get('name');
	}
	
} else {
	$query_setMenuItem = DB::SITE()->query("UPDATE menu SET name=:name, title=:title, url=:url, description=:description, show=:show WHERE id=:id", array(
		":id" => $updateID,
		":name" => $_POST['name'],
		":title" => $_POST['title'],
		":url" => $_POST['url'],
		":description" => $_POST['description'],
		":show" => $show
	))->execute();

	$id = $_POST['id'];
}


$data = DB::SITE()->query("SELECT * FROM menu WHERE id=:id")->bind(':id', $id)->single();
foreach ($overrule as $key => $value) {
	$data[$key] = $value;
}

echo json_encode($data);
