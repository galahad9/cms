<?php
require(__DIR__ . '/../../cmsFirstment.inc.php');

$menuSequence = $_REQUEST['menu'];

$counter = 0;
foreach ($menuSequence as $menuId => $subMenu) {

	$query_setMenu = DB::SITE()->query("UPDATE menu SET sequence=:sequence, under=:under WHERE id=:id", array(
		":id" => $menuId,
		":sequence" => $counter,
        ":under" => 1
	))->execute();
	
	$counter++;

    if (is_array($subMenu)) {
        $subCounter = 0;
        foreach ($subMenu as $subMenuId => $subSubmenu) {

            if (!is_array($subSubmenu)) {
                $subMenuId = $subSubmenu;
            }


            $query_setSubMenu = DB::SITE()->query("UPDATE menu SET sequence=:sequence, under=:under WHERE id=:id", array(
                ":id" => $subMenuId,
                ":sequence" => $subCounter,
                ":under" => $menuId
            ))->execute();
            $subCounter++;


            if (is_array($subSubmenu)) {

                foreach ($subSubmenu as $subSubCounter => $subSubMenuId) {

                    $query_setSubSubMenu = DB::SITE()->query("UPDATE menu SET sequence=:sequence, under=:under WHERE id=:id", array(
                        ":id" => $subSubMenuId,
                        ":sequence" => $subSubCounter,
                        ":under" => $subMenuId
                    ))->execute();


                }
            }

        }
	}

}
echo 1;
