<?php
require(__DIR__ . '/../../cmsFirstment.inc.php');

deleteTree($_POST['id']);

function deleteTree($menuItemId) {
	$menuItems = DB::SITE()->query("SELECT id FROM menu WHERE under=:id AND id <> :id", array(
		":id" => $menuItemId
	))->fetch();
	
	foreach ($menuItems as $menuItem) {
		deleteTree($menuItem['id']);
	}

	DB::SITE()->query("DELETE FROM menu WHERE id=:id", array(
		":id" => $menuItemId
	))->execute(true);
	
}

