<?php
require(__DIR__ . '/../../cmsFirstment.inc.php');

if (!empty($_POST['language'])) {
	$availableLanguages = getCMSLanguages();
	if (in_array($_POST['language'], $availableLanguages)) {
		setcookie('language', $_POST['language'], time()+(3600*24*365));
		$_SESSION['user']['language'] = $_POST['language'];
		$updateUserDefaultLanguage = DB::CMS()->query("UPDATE users SET language=:language WHERE id=:id", array(
				':id' => $_SESSION['user']['id'],
				':language' => $_POST['language']
			))->execute();
		echo 1;
	}
}