<?php
require(__DIR__ . '/../../cmsFirstment.inc.php');

//If the id is empty we show the users profile page (should work for non-admin users)
if ( (!$_SESSION['user']['admin']) && (isset($_POST['id'])) ) {
    header('Location: ' . CMS_PUBLIC_URL);
    exit;
}

$json = array();
$json['success'] = 1;

if (!empty($_POST['password'])){
    $password = sha1($_POST['password'] . ':SupRrCM5:' . strtolower($_POST['emailaddress']));
    if ($_POST['id'] != 'new') {
        $password = ", password='" . $password ."' ";
    }
} else{
    $password = "";
}

if ($_POST['id'] == 'new') { //New user

    $error = "";
    $required = array(
        'emailaddress',
        'password',
        'firstname',
    );

    foreach($required as $field){
        if (empty($_POST[$field])){
            $error = "Please fill in all required fields";
        }
    }

    if (empty($error)){
        if (!filter_var($_POST['emailaddress'], FILTER_VALIDATE_EMAIL)){
            $error = "Enter a valid email address";
        } else {
            $test = DB::CMS()->query("INSERT INTO users (emailaddress, firstname, lastname, role, language, password, active, admin, created) VALUES (:emailaddress, :firstname, :lastname, :role, :language, :password, :active, :admin, :created)", array(
                ":emailaddress" => $_POST['emailaddress'],
                ":firstname" => $_POST['firstname'],
                ":lastname" => $_POST['lastname'],
                ":role" => $_POST['role'],
                ":language" => $_POST['language'],
                ":password" => $password,
                ":active" => $_POST['active'],
                ":admin" => $_POST['admin'],
                ":created" => time()
            ))->execute();
			
            $json['success'] = 1;
        }
    } else {
        $json['success'] = 0;
        $json['error'] = $error;
    }

} else { //Edit user

    $id = (isset($_POST['id'])) ? (intval($_POST['id'])) : $_SESSION['user']['id'];

    if (empty($password)) {
        $email = DB::CMS()->query('SELECT `emailaddress` FROM users WHERE id=:id', [':id' => $id])->get('emailaddress');
        if ($email != $_POST['emailaddress']) {
            $error = 'Please enter your password to change your emailaddress';
        }
    }

    if (empty($error)) {
        if (!isset($_POST['id'])) { //Edit from profile page
            DB::CMS()->query("UPDATE users SET emailaddress=:emailaddress, firstname=:firstname, lastname=:lastname, language=:language" . $password . " WHERE id=:id", array(
                ":id" => $id,
                ":emailaddress" => $_POST['emailaddress'],
                ":firstname" => $_POST['firstname'],
                ":lastname" => $_POST['lastname'],
                ":language" => $_POST['language'],
            ))->execute();

            $_SESSION['user'] = DB::CMS()->query('SELECT * FROM users WHERE id=:id', [':id' => $id])->single();

            if (isset($_COOKIE['cms-session'])) {
                $timeOneYear = strtotime('+365 days');
                $hash = hash('sha512', uniqid().json_encode($_SESSION['user']));
                $expiredate = date('Y-m-d H:i:s', $timeOneYear);
                DB::CMS()->query("UPDATE users_sessions SET hash=:hash WHERE user_id=:user_id", [
                    ':user_id' => $_SESSION['user']['id'],
                    ':hash' => $hash,
                ])->execute(true);
                setcookie('cms-session', json_encode(['id' => $_SESSION['user']['id'], 'hash' => $hash]), $timeOneYear, '/cms/');
            }

        } else { //Edit from users page
            DB::CMS()->query("UPDATE users SET emailaddress=:emailaddress, firstname=:firstname, lastname=:lastname, role=:role, language=:language, active=:active, admin=:admin " . $password . " WHERE id=:id", array(
                ":id" => $id,
                ":emailaddress" => $_POST['emailaddress'],
                ":firstname" => $_POST['firstname'],
                ":lastname" => $_POST['lastname'],
                ":role" => $_POST['role'],
                ":language" => $_POST['language'],
                ":active" => $_POST['active'],
                ":admin" => $_POST['admin']
            ))->execute();
        }
    }
    else {
        $json['success'] = 0;
        $json['error'] = $error;
    }

}
echo json_encode($json);
