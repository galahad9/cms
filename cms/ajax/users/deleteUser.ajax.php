<?php
require(__DIR__ . '/../../cmsFirstment.inc.php');

if (!$_SESSION['user']['admin']) {
    header('Location: ' . CMS_PUBLIC_URL);
    exit;
}

$id = intval($_POST['id']);

try{
    $message = "1";
    DB::CMS()->query("DELETE FROM users WHERE id=:id", array(
        ":id" => $id
    ))->execute();
}
catch(Exception $e){
    $message = "PDO ERROR";
}

echo $message;

if ($_POST['id'] == $_SESSION['user']['id']) {
	foreach ($_SESSION as $index => $value) {
		unset($_SESSION[$index]);	
	}
}

