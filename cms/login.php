<?php
require(__DIR__ . '/cmsFirstment.inc.php');

$timeOneYear = strtotime('+365 days');

if ($_POST['action'] == 'login') {
	$user = DB::CMS()->query("SELECT * FROM users WHERE emailaddress=:emailaddress AND password=:password AND active=1", array(
		':emailaddress' => strtolower($_POST['emailaddress']),
		':password' => sha1($_POST['password'] . ':SupRrCM5:' . strtolower($_POST['emailaddress']))
	))->single();

	if (!empty($user)) {	
		$_SESSION['user'] = $user;
		setcookie('language', $_SESSION['user']['language'], $timeOneYear);

		if (isset($_POST['remember'])) {
			$hash = hash('sha512', uniqid().json_encode($user));
			$expiredate = date('Y-m-d H:i:s', $timeOneYear);
			DB::CMS()->query("INSERT INTO users_sessions (user_id, hash, expiredate, login_ip, login_agent) VALUES (:user_id, :hash, :expiredate, :login_ip, :login_agent)", [
				':user_id' => $user['id'],
				':hash' => $hash,
				':expiredate' => $expiredate,
				':login_ip' => $_SERVER['REMOTE_ADDR'],
				':login_agent' => $_SERVER['HTTP_USER_AGENT'],
			])->execute();
			setcookie('cms-session', json_encode(['id' => $user['id'], 'hash' => $hash]), $timeOneYear, '/cms/');
		}

		redirect(CMS_PUBLIC_PATH);
	} else {
		$error = 'No valid login credentials';	
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?= $cmsSettings['Name'] ?> | Sign In</title>
	<link rel="icon" type="image/png" href="<?= CMS_PUBLIC_PATH ?>admin-assets/img/<?= $cmsSettings['Image Favicon'] ?>" />
	<!-- ================== GOOGLE FONTS ==================-->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
	<!-- ======================= GLOBAL VENDOR STYLES ========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/vendor/bootstrap.css">
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/metismenu/dist/metisMenu.css">
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/switchery-npm/index.css">
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
	<!-- ======================= LINE AWESOME ICONS ===========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/icons/line-awesome.min.css">
	<!-- ======================= DRIP ICONS ===================================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/icons/dripicons.min.css">
	<!-- ======================= MATERIAL DESIGN ICONIC FONTS =================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/icons/material-design-iconic-font.min.css">
	<!-- ======================= GLOBAL COMMON STYLES ============================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/common/main.bundle.css">
	<!-- ======================= LAYOUT STYLES ===========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/layouts/horizontal/core/main.css">
	<!-- ======================= MENU TYPE ===========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/layouts/horizontal/menu-type/auto-hide.css">
	<!-- ======================= THEME COLOR STYLES ===========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/layouts/horizontal/themes/theme-j.css">
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/custom.css">
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/jquery/dist/jquery.min.js"></script>
</head>

<body class="layout-horizontal menu-auto-hide">
	<div class="container">
		<form class="sign-in-form" action="" method="POST">
			<input name="action" type="hidden" value="login">
			<div class="card">
				<div class="card-body">
					<a href="login.php" class="brand text-center d-block m-b-20">
						<?php
							echo '<img class="login-logo" src="' . CMS_PUBLIC_PATH . 'admin-assets/img/'.$cmsSettings['Image Logo Color'].'" />';	
						?>
					</a>
					<h5 class="sign-in-heading text-center m-b-20"><?= $trans['Sign in to your account'] ?></h5>

					<?php
					if($error){
						echo '<div class="alert alert-danger">'.$error.'</div>';
					}
					?>
					<div class="form-group">
						<label for="inputEmail" class="sr-only"><?= $trans['Email address'] ?></label>
						<input type="email" name="emailaddress" id="inputEmail" class="form-control" placeholder="<?= $trans['Email address'] ?>" required="" autofocus>
					</div>

					<div class="form-group">
						<label for="inputPassword" class="sr-only"><?= $trans['Password'] ?></label>
						<input type="password" name="password" id="inputPassword" class="form-control" placeholder="<?= $trans['Password'] ?>" required="">
					</div>
					<div class="checkbox m-b-10 m-t-20">
						<div class="custom-control custom-checkbox checkbox-primary form-check">
							<input type="checkbox" class="custom-control-input" id="remember" name="remember" checked="">
							<label class="custom-control-label" for="remember">	<?= $trans['Remember me'] ?></label>
						</div>
						<!-- <a href="auth.forgot-password.html" class="float-right"><?= $trans['Forgot Password?'] ?></a> -->
					</div>
					<button class="btn btn-primary btn-rounded btn-floating btn-lg btn-block" type="submit"><?= $trans['Sign In'] ?></button>
				</div>

			</div>
			
			<center>
				<?php
					$languages = getCMSLanguages();
					foreach ($languages as $language) {
						echo ' &nbsp;<a href="login.php?lang=' . $language . '"><img src="translations/flags/' . strtoupper($language) . '.png"></a>';
					}	
				?>
			</center>
		</form>
	</div>

	
	
	<!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
	<script src="admin-assets/vendor/modernizr/modernizr.custom.js"></script>
	<script src="admin-assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="admin-assets/vendor/js-storage/js.storage.js"></script>
	<script src="admin-assets/vendor/js-cookie/src/js.cookie.js"></script>
	<script src="admin-assets/vendor/pace/pace.js"></script>
	<script src="admin-assets/vendor/metismenu/dist/metisMenu.js"></script>
	<script src="admin-assets/vendor/switchery-npm/index.js"></script>
	<script src="admin-assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	<!-- ================== GLOBAL APP SCRIPTS ==================-->
	<script src="admin-assets/js/global/app.js"></script>

</body>

</html>
