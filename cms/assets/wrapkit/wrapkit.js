// Owl Carousel jQuery 3 fix
$.fn.andSelf = function() {
  return this.addBack.apply(this, arguments);
}

var TxtType = function(el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtType.prototype.tick = function() {
    var i = this.loopNum % this.toRotate.length;
    var fullTxt = this.toRotate[i];

    if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

    var that = this;
    var delta = 200 - Math.random() * 100;

    if (this.isDeleting) { delta /= 2; }

    if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
    }

    setTimeout(function() {
    that.tick();
    }, delta);
};


$(function () {
    "use strict";
    // ============================================================== 
    //This is for preloader
    // ============================================================== 
    $(function () {
        if($(".preloader").length){
            $(".preloader").fadeOut();
        }
    });
    // ============================================================== 
    // For page-wrapper height
    // ============================================================== 
    var set = function () {
        var topOffset = 390;        
        var height = ((window.innerHeight > 0) ? window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $(".page-wrapper").css("min-height", (height) + "px");
        }
       
    };
    $(window).ready(set);
    $(window).on("resize", set);

    // ============================================================== 
    //Tooltip
    // ============================================================== 
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    // ============================================================== 
    //Popover
    // ============================================================== 
    $(function () {
        $('[data-toggle="popover"]').popover()
    })
    // ============================================================== 
    // For mega menu
    // ============================================================== 
    jQuery(document).on('click', '.mega-dropdown', function (e) {
        e.stopPropagation()
    });
     jQuery(document).on('click', '.navbar-nav > .dropdown', function(e) {
         e.stopPropagation();
    });
    $(".dropdown-submenu").click(function(){
              $(".dropdown-submenu > .dropdown-menu").toggleClass("show");                     
    });
    // ============================================================== 
    // Resize all elements
    // ============================================================== 
    $("body").trigger("resize"); 
    // ============================================================== 
    //Fix header while scroll
    // ============================================================== 
     var wind = $(window);
         wind.on("load", function() {
            var bodyScroll = wind.scrollTop(),
                navbar = $(".topbar"),
                body = $("body");
            if (bodyScroll > 100) {
                navbar.addClass("fixed-header animated slideInDown");
                body.addClass('scrolled');
            } else {
                navbar.removeClass("fixed-header animated slideInDown")
                body.removeClass('scrolled');
            }
        });
        $(window).scroll(function () {
            if ($(window).scrollTop() >= 200) {
                $('.topbar').addClass('fixed-header animated slideInDown');
                $('body').addClass('scrolled');
                $('.bt-top').addClass('visible');
            } else {
                $('.topbar').removeClass('fixed-header animated slideInDown');
                $('.bt-top').removeClass('visible');
                $('body').removeClass('scrolled');
            }
        });
    // ============================================================== 
    // Animation initialized
    // ============================================================== 
    AOS.init();
    // ============================================================== 
    // Back to top
    // ============================================================== 
    $('.bt-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
    // ============================================================== 
    // Modal video stop on close
    // ============================================================== 
    // $('.modal').on('hidden.bs.modal', function () {
        // callPlayer('yt-player', 'stopVideo');
    // });
    // ============================================================== 
    // This is for the remove target blank from href
    // ============================================================== 
    $('a[target="_blank"]').removeAttr('target');
    

    $('#slider1').bsTouchSlider();
    $(".carousel .carousel-inner").swipe({
        swipeLeft: function (event, direction, distance, duration, fingerCount) {
            this.parent().carousel('next');
        }
        , swipeRight: function () {
            this.parent().carousel('prev');
        }
        , threshold: 0
    });

    $('.tgl-cl').on('click', function() {
        $('body .h17-main-nav').toggleClass("show");
    });



   
    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid transparent}";
        document.body.appendChild(css);
    };



    /*******************************/
    // this is for the testimonial 1
    /*******************************/
    $('.testi1').owlCarousel({
            loop: true
            , margin: 30
            , nav: false
            , dots: true
            , autoplay:true
            , responsiveClass: true
            , responsive: {
                0: {
                    items: 1
                    , nav: false
                }
                , 1170: {
                    items: 2
                }
            }
        })
    /*******************************/
    // this is for the testimonial 2 
    /*******************************/
    $('.testi2').owlCarousel({
        loop: true
        , margin: 20
        , nav: false
        , dots: true
        , autoplay:true
        , responsiveClass: true
        , responsive: {
            0: {
                items: 1
                , nav: false
            }
            , 1170: {
                items: 1
            }
        }
    })
    $(function () {
        // 1) ASSIGN EACH 'DOT' A NUMBER
        dotcount = 1;
        $('.testi2 .owl-dot').each(function () {
            $(this).addClass('dotnumber' + dotcount);
            $(this).attr('data-info', dotcount);
            dotcount = dotcount + 1;
        });
        // 2) ASSIGN EACH 'SLIDE' A NUMBER
        slidecount = 1;
        $('.testi2 .owl-item').not('.cloned').each(function () {
            $(this).addClass('slidenumber' + slidecount);
            slidecount = slidecount + 1;
        });
        $('.testi2 .owl-dot').each(function () {
            grab = jQuery(this).data('info');
            slidegrab = $('.slidenumber' + grab + ' img').attr('src');
            console.log(slidegrab);
            $(this).css("background-image", "url(" + slidegrab + ")");
        });
        // THIS FINAL BIT CAN BE REMOVED AND OVERRIDEN WITH YOUR OWN CSS OR FUNCTION, I JUST HAVE IT
        // TO MAKE IT ALL NEAT 
        
    });

    /*******************************/
    // this is for the testimonial 3
    /*******************************/
    $('.testi3').owlCarousel({
            loop: true
            , margin: 30
            , nav: false
            , dots: true
            , autoplay:true
            , responsiveClass: true
            , responsive: {
                0: {
                    items: 1
                    , nav: false
                }
                , 1170: {
                    items: 3
                }
            }
        })
    /*******************************/
    // this is for the testimonial 4
    /*******************************/
    $('.testi4').owlCarousel({
            loop: true
            , margin: 30
            , nav: false
            , dots: true
            , autoplay:true
            , responsiveClass: true
            , responsive: {
                0: {
                    items: 1
                    
                }
                , 1650: {
                    items: 1
                }
            }
        })
    /*******************************/
    // this is for the testimonial 5
    /*******************************/
    $('.testi5').owlCarousel({
            loop: true
            , margin: 30
            , nav: false
            , navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
            , dots: true
            , autoplay:true
            , center:true
            , responsiveClass: true
            , responsive: {
                0: {
                    items: 1
                    
                }
                , 1170: {
                    items: 3
                }
            }
        })

    /*******************************/
    // this is for the testimonial 5
    /*******************************/
    $('.testi7').owlCarousel({
            loop: true
            , margin: 30
            , nav: true
            , navText: ['<i class="ti-arrow-circle-left"></i>','<i class="ti-arrow-circle-right"></i>']
            , dots: false
            , autoplay:true
            , center:true
            , responsiveClass: true
            , responsive: {
                0: {
                    items: 1
                    
                }
                , 1650: {
                    items: 1
                }
            }
        })
    /*******************************/
    // this is for the testimonial 8 
    /*******************************/
    $('.testi8').owlCarousel({
        loop: true
        , margin: 20
        , nav: false
        , dots: true
        , autoplay:true
        , responsiveClass: true
        , responsive: {
            0: {
                items: 1
                , nav: false
            }
            , 1650: {
                items: 1
            }
        }
    })
    $(function () {
        // 1) ASSIGN EACH 'DOT' A NUMBER
        dotcount = 1;
        $('.testi8 .owl-dot').each(function () {
            $(this).addClass('dotnumber' + dotcount);
            $(this).attr('data-info', dotcount);
            dotcount = dotcount + 1;
        });
        // 2) ASSIGN EACH 'SLIDE' A NUMBER
        slidecount = 1;
        $('.testi8 .owl-item').not('.cloned').each(function () {
            $(this).addClass('slidenumber' + slidecount);
            slidecount = slidecount + 1;
        });
        $('.testi8 .owl-dot').each(function () {
            grab = jQuery(this).data('info');
            slidegrab = $('.slidenumber' + grab + ' img').attr('src');
            console.log(slidegrab);
            $(this).css("background-image", "url(" + slidegrab + ")");
        });
        // THIS FINAL BIT CAN BE REMOVED AND OVERRIDEN WITH YOUR OWN CSS OR FUNCTION, I JUST HAVE IT
        // TO MAKE IT ALL NEAT 
        
    });


    /*******************************/
    // this is for the testimonial 9
    /*******************************/
    $('.testi9').owlCarousel({
            loop: true
            , margin: 30
            , nav: false
            , dots: true
            , autoplay:true
            , responsiveClass: true
            , responsive: {
                0: {
                    items: 1
                    
                }
                , 1650: {
                    items: 1
                }
            }
        })
    /*******************************/
    // this is for the testimonial 10
    /*******************************/
    $('.testi10').owlCarousel({
            loop: true
            , margin: 30
            , nav: false
            , dots: false
            , autoplay:true
            , responsiveClass: true
            , responsive: {
                0: {
                    items: 1
                    
                }
                , 1650: {
                    items: 1
                }
            }
        })
});


