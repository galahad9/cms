<?php
require(__DIR__.'/../../../../firstment.inc.php');
header('Content-Type: application/javascript');

$menu = DB::SITE()->query("SELECT * FROM menu WHERE id=:id")->bind(':id', $_GET['id'])->single();
if(! defined('LANGUAGE')){
    define('LANGUAGE', $menu['language']);
}
if(! defined('BASE_URL')){
    define('BASE_URL', $settings['Website Url']);
}
// $files = glob_recursive(__DIR__.'/wrapkit/*');

$i = 0;
foreach (glob(__DIR__.'/wrapkit/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir){
    if(is_dir($dir)){
        $files = glob($dir.'/*.php', 0);
        foreach ($files as $key => $value) {
            require($value);
        }

    }  
};

?>
