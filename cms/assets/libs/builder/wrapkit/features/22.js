var name = 'Feature #22';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature22">
    <div class="container">
        <!-- Row -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center"> <span class="label label-success label-rounded">Feature 22</span>
                <h2 class="title">Create beautiful Websites in Record Time</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row -->
        <div class="row wrap-feature-22">
            <!-- Column -->
            <div class="col-lg-6" data-aos="flip-up" data-aos-duration="1200"> <img src="/cms/assets/wrapkit/images/features/feature22/img1.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" /> </div>
            <!-- Column -->
            <div class="col-lg-6">
                <div class="text-box"> <small class="text-info font-medium">Make your site in No-Time</small>
                    <h3 class="font-light">WrapKit helps you to <span class="text-megna">build your project</span> in record time with fun making it.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                    <p>Quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquis would be good to have.</p> <a class="btn btn-success-gradiant btn-md btn-arrow m-t-20" data-toggle="collapse" href="#f22"><span>View Feature22 code <i class="ti-arrow-right"></i></span></a> </div>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>`
});
