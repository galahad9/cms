var name = 'Feature #25';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer bg-light feature24">
    <div class="container">
        <!-- Row -->
        <div class="row justify-content-center">
            <div class="col-md-12 text-center m-b-40"><img src="/cms/assets/wrapkit/images/features/feature25/img.png" class="iimg-fluid" /></div>
            <div class="col-md-7 text-center"> <span class="label label-success label-rounded">Feature 24</span>
                <h2 class="title">Services you can Relay Upon</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
            <div class="col-md-12 m-t-20 text-center">
                <a class="btn btn-success-gradiant btn-md btn-arrow" data-toggle="collapse" href="#f25"><span>View Feature25 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
        <!-- Row -->
    </div>
</div>`
});
