var name = 'Feature #21';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature21 wrap-feature21-box" style="background-image:url(/cms/assets/wrapkit/images/features/feature21/img1.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row text-white">
            <!-- Column -->
            <div class="col-md-5 both-space">
                <div class="" data-aos="fade-right" data-aos-duration="1200"> <span class="label label-info label-rounded">Feature 21</span>
                    <h2 class="text-white m-t-20 m-b-30">Build your Project in Record Time with WrapKit</h2>
                    <p class="op-8">You can relay on our amazing features list and also our customer services will be great experience.</p> <a class="btn btn-info-gradiant btn-md btn-arrow m-t-20" data-toggle="collapse" href="#f21"><span>View Feature21 code <i class="ti-arrow-right"></i></span></a> </div>
            </div>
        </div>
    </div>
</div>`
});
