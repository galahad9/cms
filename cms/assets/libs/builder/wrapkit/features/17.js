var name = 'Feature #17';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature17">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-success label-rounded">Feature 17</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="card-group m-t-30 wrap-feature-17-box">
            <!-- card  -->
            <div class="card">
                <div class="card-body text-center">
                    <div class="p-20">
                        <div class="icon-space"><i class="display-5 text-info-gradiant icon-Target"></i></div>
                        <h4 class="font-medium">Retargeting Market</h4>
                        <p>You can relay on our amazing features list and also our customer services will be great experience worth taking.</p>
                    </div>
                </div>
            </div>
            <!-- card  -->
            <div class="card">
                <img src="/cms/assets/wrapkit/images/features/feature17/img1.jpg" class="" alt="wrapkit" />
            </div>
            <!-- card  -->
            <div class="card">
                <div class="card-body text-center">
                    <div class="p-20">
                        <div class="icon-space"><i class="display-5 text-info-gradiant icon-Mouse-3"></i></div>
                        <h4 class="font-medium">Instant Solutions</h4>
                        <p>You can relay on our amazing features list and also our customer services will be great experience worth taking.</p>
                    </div>
                </div>
            </div>
            <!-- card  -->
        </div>
        <div class="card-group wrap-feature-17-box">
            <!-- card  -->
            <div class="card">
                <img src="/cms/assets/wrapkit/images/features/feature17/img2.jpg" class="" alt="wrapkit" />
            </div>
            <!-- card  -->
            <div class="card">
                <div class="card-body text-center">
                    <div class="p-20">
                        <div class="icon-space"><i class="display-5 text-info-gradiant icon-Car-Wheel"></i></div>
                        <h4 class="font-medium">Fruitful Results</h4>
                        <p>You can relay on our amazing features list and also our customer services will be great experience worth taking.</p>
                    </div>
                </div>
            </div>
            <!-- card  -->
            <!-- card  -->
            <div class="card">
                <img src="/cms/assets/wrapkit/images/features/feature17/img3.jpg" class="" alt="wrapkit" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 m-t-40 text-center">
                <a class="btn btn-success-gradiant btn-md btn-arrow" data-toggle="collapse" href="#f17"><span>View Feature17 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</div>`
});
