var name = 'Feature #26';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature26">
    <div class="container">
        <div class="row wrap-feature-26">
            <div class="col-md-7 align-self-center">
                <div class="max-box"> <span class="label label-info label-rounded">Feature 26</span>
                    <h2 class="title m-t-20">Thousands of Great inbuld Elements to make your life easier </h2>
                    <p class="m-t-30">You can relay on our amazing features list and also our customer services will be greatexperience for you without doubt and in no-time and with great quality for design.</p>
                    <p>We guarantee you with our WrapKit that you will make your site in record time and it will be fun to work on.</p>
                    <a href="javascript:void(0)" class="linking">Learn More <i class="ti-arrow-right"></i></a>
                </div>
            </div>
            <div class="col-md-5 col-md-5"> <img src="/cms/assets/wrapkit/images/features/feature26/img1.jpg" class="img-responsive" /> </div>
        </div>
        <div class="row wrap-feature-26 m-t-40 p-t-30">
            <div class="col-md-6"> <img src="/cms/assets/wrapkit/images/features/feature26/img2.jpg" class="img-responsive" /> </div>
            <div class="col-md-6 align-self-center">
                <span class="label label-info label-rounded">Feature 26</span>
                <h2 class="title m-t-20">Get the best designed elements to make your site look modern</h2>
                <p class="m-t-30">You can relay on our amazing features list and also our customer services will be greatexperience for you without doubt and in no-time and with great quality for design.</p>
                <p>We guarantee you with our WrapKit that you will make your site in record time and it will be fun to work on.</p>
                <a class="linking" data-toggle="collapse" href="#f26">View Feature26 code  <i class="ti-arrow-right"></i></a>
            </div>
        </div>
    </div>
</div>`
});
