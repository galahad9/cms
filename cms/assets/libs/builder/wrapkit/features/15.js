var name = 'Feature #15';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light spacer feature15">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-success label-rounded">Feature 15</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="row m-t-40">
            <!-- Column -->
            <div class="col-lg-6 wrap-feature15-box">
                <div class="row">
                    <div class="col-md-12 m-b-30" data-aos="fade-down" data-aos-duration="1000">
                        <div class="d-flex no-block">
                            <div class="no-shrink"><span class="icon-round bg-white display-5 text-success "><i class="icon-Target"></i></span></div>
                            <div class="p-20">
                                <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Retargeting Market <i class="ti-arrow-right"></i></a></h5>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 m-b-30" data-aos="fade-down" data-aos-duration="2000">
                        <div class="d-flex no-block">
                            <div class="no-shrink"><span class="icon-round bg-white display-5 text-success "><i class="icon-Car-Wheel"></i></span></div>
                            <div class="p-20">
                                <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Fruitful Results <i class="ti-arrow-right"></i></a></h5>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 m-b-30" data-aos="fade-down" data-aos-duration="3000">
                        <div class="d-flex no-block">
                            <div class="no-shrink"><span class="icon-round bg-white display-5 text-success "><i class="icon-Mouse-3"></i></span></div>
                            <div class="p-20">
                                <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Instant Solutions <i class="ti-arrow-right"></i></a></h5>
                                <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-6 wrap-feature15-box" data-aos="flip-left" data-aos-duration="1200">
                <img src="/cms/assets/wrapkit/images/features/feature15/img1.png" class="img-responsive" alt="wrapkit" />
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-12 m-t-20 text-center">
                <a class="btn btn-success-gradiant btn-md btn-arrow" data-toggle="collapse" href="#f15"><span>View Feature15 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</div>`
});
