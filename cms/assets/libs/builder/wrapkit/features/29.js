var name = 'Feature #29';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer bg-success-gradiant feature29 p-b-0">
    <div class="container wrap-feature-29">
        <!-- Row -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center"> <span class="label label-inverse label-rounded">Feature 29</span>
                <h2 class="title">Seo Techniques Off / On Page</h2>
                <h6 class="subtitle">Effective search engine optimisation requires experience and expertise.</h6>
                <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" class="btn btn-outline-light btn-rounded m-t-20">Watch video <span class="btn-devider"><i class="fa fa-play"></i></span></a>
                <div class="modal fade" id="exampleModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Watch video</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                            </div>
                            <div class="modal-body" id="yt-player">
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/DDwbjWCgxVM?" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->
        <div class="row wrap-feature-29 text-white">
            <!-- Column -->
            <div class="col-lg-4 text-center">
                <i class="icon-Laptop-2 display-5"></i>
                <h5 class="title">Off Page Seo</h5>
                <p>Off-Page SEO includes not only link building but also social media promotion, bookmarking, etc. </p>
            </div>
            <!-- Column -->
            <div class="col-lg-4 text-center" data-aos="fade-up" data-aos-duration="1800" data-aos-easing="linear">
                <img src="/cms/assets/wrapkit/images/features/feature29/img1.png" class="img-responsive" alt="wrappixel" />
            </div>
            <!-- Column -->
            <div class="col-lg-4 text-center m-b-30">
                <i class="icon-Favorite-Window display-5"></i>
                <h5 class="title">On Page Seo</h5>
                <p>Off-Page SEO includes not only link building but also social media promotion, bookmarking, etc. </p>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
    </div>
</div>`
});
