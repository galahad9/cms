var name = 'Feature #14';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature14">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <span class="label label-info label-rounded">Feature 14</span>
                <h2 class="m-t-20 m-b-30">Awesome with Extra Ordinary Flexibility</h2>
                <p>You can relay on our amazing features list and also our customer services will be great experience for our users</p>
                <p>You will surely fall in love with ready touse bootstrap ui kit framework.</p>
                <a class="btn btn-info-gradiant btn-md btn-arrow m-t-20 m-b-30" data-toggle="collapse" href="#f14"><span>View Feature14 code <i class="ti-arrow-right"></i></span></a>
            </div>
            <div class="col-lg-4 wrap-feature14-box">
                <div class="" data-aos="flip-left" data-aos-duration="1200">
                    <img class="rounded img-responsive" src="/cms/assets/wrapkit/images/features/feature2/market.jpg" alt="wrappixel kit" />
                    <div class="m-t-30">
                        <h5 class="font-medium">Retargeting Market</h5>
                        <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        <a href="javascript:void(0)" class="linking">Learn More <i class="ti-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-4 wrap-feature14-box">
                <div class="" data-aos="flip-up" data-aos-duration="1200">
                    <img class="rounded img-responsive" src="/cms/assets/wrapkit/images/features/feature2/fruit.jpg" alt="wrappixel kit" />
                    <div class="m-t-30">
                        <h5 class="font-medium">Fruitful Results</h5>
                        <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        <a href="javascript:void(0)" class="linking">Learn More <i class="ti-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>`
});
