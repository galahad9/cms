var name = 'Feature #3';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light spacer feature3">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-success label-rounded">Feature 3</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="row m-t-40">
            <!-- Column -->
            <div class="col-md-6 wrap-feature3-box">
                <div class="card card-shadow" data-aos="fade-right" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space align-self-center"><i class="icon-Double-Circle display-2 text-success-gradiant"></i></div>
                        <div class="align-self-center">
                            <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Instant Solutions <i class="ti-arrow-right"></i></a></h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-6 wrap-feature3-box">
                <div class="card card-shadow" data-aos="fade-left" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space align-self-center"><i class="icon-Stopwatch display-2 text-success-gradiant"></i></div>
                        <div class="align-self-center">
                            <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Powerful Techniques <i class="ti-arrow-right"></i></a></h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-6 wrap-feature3-box">
                <div class="card card-shadow" data-aos="fade-right" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space align-self-center"><i class="icon-Thumbs-UpSmiley display-2 text-success-gradiant"></i></div>
                        <div class="align-self-center">
                            <h5 class="font-medium"><a href="javascript:void(0)" class="linking">100% Satisfaction <i class="ti-arrow-right"></i></a></h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-6 wrap-feature3-box">
                <div class="card card-shadow" data-aos="fade-left" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space align-self-center"><i class="icon-Window-2 display-2 text-success-gradiant"></i></div>
                        <div class="align-self-center">
                            <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Retargeting Market <i class="ti-arrow-right"></i></a></h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-12 m-t-20 text-center">
                <a class="btn btn-success-gradiant btn-md btn-arrow"><span>View Feature3 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</div>`
});
