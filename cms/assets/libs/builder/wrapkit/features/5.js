var name = 'Feature #5';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light spacer feature5">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-success label-rounded">Feature 5</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="row m-t-40">
            <!-- Column -->
            <div class="col-md-4 wrap-feature5-box">
                <div class="card card-shadow" data-aos="fade-right" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space"><i class="text-success-gradiant icon-Stopwatch"></i></div>
                        <div class="">
                            <h6 class="font-medium"><a href="javascript:void(0)" class="linking">Instant Solutions</a></h6>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-4 wrap-feature5-box">
                <div class="card card-shadow" data-aos="fade-down" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space"><i class="text-success-gradiant icon-Information"></i></div>
                        <div class="">
                            <h6 class="font-medium"><a href="javascript:void(0)" class="linking">Powerful Tech </a></h6>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-4 wrap-feature5-box">
                <div class="card card-shadow" data-aos="fade-left" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space"><i class="text-success-gradiant icon-Leo-2"></i></div>
                        <div class="">
                            <h6 class="font-medium"><a href="javascript:void(0)" class="linking">100% Satisfaction </a></h6>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-4 wrap-feature5-box">
                <div class="card card-shadow" data-aos="fade-right" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space"><i class="text-success-gradiant icon-Target-Market"></i></div>
                        <div class="">
                            <h6 class="font-medium"><a href="javascript:void(0)" class="linking">Targeting Market</a></h6>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-4 wrap-feature5-box">
                <div class="card card-shadow" data-aos="fade-up" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space"><i class="text-success-gradiant icon-Sunglasses-Smiley"></i></div>
                        <div class="">
                            <h6 class="font-medium"><a href="javascript:void(0)" class="linking">Goal Achievement </a></h6>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-4 wrap-feature5-box">
                <div class="card card-shadow" data-aos="fade-left" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space"><i class="text-success-gradiant  icon-Laptop-Phone"></i></div>
                        <div class="">
                            <h6 class="font-medium"><a href="javascript:void(0)" class="linking">Fully Responsive</a></h6>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-12 m-t-20 text-center">
                <a class="btn btn-success-gradiant btn-md btn-arrow"><span>View Feature5 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</div>`
});
