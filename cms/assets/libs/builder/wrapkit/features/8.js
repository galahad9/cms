var name = 'Feature #8';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light spacer feature8">
    <div class="container">
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-6">
                <small class="text-info">Success Stories</small>
                <h3>Re-Targeting gets easier with WrapKit</h3>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                <ul class="list-block m-b-30">
                    <li><i class="sl-icon-check text-info"></i> Powerful and Faster Results for your site</li>
                    <li><i class="sl-icon-check text-info"></i> Make your site in no-time with your Bootstrap WrapKit</li>
                    <li><i class="sl-icon-check text-info"></i> Tons of Features and Elements available here</li>
                    <li><i class="sl-icon-check text-info"></i> What are you wait for? Go and Get it.</li>
                </ul>
                <a class="btn btn-info-gradiant btn-md btn-arrow"><span>View Feature8 code <i class="ti-arrow-right"></i></span></a>
            </div>
            <!-- Column -->
            <div class="col-lg-6">
                <div class="p-20">
                    <img src="/cms/assets/wrapkit/images/features/feature8/market.jpg" alt="wrapkit" class="img-responsive img-shadow" data-aos="flip-right" data-aos-duration="1200" />
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
    </div>
</div>`
});
