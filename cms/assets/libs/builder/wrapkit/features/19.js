var name = 'Feature #19';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature19 p-b-0">
    <div class="container">
        <!-- row  -->
        <div class="row">
            <div class="col-lg-7 align-self-center">
                <span class="label label-primary label-rounded">Feature 19</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
                <div class="row text-inverse">
                    <div class="col-md-6 m-t-30">
                        <ul class="list-block">
                            <li><i class="sl-icon-check font-20"></i> <span>Fast & Simple WordPress Hosting</span></li>
                            <li><i class="sl-icon-check font-20"></i> <span>1-Click Staging Environment</span></li>
                            <li><i class="sl-icon-check font-20"></i> <span>All-in-One Control Panel</span></li>
                        </ul>
                    </div>
                    <div class="col-md-6 m-t-30">
                        <ul class="list-block">
                            <li><i class="sl-icon-check font-20"></i> <span>Includes 2 Wordpress Installs</span></li>
                            <li><i class="sl-icon-check font-20"></i> <span>Host Up to 100 Websites</span></li>
                            <li><i class="sl-icon-check font-20"></i> <span>1 TB Monthly BandWidth</span></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 m-t-40 m-b-30">
                        <a class="btn btn-info-gradiant btn-md btn-arrow" data-toggle="collapse" href="#f19"><span>View Feature19 code <i class="ti-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 text-center">
                <img src="/cms/assets/wrapkit/images/features/feature19/img1.jpg" alt="wrappixel" data-aos="fade-up" data-aos-duration="3000" data-aos-offset="500" />
            </div>
        </div>
    </div>
</div>`
});
