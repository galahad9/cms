var name = 'Feature #10';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="feature10">
    <div class="row">
        <!-- column  -->
        <div class="col-lg-4 bg-megna">
            <div class="wrap-feature10-box text-center">
                <div class="display-4 m-b-20"><i class="icon-Talk-Man"></i></div>
                <small class="op-8">Awesome Service</small>
                <h4 class="box-title">New Lead Generation</h4>
                <p class="op-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent esque dignissim eros a sapien tempus, ut eleifend neque convallis.</p>
                <a class="btn btn-outline-light btn-md btn-arrow"><span>View Feature6 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
        <!-- column  -->
        <!-- column  -->
        <div class="col-lg-4 bg-primary">
            <div class="wrap-feature10-box text-center">
                <div class="display-4 m-b-20"><i class="icon-Chrysler-Building"></i></div>
                <small class="op-8">Awesome Service</small>
                <h4 class="box-title">Constructive Planning</h4>
                <p class="op-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent esque dignissim eros a sapien tempus, ut eleifend neque convallis.</p>
                <a class="btn btn-outline-light btn-md btn-arrow"><span>View Feature6 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
        <!-- column  -->
        <!-- column  -->
        <div class="col-lg-4 bg-orange">
            <div class="wrap-feature10-box text-center">
                <div class="display-4 m-b-20"><i class="icon-Mouse-3"></i></div>
                <small class="op-8">Well Focused</small>
                <h4 class="box-title">Powerful Services</h4>
                <p class="op-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent esque dignissim eros a sapien tempus, ut eleifend neque convallis.</p>
                <a class="btn btn-outline-light btn-md btn-arrow"><span>View Feature6 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
        <!-- column  -->
    </div>
</div>`
});
