var name = 'Feature #12';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature12">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <span class="label label-info label-rounded">Feature 12</span>
                <h2 class="m-b-30 m-t-20">Awesome with Extra Ordinary Flexibility   Features</h2>
                <h4 class="text-muted font-light">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h4>
                <div class="row">
                    <div class="col-md-6 m-t-30">
                        <h5 class="font-medium">Instant Solutions</h5>
                        <p>You can relay on our amazing features list and also our customer services will.</p>
                    </div>
                    <div class="col-md-6 m-t-30">
                        <h5 class="font-medium">Retargeting Market</h5>
                        <p>You can relay on our amazing features list and also our customer services will.</p>
                    </div>
                    <div class="col-md-6 m-t-30">
                        <h5 class="font-medium">Fruitful Results</h5>
                        <p>You can relay on our amazing features list and also our customer services will.</p>
                    </div>
                    <div class="col-md-6 m-t-30">
                        <h5 class="font-medium">100% Satisfection</h5>
                        <p>You can relay on our amazing features list and also our customer services will.</p>
                    </div>
                    <div class="col-lg-12 m-t-30 m-b-30">
                        <a class="btn btn-info-gradiant btn-md btn-arrow" data-toggle="collapse" href="#f12"><span>View Feature12 code <i class="ti-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row wrap-feature-12">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12"><img src="/cms/assets/wrapkit/images/features/features12/img1.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" /></div>
                            <div class="col-md-12"><img src="/cms/assets/wrapkit/images/features/features12/img2.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" /></div>
                        </div>
                    </div>
                    <div class="col-md-6 uneven-box">
                        <div class="row">
                            <div class="col-md-12"><img src="/cms/assets/wrapkit/images/features/features12/img3.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" /></div>
                            <div class="col-md-12"><img src="/cms/assets/wrapkit/images/features/features12/img4.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" /></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
});
