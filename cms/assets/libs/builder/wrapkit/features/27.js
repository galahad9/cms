var name = 'Feature #27';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer bg-light feature26">
    <div class="container">
        <div class="row">
            <!-- column -->
            <div class="col-lg-6 align-self-center">
                <span class="label label-info label-rounded">Feature 27</span>
                <h2 class="title">We have covered everything for you get desire results with us</h2>
                <p class="m-t-30">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time and with great quality.</p>
                <div class="row">
                    <div class="col-md-6 m-t-40">
                        <i class="icon-Laptop-2 text-info-gradiant display-5"></i>
                        <h5 class="font-medium m-t-20 m-b-20">Your complete website in just few minutes</h5>
                        <p>You can relay on our amazing features list and also our customer services will be great experience have.</p>
                        <a class="linking font-medium" data-toggle="collapse" href="#f27">Lets Talk!  <i class="ti-arrow-right text-success"></i></a>
                    </div>
                    <div class="col-md-6  m-t-40">
                        <i class="icon-Fan text-info-gradiant display-5"></i>
                        <h5 class="font-medium m-t-20 m-b-20">Get Powerful Results with WrapKit</h5>
                        <p>You can relay on our amazing features list and also our customer services will be great experience have.</p>
                        <a class="linking font-medium" data-toggle="collapse" href="#f27">View code!  <i class="ti-arrow-right text-success"></i></a>
                    </div>
                </div>
            </div>
            <!-- column -->
            <div class="col-lg-6 text-center" data-aos="fade-up" data-aos-duration="1800" data-aos-easing="linear">
                <img src="/cms/assets/wrapkit/images/features/feature27/img1.png" class="img-fluid" alt="wrapkit" />
            </div>
        </div>
    </div>
</div>`
});
