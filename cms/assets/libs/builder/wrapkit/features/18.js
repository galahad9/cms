var name = 'Feature #18';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light spacer feature18">
    <div class="container">
        <!-- row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-danger label-rounded">Feature 18</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- row  -->
        <div class="row  wrap-feature-18">
            <!-- Column  -->
            <div class="col-lg-6" data-aos="fade-right" data-aos-duration="1200">
                <!-- card  -->
                <div class="card">
                    <div class="row">
                        <div class="col-md-5 icon-position" style="background-image:url(/cms/assets/wrapkit/images/features/feature13/img1.jpg)">
                            <div class="icon-round bg-success-gradiant text-white display-5"><i class="icon-Car-Wheel"></i></div>
                        </div>
                        <div class="col-md-7">
                            <div class="card-body p-40">
                                <h4 class="font-medium">Get Powerful Results with WrapKit</h4>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                <a href="javascript:void(0)" class="linking text-underline">Lets Talk <i class="ti-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column  -->
            <!-- Column  -->
            <div class="col-lg-6" data-aos="fade-left" data-aos-duration="1200">
                <!-- card  -->
                <div class="card">
                    <div class="row">
                        <div class="col-md-5 icon-position" style="background-image:url(/cms/assets/wrapkit/images/features/feature13/img2.jpg)">
                            <div class="icon-round bg-success-gradiant text-white display-5"><i class="icon-Target"></i></div>
                        </div>
                        <div class="col-md-7">
                            <div class="card-body p-40">
                                <h4 class="font-medium">Get Powerful Results with WrapKit</h4>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                <a href="javascript:void(0)" class="linking text-underline">Lets Talk <i class="ti-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column  -->
            <!-- Column  -->
            <div class="col-lg-6" data-aos="fade-up" data-aos-duration="1200">
                <!-- card  -->
                <div class="card">
                    <div class="row">
                        <div class="col-md-5 icon-position" style="background-image:url(/cms/assets/wrapkit/images/features/feature13/img3.jpg)">
                            <div class="icon-round bg-success-gradiant text-white display-5"><i class="icon-Mouse-3"></i></div>
                        </div>
                        <div class="col-md-7">
                            <div class="card-body p-40">
                                <h4 class="font-medium">Get Powerful Results with WrapKit</h4>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                <a href="javascript:void(0)" class="linking text-underline">Lets Talk <i class="ti-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column  -->
            <!-- Column  -->
            <div class="col-lg-6" data-aos="fade-up" data-aos-duration="1200">
                <!-- card  -->
                <div class="card">
                    <div class="row">
                        <div class="col-md-5 icon-position" style="background-image:url(/cms/assets/wrapkit/images/features/feature13/img4.jpg)">
                            <div class="icon-round bg-success-gradiant text-white display-5"><i class="icon-Car-Wheel"></i></div>
                        </div>
                        <div class="col-md-7">
                            <div class="card-body p-40">
                                <h4 class="font-medium">Get Powerful Results with WrapKit</h4>
                                <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                                <a href="javascript:void(0)" class="linking text-underline">Lets Talk <i class="ti-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column  -->
        </div>
        <div class="row">
            <div class="col-md-12 m-t-40 text-center">
                <a class="btn btn-success-gradiant btn-md btn-arrow" data-toggle="collapse" href="#f18"><span>View Feature18 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</div>`
});
