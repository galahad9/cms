var name = 'Feature #11';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light spacer feature11">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-success label-rounded">Feature 11</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="row m-t-40">
            <!-- Column -->
            <div class="col-md-4 wrap-feature11-box">
                <div class="card card-shadow" data-aos="fade-right" data-aos-duration="1200">
                    <div class="card-body">
                        <div class="icon-space">
                            <div class="icon-round bg-success-gradiant"><i class="icon-Target"></i></div>
                        </div>
                        <h5 class="font-medium">Retargeting Market</h5>
                        <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-4 wrap-feature11-box">
                <div class="card card-shadow" data-aos="fade-up" data-aos-duration="1200">
                    <div class="card-body">
                        <div class="icon-space">
                            <div class="icon-round bg-success-gradiant"><i class="icon-Car-Wheel"></i></div>
                        </div>
                        <h5 class="font-medium">Fruitful Results</h5>
                        <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-4 wrap-feature11-box">
                <div class="card card-shadow" data-aos="fade-left" data-aos-duration="1200">
                    <div class="card-body">
                        <div class="icon-space">
                            <div class="icon-round bg-success-gradiant"><i class="icon-Mouse-3"></i></div>
                        </div>
                        <h5 class="font-medium">Instant Solutions</h5>
                        <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 m-t-20 text-center">
                <a class="btn btn-success-gradiant btn-md btn-arrow" data-toggle="collapse" href="#f11"><span>View Feature11 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</div>`
});
