var name = 'Feature #4';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature4">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-danger label-rounded">Feature 4</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="row m-t-40">
            <!-- Column -->
            <div class="col-md-6 wrap-feature4-box">
                <div class="card" data-aos="zoom-out-right" data-aos-duration="1200">
                    <div class="card-body">
                        <div class="icon-round bg-light-info"><i class="icon-Clown"></i></div>
                        <h5 class="font-medium">Instant Solutions</h5>
                        <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tristique pellentesque ipsum.</p>
                        <a class="linking text-themecolor">Check the Feature4 Code <i class="ti-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-6 wrap-feature4-box">
                <div class="card" data-aos="zoom-out-left" data-aos-duration="1200">
                    <div class="card-body">
                        <div class="icon-round bg-light-info"><i class="icon-Mouse-3"></i></div>
                        <h5 class="font-medium">Powerful Techniques </h5>
                        <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tristique pellentesque ipsum. </p>
                        <a class="linking text-themecolor">Check the Feature4 Code <i class="ti-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>`
});
