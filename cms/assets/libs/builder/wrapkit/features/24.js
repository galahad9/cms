var name = 'Feature #24';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature24">
    <div class="container">
        <!-- Row -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center"> <span class="label label-success label-rounded">Feature 24</span>
                <h2 class="title">Services you can Relay Upon</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row -->
        <div class="row wrap-feature-24">
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card card-shadow">
                    <a href="javascript:void(0)" class="service-24"> <i class="icon-Target"></i>
                        <h6 class="ser-title">Retargeting Market</h6>
                    </a>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card card-shadow">
                    <a href="javascript:void(0)" class="service-24"> <i class="icon-Car-Wheel"></i>
                        <h6 class="ser-title">Digital Marketing</h6>
                    </a>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card card-shadow">
                    <a href="javascript:void(0)" class="service-24"> <i class="icon-Mouse-3"></i>
                        <h6 class="ser-title">SEO Techniques</h6>
                    </a>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card card-shadow">
                    <a href="javascript:void(0)" class="service-24"> <i class="icon-Eyeglasses-Smiley"></i>
                        <h6 class="ser-title">Client Management</h6>
                    </a>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card card-shadow">
                    <a href="javascript:void(0)" class="service-24"> <i class="icon-Target-Market"></i>
                        <h6 class="ser-title">Email Campaign</h6>
                    </a>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card card-shadow">
                    <a href="javascript:void(0)" class="service-24"> <i class="icon-Laptop-Phone"></i>
                        <h6 class="ser-title">Website Strategy</h6>
                    </a>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card card-shadow">
                    <a href="javascript:void(0)" class="service-24"> <i class="icon-Full-Bag"></i>
                        <h6 class="ser-title">eCommerce Shop</h6>
                    </a>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card card-shadow">
                    <a href="javascript:void(0)" class="service-24"> <i class="icon-Eyeglasses-Smiley"></i>
                        <h6 class="ser-title">Cloud Hosting</h6>
                    </a>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-12 m-t-20 text-center"> <a class="btn btn-outline-success btn-md btn-arrow" data-toggle="collapse" href="#f24"><span>View Feature24 code <i class="ti-arrow-right"></i></span></a></div>
        </div>
    </div>
</div>`
});
