var name = 'Feature #6';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature6">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-success label-rounded">Feature 6</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="row m-t-40">
            <!-- Column -->
            <div class="col-md-4 wrap-feature6-box">
                <div class="card bg-success-gradiant text-white" data-aos="fade-right" data-aos-duration="1200">
                    <div class="card-body">
                        <h6 class="font-medium text-white">Retargeting Market</h6>
                        <p class="m-t-20">Lorem ipsum dolor sit amet, consecte tuam porttitor, nunc et fringilla.</p>
                        <a href="#f4" class="linking">Learn More <i class="ti-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-4 wrap-feature6-box">
                <div class="card bg-info-gradiant text-white" data-aos="fade-down" data-aos-duration="1200">
                    <div class="card-body">
                        <h6 class="font-medium text-white">Instant Solutions</h6>
                        <p class="m-t-20">Lorem ipsum dolor sit amet, consecte tuam porttitor, nunc et fringilla.</p>
                        <a href="#f4" class="linking">Learn More <i class="ti-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-4 wrap-feature6-box">
                <div class="card bg-danger-gradiant text-white" data-aos="fade-left" data-aos-duration="1200">
                    <div class="card-body">
                        <h6 class="font-medium text-white">Powerful Techniques</h6>
                        <p class="m-t-20">Lorem ipsum dolor sit amet, consecte tuam porttitor, nunc et fringilla.</p>
                        <a href="#f4" class="linking">Learn More <i class="ti-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-12 m-t-20 text-center">
                <a class="btn btn-outline-success btn-md btn-arrow"><span>View Feature6 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</div>`
});
