var name = 'Feature #28';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="feature28">
    <div class="wrap-feature-28 row">
        <div class="col-lg-6 bg-cover" style="background-image:url(/cms/assets/wrapkit/images/features/feature28/img1.jpg)"></div>
        <div class="col-lg-6">
            <div class="side-content both-space">
                <span class="label label-info label-rounded">Great Feature 28</span>
                <h2 class="title">Fully Responsive</h2>
                <p>You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time and with great quality.</p>
                <a class="btn btn-info-gradiant btn-md btn-arrow m-t-20" data-toggle="collapse" href="#f28"><span>View Feature28 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
    <div class="wrap-feature-28 row">
        <div class="col-lg-6">
            <div class="side-content both-space text-right pull-right">
                <span class="label label-info label-rounded">Great Feature 28</span>
                <h2 class="title">Fully Responsive</h2>
                <p>You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time and with great quality.</p>
                <a class="btn btn-info-gradiant btn-md btn-arrow m-t-20" data-toggle="collapse" href="#f28"><span>View Feature28 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
        <div class="col-lg-6 bg-cover" style="background-image:url(/cms/assets/wrapkit/images/features/feature28/img2.jpg)"></div>
    </div>
</div>`
});
