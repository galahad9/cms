var name = 'Feature #16';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="feature16">
    <div class="row wrap-feature-16">
        <div class="col-lg-6 with-bg" style="background-image:url(/cms/assets/wrapkit/images/features/feature16/img1.jpg)"></div>
        <div class="col-lg-6 bg-info-gradiant text-white">
            <div class="with-text">
                <span class="label label-success label-rounded">Feature 16</span>
                <h2 class="text-white m-t-30 m-b-30">Awesome with Extra Ordinary Flexibility & Features</h2>
                <p class="op-7">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</p>
                <ul class="list-block m-b-30">
                    <li><i class="sl-icon-check"></i> <span>Powerful and Faster Results for your site</span></li>
                    <li><i class="sl-icon-check"></i> <span>Make your site in no-time with your Bootstrap WrapKit</span></li>
                    <li><i class="sl-icon-check"></i> <span>Tons of Features and Elements available here</span></li>
                    <li><i class="sl-icon-check"></i> <span>What are you wait for? Go and Get it.</span></li>
                </ul>
                <a class="btn btn-outline-light btn-md btn-arrow" data-toggle="collapse" href="#f16"><span>View Feature16 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</div>`
});
