var name = 'Feature #13';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light spacer feature13">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-success label-rounded">Feature 13</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="row m-t-40">
            <!-- Column -->
            <div class="col-lg-6 wrap-feature13-box">
                <div class="card card-shadow" data-aos="fade-right" data-aos-duration="1200">
                    <ul class="img-inline">
                        <li class="half-width"><img src="/cms/assets/wrapkit/images/features/feature13/img1.jpg" alt="wrapkit" class="img-responsive rounded" /></li>
                        <li class="p-20 half-width">
                            <h5 class="font-medium m-t-10"><a href="javascript:void(0)" class="linking">Instant Solutions <i class="ti-arrow-right"></i></a></h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-6 wrap-feature13-box">
                <div class="card card-shadow" data-aos="fade-left" data-aos-duration="1200">
                    <ul class="img-inline">
                        <li class="half-width"><img src="/cms/assets/wrapkit/images/features/feature13/img2.jpg" alt="wrapkit" class="img-responsive rounded" /></li>
                        <li class="p-20 half-width">
                            <h5 class="font-medium m-t-10"><a href="javascript:void(0)" class="linking">Instant Solutions <i class="ti-arrow-right"></i></a></h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-6 wrap-feature13-box">
                <div class="card card-shadow" data-aos="fade-right" data-aos-duration="1200">
                    <ul class="img-inline">
                        <li class="half-width"><img src="/cms/assets/wrapkit/images/features/feature13/img3.jpg" alt="wrapkit" class="img-responsive rounded" /></li>
                        <li class="p-20 half-width">
                            <h5 class="font-medium m-t-10"><a href="javascript:void(0)" class="linking">Instant Solutions <i class="ti-arrow-right"></i></a></h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-6 wrap-feature13-box">
                <div class="card card-shadow" data-aos="fade-left" data-aos-duration="1200">
                    <ul class="img-inline">
                        <li class="half-width"><img src="/cms/assets/wrapkit/images/features/feature13/img4.jpg" alt="wrapkit" class="img-responsive rounded" /></li>
                        <li class="p-20 half-width">
                            <h5 class="font-medium m-t-10"><a href="javascript:void(0)" class="linking">Instant Solutions <i class="ti-arrow-right"></i></a></h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-12 m-t-20 text-center">
                <a class="btn btn-success-gradiant btn-md btn-arrow" data-toggle="collapse" href="#f13"><span>View Feature13 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</div>`
});
