var name = 'Feature #23';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature23 wrap-feature23-box" style="background-image:url(/cms/assets/wrapkit/images/features/feature23/img1.jpg)">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5 text-center both-space">
                <div class="card" data-aos="flip-left" data-aos-duration="1200">
                    <div class="card-body">
                        <div class="text-box"> <span class="label label-info label-rounded">Feature 23</span>
                            <h3 class="title">Brilliant Design and Unlimited Features to Boost your Site</h3>
                            <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt</h6> <a class="btn btn-info-gradiant btn-md btn-arrow m-t-20" data-toggle="collapse" href="#f23"><span>View Feature23 code <i class="ti-arrow-right"></i></span></a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
});
