var name = 'Feature #2';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature2">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-info label-rounded">Feature 2</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="row m-t-40">
            <!-- Column -->
            <div class="col-md-4 wrap-feature2-box">
                <div class="card card-shadow" data-aos="flip-left" data-aos-duration="1200">
                    <img class="card-img-top" src="/cms/assets/wrapkit/images/features/feature2/market.jpg" alt="wrappixel kit" />
                    <div class="card-body text-center">
                        <h5 class="font-medium">Retargeting Market</h5>
                        <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-4 wrap-feature2-box">
                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                    <img class="card-img-top" src="/cms/assets/wrapkit/images/features/feature2/fruit.jpg" alt="wrappixel kit" />
                    <div class="card-body text-center">
                        <h5 class="font-medium">Fruitful Results</h5>
                        <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-4 wrap-feature2-box">
                <div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
                    <img class="card-img-top" src="/cms/assets/wrapkit/images/features/feature2/instant.jpg" alt="wrappixel kit" />
                    <div class="card-body text-center">
                        <h5 class="font-medium">Instant Solutions</h5>
                        <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 m-t-20 text-center">
                <a class="btn btn-info-gradiant btn-md btn-arrow"><span>View Feature2 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</div>`
});
