var name = 'Feature #9';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer feature9">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-success label-rounded">Feature 9</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="row m-t-40">
            <!-- Column -->
            <div class="col-md-6 wrap-feature9-box b-r b-b">
                <div class="card" data-aos="fade-right" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="align-self-center">
                            <h2 class="font-medium">Bee Seo Digital Marketing Agency</h2>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-6 wrap-feature9-box b-b">
                <div class="card" data-aos="fade-left" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space align-self-center"><i class="icon-Computer-Secure display-4 text-success-gradiant"></i></div>
                        <div class="align-self-center">
                            <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Online Marketer <i class="ti-arrow-right"></i></a></h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-6 wrap-feature9-box b-r">
                <div class="card" data-aos="fade-right" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space align-self-center"><i class="icon-Cloud-Smartphone display-4 text-success-gradiant"></i></div>
                        <div class="align-self-center">
                            <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Digital World <i class="ti-arrow-right"></i></a></h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-6 wrap-feature9-box">
                <div class="card" data-aos="fade-left" data-aos-duration="1200">
                    <div class="card-body d-flex">
                        <div class="icon-space align-self-center"><i class="icon-Business-ManWoman display-4 text-success-gradiant"></i></div>
                        <div class="align-self-center">
                            <h5 class="font-medium"><a href="javascript:void(0)" class="linking">Meet the Team <i class="ti-arrow-right"></i></a></h5>
                            <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-12 m-t-20 text-center">
                <a class="btn btn-success-gradiant btn-md btn-arrow"><span>View Feature9 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</div>`
});
