var name = 'Feature #30';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer ">
    <div class="container feature30">
        <div class="row">
            <div class="col-lg-10"><img src="/cms/assets/wrapkit/images/features/feature30/img1.jpg" class="rounded img-responsive" alt="wrappixel" /></div>
            <div class="col-lg-5 col-md-7 text-center wrap-feature30-box" data-aos="fade-left" data-aos-duration="1800" data-aos-easing="linear">
                <div class="card card-shadow">
                    <div class="card-body">
                        <div class="p-20">
                            <h3 class="title">The New way of Making Your Website in mins</h3>
                            <p>You can relay on our amazing features list and also our customer services will be great experience. You will love it for sure.</p>
                            <a class="btn btn-info-gradiant btn-md btn-arrow m-t-20" data-toggle="collapse" href="#f30"><span>View Feature30 code <i class="ti-arrow-right"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
});
