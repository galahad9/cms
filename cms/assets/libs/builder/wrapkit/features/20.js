var name = 'Feature #20';
var image = 'icons/feature.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light spacer feature20 up">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <span class="label label-success label-rounded">Feature 20</span>
                <h2 class="title">Awesome with Extra Ordinary Flexibility</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="row wrap-feature-20">
            <!-- Column -->
            <div class="col-lg-6" data-aos="flip-left" data-aos-duration="1200">
                <div class="card">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card-body d-flex no-block">
                                <div class="m-r-20"><img src="/cms/assets/wrapkit/images/features/feature13/img1.jpg" width="70" class="rounded" /></div>
                                <div>
                                    <h5 class="font-medium">Make your website in no-time with us.</h5></div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="javascript:void(0)" class="text-white linking bg-success-gradiant">Lets Talk <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-6" data-aos="flip-right" data-aos-duration="1200">
                <div class="card">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card-body d-flex no-block">
                                <div class="m-r-20"><img src="/cms/assets/wrapkit/images/features/feature13/img2.jpg" width="70" class="rounded" /></div>
                                <div>
                                    <h5 class="font-medium">Make your website in no-time with us.</h5></div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="javascript:void(0)" class="text-white linking bg-success-gradiant">Lets Talk <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-6" data-aos="zoom-in" data-aos-duration="1200">
                <div class="card">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card-body d-flex no-block">
                                <div class="m-r-20"><img src="/cms/assets/wrapkit/images/features/feature13/img3.jpg" width="70" class="rounded" /></div>
                                <div>
                                    <h5 class="font-medium">Make your website in no-time with us.</h5></div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="javascript:void(0)" class="text-white linking bg-success-gradiant">Lets Talk <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-6" data-aos="zoom-in" data-aos-duration="1200">
                <div class="card">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card-body d-flex no-block">
                                <div class="m-r-20"><img src="/cms/assets/wrapkit/images/features/feature13/img4.jpg" width="70" class="rounded" /></div>
                                <div>
                                    <h5 class="font-medium">Make your website in no-time with us.</h5></div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="javascript:void(0)" class="text-white linking bg-success-gradiant">Lets Talk <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-12 m-t-40 text-center">
                <a class="btn btn-success-gradiant btn-md btn-arrow" data-toggle="collapse" href="#f20"><span>View Feature20 code <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
        <!-- Row  -->
    </div>
</div>`
});
