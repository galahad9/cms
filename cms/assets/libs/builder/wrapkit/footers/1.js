/*
Copyright 2017 Ziadin Givan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

https://github.com/givanz/VvvebJs
*/
var name = 'Footer #1';
var image = 'icons/map.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="footer1 font-14">
    <div class="f1-topbar">
        <div class="container">
            <!-- Row  -->
            <div class="row">
                <!-- Column -->
                <div class="col-md-12" data-aos="fade-right" data-aos-duration="1200">
                    <nav class="navbar navbar-expand-lg f1-nav"> <a class="navbar-brand hidden-md-up" href="#">Footer Navbar</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ft1" aria-controls="ft1" aria-expanded="false" aria-label="Toggle navigation"> <span class="ti-menu"></span> </button>
                        <div class="collapse navbar-collapse" id="ft1">
                            <ul class="navbar-nav">
                                {{ MENU }}
                            </ul>
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item"><a class="nav-link text-dark text-uppercase" href="#"><i class="icon-Mail-Read text-danger font-20 m-r-10"></i>Request a  Free Consultation</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <!-- Column -->
            </div>

        </div>
    </div>
    <div class="f1-middle">
        <div class="container">
            <div class="row">
                <!-- Column -->
                <div class="col-lg-3 col-md-6">
                    {{ LOGO }}
                    <p class="m-t-20">You can relay on our amazing features list and also our customer services will be great experience.</p>
                    <p>our amazing features list and also our customer services is great.</p>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-3 col-md-6">
                   <div class="d-flex no-block m-b-10 m-t-20">
                        <div class="display-7 m-r-20 align-self-top"><i class="icon-Location-2"></i></div>
                        <div class="info">
                            <span class="font-medium text-dark db m-t-5">{{ Website Name }}</span><br/>
                            <p>{{ Street }}<br/>
                            {{ Zipcode }} {{ City }}</p>
                       </div>
                   </div>
                   <div class="d-flex no-block m-b-10">
                        <div class="display-7 m-r-20 align-self-top"><i class="icon-Phone-2"></i></div>
                        <div class="info">
                            <a href="tel:{{ Phonenumber }}" class="font-medium text-dark db  m-t-5">{{ Phonenumber }}</a>
                       </div>
                   </div>
                   <div class="d-flex no-block m-b-30">
                        <div class="display-7 m-r-20 align-self-top"><i class="icon-Mail"></i></div>
                        <div class="info">
                            <a href="mailto:{{ Emailaddress }}" class="font-medium text-dark db  m-t-5">{{ Emailaddress }}</a>
                       </div>
                   </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-6 col-md-12 m-t-5">
                    <h6 class="font-medium m-t-20">Services we Offer</h6>
                    <ul class="general-listing two-part with-arrow m-t-10">
                        <li><a href="#"><i class="fa fa-angle-right"></i> Website Design</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Application Development</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Mobile Development</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Search Engine Optimzation</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Digital Marketing</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Website Development </a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> eBook Writing </a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> SaaS base Applications</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Ruby on Rails Development </a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Php Development</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Row  -->
    <div class="f1-bottom-bar">
        <div class="container">
        <div class="d-flex">
            <div class="m-t-10 m-b-10">© All Rights Reserved by {{ Website Name }}.</div>
            <div class="links ml-auto m-t-10 m-b-10">
                <a href="#" class="link p-10"><i class="fa fa-facebook"></i></a>
                <a href="#" class="link p-10"><i class="fa fa-twitter"></i></a>
                <a href="#" class="link p-10"><i class="fa fa-linkedin"></i></a>
                <a href="#" class="link p-10"><i class="fa fa-pinterest"></i></a>
                <a href="#" class="link p-10"><i class="fa fa-instagram"></i></a>
            </div>
        </div>
        </div>
    </div>
</div>`
});
