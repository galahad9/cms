/*
Copyright 2017 Ziadin Givan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

https://github.com/givanz/VvvebJs
*/
var name = 'Footer #4';
var image = 'icons/map.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<footer class="footer6 bg-inverse spacer">
      <div class="container">
          <div class="row">
              <!-- coluumn -->
              <div class="col-lg-4">
                  <div class="d-flex no-block m-b-10 text-white">
                      <div class="display-7 m-r-20 "><i class="icon-Phone-2"></i></div>
                      <div class="info">
                          <span class="db m-t-5">{{ Phonenumber }}</span>
                     </div>
                 </div>
              </div>
              <!-- coluumn -->
              <!-- coluumn -->
              <div class="col-lg-4">
                  <div class="d-flex no-block m-b-10">
                      <div class="display-7 m-r-20 text-white"><i class="icon-Mail"></i></div>
                      <div class="info">
                          <a href="mailto:{{ Emailaddress }}" class="db white-link m-t-5">{{ Emailaddress }}</a>
                     </div>
                  </div>
              </div>
              <!-- coluumn -->
              <!-- coluumn -->
              <div class="col-lg-4 ml-auto">
                  <div class="ml-auto round-social dark">
                          <a href="#" class=""><i class="fa fa-facebook"></i></a>
                          <a href="#" class=""><i class="fa fa-twitter"></i></a>
                          <a href="#" class=""><i class="fa fa-linkedin"></i></a>
                          <a href="#" class=""><i class="fa fa-pinterest"></i></a>
                          <a href="#" class=""><i class="fa fa-instagram"></i></a>
                      </div>
              </div>
              <div class="col-md-12 b-t m-t-40">
                  <div class="p-t-20">Copyright 2018. All Rights Reserved by {{ Website Name }}.</div>
              </div>
          </div>
      </div>
  </footer>`
});
