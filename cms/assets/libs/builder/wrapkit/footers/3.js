/*
Copyright 2017 Ziadin Givan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

https://github.com/givanz/VvvebJs
*/
var name = 'Footer #3';
var image = 'icons/map.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="footer4 spacer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 m-b-30">
                    <h5 class="m-b-20">Address</h5>
                    <p>{{ Street }}<br>
                    {{ Zipcode }}  {{ City }}</p>
                </div>
                <div class="col-lg-3 col-md-6 m-b-30">
                    <h5 class="m-b-20">Phone</h5>
                    <p><a href="tel:{{ Phonenumber }}">{{ Phonenumber }}</a></p>
                </div>
                <div class="col-lg-3 col-md-6 m-b-30">
                    <h5 class="m-b-20">Email</h5>
                    <p><a href="mailto:{{ Emailaddress }}" class="link">{{ Emailaddress }}</a></p>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="m-b-20">Social</h5>
                  <div class="round-social light">
                        <a href="#" class="link"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="link"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="link"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
            <div class="f4-bottom-bar">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-expand-lg h1-nav">
                          {{ LOGO }}
                          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header1" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="ti-menu"></span>
                          </button>

                          <div class="collapse navbar-collapse" id="header1">
                              	<span class="hidden-lg-down">All Rights Reserved by {{ Website Name }}.</span>
                              	<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                              		{{ MENU }}
                            	</ul>
                          </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>`
});
