/*
Copyright 2017 Ziadin Givan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

https://github.com/givanz/VvvebJs
*/
var name = 'Footer #2';
var image = 'icons/map.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="footer2 font-14 bg-dark">
    <div class="f2-topbar container">
        <div class="d-flex">
            {{ LOGO }}
            <div class="ml-auto align-self-center">
                <span class="text-white">{{ Website Name }}</span> © {{ "now"|date("Y") }} All rights reserved.
            </div>
        </div>
    </div>
    <div class="f2-middle">
        <div class="container">
            <div class="row">
                <!-- Column -->
                <div class="col-lg-4 col-md-6">
                    <p class="p-t-10">Nullam erat ametam arcu lorme pharetra id risus act sectetur ipsum luctus est ullam erat ametam arcu lorme pharetra id us act sectetur ipsum luctus est. </p>
                    <p>Vestibulum in accumsa maga eds maurise elit tincidunt turpis id semper. </p>
                    <div class="m-t-20 m-b-30">
                        <a href="#" class="link"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="link"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="link"><i class="fa fa-linkedin"></i></a>
                        <a href="#" class="link"><i class="fa fa-pinterest"></i></a>
                        <a href="#" class="link"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-lg-2 col-md-6">
                    <ul class="general-listing">
                        {{ MENU }}
                    </ul>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-3 col-md-6 info-box">
                   <div class="d-flex no-block">
                        <div class="display-7 m-r-20 align-self-top"><i class="icon-Location-2"></i></div>
                        <div class="info">
                            <p>{{ Website Name }}<br/>
                            {{ Street }}<br/>
                            {{ Zipcode }} {{ City }}</p>
                       </div>
                   </div>
                   <div class="d-flex no-block">
                        <div class="display-7 m-r-20 align-self-top"><i class="icon-Phone-2"></i></div>
                        <div class="info">
                            <a href="tel:{{ Phonenumber }}" class="font-medium text-muted db  m-t-5">{{ Phonenumber }}</a>
                        </div>
                   </div>
                   <div class="d-flex no-block">
                        <div class="display-7 m-r-20 align-self-top"><i class="icon-Mail"></i></div>
                        <div class="info">
                            <a href="mailto:{{ Emailaddress }}" class="font-medium text-muted db  m-t-5">{{ Emailaddress }}</a>
                       </div>
                   </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-3 col-md-6">
                    <div class="subscribe-box">
                        <div class="display-4 text-white"><i class="icon-Mail-3"></i></div>
                        <p>Nullam erat ametam arcuing lorme pharetra id risus act sectetur ipsum luctus est. </p>
                        <form>
                            <div class="m-b-20"><input class="form-control" placeholder="enter email"></div>
                            <button class="btn btn-danger-gradiant">JOIN US</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
});
