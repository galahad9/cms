var name = 'Simple #1';
var image = 'icons/cta.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer c2a2">
        <div class="container">
            <!-- Row -->
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <h2 class="title font-light">Are you happy with what we offer? Grab your wrapkit copy now</h2>
                    <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
                    <div class="m-t-40">
                        <a class="btn btn-success-gradiant btn-rounded btn-md btn-arrow m-t-20 m-r-10" href="#"><span>Buy Wrapkit <i class="ti-arrow-right"></i></span></a>
                        <a class="btn btn-outline-inverse btn-rounded btn-md btn-arrow m-t-20" href="#"><span>Live Preview <i class="ti-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>`
});
