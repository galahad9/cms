var name = 'Parallax #2';
var image = 'icons/cta.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer c2a10" style="background-image:url(/cms/assets/wrapkit/images/c2a/5.jpg)">
        <div class="container">
            <!-- Row -->
            <div class="row justify-content-center">
                <div class="col-md-5 text-center both-space">
                    <div class="card" data-aos="flip-left" data-aos-duration="1200">
                        <div class="card-body">
                            <div class="text-box">
                                <h3 class="title">Brilliant Design and Unlimited Features to Boost your Site</h3>
                                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt</h6> <a class="btn btn-info-gradiant btn-md btn-arrow m-t-20" href="#"><span>View Other Features <i class="ti-arrow-right"></i></span></a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`
});
