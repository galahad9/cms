var name = 'Image Block';
var image = 'icons/cta.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="c2a8" style="background-image:url(/cms/assets/wrapkit/images/c2a/6.jpg); background-repeat:no-repeat">
        <div class="container">
            <!-- Row -->
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7 text-center both-space">
                    <div class="card bg-danger-gradiant" data-aos="flip-left" data-aos-duration="1200">
                        <div class="card-body">
                            <div class="text-box"> 
                                <h3 class="title text-white font-stylish">Make your Reservation for Delicious Food</h3>
                                <h6 class="subtitle text-white op-5">You can relay on our amazing features list and also our customer services will be great experience for you without doubt</h6> <a class="btn bg-white text-danger btn-rounded btn-md btn-arrow m-t-20" href="#"><span>Reserve Your Table <i class="ti-arrow-right"></i></span></a> </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            </div>
        </div>
    </div>`
});
