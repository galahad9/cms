var name = 'Simple #3';
var image = 'icons/cta.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer c2a4">
        <div class="container">
            <div class="row p-b-30">
                <div class="col-lg-9 col-md-7">
                    <h2 class="font-bold">Happy with what we offer? Grab it Now!</h2>
                    <h6 class="subtitle">Yeah ! Its pretty cool about what you are offering and i starterd to feel in love with your Amazing UI Kit.</h6>
                </div>
                <div class="col-lg-3 col-md-5 align-self-center">
                    <a class="btn btn-danger-gradiant btn-md btn-arrow btn-rounded m-b-20 pull-right" data-toggle="collapse" href="#f1"><span>BUY BASIC <i class="ti-arrow-right"></i></span></a>
                </div>
            </div>
        </div>    
    </div>`
});
