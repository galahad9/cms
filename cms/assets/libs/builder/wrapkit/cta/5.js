var name = 'Simple #4';
var image = 'icons/cta.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="mini-spacer c2a5" style="background-image:url(/cms/assets/wrapkit/images/c2a/2.jpg)">
        <div class="container">
            <div class="d-flex">
                <div class="display-7 align-self-center">
                    <h3 class="text-white">Are you looking for Online Appointment?</h3>
                </div>
                <div class="ml-auto m-t-10 m-b-10"><button class="btn bg-white text-inverse btn-md">Book an Appointment</button></div>
            </div>
        </div>
    </div>`
});
