var name = 'Showcase';
var image = 'icons/cta.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-info c2a9" style="background-image:url(/cms/assets/wrapkit/images/c2a/4.jpg)">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <h6 class="font-16 subtitle text-white">The fast and easiest way to</h6>
                    <h1 class="text-white text-uppercase">showcase your photos</h1>
                    <div class="m-t-40">
                        <button class="btn bg-white text-dark btn-rounded btn-md">Get Free Access</button>
                    </div>
                </div>
            </div>
        </div>
    </div>`
});
