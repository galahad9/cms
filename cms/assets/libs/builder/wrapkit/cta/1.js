var name = 'Parallax #1';
var image = 'icons/cta.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="spacer bg-light c2a1" style="background-image:url(/cms/assets/wrapkit/images/c2a/1.jpg)">
    <div class="container">
        <!-- Row -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <h2 class="title text-white font-medium">Become a Volunteer</h2>
                <p class="font-light text-white op-8">Lorem ipsum dolor sit amet, consectetur adipiscelit. Nam malesu dolor sit amet, consectetur adipiscelit. consectetur adipiscelit. Nam malesu dolor.</p>
                <a class="btn btn-danger-gradiant btn-md btn-arrow m-t-20 text-uppercase" href="#"><span>join us now<i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
        <!-- Row -->
    </div>
</div>`
});
