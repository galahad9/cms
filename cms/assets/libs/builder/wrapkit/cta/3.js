var name = 'Simple #2';
var image = 'icons/cta.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="mini-spacer bg-light c2a3">
        <div class="container">
            <div class="d-flex">
                <div class="display-7 align-self-center">
                    <h3 class="">We are leading Manufacturing and Exporting company</h3>
                    <h6 class="font-16 subtitle">You will feel great using our quality products and services</h6>
                </div>
                <div class="ml-auto m-t-10 m-b-10"><button class="btn btn-info-gradiant btn-md">Contact Us</button></div>
            </div>
        </div>
    </div>`
});
