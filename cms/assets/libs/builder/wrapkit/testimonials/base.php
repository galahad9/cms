Vvveb.ComponentsGroup['Testimonials'] = [];
<?php

$files = glob($dir.'/*.js', 0);
foreach ($files as $key => $value) {
	$dir = str_replace(basename($value), '', $value);
	$group = explode('/', trim($dir, '/'));
	$group = end($group);
	$filename = str_replace('.js', '', basename($value));
	if(file_exists($dir.'/'.$filename.'.html')){
		$html = file_get_contents($dir.'/'.$filename.'.html');
		 //var html = `<?= processPlaceholdersBuilder($html) ?>
		var html = `<?= $html ?>`;
	<?php } ?>
	
	var component = '<?= $group ?>/<?= $group ?><?= $filename ?>';
	
	<?php
	require($value);
	?>
	Vvveb.ComponentsGroup['Testimonials'].push(component);
	<?php
}
?>

