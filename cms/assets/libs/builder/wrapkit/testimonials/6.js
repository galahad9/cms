var name = 'Testimonial #6';
var image = 'icons/testimonial.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="testimonial6 spacer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-3" data-aos="fade-right">
                    <div class="nav flex-column nav-pills testi6" id="v-pills-tab" role="tablist">
                        <a class="nav-link active" data-toggle="pill" href="#t6-1" role="tab" aria-controls="t6-1" aria-expanded="true">
                          <img src="/cms/assets/wrapkit/images/testimonial/1.jpg" alt="wrapkit" class="circle" />
                      </a>
                        <a class="nav-link" data-toggle="pill" href="#t6-2" role="tab" aria-controls="t6-2" aria-expanded="true">
                         <img src="/cms/assets/wrapkit/images/testimonial/2.jpg" alt="wrapkit" class="circle" />      
                      </a>
                        <a class="nav-link" data-toggle="pill" href="#t6-3" role="tab" aria-controls="t6-3" aria-expanded="true">
                         <img src="/cms/assets/wrapkit/images/testimonial/3.jpg" alt="wrapkit" class="circle" />      
                      </a>
                        <a class="nav-link" data-toggle="pill" href="#t6-4" role="tab" aria-controls="t6-4" aria-expanded="true">
                         <img src="/cms/assets/wrapkit/images/testimonial/4.jpg" alt="wrapkit" class="circle" />  
                      </a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 ml-auto align-self-center" data-aos="fade-up">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="t6-1" role="tabpanel">
                            <h2 class="font-medium">What our Customers says</h2>
                            <h6 class="subtitle m-t-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do mode tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h6>
                            <div class="d-flex m-t-40">
                                <div>
                                    <h5 class="m-b-0 text-uppercase">Hanna Gover</h5>
                                    <h6 class="subtitle">Texas</h6>
                                </div>
                                <button class="btn btn-circle btn-danger btn-md ml-auto"><i class="fa fa-quote-left"></i></button>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="t6-2" role="tabpanel">
                            <h2 class="font-medium">What our Customers says</h2>
                            <h6 class="subtitle m-t-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do mode tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h6>
                            <div class="d-flex m-t-40">
                                <div>
                                    <h5 class="m-b-0 text-uppercase">Hanna Gover</h5>
                                    <h6 class="subtitle">Texas</h6>
                                </div>
                                <button class="btn btn-circle btn-danger btn-md ml-auto"><i class="fa fa-quote-left"></i></button>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="t6-3" role="tabpanel">
                            <h2 class="font-medium">What our Customers says</h2>
                            <h6 class="subtitle m-t-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do mode tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h6>
                            <div class="d-flex m-t-40">
                                <div>
                                    <h5 class="m-b-0 text-uppercase">Hanna Gover</h5>
                                    <h6 class="subtitle">Texas</h6>
                                </div>
                                <button class="btn btn-circle btn-danger btn-md ml-auto"><i class="fa fa-quote-left"></i></button>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="t6-4" role="tabpanel">
                            <h2 class="font-medium">What our Customers says</h2>
                            <h6 class="subtitle m-t-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do mode tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h6>
                            <div class="d-flex m-t-40">
                                <div>
                                    <h5 class="m-b-0 text-uppercase">Hanna Gover</h5>
                                    <h6 class="subtitle">Texas</h6>
                                </div>
                                <button class="btn btn-circle btn-danger btn-md ml-auto"><i class="fa fa-quote-left"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`
});
