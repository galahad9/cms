var name = 'Testimonial #7';
var image = 'icons/testimonial.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="testimonial7 spacer bg-light">
        <div class="container">
            <div class="owl-carousel owl-theme testi7">
                <!-- item -->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-7 col-md-6">
                            <img src="/cms/assets/wrapkit/images/testimonial/be-af-1.png" alt="wrapkit" class="img-fluid" />
                        </div>
                        <div class="col-lg-5 col-md-6">
                            <h2 class="text-uppercase title">Valuble Reviews</h2>
                            <p class="m-t-30">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi </p>
                            <h6 class="m-t-40 m-b-0">Michelle Anderson</h6>
                            <span>Partner, Brevin</span>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-7 col-md-6">
                            <img src="/cms/assets/wrapkit/images/testimonial/be-af-1.png" alt="wrapkit" class="img-fluid" />
                        </div>
                        <div class="col-lg-5 col-md-6">
                            <h2 class="text-uppercase title">Valuble Reviews</h2>
                            <p class="m-t-30">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi </p>
                            <h6 class="m-t-40 m-b-0">Michelle Anderson</h6>
                            <span>Partner, Brevin</span>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-7 col-md-6">
                            <img src="/cms/assets/wrapkit/images/testimonial/be-af-1.png" alt="wrapkit" class="img-fluid" />
                        </div>
                        <div class="col-lg-5 col-md-6">
                            <h2 class="text-uppercase title">Valuble Reviews</h2>
                            <p class="m-t-30">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi </p>
                            <h6 class="m-t-40 m-b-0">Michelle Anderson</h6>
                            <span>Partner, Brevin</span>
                        </div>
                    </div>
                </div>
                <!-- item -->
            </div>
        </div>
    </div>`
});
