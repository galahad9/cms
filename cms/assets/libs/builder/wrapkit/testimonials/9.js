var name = 'Testimonial #9';
var image = 'icons/testimonial.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="testimonial9 spacer bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6">
                    <h2 class="m-t-40">What Our Customers Say</h2>
                    <span class="devider bg-info"></span>
                    <p>We care what our customers think of us and so should you. We are partners in your business and your success is ours in your business.</p>
                </div>
                <div class="col-lg-6 col-md-6 ml-auto">
                    <div class="owl-carousel owl-theme testi9">
                        <!-- item -->
                        <div class="item">
                            <div class="card card-shadow">
                                <div class="p-40">
                                    <h5 class="text">WrapKit has given our websites huge national presence. We are #1 on page one in Google search results for every website we’ve built, and rank for more keywords than I ever expected in a very competitive, high-value customer industry. In addition,</h5>
                                </div>
                            </div>
                            <div class="d-flex no-block align-items-center">
                                <div class="customer-thumb"><img src="/cms/assets/wrapkit/images/testimonial/1.jpg" alt="wrapkit" class="circle" /></div>
                                <div class="">
                                    <h6 class="font-bold m-b-0">Michael Anderson</h6><span class="font-13">Project client</span></div>
                            </div>
                        </div>
                        <!-- item -->
                        <!-- item -->
                        <div class="item">
                            <div class="card card-shadow">
                                <div class="p-40">
                                    <h5 class="text">WrapKit has given our websites huge national presence. We are #1 on page one in Google search results for every website we’ve built, and rank for more keywords than I ever expected in a very competitive, high-value customer industry. In addition,</h5>
                                </div>
                            </div>
                            <div class="d-flex no-block align-items-center">
                                <div class="customer-thumb"><img src="/cms/assets/wrapkit/images/testimonial/2.jpg" alt="wrapkit" class="circle" /></div>
                                <div class="">
                                    <h6 class="font-bold m-b-0">Michael Anderson</h6><span class="font-13">Project client</span></div>
                            </div>
                        </div>
                        <!-- item -->
                        <!-- item -->
                        <div class="item">
                            <div class="card card-shadow">
                                <div class="p-40">
                                    <h5 class="text">WrapKit has given our websites huge national presence. We are #1 on page one in Google search results for every website we’ve built, and rank for more keywords than I ever expected in a very competitive, high-value customer industry. In addition,</h5>
                                </div>
                            </div>
                            <div class="d-flex no-block align-items-center">
                                <div class="customer-thumb"><img src="/cms/assets/wrapkit/images/testimonial/3.jpg" alt="wrapkit" class="circle" /></div>
                                <div class="">
                                    <h6 class="font-bold m-b-0">Michael Anderson</h6><span class="font-13">Project client</span></div>
                            </div>
                        </div>
                        <!-- item -->
                    </div>
                </div>
            </div>
        </div>
    </div>`
});
