var name = 'Testimonial #4';
var image = 'icons/testimonial.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="testimonial4 spacer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-7 align-self-center" data-aos="fade-right">
                    <h2>Thousands of Women are Happy Using Our Products</h2>
                    <div class="owl-carousel owl-theme testi4 m-t-40">
                        <!-- item -->
                        <div class="item">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <h6 class="m-t-40 m-b-0">Michelle Anderson</h6>
                            <span>Partner, Brevin</span>
                        </div>
                        <!-- item -->
                        <!-- item -->
                        <div class="item">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <h6 class="m-t-40 m-b-0">Michelle Anderson</h6>
                            <span>Partner, Brevin</span>
                        </div>
                        <!-- item -->
                        <!-- item -->
                        <div class="item">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <h6 class="m-t-40 m-b-0">Michelle Anderson</h6>
                            <span>Partner, Brevin</span>
                        </div>
                        <!-- item -->
                    </div>
                </div>
                <div class="col-lg-6 col-md-5" data-aos="fade-left">
                    <img src="/cms/assets/wrapkit/images/testimonial/women.png" alt="wrapkit" class="img-fluid" />
                </div>
            </div>
        </div>
    </div>`
});
