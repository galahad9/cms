var name = 'Testimonial #8';
var image = 'icons/testimonial.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="testimonial8">
        <div class="owl-carousel owl-theme testi8 text-center">
            <!-- item -->
            <div class="item">
                <div class="bg" style="background-image:url(/cms/assets/wrapkit/images/testimonial/background1.jpg)">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 col-md-10">
                                <h2 class="text-white">"Needless to say we are extremely satisfied with the results. Thank you for making it painless, pleasant and most of all hassle free!"</h2>
                                <a href="#" class="play-icon m-t-20 db" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-play-circle"></i></a>
                                <h5 class="text-white m-b-0 m-t-30">Mark Freeman</h5>
                                <span class="text-white font-13">VP of Design at xyz inc.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- item -->
            <div class="item">
                <div class="bg" style="background-image:url(/cms/assets/wrapkit/images/testimonial/background1.jpg)">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 col-md-10">
                                <h2 class="text-white">"Needless to say we are extremely satisfied with the results. Thank you for making it painless, pleasant and most of all hassle free!"</h2>
                                <a href="#" class="play-icon m-t-20 db" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-play-circle"></i></a>
                                <h5 class="text-white m-b-0 m-t-30">Mark Freeman</h5>
                                <span class="text-white font-13">VP of Design at xyz inc.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- item -->
            <!-- item -->
            <div class="item">
                <div class="bg" style="background-image:url(/cms/assets/wrapkit/images/testimonial/background1.jpg)">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 col-md-10">
                                <h2 class="text-white">"Needless to say we are extremely satisfied with the results. Thank you for making it painless, pleasant and most of all hassle free!"</h2>
                                <a href="#" class="play-icon m-t-20 db" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-play-circle"></i></a>
                                <h5 class="text-white m-b-0 m-t-30">Mark Freeman</h5>
                                <span class="text-white font-13">VP of Design at xyz inc.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- item -->
        </div>
        <!-- Popup -->
        <div class="modal fade" id="exampleModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Watch video</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="yt-player">
                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/DDwbjWCgxVM?" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!-- Popup -->
    </div>`
});
