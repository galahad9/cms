var name = 'Testimonial #3';
var image = 'icons/testimonial.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="testimonial3 spacer bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <span class="label label-success label-rounded">Testimonial 1</span>
                    <h2 class="title">Check what our Customers are Saying</h2>
                    <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
                </div>
            </div>
            <!-- Row  -->
            <div class="owl-carousel owl-theme testi3 m-t-40">
                <!-- item -->
                <div class="item" data-aos="fade-right">
                    <div class="card card-shadow">
                        <div class="card-body">
                            <h6 class="font-light m-b-30">“Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras venene veliel vestibulum.”</h6>
                            <div class="d-flex no-block align-items-center">
                                <span class="thumb-img"><img src="/cms/assets/wrapkit/images/testimonial/1.jpg" alt="wrapkit" class="circle"/></span>
                                <div class="m-l-20">
                                    <h6 class="m-b-0 customer">Michelle Anderson</h6>
                                    <div class="font-10">
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-muted"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item" data-aos="fade-up">
                    <div class="card card-shadow">
                        <div class="card-body">
                            <h6 class="font-light m-b-30">“Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras venene veliel vestibulum.”</h6>
                            <div class="d-flex no-block align-items-center">
                                <span class="thumb-img"><img src="/cms/assets/wrapkit/images/testimonial/2.jpg" alt="wrapkit" class="circle"/></span>
                                <div class="m-l-20">
                                    <h6 class="m-b-0 customer">Mark mesty</h6>
                                    <div class="font-10">
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-muted"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item" data-aos="fade-left">
                    <div class="card card-shadow">
                        <div class="card-body">
                            <h6 class="font-light m-b-30">“Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras venene veliel vestibulum.”</h6>
                            <div class="d-flex no-block align-items-center">
                                <span class="thumb-img"><img src="/cms/assets/wrapkit/images/testimonial/3.jpg" alt="wrapkit" class="circle"/></span>
                                <div class="m-l-20">
                                    <h6 class="m-b-0 customer">Limpsy adam</h6>
                                    <div class="font-10">
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-muted"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item">
                    <div class="card card-shadow">
                        <div class="card-body">
                            <h6 class="font-light m-b-30">“Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras venene veliel vestibulum.”</h6>
                            <div class="d-flex no-block align-items-center">
                                <span class="thumb-img"><img src="/cms/assets/wrapkit/images/testimonial/1.jpg" alt="wrapkit" class="circle"/></span>
                                <div class="m-l-20">
                                    <h6 class="m-b-0 customer">Michelle Anderson</h6>
                                    <div class="font-10">
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-success"><i class="fa fa-star"></i></a>
                                        <a href="" class="text-muted"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- item -->
            </div>
        </div>
    </div>`
});
