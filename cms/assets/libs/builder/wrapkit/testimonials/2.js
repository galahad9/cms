var name = 'Testimonial #2';
var image = 'icons/testimonial.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="testimonial2 spacer">
        <div class="container">
            <div class="heading">
            </div>
            <div class="owl-carousel owl-theme testi2 m-t-40">
                <div class="item">
                    <div class="row po-relative">
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <button class="btn btn-circle btn-danger btn-md"><i class="fa fa-quote-left"></i></button>
                            <h3 class="m-t-20 m-b-20">Customer Reviews</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>
                            <h5 class="m-t-30">Simon Duo</h5>
                            <h6 class="subtitle">Partner, Brevin</h6>
                        </div>
                        <div class="col-lg-6 col-md-6 image-thumb hidden-md-down">
                            <img src="/cms/assets/wrapkit/images/testimonial/1.jpg" alt="wrapkit" class="circle img-fluid" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row po-relative">
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <button class="btn btn-circle btn-danger btn-md"><i class="fa fa-quote-left"></i></button>
                            <h3 class="m-t-20 m-b-20">Customer Reviews</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>
                            <h5 class="m-t-30">Simon Duo</h5>
                            <h6 class="subtitle">Partner, Brevin</h6>
                        </div>
                        <div class="col-lg-6 col-md-6 image-thumb hidden-md-down">
                            <img src="/cms/assets/wrapkit/images/testimonial/2.jpg" alt="wrapkit" class="circle img-fluid" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row po-relative">
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <button class="btn btn-circle btn-danger btn-md"><i class="fa fa-quote-left"></i></button>
                            <h3 class="m-t-20 m-b-20">Customer Reviews</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>
                            <h5 class="m-t-30">Simon Duo</h5>
                            <h6 class="subtitle">Partner, Brevin</h6>
                        </div>
                        <div class="col-lg-6 col-md-6 image-thumb hidden-md-down">
                            <img src="/cms/assets/wrapkit/images/testimonial/3.jpg" alt="wrapkit" class="circle img-fluid" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`
});
