var name = 'Testimonial #5';
var image = 'icons/testimonial.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="testimonial5 spacer bg-inverse">
        <div class="container">
            <!-- Row -->
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <span class="label label-success label-rounded">Testimonial 5</span>
                    <h2 class="title text-white">Few Words from our Valuable Customers</h2>
                    <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
                </div>
            </div>
            <!-- Row -->
            <div class="owl-carousel owl-theme testi5 m-t-40 text-center">
                <!-- item -->
                <div class="item">
                    <div class="content">“It was incredibly refreshing to work with an agency that focused on our needs as business.”</div>
                    <div class="customer-thumb">
                        <img src="/cms/assets/wrapkit/images/testimonial/1.jpg" alt="wrapkit" class="circle" />
                        <h6 class="text-white m-b-0">Erin Wilson</h6>
                        <p>Marketing manager</p>
                    </div>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item">
                    <div class="content">“It was incredibly refreshing to work with an agency that focused on our needs as business.”</div>
                    <div class="customer-thumb">
                        <img src="/cms/assets/wrapkit/images/testimonial/2.jpg" alt="wrapkit" class="circle" />
                        <h6 class="text-white m-b-0">Erin Wilson</h6>
                        <p>Marketing manager</p>
                    </div>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item">
                    <div class="content">“It was incredibly refreshing to work with an agency that focused on our needs as business.”</div>
                    <div class="customer-thumb">
                        <img src="/cms/assets/wrapkit/images/testimonial/3.jpg" alt="wrapkit" class="circle" />
                        <h6 class="text-white m-b-0">Erin Wilson</h6>
                        <p>Marketing manager</p>
                    </div>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item">
                    <div class="content">“It was incredibly refreshing to work with an agency that focused on our needs as business.”</div>
                    <div class="customer-thumb">
                        <img src="/cms/assets/wrapkit/images/testimonial/3.jpg" alt="wrapkit" class="circle" />
                        <h6 class="text-white m-b-0">Erin Wilson</h6>
                        <p>Marketing manager</p>
                    </div>
                </div>
                <!-- item -->
            </div>
        </div>
    </div>`
});
