var name = 'Testimonial #1';
var image = 'icons/testimonial.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="testimonial1 spacer bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <span class="label label-success label-rounded">Testimonial 1</span>
                    <h2 class="title">Check what our Customers are Saying</h2>
                    <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
                </div>
            </div>
            <!-- Row  -->
            <div class="owl-carousel owl-theme testi1 m-t-40">
                <!-- item -->
                <div class="item">
                    <div class="card card-shadow">
                        <div class="card-body">
                            <div class="thumb bg-success-gradiant"><img src="/cms/assets/wrapkit/images/testimonial/1.jpg" alt="wrapkit" class="thumb-img circle" /> Michelle Anderson</div>
                            <h5 class="font-light">You can relay on our amazing features list and also our customer services will be great experience. You can relay on our amazing features.</h5>
                            <span class="devider"></span>
                            <h6>Managing Director, Theme Designer</h6>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item">
                    <div class="card card-shadow">
                        <div class="card-body">
                            <div class="thumb bg-success-gradiant"><img src="/cms/assets/wrapkit/images/testimonial/2.jpg" alt="wrapkit" class="thumb-img circle" /> Michelle Anderson</div>
                            <h5 class="font-light">You can relay on our amazing features list and also our customer services will be great experience. You can relay on our amazing features.</h5>
                            <span class="devider"></span>
                            <h6>CEO, Theme Designer</h6>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item">
                    <div class="card card-shadow">
                        <div class="card-body">
                            <div class="thumb bg-success-gradiant"><img src="/cms/assets/wrapkit/images/testimonial/3.jpg" alt="wrapkit" class="thumb-img circle" /> Michelle Anderson</div>
                            <h5 class="font-light">You can relay on our amazing features list and also our customer services will be great experience. You can relay on our amazing features.</h5>
                            <span class="devider"></span>
                            <h6>Managing Director, Theme Designer</h6>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item">
                    <div class="card card-shadow">
                        <div class="card-body">
                            <div class="thumb bg-success-gradiant"><img src="/cms/assets/wrapkit/images/testimonial/1.jpg" alt="wrapkit" class="thumb-img circle" /> Michelle Anderson</div>
                            <h5 class="font-light">You can relay on our amazing features list and also our customer services will be great experience. You can relay on our amazing features.</h5>
                            <span class="devider"></span>
                            <h6>Managing Director, Theme Designer</h6>
                        </div>
                    </div>
                </div>
                <!-- item -->
            </div>
        </div>
    </div>`
});
