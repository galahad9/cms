var name = 'Testimonial #10';
var image = 'icons/testimonial.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="testimonial10 spacer bg-success-gradiant">
        <div class="container">
            <div class="owl-carousel owl-theme text-center testi10">
                <!-- item -->
                <div class="item">
                    <div class="quote-bg">
                        <h3 class="font-light text-white">WrapKit has given our websites huge national presence.We are #1 on page one in Google search results for every website we’ve built, and rank for more keywords than I ever expected in a very competitive, high-value customer industry. In addition</h3>
                    </div>
                    <div class="customer-thumb"><img src="/cms/assets/wrapkit/images/testimonial/1.jpg" alt="wrapkit" class="circle" /></div>
                    <h6 class="text-white m-b-0 font-medium">Michael Anderson</h6>
                    <span class="text-white">Project client</span>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item">
                    <div class="quote-bg">
                        <h3 class="font-light text-white">WrapKit has given our websites huge national presence.We are #1 on page one in Google search results for every website we’ve built, and rank for more keywords than I ever expected in a very competitive, high-value customer industry. In addition</h3>
                    </div>
                    <div class="customer-thumb"><img src="/cms/assets/wrapkit/images/testimonial/2.jpg" alt="wrapkit" class="circle" /></div>
                    <h6 class="text-white m-b-0 font-medium">Michael Anderson</h6>
                    <span class="text-white">Project client</span>
                </div>
                <!-- item -->
                <!-- item -->
                <div class="item">
                    <div class="quote-bg">
                        <h3 class="font-light text-white">WrapKit has given our websites huge national presence.We are #1 on page one in Google search results for every website we’ve built, and rank for more keywords than I ever expected in a very competitive, high-value customer industry. In addition</h3>
                    </div>
                    <div class="customer-thumb"><img src="/cms/assets/wrapkit/images/testimonial/3.jpg" alt="wrapkit" class="circle" /></div>
                    <h6 class="text-white m-b-0 font-medium">Michael Anderson</h6>
                    <span class="text-white">Project client</span>
                </div>
                <!-- item -->
            </div>
        </div>
    </div>`
});
