/*
Copyright 2017 Ziadin Givan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

https://github.com/givanz/VvvebJs
*/
var name = 'Banner #2';
var image = 'icons/map.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="static-slider7" data-slider7 style="background-image:url(/cms/assets/wrapkit/images/sliders/static-slider/slider7/img1.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="col-md-8 align-self-center text-center" data-aos="fade-right" data-aos-duration="1200">
                <h1 class="title text-white typewrite" data-typewrite data-period="2000" data-type='[ "Colour", "Wrapkit" ]'></h1>
                <h4 class="text-white">Awesome Extra Ordinary Flexibility</h4>
                <a class="btn btn-danger-gradiant btn-rounded btn-md btn-arrow m-t-20" data-toggle="collapse" href=""><span>Products <i class="ti-arrow-right"></i></span></a>
                <a class="btn btn-outline-light btn-rounded btn-md m-t-20" data-toggle="modal" data-target="#static-slide7"  href=""><i class="fa fa-play m-r-10"></i> Intro </a>
            </div>
            <div class="modal fade" id="static-slide7">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Watch video</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body" id="yt-player">
                            <iframe class="iframe-youtube" width="100%" height="315" src="https://www.youtube.com/embed/DDwbjWCgxVM?" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`,
    attributes: ["data-slider7"],
	afterDrop: function (node) {
		var element = node.find('.typewrite')[0];
		var toRotate = element.getAttribute('data-type');
        var period = element.getAttribute('data-period');
        if (toRotate) {
        	 new TxtType(element, JSON.parse(toRotate), period);
        }
        
        return node;
	},
	// init: function (node) {},
	// beforeInit: function (node) {},
	// onChange: function (node, property, value) {},
	properties: [{
        name: "Typewriter",
        key: "banner-typewriter",
        htmlAttr: "data-type",
        child: ".typewrite",
        inputtype: TextInput
    },{
        name: "Youtube video",
        key: "youtube-url",
        htmlAttr: "src",
        child: ".iframe-youtube",
        inputtype: TextInput
    }, {
        name: "Background Image",
        key: "background-image",
		child: ".static-slider7",
        inputtype: ImageInput,
		onChange: function(node, value) {
			
			$(node).css('background-image', 'url(' + value + ')');
			
			return node;
		}        

   	}],
});
