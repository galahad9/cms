/*
Copyright 2017 Ziadin Givan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

https://github.com/givanz/VvvebJs
*/
var name = 'Banner #1';
var image = 'icons/map.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="static-slider2 spacer">
        <div class="container">
            <!-- Row  -->
            <div class="row">
                <!-- Column -->
                <div class="col-md-5 align-self-center" data-aos="fade-right" data-aos-duration="1200">
                    <h1 class="title" data-typewrite ><span class="typewrite" data-period="2000" data-type='[ "Creative", "Jquery", "Design", "Angular" ]'></span><br/>Done<span class="text-megna">.</span></h1>
                    <h6 class="subtitle">I’m John Doe, a Web & Mobile App Designer,Specialize in Website Design, Illustration& Mobile Application Work.</h6>
                    <a class="btn btn-success-gradiant btn-md btn-arrow m-t-20 m-b-10" data-toggle="collapse" href=""><span>Hire Me <i class="ti-arrow-right"></i></span></a>
                    <a class="btn btn-link btn-md btn-arrow m-t-20 m-b-10 font-medium" data-toggle="collapse" href=""><span class="underline">Check my work <i class="ti-arrow-right"></i></span></a>
                </div>
                <!-- Column -->
                <div class="col-md-7 img-anim" data-aos="fade-up" data-aos-duration="2200">
                    <img src="/cms/assets/wrapkit/images/sliders/static-slider/slider2/img1.jpg" alt="wrapkit" class="img-fluid m-t-20"/>
                </div>
            </div>
        </div>
    </div>`,
    attributes: ["data-typewrite"],
	afterDrop: function (node) {
		var element = node.find('.typewrite')[0];
		var toRotate = element.getAttribute('data-type');
        var period = element.getAttribute('data-period');
        if (toRotate) {
        	 new TxtType(element, JSON.parse(toRotate), period);
        }
        
        return node;
	},
	// init: function (node) {},
	// beforeInit: function (node) {},
	// onChange: function (node, property, value) {},
	properties: [{
        name: "Typewriter",
        key: "banner1-typewriter",
        htmlAttr: "data-type",
        child: ".typewrite",
        inputtype: TextInput
    }],
});
