var name = 'Navigation #10';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header14 po-relative">
        <!-- Topbar  -->
        <div class="h14-topbar">
            <div class="container">
                <nav class="navbar navbar-expand-lg font-14">
                    <a class="navbar-brand hidden-lg-up" href="#">Top Menu</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header14a" aria-controls="header14a" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sl-icon-options"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="header14a">
                        <ul class="navbar-nav">
                            <li class="nav-item"><a class="nav-link">We build awesome kits like this one here!</a></li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link"><i class="fa fa-clock-o"></i> Mon to Sat - 9:00-18:00 | Sunday - Closed</a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- Infobar  -->
        <div class="h14-infobar">
            <div class="container">
                <nav class="navbar navbar-expand-lg h14-info-bar">
                    {{ LOGO }}
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h14-info" aria-controls="h14-info" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sl-icon-options-vertical"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="h14-info">
                        <ul class="navbar-nav ml-auto text-uppercase">
                            <li class="nav-item">
                                <a class="nav-link" href="mailto:{{ Emailaddress }}">
                                    <div class="display-6 m-r-10"><i class="icon-Mail"></i></div>
                                    <div><small>Email us at</small>
                                        <h6 class="font-bold">{{ Emailaddress }}</h6></div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="tel:{{ Phonenumber }}">
                                    <div class="display-6 m-r-10"><i class="icon-Phone-2"></i></div>
                                    <div><small>CALL US NOW</small>
                                        <h6 class="font-bold">{{ Phonenumber }}</h6></div>
                                </a>
                            </li>
                            <li class="nav-item donate-btn"><a href="#" class="btn btn-outline-info">Request a Quote</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- Navbar  -->
        <div class="h14-navbar">
            <div class="container">
                <nav class="navbar navbar-expand-lg h14-nav">
                    <a class="hidden-lg-up">Navigation</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header14" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="ti-menu"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="header14">
                        <div class="hover-dropdown">
                            <ul class="navbar-nav">
                               {{ MENU }}
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>`
});
