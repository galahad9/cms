var name = 'Navigation #7';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header11 po-relative">
        <div class="container">
            <!-- Header 1 code -->
            <nav class="navbar navbar-expand-lg h11-nav">
                {{ LOGO }}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header11" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>
                <div class="collapse navbar-collapse hover-dropdown flex-column" id="header11">
                    <div class="ml-auto h11-topbar">
                        <ul class="list-inline ">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a><i class="icon-Phone-2"></i> 215 123 4567</a></li>
                        </ul>
                    </div>
                    <ul class="navbar-nav ml-auto font-13">
                       {{ MENU }}
                    </ul>
                </div>
            </nav>
        </div>
    </div>`
});
