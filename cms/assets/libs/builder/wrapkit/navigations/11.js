var name = 'Navigation #11';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header15 po-relative">
        <!-- Topbar  -->
        <div class="h15-topbar">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand hidden-lg-up" href="#">Top Menu</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header15a" aria-controls="header15a" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sl-icon-options"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="header15a">
                        <ul class="navbar-nav font-14">
                            <li class="nav-item">Opening Hours : <span class="text-dark">Monday</span> to <span class="text-dark">Saturday</span> - <span class="text-dark">8am to 9pm</span></li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-instagram"></i></a></li>
                           
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- Infobar  -->
        <div class="h15-infobar">
            <div class="container">
                <nav class="navbar navbar-expand-lg h15-info-bar">
                    {{ LOGO }}
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h15-info" aria-controls="h15-info" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sl-icon-options-vertical"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="h15-info">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link">
                                    <div class="display-6 m-r-10"><i class="icon-Location-2"></i></div>
                                    <div><small>{{ Street }}<br/>{{ Zipcode }}  {{ City }}</small></div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">
                                    <div class="display-6 m-r-10"><i class="icon-Phone-2"></i></div>
                                    <div><small>GET APPOINTMENT</small>
                                        <h5 class="font-bold">{{ Phonenumber }}</h5></div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- Navbar  -->
        <div class="h15-navbar">
            <div class="container">
                <nav class="navbar navbar-expand-lg h15-nav">
                    <a class="hidden-lg-up">Navigation</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header15" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="ti-menu"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="header15">
                        <div class="hover-dropdown">
                            <ul class="navbar-nav">
                                {{ MENU }}
                            </ul>
                        </div>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item search"><a class="nav-link" href="javascript:void(0)"><i class="fa fa-calendar m-r-10"></i> Book an Appointment</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>`
});
