var name = 'Navigation #5';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header">
        <div class="h6-topbar bg-light">
            <div class="container">
                <div class="d-flex justify-content-between font-14">
                    <div class=" hidden-md-down align-self-center">
                        Welcome to {{ Website Name }}
                    </div>
                    <div class="">
                        <ul class="list-inline authentication-box">
                            <li><a href="">Login </a></li>
                            <li><a href="" class="b-l">Create Account </a></li>
                            <li><a href=""><i class="icon-Shopping-Cart"></i> </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container po-relative">
            <nav class="navbar navbar-expand-lg h6-nav-bar">
                {{ LOGO }}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h6-info1" aria-controls="h6-info" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>
                <div class="collapse navbar-collapse hover-dropdown font-14" id="h6-info1">
                    <ul class="navbar-nav">
                        {{ MENU }}
                    </ul>
                    <div class="ml-auto act-buttons">
                        <button class="btn btn-secondary font-13">Contact Us</button>
                        <button class="btn btn-danger-gradiant font-14">Sign Up</button>
                    </div>
                </div>
            </nav>
        </div>
    </div>`
});
