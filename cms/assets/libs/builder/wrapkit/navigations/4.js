var name = 'Navigation #4';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header5">
        <div class="container po-relative">
            <nav class="navbar navbar-expand-lg h5-nav-bar">
                {{ LOGO }}
                <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#h5-info" aria-controls="h4-info" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>
                <div class="collapse navbar-collapse text-uppercase" id="h5-info">
                    <ul class="navbar-nav m-auto">
                       {{ MENU }}
                    </ul>
                    <div class="rounded-button">
                        <a href="#">Login</a> - OR - <a href="#">Register</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>`
});
