var name = 'Navigation #12';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header16 po-relative">
        <!-- Topbar  -->
        <div class="h16-topbar">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand hidden-lg-up" href="#">Top Menu</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header16a" aria-controls="header16a" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sl-icon-options"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="header16a">
                        <ul class="navbar-nav font-14">
                            <li class="nav-item">Welcome to {{ Website Name }}</li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-envelope"></i> info@wrappixel.com</a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- Infobar  -->
        <div class="h16-infobar">
            <div class="container">
                <nav class="navbar navbar-expand-lg h16-info-bar">
                    <a class="navbar-brand"><img src="../assets/images/logos/red-logo-text.jpg" alt="wrapkit"/></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h16-info" aria-controls="h16-info" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sl-icon-options-vertical"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="h16-info">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link">
                                    <div class="display-6 m-r-10"><i class="icon-Mail text-danger"></i></div>
                                    <div><small>Mail us</small>
                                        <h5 class="font-bold">{{ Emailaddress }}</h5></div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="tel:{{ Phonenumber }}">
                                    <div class="display-6 m-r-10"><i class="icon-Phone-2 text-danger"></i></div>
                                    <div><small>Book Table</small>
                                        <h5 class="font-bold">{{ Phonenumber }}</h5></div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- Navbar  -->
        <div class="h16-navbar">
            <div class="container">
                <nav class="navbar navbar-expand-lg h16-nav">
                    <a class="hidden-lg-up">Navigation</a>
                    <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#header16" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="ti-menu"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="header16">
                        <div class="hover-dropdown">
                            <ul class="navbar-nav">
                               {{ MENU }}
                            </ul>
                        </div>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item search"><a class="nav-link" href="javascript:void(0)">RESERVE TABLE</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>`
});
