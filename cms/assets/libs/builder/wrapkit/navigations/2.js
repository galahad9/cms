var name = 'Navigation #2';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header2">
        <div class="container">
            <nav class="navbar navbar-expand-lg h2-nav">
                {{ LOGO }}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header2" aria-controls="header2" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="ti-menu"></span>
                </button>
                <div class="collapse navbar-collapse hover-dropdown" id="header2">
                    <ul class="navbar-nav">
                        {{ MENU }}
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active"><a class="nav-link" href="#"><i class="fa fa-comments"></i> Need help?</a></li>
                        <li class="nav-item active"><a class="nav-link" href="#">Login</a></li>
                        <li class="nav-item"><a class="btn btn-rounded btn-dark" href="#">Sign up</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>`
});
