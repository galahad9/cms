var name = 'Navigation #6';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header7">
        <div class="container po-relative">
            <div class="h7-topbar">
                <div class="d-flex justify-content-between font-14">
                    <div class=" hidden-md-down align-self-center">
                        Welcome to {{ Website Name }}
                    </div>
                    <div class="con-btn">
                        <a class="btn btn-success-gradiant btn-rounded btn-sm" data-toggle="collapse" href="">Free consulations</a>
                    </div>
                </div>
            </div>
            <div class="h7-nav-bar">
                <div class="logo-box">{{ LOGO </div>
                <button class="btn btn-success btn-circle hidden-md-up op-clo"><i class="ti-menu"></i></button>
                <nav class="h7-nav-box">
                    <div class="h7-mini-bar">
                        <div class="d-flex justify-content-between">
                            <div class="gen-info font-14">
                                <span><i class="fa fa-envelope text-success-gradiant"></i> {{ Emailaddress }}</span>
                                <span><i class="fa fa-phone-square text-success-gradiant"></i> {{ Phonenumber }}</span>
                                <span class="hidden-lg-down"><i class="fa fa-clock-o text-success-gradiant"></i> 8.00 AM - 6:00PM</span>
                            </div>
                            <div class="social-info hidden-lg-down">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-youtube-play"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="main-nav">
                        <ul>
                            {{ MENU }}
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>`
});
