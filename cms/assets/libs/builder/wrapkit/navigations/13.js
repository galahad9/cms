var name = 'Navigation #13';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header17">
        <div class="container">
            <!-- Header 13 navabar -->
            <nav class="h17-nav">
                <div class="">
                    {{ LOGO }}
                    <div class="call-info dl hidden-md-down">
                        <small class="text-muted">CALL US ON</small>
                        <h5 class="m-b-0 font-medium">{{ Phonenumber }}</h5>
                    </div>
                </div>
                <div class="ml-auto align-self-center">
                    <ul class="list-inline h17nav-bar">
                        <li><a class="btn btn-success-gradiant hidden-md-down text-white" href="#">Get Free Quote</a></li>
                        <li><a class="nav-link tgl-cl" href="javascript:void(0)"><i class="ti-menu "></i></a></li>
                    </ul>
                </div>
            </nav>
            <!-- End Header 13 navabar -->
        </div>
        <div class="h17-main-nav animated fadeInDown justify-content-center" data-aos="fade-up">
            <div class="h-17navbar align-self-center">
                <a href="javascript:void(0)" class="close-icons tgl-cl"><i class="ti-close"></i></a>
                <ul class="nav-menu">
                    {{ MENU }}
                </ul>
                <ul class="info-nav">
                    <li class="half-width"><a href="mailto:{{ Emailaddress }}"><i class="fa fa-envelope text-danger"></i> {{ Emailaddress }}</a></li>
                    <li class="half-width"><a href="tel:{{ Phonenumber }}"><i class="fa fa-phone text-danger"></i> {{ Phonenumber }}</a></li>
                </ul>
                <ul class="social-nav">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div>
        </div>
    </div>`
});
