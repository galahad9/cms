var name = 'Navigation #3';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header4 po-relative">
    <div class="h4-topbar">
        <div class="container">
            <nav class="navbar navbar-expand-lg h4-nav">
                <a class="hidden-lg-up">Navigation</a>
                <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#header4" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="ti-menu"></span>
                </button>
                <div class="collapse navbar-collapse" id="header4">
                    <div class="hover-dropdown">
                        <ul class="navbar-nav">
                            {{ MENU }}
                        </ul>
                    </div>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item search dropdown"><a class="nav-link dropdown-toggle" href="javascript:void(0)" id="h4-sdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i></a>
                            <div class="dropdown-menu b-none dropdown-menu-right animated fadeInDown" aria-labelledby="h4-sdropdown">
                                <input class="form-control" type="text" placeholder="Type & hit enter" />
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <div class="h4-navbar">
        <div class="container">
            <nav class="navbar navbar-expand-lg h4-nav-bar">
                {{ LOGO }}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h4-info" aria-controls="h4-info" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sl-icon-options-vertical"></span>
                </button>
                <div class="collapse navbar-collapse" id="h4-info">
                    <ul class="navbar-nav ml-auto text-uppercase">
                        <li class="nav-item">
                            <a class="nav-link">
                                <div class="display-6 m-r-10"><i class="icon-Mail"></i></div>
                                <div><small>Email us at</small>
                                    <h6 class="font-bold">{{ Emailaddress }}</h6></div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">
                                <div class="display-6 m-r-10"><i class="icon-Phone-2"></i></div>
                                <div><small>CALL US NOW</small>
                                    <h6 class="font-bold">{{ Phonenumber /h6></div>
                            </a>
                        </li>
                        <li class="nav-item donate-btn"><a href="#" class="btn btn-outline-danger">Donate now</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>`
});
