var name = 'Navigation #9';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header13 po-relative">
        <!-- Header 13 topbar -->
        <div class="h13-topbar">
            <div class="container">
                <nav class="navbar navbar-expand-lg font-14">
                    <a class="navbar-brand hidden-lg-up" href="#">Top Menu</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header13a" aria-controls="header13a" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sl-icon-options"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="header13a">
                        <ul class="navbar-nav">
                            <li class="nav-item"><a class="nav-link" href="mailto:{{ Emailaddress }}"><i class="fa fa-envelope"></i> {{ Emailaddress }}</a></li>
                            <li class="nav-item"><a class="nav-link"><i class="fa fa-clock-o"></i> 8.00AM - 6:00PM</a></li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- Header 13 topbar -->
        <div class="container">
            <!-- Header 13 navabar -->
            <nav class="navbar navbar-expand-lg hover-dropdown h13-nav">
                {{ LOGO }}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header13" aria-controls="header13" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="ti-menu"></span>
                </button>
                <div class="collapse navbar-collapse" id="header13">
                    <div class="call-info hidden-md-down">
                        <small class="text-muted">CALL US ON</small>
                        <h5 class="m-b-0 font-medium">{{ Phonenumber }}</h5>
                    </div>
                    <ul class="navbar-nav ml-auto">
                        {{ MENU }}
                    </ul>
                </div>
            </nav>
            <!-- End Header 13 navabar -->
        </div>
    </div>`
});
