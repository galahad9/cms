var name = 'Navigation #1';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header1 header-position">
    <div class="container">
        <!-- Header 1 code -->
        <nav class="navbar navbar-expand-lg h1-nav">
           	{{ LOGO }}
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header1" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ti-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="header1">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    {{ MENU }}
    				<li class="nav-item"><a class="btn btn-outline-success" href="#">Hire Me</a></li>
                </ul>
            </div>
        </nav>
        <!-- End Header 1 code -->
        <div class="row">
            <div class="col-md-12 text-center spacer">
            </div>
        </div>
    </div>
</div>`
});
