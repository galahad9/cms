var name = 'Navigation #8';
var image = 'icons/menu.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="header12 po-relative">
        <!-- Header 12 topbar -->
        <div class="h12-topbar">
            <div class="container">
                <nav class="navbar navbar-expand-lg font-14">
                    <a class="navbar-brand hidden-lg-up" href="#">Top Menu</a>
                    <button class="navbar-toggler text-white op-5" type="button" data-toggle="collapse" data-target="#header12" aria-controls="header12" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sl-icon-options-vertical"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="header12">
                        <ul class="navbar-nav">
                            <li class="nav-item active"><a class="nav-link" href="#">Login</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Register</a></li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active"><a class="nav-link" href="mailto:{{ Emailaddress }}"><i class="fa fa-envelope"></i> {{ Emailaddress }}</a></li>
                            <li class="nav-item active"><a class="nav-link" href="tel:{{ Phonenumber }}"><i class="icon-Phone-2"></i> {{ Phonenumber }}</a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- Header 12 topbar -->
        <div class="container">
            <!-- Header 12 navabar -->
            <nav class="navbar navbar-expand-lg hover-dropdown h12-nav">
                {{ LOGO }}
                <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#header12" aria-controls="header12" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="ti-menu"></span>
                </button>
                <div class="collapse navbar-collapse" id="header12-1">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        {{ MENU }}
                    </ul>
                </div>
            </nav>
            <!-- End Header 12 navabar -->
        </div>
    </div>`
});
