var name = 'Slider #9';
var image = 'icons/slider.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light">
    <section id="slider-sec" class="slider9">
        <div id="slider9" class="carousel bs-slider slide  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="7000">
            <ol class="carousel-indicators">
                <li data-target="#slider9" data-slide-to="0" class="active"></li>
                <li data-target="#slider9" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="row bg-info">
                        <div class="col-md-7 d-block d-md-none">
                            <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider9/slide1.jpg" alt="We are Digital Agency" class="slide-image" /> </div>
                        <div class="col-md-5">
                            <!-- Slide Text Layer -->
                            <div class="slide-text slide_style_left">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-11 col-lg-9 ml-auto">
                                            <h2 data-animation="animated fadeInLeft" class="title text-white font-bold m-0 p-b-40">Dont Worry for your Accounting, Leave that to us.</h2>
                                            <p class="p-0 m-b-40 hidden-md-down" data-animation="animated fadeInUp">We offer services in various jonar of Accounting. Let us take your all worry regarding your daily accounting and take rest.</p>
                                            <a class="btn text-white btn-md btn-arrow" data-animation="animated fadeInLeft" data-toggle="collapse" href="#"> <span>Ask for Questions <i class="ti-arrow-right"></i></span> </a>
                                            <p class="font-14 p-t-30 m-0">Do you have any Query? <a href="#" class="text-white">Contact Us</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 d-none d-none d-md-block">
                            <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider9/slide1.jpg" alt="We are Digital Agency" class="slide-image" /> </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <div class="carousel-item">
                    <div class="row bg-info">
                        <div class="col-md-7 d-block d-md-none">
                            <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider9/slide2.jpg" alt="We are Digital Agency" class="slide-image" /> </div>
                        <div class="col-md-5">
                            <!-- Slide Text Layer -->
                            <div class="slide-text slide_style_left">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-11 col-lg-9 ml-auto">
                                            <h2 data-animation="animated fadeInLeft" class="title text-white font-bold m-0 p-b-40">Accounting Made Easy, Fast & Reliable with WrapKit Project</h2>
                                            <p class="p-0 m-b-40 hidden-md-down" data-animation="animated fadeInUp">We offer services in various jonar of Accounting. Let us take your all worry regarding your daily accounting and take rest.</p>
                                            <a class="btn text-white btn-md btn-arrow" data-animation="animated fadeInLeft" data-toggle="collapse" href="#"> <span>Ask for Question <i class="ti-arrow-right"></i></span> </a>
                                            <p class="font-14 p-t-30 m-0">Do you have any Query? <a href="#" class="text-white">Contact Us</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 d-none d-md-block">
                            <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider9/slide2.jpg" alt="We are Digital Agency" class="slide-image" /> </div>
                    </div>
                </div>
                <!-- Second Slide -->
                <!-- End of Slide -->
                <!-- End of Wrapper For Slides -->
            </div>
        </div>
        <!-- End Slider -->
    </section>
</div>`
});
