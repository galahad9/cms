var name = 'Slider #5';
var image = 'icons/slider.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light">
    <section id="slider-sec" class="slider5">
        <div id="slider5" class="carousel bs-slider control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="7000">
            <ol class="carousel-indicators">
                <li data-target="#slider5" data-slide-to="0" class="active"></li>
                <li data-target="#slider5" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider5/slide1.jpg" alt="We are Digital Agency" class="slide-image" />
                    <div class="bs-slider-overlay"></div>
                    <!-- Slide Text Layer -->
                    <div class="slide-text slide_style_left">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 col-lg-5 ml-auto">
                                    <h2 data-animation="animated zoomInRight" class="title text-uppercase font-bold m-0 p-b-20">Friut Salad</h2>
                                    <p data-animation="animated lightSpeedIn" class="m-b-40 d-none d-md-block">Not like the brazen giant of Greek fame With conquering limbs astride from land to land Here</p>
                                    <a class="btn btn-outline-danger btn-md btn-rounded btn-arrow" data-animation="animated fadeInRight" data-toggle="collapse" href="#"> <span>Today's Special <i class="ti-arrow-right"></i></span> </a>
                                    <div class="promo-code" data-animation="animated fadeInUp">
                                        <h3 class="title font-semibold text-uppercase m-b-0">Hot Summer - 35 % OFF</h3>
                                        <p>Apply promocode '154FEB'
                                            <p>
                                    </div>
                                </div>
                                <div class="col-lg-1 hidden-md-down"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- Second Slide -->
                <div class="carousel-item">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider5/slide2.jpg" alt="Building Magical Apps" class="slide-image" />
                    <div class="bs-slider-overlay"></div>
                    <!-- Slide Text Layer -->
                    <div class="slide-text">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 col-lg-5 ml-auto">
                                    <h2 data-animation="animated flipInX" class="title text-uppercase font-bold m-0 p-b-20">Enjoy Delicious Food Now</h2>
                                    <p data-animation="animated lightSpeedIn" class="m-b-40 d-none d-md-block">Not like the brazen giant of Greek fame With conquering limbs astride from land to land Here </p>
                                    <a class="btn btn-danger btn-md btn-rounded btn-arrow" data-animation="animated fadeInRight" data-toggle="collapse" href="#"> <span>Today's Special <i class="ti-arrow-right"></i></span> </a>
                                    <div class="promo-code" data-animation="animated fadeInUp">
                                        <h3 class="title font-semibold text-uppercase m-b-0">Hot Summer - 35 % OFF</h3>
                                        <p>Apply promocode '154FEB'
                                            <p>
                                    </div>
                                </div>
                                <div class="col-lg-1 hidden-md-down"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>`
});
