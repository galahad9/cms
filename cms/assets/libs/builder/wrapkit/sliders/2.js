var name = 'Slider #2';
var image = 'icons/slider.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light">
    <section id="slider-sec" class="slider2">
        <div id="slider2" class="carousel bs-slider slide  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="7000">
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider2/slide1.jpg" alt="We are Digital Agency" class="slide-image" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text slide_style_left">
                        <div class="col-md-9 col-lg-6">
                            <label class="label label-warning text-uppercase font-semibold" data-animation="animated fadeInDown">Welcome To</label>
                            <h2 data-animation="animated zoomInRight" class="title text-white">Machinery and Hardware Company, which makes Quality Products</h2> </div>
                        <div class="col-md-8 col-lg-5">
                            <p data-animation="animated fadeInLeft" class="m-t-40 m-b-40 hidden-sm-down">We offer services in various jonar of Accounting. Let us take your all worry regarding your daily accounting and take rest.</p>
                        </div>
                        <div class="col-sm-12 btn-pad">
                            <a class="btn btn-info-gradiant btn-md btn-arrow" data-animation="animated fadeInLeft" data-toggle="collapse" href="#"> <span>Ask for Question <i class="ti-arrow-right"></i></span> </a> <a class="btn text-white" data-animation="animated fadeInRight" data-toggle="modal" data-target="#video" href="#"><i class="icon-Play-Music text-white vm m-r-10"></i> Watch Video</a> </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- Second Slide -->
                <div class="carousel-item">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider2/slide2.jpg" alt="Building Magical Apps" class="slide-image" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text">
                        <div class="col-md-9 col-lg-6">
                            <h2 data-animation="animated flipInX" class="title text-white">Machinery and Hardware Company, which makes Quality Products</h2> </div>
                        <div class="col-md-8 col-lg-5">
                            <p data-animation="animated lightSpeedIn" class="m-t-40 m-b-40 hidden-sm-down">We offer services in various jonar of Accounting. Let us take your all worry regarding your daily accounting and take rest.</p>
                        </div>
                        <div class="col-sm-12 btn-pad">
                            <a class="btn btn-info-gradiant btn-md btn-arrow" data-animation="animated fadeInUp" data-toggle="collapse" href="#"> <span>Ask for Question <i class="ti-arrow-right"></i></span> </a>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- Third Slide -->
                <div class="carousel-item">
                    <!-- Slide Background -->
                    <video width="100%" autoplay loop>
                        <source src="../assets/videos/welding.mp4" type="video/mp4"> Your browser does not support HTML5 video. </video>
                    <div class="bs-slider-overlay"></div>
                    <!-- Slide Text Layer -->
                    <div class="slide-text">
                        <div class="col-md-9 col-lg-6">
                            <h2 data-animation="animated flipInX" class="title text-white">Machinery and Hardware Company.</h2> </div>
                        <div class="col-md-8 col-lg-5">
                            <p data-animation="animated lightSpeedIn" class="m-t-40 m-b-40 hidden-sm-down">We offer services in various jonar of Accounting. Let us take your all worry regarding your daily accounting and take rest.</p>
                        </div>
                        <div class="col-sm-12 btn-pad">
                            <a class="btn btn-info-gradiant btn-md btn-arrow" data-animation="animated fadeInUp" data-toggle="collapse" href="#"> <span>Ask for Question <i class="ti-arrow-right"></i></span> </a>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- End of Wrapper For Slides -->
                <!-- Slider Control -->
                <div class="slider-control">
                    <!-- Left Control -->
                    <a class="left carousel-control-prev text-white font-14" href="#slider2" role="button" data-slide="prev"> <span class="ti-arrow-left" aria-hidden="true"></span> <b class="sr-only font-normal">Previous</b> </a>
                    <!-- Right Control -->
                    <a class="right carousel-control-next text-white font-14" href="#slider2" role="button" data-slide="next"> <span class="ti-arrow-right" aria-hidden="true"></span> <b class="sr-only font-normal">Next</b> </a>
                </div>
                <!-- End of Slider Control -->
            </div>
        </div>
        <!-- End Slider -->
        <!-- Slider Modal -->
        <div class="modal bd-example-modal-lg fade" id="video" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTitle">Your Video Title Here</h5>
                        <button type="button" class="close font-20" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true" class="ti-close"></span> </button>
                    </div>
                    <div class="modal-body">
                        <video width="100%" controls>
                            <source src="../assets/videos/welding.mp4" type="video/mp4"> Your browser does not support HTML5 video. </video>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Slider Modal -->
    </section>
</div>`
});
