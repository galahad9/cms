var name = 'Slider #7';
var image = 'icons/slider.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light">
    <section id="slider-sec" class="slider7">
        <div id="slider7" class="carousel bs-slider slide  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="7000">
            <ol class="carousel-indicators">
                <li data-target="#slider7" data-slide-to="0" class="active"></li>
                <li data-target="#slider7" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider7/slide1.jpg" alt="We are Digital Agency" class="slide-image" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text slide_style_left">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-10 col-md-8 col-lg-5" data-animation="animated slideInDown">
                                    <div class="bg-info-gradiant text-center slide-content po-relative">
                                        <label class="label text-primary font-14 m-0 label-rounded bg-white">Healthcare</label>
                                        <h2 data-animation="animated flipInX" class="title m-b-0 text-white">Get Well Soon</h2>
                                        <p data-animation="animated fadeInLeft" class="m-t-30 text-white op-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris hendrerit fringilla ligula, nec congue leo pharetra in.</p> <a href="#" class="bg-white text-left text-info"><i class="icon icon-Right"></i></a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- Second Slide -->
                <div class="carousel-item">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider7/slide2.jpg" alt="Building Magical Apps" class="slide-image" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-10 col-lg-5">
                                    <div class="bg-info-gradiant text-center slide-content" data-animation="animated slideInDown">
                                        <label class="label text-primary font-14 m-0 label-rounded bg-white">General</label>
                                        <h2 data-animation="animated flipInY" class="title m-b-0 text-white">Feel Like Home</h2>
                                        <p data-animation="animated fadeInRight" class="m-t-30 text-white op-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris hendrerit fringilla ligula, nec congue leo pharetra in.</p> <a href="#" class="bg-white text-left text-info"><i class="icon icon-Right"></i></a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- End of Wrapper For Slides -->
                <!-- Slider Control -->
                <div class="slider-control hide">
                    <!-- Left Control -->
                    <a class="left carousel-control-prev text-danger font-14" href="#slider7" role="button" data-slide="prev"> <span class="bg-white text-center ti-arrow-left" aria-hidden="true"></span> <b class="sr-only font-normal">Previous</b> </a>
                    <!-- Right Control -->
                    <a class="right carousel-control-next text-danger font-14" href="#slider7" role="button" data-slide="next"> <span class="bg-white text-center ti-arrow-right" aria-hidden="true"></span> <b class="sr-only font-normal">Next</b> </a>
                </div>
                <!-- End of Slider Control -->
            </div>
        </div>
        <!-- End Slider -->
    </section>
</div>`
});
