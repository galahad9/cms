var name = 'Slider #4';
var image = 'icons/slider.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light">
    <section id="slider-sec" class="slider4">
        <div id="slider4" class="carousel bs-slider slide  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="7000">
            <ol class="carousel-indicators">
                <li data-target="#slider4" data-slide-to="0" class="active"></li>
                <li data-target="#slider4" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider4/slide1.jpg" alt="We are Digital Agency" class="slide-image" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text slide_style_left">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 col-lg-6">
                                    <label class="font-20" data-animation="animated fadeInDown">Best Accounting Services</label>
                                    <h2 data-animation="animated fadeInLeft" class="title font-bold m-0 p-b-40">Accounting Made Easier, Faster & Managable</h2>
                                    <ul class="p-0 m-b-40 hidden-sm-down" data-animation="animated fadeInUp">
                                        <li><i class="sl-icon-check text-primary m-r-15"></i> Powerful and Faster Results for your site</li>
                                        <li><i class="sl-icon-check text-primary m-r-15"></i> Make your site in no-time with your Bootstrap WrapKit</li>
                                        <li><i class="sl-icon-check text-primary m-r-15"></i> Tons of Features and Elements available here</li>
                                    </ul>
                                    <a class="btn btn-info-gradiant btn-md btn-arrow" data-animation="animated fadeInLeft" data-toggle="collapse" href="#"> <span>Check Our Services <i class="ti-arrow-right"></i></span> </a>
                                    <a class="btn btn-secondary btn-md btn-arrow m-l-20" data-animation="animated fadeInRight" data-toggle="collapse" href="#"> <span>Ask for Quatation <i class="ti-arrow-right"></i></span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- Second Slide -->
                <div class="carousel-item">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider4/slide2.jpg" alt="Building Magical Apps" class="slide-image" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 col-lg-6">
                                    <label class="font-20" data-animation="animated fadeInDown">Best Accounting Services</label>
                                    <h2 data-animation="animated flipInX" class="title font-bold m-0 p-b-30">Get Free from Accounting related Headaches</h2>
                                    <p data-animation="animated fadeInLeft" class="m-b-40 hidden-sm-down">We offer services in various jonar of Accounting.Let us take your all worry regarding your daily accounting and take rest.</p>
                                    <a class="btn btn-info-gradiant btn-md btn-arrow" data-animation="animated fadeInUp" data-toggle="collapse" href="#"> <span>Check Our Services <i class="ti-arrow-right"></i></span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- End of Wrapper For Slides -->
                <!-- Slider Control -->
                <div class="slider-control">
                    <!-- Left Control -->
                    <a class="left carousel-control-prev text-white font-14" href="#slider4" role="button" data-slide="prev"> <span class="ti-arrow-left" aria-hidden="true"></span> <b class="sr-only font-normal">Previous</b> </a>
                    <!-- Right Control -->
                    <a class="right carousel-control-next text-white font-14" href="#slider4" role="button" data-slide="next"> <span class="ti-arrow-right" aria-hidden="true"></span> <b class="sr-only font-normal">Next</b> </a>
                </div>
                <!-- End of Slider Control -->
            </div>
        </div>
        <!-- End Slider -->
    </section>
</div>`
});
