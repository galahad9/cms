var name = 'Slider #10';
var image = 'icons/slider.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light">
    <section id="slider-sec" class="slider10">
        <div id="slider10" class="carousel bs-slider slide  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="7000">
            <ol class="carousel-indicators">
                <li data-target="#slider7" data-slide-to="0" class="active"></li>
                <li data-target="#slider7" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider10/slide2.jpg" alt="We are Digital Agency" class="slide-image" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text slide_style_left">
                        <div class="container" data-animation="animated fadeInLeft">
                            <div class="bg-white slide-content po-relative">
                                <label class="label text-white bg-purple">ON RENT</label>
                                <h3 data-animation="animated flipInX" class="m-b-10 m-t-0"><a href="#" class="link">Magical Apartment</a></h3>
                                <h6>36, Kingstone Cornern, Melbourne, Australia - 3890</h6>
                                <p class="m-t-20">Lorem ipsum dolor sit amet, consectet cing elit, sed diam nonummy.</p>
                                <h2 class="text-info m-b-0">$685,000 <span class="tag bg-danger">Hot Offer</span></h2> </div>
                            <div class="bg-light slide-content specifiaction-box">
                                <div class="row">
                                    <div class="col-4">
                                        <h6 class="specifiaction text-muted">Bedrooms</h6>
                                        <h5 class="m-b-0 font-medium">4</h5> </div>
                                    <div class="col-4">
                                        <h6 class="specifiaction text-muted">Bathrooms</h6>
                                        <h5 class="m-b-0 font-medium">2</h5> </div>
                                    <div class="col-4">
                                        <h6 class="specifiaction text-muted">Garages</h6>
                                        <h5 class="m-b-0 font-medium">1</h5> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- Second Slide -->
                <div class="carousel-item">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider10/slide2.jpg" alt="Building Magical Apps" class="slide-image" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text">
                        <div class="container" data-animation="animated fadeInUp">
                            <div class="bg-white slide-content po-relative">
                                <label class="label text-white bg-purple">ON RENT</label>
                                <h3 data-animation="animated flipInX" class="m-b-10 m-t-0"><a href="#" class="link">Magical Apartment</a></h3>
                                <h6>36, Kingstone Cornern, Melbourne, Australia - 3890</h6>
                                <p data-animation="animated fadeInLeft" class="m-t-20">Lorem ipsum dolor sit amet, consectet cing elit, sed diam nonummy.</p>
                                <h2 class="text-info m-b-0">$685,000 <span class="tag bg-danger">Hot Offer</span></h2> </div>
                            <div class="bg-light slide-content specifiaction-box">
                                <div class="row">
                                    <div class="col-4">
                                        <h6 class="specifiaction text-muted">Bedrooms</h6>
                                        <h5 class="m-b-0 font-medium">4</h5> </div>
                                    <div class="col-4">
                                        <h6 class="specifiaction text-muted">Bathrooms</h6>
                                        <h5 class="m-b-0 font-medium">2</h5> </div>
                                    <div class="col-4">
                                        <h6 class="specifiaction text-muted">Garages</h6>
                                        <h5 class="m-b-0 font-medium">1</h5> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- End of Wrapper For Slides -->
                <!-- Slider Control -->
                <div class="slider-control hide">
                    <!-- Left Control -->
                    <a class="left carousel-control-prev text-danger font-14" href="#slider7" role="button" data-slide="prev"> <span class="bg-white text-center ti-arrow-left" aria-hidden="true"></span> <b class="sr-only font-normal">Previous</b> </a>
                    <!-- Right Control -->
                    <a class="right carousel-control-next text-danger font-14" href="#slider7" role="button" data-slide="next"> <span class="bg-white text-center ti-arrow-right" aria-hidden="true"></span> <b class="sr-only font-normal">Next</b> </a>
                </div>
                <!-- End of Slider Control -->
            </div>
        </div>
        <!-- End Slider -->
    </section>
</div>`
});
