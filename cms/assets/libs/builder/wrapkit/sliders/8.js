var name = 'Slider #8';
var image = 'icons/slider.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light">
    <section id="slider-sec" class="slider8">
        <div id="slider8" class="carousel bs-slider slide control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="7000">
            <ol class="carousel-indicators">
                <li data-target="#slider8" data-slide-to="0" class="active"></li>
                <li data-target="#slider8" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider8/slide1.jpg" alt="We are Digital Agency" class="slide-image" data-animation="animated fadeIn" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text slide_style_left slide1">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-10 col-md-8 col-lg-4 ml-auto" data-animation="animated slideInDown">
                                    <div class="bg-white slide-content po-relative">
                                        <div>
                                            <label class="label label-inverse font-12 text-uppercase font-bold m-0" data-animation="animated fadeInLeft">Featured</label> <b data-animation="animated fadeInRight" class="text-uppercase font-12 font-bold pull-right">193 Days Left</b> </div>
                                        <h2 data-animation="animated flipInX" class="title m-t-20 m-b-0 text-inverse text-uppercase font-bold">GIVE WATER TO THOSE CHILDS WHO REALLY NEEDS IT</h2>
                                        <p data-animation="animated flipInX" class="m-t-30 d-none d-md-block">Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                                        <div class="row m-t-30">
                                            <div class="col-6 b-t b-r p-t-20" data-animation="animated fadeInLeft">
                                                <p class="text-uppercase font-12 m-0">Raised</p>
                                                <h3 class="text-success m-0">$65,360</h3> </div>
                                            <div class="col-6 col-sm-6 b-t p-t-20 p-t-20 p-l-20" data-animation="animated fadeInRight">
                                                <p class="text-uppercase font-12 m-0">Goal</p>
                                                <h3 class="text-inverse m-0">$124,500</h3> </div>
                                        </div>
                                        <div class="text-center">
                                            <a class="btn btn-success-gradiant btn-md btn-arrow m-t-30" data-animation="animated fadeInLeft" data-toggle="collapse" href="#"> <span class="text-uppercase font-14 font-bold">Donate Now <i class="ti-arrow-right"></i></span> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- Second Slide -->
                <div class="carousel-item">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider8/slide2.jpg" alt="Building Magical Apps" class="slide-image" data-animation="animated fadeIn" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-10 col-lg-5">
                                    <h2 data-animation="animated fadeInLeft" class="title m-0 text-white text-uppercase">BRING SMILE ON THOSE CHILDS WHO ARE IN NEEDS OF THAT</h2>
                                    <a class="btn btn-success-gradiant btn-md btn-arrow m-t-30" data-animation="animated fadeInUp" data-toggle="collapse" href="#"> <span class="text-uppercase font-14 font-bold">Donate Now <i class="ti-arrow-right"></i></span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- End of Wrapper For Slides -->
                <!-- Slider Control -->
                <div class="slider-control hide">
                    <!-- Left Control -->
                    <a class="left carousel-control-prev text-danger font-14" href="#slider8" role="button" data-slide="prev"> <span class="bg-white text-center ti-arrow-left" aria-hidden="true"></span> <b class="sr-only font-normal">Previous</b> </a>
                    <!-- Right Control -->
                    <a class="right carousel-control-next text-danger font-14" href="#slider8" role="button" data-slide="next"> <span class="bg-white text-center ti-arrow-right" aria-hidden="true"></span> <b class="sr-only font-normal">Next</b> </a>
                </div>
                <!-- End of Slider Control -->
            </div>
        </div>
        <!-- End Slider -->
    </section>
</div>`
});
