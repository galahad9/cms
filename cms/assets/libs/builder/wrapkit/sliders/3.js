var name = 'Slider #3';
var image = 'icons/slider.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light">
    <section id="slider-sec" class="slider3">
        <div id="slider3" class="carousel bs-slider control-round indicators-line" data-ride="carousel" data-pause="always" data-interval="false">
            <ol class="carousel-indicators hide">
                <li data-target="#slider3" data-slide-to="0" class="active"></li>
                <li data-target="#slider3" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider3/slide1.jpg" alt="We are Digital Agency" class="slide-image" data-animation="animated fadeIn" />
                    <div class="bs-slider-overlay"></div>
                    <!-- Slide Text Layer -->
                    <div class="slide-text slide_style_left">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-lg-6 align-self-center">
                                    <h2 data-animation="animated zoomInRight" class="title text-white">Meet WrapKit, Your One Stop Solution for all Front-end & Backend Needs.</h2>
                                    <p data-animation="animated fadeInLeft" class="m-t-40 m-b-40 hidden-sm-down">Wrapkit will help you to build your Website and Backend Design in record time with fun to work on. Try yourself!</p>
                                    <a class="btn btn-info-gradiant btn-md btn-arrow" data-animation="animated fadeInLeft" data-toggle="collapse" href="#"> <span>Get Register Now! <i class="ti-arrow-right"></i></span> </a>
                                </div>
                                <div class="col-lg-5 ml-auto hidden-md-down align-self-center"> <img src="/cms/assets/wrapkit/images/sliders/slider3/video-img.jpg" class="video-img img-responsive" alt="Video" data-animation="animated fadeInRight" />
                                    <div class="embed-responsive hide embed-responsive-16by9" data-animation="animated fadeIn">
                                        <video class="video" controls>
                                            <source src="../assets/videos/running.mp4" type="video/mp4"> Your browser does not support HTML5 video. </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- Second Slide -->
                <div class="carousel-item">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider3/slide2.jpg" alt="Building Magical Apps" class="slide-image" data-animation="animated fadeIn" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text">
                        <div class="col-md-10 col-lg-7">
                            <h2 data-animation="animated flipInX" class="title m-b-40 text-white">Meet WrapKit, Your One Stop Solution for all Front-end & Backend Needs.</h2>
                            <a class="btn bg-danger-gradiant btn-md btn-arrow" data-animation="animated fadeInUp" data-toggle="collapse" href="#"> <span>Get Register Now! <i class="ti-arrow-right"></i></span> </a>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- End of Wrapper For Slides -->
                <!-- Slider Control -->
                <div class="slider-control">
                    <!-- Left Control -->
                    <a class="left carousel-control-prev text-white font-14" href="#slider3" role="button" data-slide="prev"> <span class="ti-arrow-left" aria-hidden="true"></span> <b class="sr-only font-normal">Previous</b> </a>
                    <!-- Right Control -->
                    <a class="right carousel-control-next text-white font-14" href="#slider3" role="button" data-slide="next"> <span class="ti-arrow-right" aria-hidden="true"></span> <b class="sr-only font-normal">Next</b> </a>
                </div>
                <!-- End of Slider Control -->
            </div>
        </div>
        <!-- End Slider -->
    </section>
</div>`
});
