var name = 'Slider #1';
var image = 'icons/slider.svg';


Vvveb.Components.extend("_base", component, {
    name: name,
    image: image,
    html: `<div class="bg-light">
    <section id="slider-sec" class="slider1">
        <div id="slider1" class="carousel bs-slider slide  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="7000">
            <ol class="carousel-indicators">
                <li data-target="#slider1" data-slide-to="0" class="active"></li>
                <li data-target="#slider1" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider1/slide1.jpg" alt="We are Digital Agency" class="slide-image" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text slide_style_left">
                        <h2 data-animation="animated zoomInRight" class="bg-success-gradiant title">We are Digital Agency</h2>
                        <p data-animation="animated fadeInLeft">
                            <a class="bg-white btn btn-md btn-arrow" data-toggle="collapse" href="#"> <span>We offer Design & Development <i class="ti-arrow-right"></i></span> </a>
                        </p>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- Second Slide -->
                <div class="carousel-item">
                    <!-- Slide Background --><img src="/cms/assets/wrapkit/images/sliders/slider1/slide2.jpg" alt="Building Magical Apps" class="slide-image" />
                    <!-- Slide Text Layer -->
                    <div class="slide-text slide_style_center">
                        <h2 data-animation="animated flipInX" class="bg-success-gradiant title">Building Magical Apps</h2>
                        <p data-animation="animated lightSpeedIn">
                            <a class="bg-white btn btn-md btn-arrow" data-toggle="collapse" href="#"> <span>Raw Application Development <i class="ti-arrow-right"></i></span> </a>
                        </p>
                    </div>
                </div>
                <!-- End of Slide -->
                <!-- End of Wrapper For Slides -->
            </div>
        </div>
        <!-- End Slider -->
    </section>
</div>`
});
