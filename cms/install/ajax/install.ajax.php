<?php

require(__DIR__ . '/../installFirstment.inc.php');

$main_dir = str_replace(['/cms/install/pages', '/cms/install/ajax'], '', __DIR__);

ob_start();
try {

	// Truncate CMS database
	// $tables = DB::CMS()->query("SELECT * FROM sqlite_master WHERE type='table' AND name != 'sqlite_sequence'")->fetch();
	// foreach ($tables as $key => $table) {
	// 	if($table['name'] == 'settings'){
	// 		DB::CMS()->query("UPDATE `".$table['name']."` SET `value`=''")->execute();
	// 	} else {
	// 		// DB::CMS()->query("DROP TABLE `".$table['name']."`")->execute();
	// 		// DB::CMS()->query($table['sql'])->execute();
	// 	}
	// }

	// Set CMS settings
	DB::CMS()->query("UPDATE settings SET `value`=:name WHERE `name`='Name'")->bind(':name', $_POST['cmsname'])->execute();

	file_put_contents(ROOT_PATH.'/.domain', DOMAIN);

	// User Creation CMS
	$emailaddress = $_POST['email'];
	$password = sha1($_POST['password'] . ':SupRrCM5:' . strtolower($emailaddress));
	
	$user = DB::CMS()->query("INSERT INTO users (emailaddress, firstname, lastname, role, language, password, active, admin, created) VALUES (:emailaddress, :firstname, :lastname, :role, :language, :password, :active, :admin, :created)", array(
	    ":emailaddress" => $emailaddress,
	    ":firstname" => $_POST['fname'],
	    ":lastname" => $_POST['lname'],
	    ":role" => 'Admin',
	    ":language" => 'nl',
	    ":password" => $password,
	    ":active" => 1,
	    ":admin" => 1,
	    ":created" => time()
	))->execute();

	// Truncate SITE database
	// $tables = DB::SITE()->query("SELECT name FROM sqlite_master WHERE type='table' AND name != 'sqlite_sequence'")->fetch();
	// foreach ($tables as $key => $table) {
	// 	if($table['name'] == 'settings'){
	// 		DB::SITE()->query("UPDATE `".$table['name']."` SET `value`=''")->execute();
	// 	} else {
	// 		// DB::SITE()->query("DROP TABLE `".$table['name']."`")->execute();
	// 		// DB::SITE()->query($table['sql'])->execute();
	// 	}
	// }

	// Set Site settings
	DB::SITE()->query("UPDATE settings SET `value`=:name WHERE `name`='Website Name'")->bind(':name', $_POST['website_name'])->execute();
	DB::SITE()->query("UPDATE settings SET `value`=:name WHERE `name`='Website Url'")->bind(':name', $_POST['website_url'])->execute();
	DB::SITE()->query("UPDATE settings SET `value`=:name WHERE `name`='Online'")->bind(':name', intval($_POST['online']))->execute();
} catch(Exception $e){
	echo $e->getMessage();
}

$db_output = ob_get_contents();
ob_end_clean();

$composer_dir = $main_dir;
putenv("COMPOSER_HOME=$main_dir");
$command = "cd $main_dir && ".PHP." composer.phar install --no-interaction --ignore-platform-reqs 2>&1";
ob_start();
system($command);
$composer = ob_get_clean();


$status = 0;
if(strlen($db_output) < 5){
	$status = 1;
}
echo json_encode([
	'status' => $status,
	'composer' => $composer,
	'database' => $db_output
]);