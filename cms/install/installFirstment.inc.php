<?php
define('NO_AUTH_REQUIRED', 1);
define('IS_INSTALL', 1);

set_time_limit(-1);

if(file_exists(__DIR__.'/../../vendor/autoload.php')){
	header("Location: /cms/");
	exit;
}


if(! file_exists(__DIR__.'/../../database/cms.sqlite3')){
	copy(__DIR__.'/../../update/install/database/cms.sqlite3', __DIR__.'/../../database/cms.sqlite3');
	copy(__DIR__.'/../../update/install/database/site.sqlite3', __DIR__.'/../../database/site.sqlite3');
	copy(__DIR__.'/../../update/install/database/core.sqlite3', __DIR__.'/../../database/core.sqlite3');
}

require(__DIR__ . '/../cmsFirstment.inc.php');