<?php

$main_dir = str_replace('/cms/install/pages', '', __DIR__);
?>



<div class="row">
	<div class="col-md-12">
		<div class="card">
			<h5 class="card-header">Install CMS</h5>
			<div class="card-body">
				<form id="horizontal-wizard" action="#">
					<h3>CMS Settings</h3>
					<section>
						<h5 class="card-title">CMS Settings</h5>
						<div class="form-group">
							<label for="cmsname">CMS Name*</label>
							<input type="text" class="form-control required" name="cmsname" id="cmsname" value="Zylon">
						</div>
						<p>&nbsp;</p>
						<h5 class="card-title">SITE Settings</h5>
						<div class="form-group">
							<label for="cmsname">Website Name*</label>
							<input type="text" class="form-control required" name="website_name" id="website_name" value="">
						</div>
						<div class="form-group">
							<label for="cmsname">Website URL*</label>
							<input type="text" class="form-control required" name="website_url" id="website_url" value="<?= DOMAIN ?>">
						</div>
						<div class="form-group">
							<label for="cmsname">Online</label>
							<input class="tgl tgl-light tgl-primary" id="cb1" type="checkbox" name="online" value="1" style="display:none">
							<label class="tgl-btn" for="cb1" style="    transform: translate(15px, 15px);"></label>
						</div>
					</section>
					<h3>Account Setup</h3>
					<section>
						<h5 class="card-title">Account Setup</h5>
						<div class="form-group">
							<label for="email">Email address *</label>
							<input type="text" class="form-control required" name="email" id="email" >
						</div>
						<div class="form-group">
							<label for="fname">First name</label>
							<input type="text" class="form-control" name="fname" id="fname">
						</div>
						<div class="form-group">
							<label for="lname">Last name</label>
							<input type="text" class="form-control" name="lname" id="lname">
						</div>
						<div class="form-group">
							<label for="password">Password *</label>
							<input id="password" name="password" type="password" class="form-control required">
						</div>
						<div class="form-group">
							<label for="confirm">Confirm Password *</label>
							<input id="confirm" name="confirm" type="password" class="form-control required">
						</div>
					</section>
					<h3>Install</h3>
					<section>
						<h5 class="card-title">Install Walkthrough</h5>
						
						<dl class="row">
							<dt class="col-sm-3">Step #1 - Folder permissions</dt>
							<dd class="col-sm-9">
								give folders the right permissions:<br>
								<code>0777 - /cms/logs</code><br>
								<code>0777 - /cache</code><br>
							</dd>
						</dl>

						<dl class="row">
							<dt class="col-sm-3">Step #2 - Setup cronjobs</dt>
							<dd class="col-sm-9">
								
								Setup a cronjob for every minute:<br>
								<code>* * * * * <?= str_replace('cms/install/pages', '', __DIR__) ?>/cms/cron.inc.php</code><br>
								
								Setup a update cronjob for every hour:<br>
								<code>0 * * * * cd <?= str_replace('cms/install/pages', '', __DIR__) ?> && cms/libs/update.sh</code>
							</dd>
						</dl>

						<p>Press the button to install the CMS. <strong>Do not refresh the page while installing</strong></p>
					</section>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(function() {
		var form = $("#horizontal-wizard").show();
		form
			.steps({
				headerTag: "h3",
				bodyTag: "section",
				transitionEffect: "slideLeft",
				stepsOrientation: "horizontal",
				onStepChanging: function(event, currentIndex, newIndex) {
					// Allways allow previous action even if the current form is not valid!
					if (currentIndex > newIndex) {
						return true;
					}
					// Needed in some cases if the user went back (clean up)
					if (currentIndex < newIndex) {
						// To remove error styles
						form.find(".body:eq(" + newIndex + ") label.error").remove();
						form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
					}
					form.validate().settings.ignore = ":disabled,:hidden";
					return form.valid();
				},
				onStepChanged: function(event, currentIndex, priorIndex) {
					// Used to skip the "Warning" step if the user is old enough and wants to the previous step.
					if (currentIndex === 2 && priorIndex === 3) {
						form.steps("previous");
					}
				},
				onFinishing: function(event, currentIndex) {
					form.validate().settings.ignore = ":disabled";
					return form.valid();
				},
				onFinished: function(event, currentIndex) {

					$("body").append('<div class="qt-block-ui"></div>');
			        var $block = $(".qt-block-ui");
			        console.log(form);

					$.post('ajax/install.ajax.php', form.serialize(), function(data, textStatus, xhr) {
						$block.remove();
						console.log(data);
						var result = $.parseJSON(data);
						if(result.status == 1){
							swal({
								type: "success",
								title: "Install Complete!",
								showConfirmButton: false,
								onClose: function(){
									window.location.href='/cms/';
								}
							});
						} else {
							console.error(result);
							swal({
								type: "error",
								title: "Install Error! See console for error.",
								showConfirmButton: false
							});
						}
					}).fail(function(xhr, status, error){
						$block.remove();
						console.error(xhr);
						console.error(status);
						console.error(error);
						swal({
							type: "error",
							title: "Install Error! See console for error.",
							showConfirmButton: false
						});
					});
				}
			})
			.validate({
				errorPlacement: function errorPlacement(error, element) {
					element.after(error);
				},
				rules: {
					confirm: {
						equalTo: "#password"
					}
				}
			});
    });

</script>