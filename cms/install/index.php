<?php
require(__DIR__ . '/installFirstment.inc.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?= $cmsSettings['Name'] ?></title>

	<?php
	if(file_exists(CMS_ROOT_PATH.'admin-assets/img/favicon/html.html')){
		include(CMS_ROOT_PATH.'admin-assets/img/favicon/html.html');
	}
	?>
	<!-- ================== GOOGLE FONTS ==================-->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
	<!-- ======================= GLOBAL VENDOR STYLES ========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/vendor/bootstrap.css">
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/metismenu/dist/metisMenu.css">
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/switchery-npm/index.css">
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"></script>
	<!-- ======================= LINE AWESOME ICONS ===========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/icons/line-awesome.min.css">
	<!-- ======================= DRIP ICONS ===================================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/icons/dripicons.min.css">
	<!-- ======================= MATERIAL DESIGN ICONIC FONTS =================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/icons/material-design-iconic-font.min.css">
	<!-- ======================= PAGE VENDOR STYLES ===========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/jvectormap-next/jquery-jvectormap.css">
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
	<!-- ======================= GLOBAL COMMON STYLES ============================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/common/main.bundle.css">
	<!-- ======================= LAYOUT STYLES ===========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/layouts/horizontal/core/main.css">
	<!-- ======================= MENU TYPE ===========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/layouts/horizontal/menu-type/auto-hide.css">
	<!-- ======================= THEME COLOR STYLES ===========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/layouts/horizontal/themes/theme-j.css">
	<!-- ======================= CUSTOM STYLES ===========================-->
	<link rel="stylesheet" href="<?= CMS_PUBLIC_PATH ?>admin-assets/css/custom.css">
	<!-- ======================= JQUERY ===========================-->
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/jquery/dist/jquery.min.js"></script>
	<!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/modernizr/modernizr.custom.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/js-storage/js.storage.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/js-cookie/src/js.cookie.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/pace/pace.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/metismenu/dist/metisMenu.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/switchery-npm/index.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/dropzone/dropzone.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/datatables.net/js/jquery.dataTables.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/tinymce/tinymce.min.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/tinymce/jquery.tinymce.min.js"></script>

	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/jquery-validation/jquery.validate.min.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/jquery-validation/additional-methods.min.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/vendor/jquery-steps/jquery.steps.min.js"></script>

	<!-- ================== GLOBAL APP SCRIPTS ==================-->
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/js/global/app.js"></script>
	<script src="<?= CMS_PUBLIC_PATH ?>admin-assets/js/sienn.js"></script>
	

</head>
<body class="layout-horizontal menu-auto-hide">
	<!-- START APP WRAPPER -->
	<div id="app">
		<!-- START TOP HEADER WRAPPER -->
		<div class="header-wrapper">
			<div class="header-top">
				<!-- START MOBILE MENU TRIGGER -->
				<ul class="mobile-only navbar-nav nav-left">
					<li class="nav-item">
						<a href="javascript:void(0)" data-toggle-state="aside-left-open">
							<i class="icon dripicons-align-left"></i>
						</a>
					</li>
				</ul>
				<!-- END MOBILE MENU TRIGGER -->
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-lg-6">
							<ul class="site-logo">
								<li>
									<!-- START LOGO -->
									<a href="/cms/">
										<div class="logo">
											<img src="/cms/admin-assets/img/<?= $cmsSettings['Image Logo White'] ?>" />
										</div>
									</a>
									<!-- END LOGO -->
								</li>
							</ul>
						</div>
						<div class="col-lg-6">
							<!-- START TOP TOOLBAR WRAPPER -->
						<div class="top-toolbar-wrapper">
							<nav class="top-toolbar navbar flex-nowrap">
								<ul class="navbar-nav nav-right">
									
									<li class="nav-item dropdown dropdown-menu-lg">
										<a class="nav-link nav-pill user-avatar" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
											<img src="http://via.placeholder.com/35x35" class="w-35 rounded-circle" alt="<?= $_SESSION['user']['firstname'] . ' ' . $_SESSION['user']['lastname'] ?>">
										</a>
										<div class="dropdown-menu dropdown-menu-right dropdown-menu-accout">
											<div class="dropdown-header pb-3">
												<div class="media d-user">
													<img class="align-self-center mr-3 w-40 rounded-circle" src="http://via.placeholder.com/35x35" alt="Albert Einstein">
													<div class="media-body">
														<h5 class="mt-0 mb-0"><?= $_SESSION['user']['firstname'] . ' ' . $_SESSION['user']['lastname'] ?></h5>
														<span><?= $_SESSION['user']['emailaddress'] ?></span>
													</div>
												</div>
											</div>
											<a class="dropdown-item" style="color: #839bb3!important;"><i class="icon dripicons-flag" style="color: #839bb3!important;"></i> <?= $trans['Change language'] ?> 
												<?php
													$languages = getCMSLanguages();
													foreach ($languages as $language) {
														echo ' &nbsp;<img src="/cms/translations/flags/' . strtoupper($language) . '.png" onclick="changeLanguage(\'' . $language . '\')" style="cursor: pointer;">';
													}		
												?>
											<a class="dropdown-item" href="index.php?page=user-edit"><i class="icon dripicons-lock"></i> <?= $trans['Change password'] ?></a>
											<a class="dropdown-item" href="logout.php"><i class="icon dripicons-power"></i> <?= $trans['Sign out'] ?></a>
										</div>
									</li>
									
									<li class="nav-item">
										<a href="logout.php" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?= $trans['Sign out'] ?>">
											<i class="icon dripicons-power"></i>
										</a>
									</li>
								</ul>
							</nav>
						</div>
					<!-- END TOP TOOLBAR WRAPPER -->
						</div>
					</div>
				</div>
				<!-- START MOBILE TOOLBAR TRIGGER -->
				<ul class="mobile-only navbar-nav nav-right">
					<li class="nav-item">
						<a href="javascript:void(0)" data-toggle-state="mobile-topbar-toggle">
							<i class="icon dripicons-dots-3 rotate-90"></i>
						</a>
					</li>
				</ul>
				<!-- END MOBILE TOOLBAR TRIGGER -->
			</div>
			<?php 
			if(! IS_INSTALL){ ?>
			<!-- START HEADER BOTTOM -->
			<div class="header-bottom">
				<div class="container">
					<!-- START MAIN MENU -->
					<nav class="main-menu">
						<ul class="nav metismenu">
							<li class="sidebar-header mobile-only"><span><?= $trans['NAVIGATION'] ?></span></li>
							
							<li>
								<a class="has-arrow" href="index.php?page=dashboard" aria-expanded="false"><i class="icon dripicons-meter"></i><span class="hide-menu">Dashboard</span></a>
							</li>
							
							<li>
								<a class="has-arrow" href="index.php?page=menu" aria-expanded="false"><i class="la la-file-text"></i><span class="hide-menu">Content</span></a>
							</li>
							<li>
								<a class="has-arrow " href="index.php?page=modules" aria-expanded="false"><i class="icon dripicons-jewel"></i><span class="hide-menu">Modules</span></a>
								<ul aria-expanded="false" class="collapse">
									<li><a href="index.php?page=modules"><?= $trans['Overview'] ?></a></li>
									<?php
									foreach (getPlugins() as $key => $plugin) {
										echo '<li><a '.((count($plugin->menu)) ? 'class="has-arrow"' : '').' href="index.php?module='.$plugin->url().'">'.$plugin->name().'</a>';
											if($plugin->menu){
												echo '<ul aria-expanded="false" class="collapse">';
												foreach ($plugin->menu as $key => $item) {
													echo '<li><a href="'.$item['url'].'">'.$item['name'].'</a>';
												}
												echo '</ul>';
											}
										echo '</li>';
									}
									?>
								</ul>
							</li>
							
							
							<li>
								<a class="has-arrow " href="#" aria-expanded="false"><i class="icon dripicons-gear"></i><span class="hide-menu"><?= $trans['Settings'] ?></span></a>
								<ul aria-expanded="false" class="collapse">
									<li><a href="index.php?page=settings"><?= $trans['Website'] ?> <?= $trans['Settings'] ?></a></li>
									
									<li><a href="index.php?page=revisions/list"><?= $trans['Revisions'] ?></a></li>
									<li><a href="index.php?page=updates"><?= $trans['Updates'] ?> <?= ((Core::count() > 0) ? '<span class="badge badge-danger">'.Core::count().'</span>' : '') ?></a></li>
								</ul>
							</li>
							<?php
							if($_SESSION['user']['admin']){ ?>
								<li>
									<a class="has-arrow " href="#" aria-expanded="false"><i class="icon dripicons-gear"></i><span class="hide-menu"><?= $trans['Admin'] ?></span></a>
									<ul aria-expanded="false" class="collapse">
										<li><a href="index.php?page=settings&type=cms"><?= $trans['CMS'] ?> <?= $trans['Settings'] ?></a></li>
										<li><a href="index.php?page=users"><?= $trans['CMS'] ?> <?= $trans['Users'] ?></a></li>
										<li><a href="index.php?page=translations"><?= $trans['Translations'] ?></a></li>
										<li><a href="http://sienn.wux.nl/cms/libs/dbadmin.php?view=structure" target="_blank">DB Admin</a></li>
									</ul>
								</li>
							<?php } ?>
							
						</ul>
					</nav>
					<!-- END MAIN MENU -->
				</div>
			</div>
			<!-- END HEADER BOTTOM -->
			<?php } ?>
		</div>

		<?php
		if($_GET['page'] != 'builder'){ ?>
			<!-- END TOP HEADER WRAPPER -->
			<div class="content-wrapper">
				<div class="content container">
				<?php
			}

			echo getAlerts();

					$_GET['page'] = (empty($_GET['page'])) ? 'dashboard' : $_GET['page'];
					
					if(! $_GET['module'] && file_exists(__DIR__.'/pages/' . $_GET['page'] . '.inc.php')){
						include(__DIR__.'/pages/' . $_GET['page'] . '.inc.php');

					} elseif($_GET['module'] && is_dir(PLUGINS_DIR.'/'. $_GET['module'] )){
						include(PLUGINS_DIR.'/'. $_GET['module'].'/cms/pages/'.$_GET['page'].'.inc.php');
					} else {
						header('HTTP/1.1 404 Not Found');
						include(__DIR__.'/pages/404.inc.php');
					}
				?>
			</div>
		</div>
	</div>
	<!-- END CONTENT WRAPPER -->
		
	
	
	<script>
	function changeLanguage(language) {
		$.post('ajax/translations/setLanguage.ajax.php', {language:language}, function (data) {
			location.reload();
		});
		
	}
	</script>

	</body>
	</html>
