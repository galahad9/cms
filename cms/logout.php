<?php
require(__DIR__ . '/cmsFirstment.inc.php');


if (isset($_COOKIE['cms-session'])) {
	$session = json_decode($_COOKIE['cms-session'], true);
	DB::CMS()->query('DELETE FROM users_sessions WHERE user_id=:user_id AND hash=:hash', [':user_id' => $session['id'], ':hash' => $session['hash']])->execute();
	unset($_COOKIE['cms-session']);
	setcookie('cms-session', null, -1);
}


foreach ($_SESSION as $index => $value) {
	unset($_SESSION[$index]);	
	session_destroy();
}


redirect(CMS_PUBLIC_PATH . 'login.php');