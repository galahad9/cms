<?php
define('NO_AUTH_REQUIRED', true);
require(__DIR__ . '/cmsFirstment.inc.php');

//
// Add this line to your crontab file:
//
// * * * * * cd /path/to/project && php jobby.php 1>> /dev/null 2>&1
//

// Creates pid file to check if cronjob is setup properly 
if(! is_dir(ROOT_PATH.'/cms/logs')){
	mkdir(ROOT_PATH.'/cms/logs', 0777);
}
file_put_contents(ROOT_PATH.'/cms/logs/cron.pid', date('d-m-Y H:i:s'));

// Init Core
$core = new Core;
$settings = $core->setting('SITE');
$cmsSettings = $core->setting('CMS');
$version = $core->git()->branche('current');

// Init cronjobs
$jobby = new \Jobby\Jobby();



$jobby->add('CallToZylon', [
    'closure' => function() use ($version, $settings, $cmsSettings) {

        $client = new GuzzleHttp\Client(['base_uri' => 'http://admin.zyloncms.nl/rest/api/']);
        $response = $client->request('POST', 'website', [
            'form_params' => [
                'name' => $settings['Website Name'],
                'domain' => $settings['Website Url'],
                'version' => $version,
                'online' => (($settings['Online'] == "true") ? 1 : 0),
                'secret' => md5($settings['Website Url']),
                'key' => sha1($settings['Website Url']),
                'settings' => $settings,
                'cmsSettings' => $cmsSettings
            ]
        ]);

        echo date('d-m-Y H:i:s')." \t CallToZylon:\t ".$response->getBody()->read(1024)."\n";

        return true;
    },
    'schedule' => '0 * * * *', // every hour
    // 'schedule' => function(){ return true; },
    'output' => 'logs/command.log',
    'enabled' => true,
    'debug' => DEBUG
]);

$jobby->run();
