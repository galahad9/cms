<?php
header("Content-Type: text/css", true);
header("X-Content-Type-Options: nosniff");
// die('ss');
$cacheSeconds = 60*60*24*30;
session_cache_limiter('');
header('Pragma: public');
header('Cache-Control: max-age='.$cacheSeconds.', public');
header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + ($cacheSeconds)));

define('IS_CSS', 1);
define('IS_ASSET', 1);

require('firstment.inc.php');


$cssFileUrl = '/'.trim(__DIR__,'/').'/cms/'. $_GET['file'];
// $cssFileUrl = $_GET['file'];
// dd($cssFileUrl);
if(file_exists($cssFileUrl)){
	include($cssFileUrl);	
} else {
	header("HTTP/1.1 404 Not Found");
}



// echo $cssFile;
// use MatthiasMullie\Minify;
