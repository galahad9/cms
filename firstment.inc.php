<?php
error_reporting(E_ALL ^ (E_NOTICE | E_DEPRECATED));
date_default_timezone_set('Europe/Amsterdam');

session_start();

define('DEBUG', true);

$databaseConfig = array();
$databaseConfig['cms']['name'] = 'cms';
$databaseConfig['site']['name'] = 'site';

if(
	! defined('IS_INSTALL') && 
	! defined('IS_CSS') && 
	! file_exists(__DIR__.'/vendor/autoload.php')
){
	header("Location: /cms/install/");
	exit;
}

@ include_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/databaseConnection.inc.php');
require_once(__DIR__ . '/defines.inc.php');
require_once(__DIR__ . '/functions.inc.php');


$settings = loadSettings('SITE');
$cmsSettings = loadSettings('CMS');
$builderSettings = [
	'public_url' => CMS_PUBLIC_PATH.'index.php?page=builder',
	'assets_base' => CMS_PUBLIC_PATH.'assets/',
	'snippet_file' => CMS_PUBLIC_PATH.'ajax/builder/getSnippets.ajax.php',
	'icon_select' => CMS_PUBLIC_PATH.'assets/builder/iconselect/selecticon-dark.html',
	'image_ajax' => CMS_PUBLIC_PATH.'ajax/builder/setImage.ajax.php',
	'image_ajax_base64' => CMS_PUBLIC_PATH.'ajax/builder/setBase64Image.ajax.php',
	'imagemodule_ajax' => CMS_PUBLIC_PATH.'ajax/builder/setModuleImage.ajax.php',
	'images_folder' => trim(__DIR__,'/').'/website/uploads/'
];

if(! IS_INSTALL && ! IS_ASSET ){
	loadPlugins();

	if(plugin('posttype')){
		$posttype = plugin('posttype');
		$posttype->register(); 
	}
}

?>