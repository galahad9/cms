Zylon
====

A [Sophisticated, lightweight & simple CMS][zylon-com].

Zylon is a tool for Content Management, which strives to be as simple and
straightforward as possible.

It is quick to set up, easy to configure, uses elegant templates, and above
all, it's a joy to use!


Installation
------------

### Using Plesk
- 1. Setup domain in Plesk
- 2. Add Git repository through Git Plesk. Use Remote Git hosting and paste the https url into the input. 
- 3. To auto-update go to "Repository Settings" and get the "Webhook URL". Paste this url in Stash Hooks[stash-hook]
- 4. Go to the website and follow the installer.
- 5. Done!

**NOTE:** Enjoy!

[zylon-com]: https://www.zyloncms.com
[stash-hook]: http://stash.wux.nl/projects/SIENN/repos/cms/settings/hooks

